#!/bin/bash -x
# THIS SCRIPT MUST BE EXECUTED IN hdd/
# USAGE : sudo ./cut.sh /dev/sdx (with x the device of the sd card)
echo $1
rm img.dmg
dd if=virt_hdd.dmg | pv | dd of=img.dmg bs=512 count=10000
sudo dd if=img.dmg | pv | sudo dd of=$1
sudo umount $1
sudo eject $1
