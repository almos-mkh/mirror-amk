///////////////////////////////////////////////////////////////////////////////
// File   :  idbg.c
// Date   :  May 2018
// Author :  Alain Greiner <alain.greiner@lip6.fr>
///////////////////////////////////////////////////////////////////////////////
// This single thread interactive debuger can be used to access and display
// various kernel structures 
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <stdio.h>
#include <almosmkh.h>

///////////
void main( void )
{
    unsigned long long  cycle;
    unsigned int        cxy;
    unsigned int        lid;

    get_cycle( &cycle );
    get_core( &cxy , &lid );

    printf( "\n[IDBG] starts on core[%x,%d] / cycle %d\n",
    cxy , lid , (unsigned int)cycle ); 

    idbg();

} // end main()

