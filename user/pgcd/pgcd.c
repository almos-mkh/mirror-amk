///////////////////////////////////////////////////////////////////////////////
// File   :  pgcd.c
// Date   :  November 2017
// Author :  Alain Greiner <alain.greiner@lip6.fr>
///////////////////////////////////////////////////////////////////////////////
// This single thread interactive application computes the PGCD.
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>
#include <almosmkh.h>

/////////////////
void main( void )
{
    int                opx;
    int                opy;
    unsigned long long cycle;
    unsigned int       cxy;
    unsigned int       lid;

    get_cycle( &cycle );
    get_core( &cxy , &lid );

    printf( "\n\n[PGCD] starts on core[%x,%d] / cycle %d\n",
    cxy , lid , (unsigned int)cycle ); 

    while (1) 
    {
        printf("\n*******************\n");
        printf("operand X = ");
        opx = get_uint32();
        printf("\n");
        printf("operand Y = ");
        opy = get_uint32();
        printf("\n");

        if( (opx == 0) || (opy == 0) ) 
        {
           printf("operands must be positive and larger than 0 => exit\n");
           exit( 0 );
        } 
        else 
        {
            while (opx != opy) 
            {
                if(opx > opy)   opx = opx - opy;
                else            opy = opy - opx;
            }
            printf("pgcd      = %d", opx);
        }
    }
} // end pgcd

