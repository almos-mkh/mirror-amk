# Parameters definition for the ALMOS-MKH Makefile

ARCH      = /users/alain/soc/tsar-trunk-svn-2013/platforms/tsar_generic_iob
X_SIZE    = 1
Y_SIZE    = 2
NB_PROCS  = 1
NB_TTYS   = 3
IOC_TYPE  = IOC_BDV
TXT_TYPE  = TXT_TTY
FBF_TYPE  = FBF_SCL
SYS_CLK   = 50000
