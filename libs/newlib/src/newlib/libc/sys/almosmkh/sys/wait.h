#ifndef _SYS_WAIT_
#define _SYS_WAIT_

#include <almos-mkh/wait.h>

typedef int pid_t;

/***************************************************************************************** 
 * This blocking function returns only when one child process of the calling process
 * changes state (from RUNNING to STOPPED / EXITED / KILLED). It returns the terminating
 * child process PID, and set in the <status> buffer the new child process state.
 ***************************************************************************************** 
 * @ status    : [out] terminating child process state.
 * @ returns terminating child process pid.
 ****************************************************************************************/
pid_t wait( int * status );

#endif

