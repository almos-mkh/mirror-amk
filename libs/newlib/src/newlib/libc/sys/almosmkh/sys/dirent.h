#ifndef _SYS_DIRENT_H

//#include <almos-mkh/dirent.h>


// TODO, wait struct dirent POSIX compliante

#define DIRENT_NAME_MAX_LENGTH  56
#define DIRENT_MAX_NUMBER       63


struct dirent
{
    unsigned int   d_ino;                                /*! inode identifier              */
    unsigned int   type;                                /*! inode type                    */
    char           d_name[DIRENT_NAME_MAX_LENGTH];        /*! directory entry name         */
};
typedef struct user_directory
{
    struct dirent   entry[DIRENT_MAX_NUMBER];
    unsigned int    current;
}
DIR;

// FIN TODO

DIR * opendir( const char * pathname );
struct dirent * readdir( DIR * dirp );
int closedir( DIR * dirp );
/*En plus int getcwd( char       * buf,
            unsigned int nbytes );
int rmdir( char * pathname ); */



#endif
