#ifndef _SYS_MMAN_H_
#define _SYS_MMAN_H_
#include <almos-mkh/mman.h>


void * mmap( void         * addr,
             unsigned int   length,
             int            prot,
             int            flags,
             int            fd,
             unsigned int   offset );

int munmap( void         * addr,
            unsigned int   size );

#endif
