#ifndef _SYSCALLS_ALMOSMKH_H_
#define _SYSCALLS_ALMOSMKH_H_
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/times.h>



void _exit(int status);
int _close(int fildes);
//extern char **environ;
int _execve(const char *path, char *const argv[], char *const envp[]);
pid_t _fork(void);
int _fstat(int fildes, struct stat *buf); // Manquant
pid_t _getpid(void);
int _gettimeofday(struct timeval *ptimeval, void *ptimezone);
int _isatty(int fildes);
int _kill(pid_t pid, int sig);
int _link(const char *path1, const char *path2); // Manquant
off_t _lseek(int fildes, off_t offset, int whence);
int _open(const char *path, int oflag, ...); // ... ?
int pipe(int fildes[2]);
ssize_t _read(int fildes, void *buf, size_t nbyte);
int _stat(const char * restrict path, struct stat * restrict buf); // vfs_stat a écrire
clock_t times(struct tms *buffer); // Manquant
int _unlink(const char *path); // vfs_unlink a écrire
pid_t _wait(int *stat_loc);
ssize_t _write(int fildes, const void *buf, size_t nbyte);


#endif  // _SYSCALLS_ALMOSMKH_H_
