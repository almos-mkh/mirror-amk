#include <almos-mkh.h>

//////////////////////////////////
void * malloc( unsigned int size )
{
    // get cluster identifier
    unsigned int cxy;
    unsigned int lid;
    get_core( &cxy , &lid );

    return remote_malloc( size, cxy );
} 


///////////////////////////////////
void * calloc ( unsigned int count,
                unsigned int size )
{
    // get calling core cluster identifier
    unsigned int cxy;
    unsigned int lid;
    get_core( &cxy , &lid );

    return remote_calloc( count , size , cxy );
}


///////////////////////////////////
void * realloc ( void        * ptr,
                 unsigned int  size )
{
    // get calling core cluster identifier
    unsigned int cxy;
    unsigned int lid;
    get_core( &cxy , &lid );

    return remote_realloc( ptr , size , cxy );
}



///////////////////////
void free( void * ptr )
{
    // get calling core cluster identifier
    unsigned int cxy;
    unsigned int lid;
    get_core( &cxy , &lid );

    remote_free( ptr , cxy );
}

// Local Variables:
// tab-width: 4
// c-basic-offset: 4
// c-file-offsets:((innamespace . 0)(inline-open . 0))
// indent-tabs-mode: nil
// End:
// vim: filetype=c:expandtab:shiftwidth=4:tabstop=4:softtabstop=4

//TODO pourquoi je fournis les version réentrante là ?
void *
_malloc_r (struct _reent *r, size_t sz)
{
  return malloc (sz);
}
void *
_calloc_r (struct _reent *r, size_t a, size_t b)
{
  return calloc (a, b);
}
void
_free_r (struct _reent *r, void *x)
{
  free (x);
}
void *
_realloc_r (struct _reent *r, void *x, size_t sz)
{
  return realloc (x, sz);
}




