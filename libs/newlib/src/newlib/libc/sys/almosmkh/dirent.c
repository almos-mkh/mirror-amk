#include <dirent.h>
#include <syscalls_numbers.h>
#include <hal_user.h>
//////////////////////////////////////
DIR * opendir( const char * pathname )
{
    DIR   * dirp; 
    int     error;
    error = hal_user_syscall( SYS_OPENDIR,
                              (int)pathname,
                              (int)&dirp, 0, 0 );
    if( error ) return NULL;
    else        return dirp;
}

/////////////////////////////////////
struct dirent * readdir( DIR * dirp )
{
    struct dirent * dentp;
    int             error;
    error = hal_user_syscall( SYS_READDIR,
                              (int)dirp,
                              (int)&dentp, 0, 0 );
    if( error ) return NULL;
    else        return dentp;
}

//////////////////////////
int closedir( DIR * dirp )
{
    return hal_user_syscall( SYS_CLOSEDIR,
                             (int)dirp, 0, 0, 0 );
}


