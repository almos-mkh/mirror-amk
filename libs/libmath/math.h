/*
 * math.h - User level <math> library definition.
 * 
 * Author     Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef MATH_H_
#define MATH_H_

/*****************************************************************************************
 * This file defines the user level, mathematical functions library.
 * Most of this code is a modification of the <ulibc> library, developped by Sun 
 * Microsystem Inc in 1993, and ported to the ALMOS-MKH 0S in August 2018.
 ****************************************************************************************/


/*****************************************************************************************
 * This function returns the absolute value of the <x> argument.
 ****************************************************************************************/
double	fabs	(double x);

/*****************************************************************************************
* This function returns the remainder of <x/y>.
 ****************************************************************************************/
double	fmod	(double x, double y);

/*****************************************************************************************
 * This function round to the largest integer value not larger than <x>.
 ****************************************************************************************/
double	floor	(double x);

/*****************************************************************************************
 * This function round to the smallest integer value not less than <x>.
 ****************************************************************************************/
double	ceil	(double x);

/*****************************************************************************************
 * This function returns the sine function of the <x> argument in radian.
 ****************************************************************************************/
double	sin		(double x);

/*****************************************************************************************
 * This function returns the cosine function of the <x> argument in radian.
 ****************************************************************************************/
double	cos		(double x);

/*****************************************************************************************
 * This function returns the value of <x> raised to the power of <y>.
 ****************************************************************************************/
double	pow		(double x, double y);

/*****************************************************************************************
 * This function returns the base-e exponential of <x>. 
 ****************************************************************************************/
double  exp     (double x);

/*****************************************************************************************
 * This function returns non-zero if <x> is not a number (NaN).
 ****************************************************************************************/
int		isnan	(double x);

/*****************************************************************************************
 * This function returns non-zero if <x> is infinite.
 ****************************************************************************************/
int		isfinite(double x);

/*****************************************************************************************
 * These functions return x*(2**n), computed by exponent manipulation.
 ****************************************************************************************/
double	scalbln	(double x, long n);
double	scalbn	(double x, int n);

/*****************************************************************************************
 * This function changes the sign of <x> to that of <y>.
 ****************************************************************************************/
double	copysign(double x, double y);

/*****************************************************************************************
 * This function return the integral value nearest to <x> (according to the 
 * prevailing rounding mode) in floating-point format.
 ****************************************************************************************/
double 	rint	(double x);


#endif
