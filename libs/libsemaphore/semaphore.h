/*
 * pthread.h - User level <semaphore> library definition.
 * 
 * Author     Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _SEMAPHORE_H_
#define _SEMAPHORE_H_

//////////////////////////////////////////////////////////////////////////////////////////////
//             POSIX unnamed semaphores related functions
//
// This synchonisation object allows several thread in a given process to share one
// (or several) shared resource(s).
// - The sem_wait() function allows a thread to take one resource and atomically
//   decrement the semaphore count. If the count value is zero, the calling thread 
//   blocks and deschedules. It is unblocked by another thread when it becomes possible
//   to make the decrement.
// - the sem_post() functions allows a thread to release a resource and atomicalli
//   increment the semaphore count. 
//////////////////////////////////////////////////////////////////////////////////////////////

#include <shared_semaphore.h>

/********************************************************************************************* 
 * This function initializes an unnamed semaphore.
 * Initializing a semaphore that has already been initialized results in undefined behavior.
 *********************************************************************************************
 * @ sem         : [in]  pointer on semaphore.
 * @ pshared     : [in]  unsupported => must be zero. 
 * @ count       : [in]  initial semaphore value.
 * @ return 0 if success / return -1 if failure.
 ********************************************************************************************/
int sem_init( sem_t        * sem,
              int            pshared,
              unsigned int   count );

/********************************************************************************************* 
 * This blocking function takes a semaphore. It decrements the semaphore pointed to by <sem>.
 * If the semaphore's value is greater than zero, then the decrement proceeds, and the
 * function returns immediately. If the semaphore currently has the value zero, the calling
 * thread registers in the associated waiting queue, blocks and deschedules.
 * It will be unblocked by another thread when it becomes possible to perform the decrement.
 *********************************************************************************************
 * @ sem         : [in]  pointer on semaphore.
 * @ return 0 if success / return -1 if failure.
 ********************************************************************************************/
int sem_wait( sem_t * sem );

/********************************************************************************************* 
 * This function releases a semaphore. It increments the semaphore pointed to by <sem>.
 * If the semaphore's value consequently becomes greater than zero, then another thread
 * blocked in a sem_wait() call will be woken up to try to take the semaphore.
 *********************************************************************************************
 * @ sem         : [in]  pointer on semaphore.
 * @ return 0 if success / return -1 if failure.
 ********************************************************************************************/
int sem_post( sem_t * sem );

/********************************************************************************************* 
 * This function destroys the semaphore pointed to by <sem>.
 *********************************************************************************************
 * @ sem         : [in]  pointer on semaphore.
 * @ return 0 if success / return -1 if failure.
 ********************************************************************************************/
int sem_destroy( sem_t * sem );

/********************************************************************************************* 
 * This function places the current value of the semaphore pointed to by <sem>
 * into the integer pointed to by <sval>.
 *********************************************************************************************
 * @ sem         : [in]  pointer on semaphore.
 * @ value       : [out] buffer for semaphore current value.
 * @ return 0 if success / return -1 if failure.
 ********************************************************************************************/
int sem_getvalue( sem_t * sem,
                  int   * value );


#endif  // _SEMAPHORE_H_
