/*
 * pthread.c - User level <semaphore> library implementation.
 * 
 * Author     Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_user.h>
#include <hal_shared_types.h>
#include <semaphore.h>
#include <shared_semaphore.h>
#include <syscalls_numbers.h>
#include <stdio.h>

/////////////////////////////////
int sem_init( sem_t        * sem,
              int            pshared,
              unsigned int   init_value )
{
    if( pshared )
    {
        printf("[ERROR] in %s : pshared argument must be zero\n", __FUNCTION__ );
        return -1;
    }

    return hal_user_syscall( SYS_SEM,
                             (reg_t)sem,
                             SEM_INIT,
                             (reg_t)init_value,
                             0 ) ;
}

///////////////////////////
int sem_wait( sem_t * sem )
{
    return hal_user_syscall( SYS_SEM,
                             (reg_t)sem, 
                             SEM_WAIT, 0, 0 );
}

///////////////////////////
int sem_post( sem_t * sem )
{
    return hal_user_syscall( SYS_SEM,
                             (reg_t)sem, 
                             SEM_POST, 0, 0 );
}

//////////////////////////////
int sem_destroy( sem_t * sem )
{
    return hal_user_syscall( SYS_SEM,
                             (reg_t)sem, 
                             SEM_DESTROY, 0, 0 );
}

//////////////////////////////
int sem_getvalue( sem_t * sem,
                  int   * current_value )
{
    return hal_user_syscall( SYS_SEM,
                             (reg_t)sem,
                             SEM_GETVALUE,
                             0, 
                             (reg_t)current_value );
}


// Local Variables:
// tab-width: 4
// c-basic-offset: 4
// c-file-offsets:((innamespace . 0)(inline-open . 0))
// indent-tabs-mode: nil
// End:
// vim: filetype=c:expandtab:shiftwidth=4:tabstop=4:softtabstop=4

