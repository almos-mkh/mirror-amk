/*
 * dirent.h - User level <dirent> library definition.
 * 
 * Author     Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _DIRENT_H_
#define _DIRENT_H_

#include <shared_dirent.h>

/***************************************************************************************** 
 * This file defines the user level, directory entries related library.
 * All these functions make a system call to access the kernel VFS.
 ****************************************************************************************/

/***************************************************************************************** 
 * This function opens the directory identified by the <pathname> argument, 
 * associates a directory stream with it and returns an user space pointer to identify 
 * this directory stream in subsequent readdir() or closedir() operations.  
 *****************************************************************************************
 * @ pathname   : pathname (can be relative or absolute).
 * @ returns DIR* pointer if success / returns NULL if pathname cannot be accessed.
 ****************************************************************************************/
DIR * opendir( const char * pathname );

/***************************************************************************************** 
 * This function returns a pointer to the next directory entry. 
 *****************************************************************************************
 * @ dirp     : DIR pointer identifying the directory. 
 * @ returns dirent* pointer / returns NULL upon reaching end of directory or on error.
 ****************************************************************************************/
struct dirent * readdir( DIR * dirp );

/***************************************************************************************** 
 * This function closes the directory identified by the <dirp> argument, and releases
 * all structures associated with the <dirp> pointer.
 *****************************************************************************************
 * @ dirp     : DIR pointer identifying the directory. 
 * @ returns 0 if success / returns -1 if failure.
 ****************************************************************************************/
int closedir( DIR * dirp );

#endif
