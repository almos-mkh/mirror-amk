/*
 * wait.h - User level <wait> library definition.
 * 
 * Author     Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _WAIT_H_
#define _WAIT_H_

/***************************************************************************************** 
 * This file defines the user level, memory <wait> library.
 * All these functions make a system call to access the kernel structures.
 * The user/kernel shared structures and mnemonics are defined in
 * the <kernel/syscalls/shared_include/shared_wait.h> file.
 ****************************************************************************************/

#include <shared_wait.h>

/***************************************************************************************** 
 * This blocking function returns only when one child process of the calling process
 * changes state (from RUNNING to STOPPED / EXITED / KILLED). It returns the terminating
 * child process PID, and set in the <status> buffer the terminating child process state.
 ***************************************************************************************** 
 * @ status    : [out] pointer on buffer for terminating child process state.
 * @ returns terminating child process pid.
 ****************************************************************************************/
int wait( int * status );

#endif

