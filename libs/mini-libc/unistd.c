/*
 * unistd.c - User level <unistd> library implementation.
 * 
 * Author     Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <unistd.h>
#include <hal_shared_types.h>
#include <hal_user.h>
#include <syscalls_numbers.h>
#include <shared_signal.h>

//////////////////////////////////
unsigned alarm( unsigned seconds )
{
    return hal_user_syscall( SYS_ALARM,
                             (reg_t)seconds, 0, 0, 0 );
}

//////////////////////////////////
int chdir( const char * pathname )
{
    return hal_user_syscall( SYS_CHDIR,
                             (reg_t)pathname, 0, 0, 0 );
}

///////////////////
int close( int fd )
{
    return hal_user_syscall( SYS_CLOSE,
                             (reg_t)fd, 0, 0, 0 );
}

///////////////////////////
int execve( char  * pathname,
            char ** argv,
            char ** envp )
{
    return hal_user_syscall( SYS_EXEC,
                             (reg_t)pathname,
                             (reg_t)argv,
                             (reg_t)envp, 0 );
}

////////////////
int fork( void )
{
    return hal_user_syscall( SYS_FORK, 0, 0, 0, 0 );
}

/////////////////////////////
int getcwd( char       * buf,
            unsigned int bytes )
{
    return hal_user_syscall( SYS_GETCWD,
                             (reg_t)buf,
                             (reg_t)bytes, 0, 0 );
}

////////////
int getpid( void )
{
    return hal_user_syscall( SYS_GETPID, 0, 0, 0, 0 );
}

///////////////////
int isatty(int fd )
{
    return hal_user_syscall( SYS_ISATTY,
                             (reg_t)fd, 0, 0, 0 );
}

///////////////////////////
int lseek( int          fd,
           unsigned int offset,
           int          whence )
{
    return hal_user_syscall( SYS_LSEEK,
                             (reg_t)fd,
                             (reg_t)offset,
                             (reg_t)whence, 0 );
}

/////////////////
int pause( void )
{
    return hal_user_syscall( SYS_KILL, 
                             getpid(),
                             SIGSTOP, 0, 0 );
}

/////////////////////
int pipe( int fd[2] )
{
    return hal_user_syscall( SYS_PIPE, 
                             (reg_t)fd, 0, 0, 0 );
}

//////////////////////////
int read( int          fd,
          void       * buf,
          unsigned int count )
{
    return hal_user_syscall( SYS_READ,
                             (reg_t)fd,
                             (reg_t)buf,
                             (reg_t)count, 0 );
}

////////////////////////////
int rmdir( char * pathname )
{
    return hal_user_syscall( SYS_RMDIR,
                             (reg_t)pathname, 0, 0, 0 );
} 

///////////////////////////////////
int unlink( const char * pathname )
{
    return hal_user_syscall( SYS_UNLINK,
                             (reg_t)pathname, 0, 0, 0 );
}

///////////////////////////
int write( int          fd,
           const void * buf,
           unsigned int count )
{
    return hal_user_syscall( SYS_WRITE,
                             (reg_t)fd,
                             (reg_t)buf,
                             (reg_t)count, 0 );
}



