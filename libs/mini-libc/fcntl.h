/*
 * fcntl.h - User level <fcntl> library definition.
 * 
 * Author     Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _FCNTL_H_
#define _FCNTL_H_

/***************************************************************************************** 
 * This file defines the user side, file system related <fcntl> library.
 * All these functions make a system call to access the kernel VFS.
 * The user/kernel shared structures and mnemonics are defined in 
 * the <syscalls/shared_include/shared_fcntl.h> file.
 ****************************************************************************************/

#include <shared_fcntl.h>

/***************************************************************************************** 
 * This function creates a new open file descriptor for the file identified by the
 * <pathname> argument, and returns the file descriptor value. 
 * The supported flags are descripted in the <syscalls/shared_include/shared_fcntl.h>.
 * - O_RDONLY   : open file in read-only mode
 * - O_WRONLY   : open file in write-only mode 
 * - O_RDWR     : open file in read/write mode 
 * - O_NONBLOCK : do not block if data non available
 * - O_APPEND   : append on each write
 * - O_CREAT    : create file if it does not exist 
 * - O_TRUNC    : file length is forced to 
 * - O_EXCL     : error if VFS_O_CREAT and file exist
 * - O_SYNC	    : synchronize File System on each write
 * - O_CLOEXEC  : set the close-on-exec flag in file descriptor
 * - O_DIR      : new file descriptor is for a directory 
 * When the O_CREAT is set, the <mode> argument defines the access rights, as described
 * for chmod().
 ***************************************************************************************** 
 * @ pathname  : pathname in VFS (absolute, or relative to current working directory).
 * @ oflags    : bit vector of flags.
 * @ mode      : access rights.
 * @ return file descriptor value if success / returns -1 if failure.
 ****************************************************************************************/
int open( const char * pathname,
          int          flags,
          int          mode );


#endif
