/*
 * stdlib.c - User level <stdlib> library implementation.
 * 
 * Author     Alain Greiner (2016,2017)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <stdlib.h>
#include <hal_shared_types.h>
#include <hal_user.h>
#include <syscalls_numbers.h>
#include <almosmkh.h>
#include <stdio.h>

////////////////////////////////////////////////////////////////////////////////////////// 
//           Global variables                   
//////////////////////////////////////////////////////////////////////////////////////////

unsigned int  rand_seed_value = 1;   // set by srand() , used by rand()

//////////////////////////
int atoi(const char * str)
{
    int res  = 0;      // Initialize result
    int sign = 1;      // Initialize sign as positive
    int i    = 0;      // Initialize index of first digit

    if( (str[0] == '0') && ((str[1] == 'x') || (str[1] == 'X')) )   // hexa
    {
        i = 2;

        while( str[i] != 0 )
        {
            if     ( (str[i] >= '0') && (str[i] <= '9') ) res = (res<<4) + (str[i] - '0');
            else if( (str[i] >= 'A') && (str[i] <= 'F') ) res = (res<<4) + (str[i] - 'A');
            else if( (str[i] >= 'a') && (str[i] <= 'f') ) res = (res<<4) + (str[i] - 'a');
            else return 0;
            i++;
        }
    }
    else                                                            // decimal
    {
        if (str[0] == '-')  //  number is negative, update sign
        {
            sign = -1;  
            i++;           // Also update index of first digit
        }

        while( str[i] != 0 )
        {
            if( (str[i] >= '0') && (str[i] <= '9') ) res = (res*10) + (str[i] - '0');
            else return 0;
            i++;
        }
    }

    // Return result with sign
    return sign*res;
}

////////////////////////////
double atof(const char *str)
{
    const char *pstr = str;
    double res = 0;
    double exp = 0.1;
    short sign = 1;
    short dec = 0;

    while (*pstr != '\0')
    {
        if (*pstr == '-')
        {
            if (str != pstr) break;
            sign = -1;
        }
        
        else if (*pstr == '.')
        {
            if (dec) break;
            dec = 1;
        }
        
        else if (*pstr >= '0' && *pstr <= '9')
        {
            if (dec)
            {
                res = res + ((*pstr - '0')*exp);
                exp = exp / 10;
            }
            else
            {
                res = (res * 10) + (*pstr - '0');
            }
        }
        
        else
        {
            break;
        }
        pstr++;
    }
    return sign * res;
}

////////////////
int rand( void )
{
    unsigned long long cycle;

    get_cycle( &cycle );

    unsigned int x = (unsigned int)cycle * rand_seed_value;

    if ((x & 0xFF) > 0x7F) 
    {
        return (x*x & RAND_MAX);
    }
    else 
    {
        return (x*x*x & RAND_MAX);
    }
}

///////////////////////////////
void srand( unsigned int seed )
{
    rand_seed_value = seed;
}

//////////////////////////////////
void * malloc( unsigned int size )
{
    // get cluster identifier
    unsigned int cxy;
    unsigned int lid;
    get_core( &cxy , &lid );

    return remote_malloc( size, cxy );
} 

///////////////////////////////////
void * calloc ( unsigned int count,
                unsigned int size )
{
    // get calling core cluster identifier
    unsigned int cxy;
    unsigned int lid;
    get_core( &cxy , &lid );

    return remote_calloc( count , size , cxy );
}

///////////////////////////////////
void * realloc ( void        * ptr,
                 unsigned int  size )
{
    // get calling core cluster identifier
    unsigned int cxy;
    unsigned int lid;
    get_core( &cxy , &lid );

    return remote_realloc( ptr , size , cxy );
}

///////////////////////
void free( void * ptr )
{
    // get calling core cluster identifier
    unsigned int cxy;
    unsigned int lid;
    get_core( &cxy , &lid );

    remote_free( ptr , cxy );
}

///////////////////////
void exit( int status )
{
    hal_user_syscall( SYS_EXIT,
                      (reg_t)status, 0, 0, 0 );
}


