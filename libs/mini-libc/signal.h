/*
 * signal.c - User side signals related syscalls implementation.
 * 
 * Author     Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */
#ifndef _SIGNAL_H_
#define _SIGNAL_H_

/***************************************************************************************** 
 * This file defines the user side, signals related library.
 * All these functions make a system call to access the kernel VFS.
 * The user/kernel shared structures and mnemonics are defined in 
 * the <syscalls/shared_include/shared_signal.h> file.
 ****************************************************************************************/

#include <shared_signal.h>

/***************************************************************************************** 
 * This function associate a specific signal handler to a given signal type.
 * The handlers for the SIGKILL and SIGSTOP signals cannot be redefined.
 * TODO : this function is not implemented yet.
 ***************************************************************************************** 
 * @ sig_id    : index defining signal type (from 1 to 31).
 * @ handler   : pointer on fonction implementing the specific handler.
 * @ return 0 if success / returns -1 if failure.
 ****************************************************************************************/
int signal( unsigned int   sig_id,
            void         * handler );

/***************************************************************************************** 
 * This function implements the "kill" system call on the user side.
 * It register the signal defined by the <sig_id> argument in all thread descriptors 
 * of a target process identified by the <pid> argument. This is done in all clusters
 * containing threads for the target process.
 * It can be executed by any thread running in any cluster, as this function uses
 * remote access to traverse the list of process copies stored in the owner cluster.
 * This function does nothing for (sig_id == 0). This can be used to check process pid.
 * The signals SIGKILL and SIGSTOP cannot be caught, blocked, or ignored.
 * TODO : only SIGKILL / SIGSTOP / SIGCONT values.
 ***************************************************************************************** 
 * @ pid      : target process identifier.
 * @ sig_id   : index defining the signal type.
 * @ return 0 if success / returns -1 if failure.
 ****************************************************************************************/
int kill( unsigned int  pid,
          unsigned int  sig_id );

#endif
