/*
 * string.h - User level <string> library definition.
 * 
 * Author     Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _STRING_H_
#define _STRING_H_

/***************************************************************************************** 
 * This file defines the user level, character string related <string> library.
 * These functions do not use any syscall.
 ****************************************************************************************/


/********************************************************************************************
 * This function returns the length of a character string.
 ********************************************************************************************
 * @ s     : pointer on the string.
 * @ return the number of characters preceding the NUL character.
 *******************************************************************************************/
unsigned int strlen( const char * s );

/********************************************************************************************
 * This function attempts to compute the length of a string, but never scans beyond 
 * <maxlen> characters.
 ********************************************************************************************
 * @ s     : pointer on the string.
 * returns min( maxlen , number of characters preceding the NUL character).
 *******************************************************************************************/
unsigned int strnlen( const char * str, 
                      unsigned int     maxlen );

/********************************************************************************************
 * This function compares lexicographically the strings s1 and s2.
 * Characters are considered unsigned.
 * It does not compare characters after the first NUL character.
 ********************************************************************************************
 * @ s1   : pointer on string.
 * @ s2   : pointer on string.
 * @ return 0 if s1 == s2 / return 1 if s1 > s2 / return -1 if s1 < s2
 *******************************************************************************************/
int strcmp ( const char * s1, 
             const char * s2 );

/********************************************************************************************
 * This function compares lexicographically the strings s1 and s2.
 * It does not compare more than <n> characters and stops after the first NUL character.
 ********************************************************************************************
 * @ s1   : pointer on string.
 * @ s2   : pointer on string.
 * @ n    : max number of compared characters.
 * @ return 0 if s1 == s2 / return 1 if s1 > s2 / return -1 if s1 < s2
 *******************************************************************************************/
int strncmp ( const char     * s1,
              const char     * s2,
              unsigned int     n );

/********************************************************************************************
 * this function copies the <src> buffer to the <dst> buffer, including the terminating NUL.
 ********************************************************************************************
 * @ dst   : pointer on destination buffer.
 * @ src   : pointer on source buffer.
 *******************************************************************************************/
char * strcpy ( char * dst, 
                char * src );

/********************************************************************************************
 * This function copies <count> characters from the <src> buffer to the <dst> buffer.
 ********************************************************************************************
 * @ dst   : pointer on destination buffer.
 * @ src   : pointer on source buffer.
 * @ count : number of characters to be copied.
 *******************************************************************************************/
char * strncpy ( char         * dst,
                 char         * src,
                 unsigned int   count );

/********************************************************************************************
 * This function locates the first occurence of the <find> string in the <s> string.
 ********************************************************************************************
 * @ s     : string to be analysed.
 * @ find  : searched string
 * @ return pointer on the found string / return NULL if not found.
 *******************************************************************************************/
char * strstr( char       * s,
               const char * find);

/********************************************************************************************
 * This function locates the first occurence of the <c> character in the <s> string.
 ********************************************************************************************
 * @ s     : string to be analysed.
 * @ c     : searched character value (casted to a char)
 * @ return pointer on the found character / return NULL if not found.
 *******************************************************************************************/
char * strchr ( const char * s,
                int          c );

/********************************************************************************************
 * This function locates the last occurence of the <c> character in the <s> string.
 ********************************************************************************************
 * @ s     : string to be analysed.
 * @ c     : searched character value (casted to a char)
 * @ return pointer on the found character / return NULL if not found.
 *******************************************************************************************/
char * strrchr ( const char * t,
                 int          c );


/*********************************************************************************************
 * This function copies the content of a <src> byte array to a <dst> byte array.
 * Behaviour is undefined if <src> and <dst> arrays overlap.
 *********************************************************************************************
 * @ dst     : pointer on destination array.
 * @ dst     : pointer on source array.
 * @ size    : number of bytes to move.
 * @ return first argument.
 ********************************************************************************************/
void * memcpy( void         * dst, 
               const void   * src,
               unsigned int   size );

/*********************************************************************************************
 * This function fill a byte array with a byte value.
 *********************************************************************************************
 * @ dst     : pointer on the byte array.
 * @ s       : byte value (will be casted to unsigned char).
 * @ size    : number of bytes to be set.
 * @ return first argument.
 ********************************************************************************************/
void * memset( void        * dst,
               int           s, 
               unsigned int  size);

#endif	/* _STRING_H_ */

