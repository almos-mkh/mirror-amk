/*
 * ctype.c - User level <ctype> library definition.
 * 
 * Author     Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _CTYPE_H_
#define _CTYPE_H_

/********************************************************************************************
 * This file defines the user level character classification <ctype> library.
 * These functions do not make any syscall.
 *******************************************************************************************/


/********************************************************************************************
 * This function returns a lower case ASCII code if input is in [A...Z] range.
 * It returns the input character when it is not in the [A...Z] range.
 ********************************************************************************************
 * @ c     : the 8 LSB bits define the character to be forced to lower case.
 * @ return lower case ASCII code.
 *******************************************************************************************/
inline int tolower( int  c );

/********************************************************************************************
 * This function returns an upper case ASCII code if input is in [a...z] range.
 * It returns the input character when it is not in the [a...z] range.
 ********************************************************************************************
 * @ c     : the 8 LSB bits define the character to be forced to upper case.
 * @ return upper case ASCII code.
 *******************************************************************************************/
inline int toupper( int  c );


#endif
