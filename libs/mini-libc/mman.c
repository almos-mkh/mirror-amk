/*
 * mman.c - User level <mman> library implementation.
 * 
 * Author     Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <mman.h>
#include <hal_shared_types.h>
#include <hal_user.h>
#include <syscalls_numbers.h>
#include <stdio.h>

////////////////////////////////
int munmap( void         * addr,
            unsigned int   size )
{
    return hal_user_syscall( SYS_MUNMAP,
                             (reg_t)addr,
                             (reg_t)size, 0, 0 );
}

///////////////////////////////
void * mmap( void       * addr,
             unsigned int length,
             int          prot,
             int          flags,
             int          fd,
             unsigned int offset )
{
    mmap_attr_t attr;

    // addr argument must be NULL
    if( addr != NULL )
    {
        printf("\n[ERROR] in %s : the <addr> argument must be NULL\n", __FUNCTION__ );
        return NULL;
    }

    //set the mmap attributes 
    attr.addr   = NULL;
    attr.length = length;
    attr.prot   = prot;
    attr.flags  = flags;
    attr.fdid   = fd;
    attr.offset = offset;

    if( hal_user_syscall( SYS_MMAP,
                          (reg_t)&attr, 0, 0, 0 ) ) return NULL;
    else                                            return attr.addr;
}


