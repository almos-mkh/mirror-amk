/*
 * mman.h - User level <mman> library definition.
 * 
 * Author     Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _MMAN_H_
#define _MMAN_H_

/***************************************************************************************** 
 * This file defines the user level, memory mapping related <mman> library.
 * All these functions make a system call to access the calling thread VMM.
 * The user/kernel shared structures and mnemonics are defined in
 * the <syscalls/shared_include/shared_mman.h> file.
 ****************************************************************************************/

#include <shared_mman.h>

/***************************************************************************************** 
 * This function map physical memory (or a file) for a new vseg in the calling thread
 * virtual space, as defined by the arguments.
 *****************************************************************************************
 * @ addr    : requested address in virtual space / unsupported : should be NULL.
 * @ length  : requested number of bytes.
 * @ prot    : access mode bit vector (PROT_EXEC / PROT_READ / PROT_WRITE)
 * @ flags   : bit_vector (MAP_FILE / MAP_ANON / MAP_REMOTE / MAP_PRIVATE / MAP_SHARED)
 * @ fdid    : file descriptor index (if MAP_FILE).
 * @ offset  : offset in file (if MAP_FILE).
 * @ returns base address in virtual space / returns NULL if failure.
 ****************************************************************************************/
void * mmap( void         * addr,
             unsigned int   length,
             int            prot,
             int            flags,
             int            fd,
             unsigned int   offset );

/***************************************************************************************** 
 * This function unmap an existing mapping in the calling thread virtual space,
 * as defined by the <vaddr> and <size> arguments.
 *****************************************************************************************
 * @ addr    : address in virtual space / unused : should be NULL.
 * @ size    : requested number of bytes.
 * @ return 0 if success / return -1 if failure.
 ****************************************************************************************/
int munmap( void         * addr,
            unsigned int   size );

#endif
