/*
 * stat.h - User side file stat related syscalls definition.
 * 
 * Author     Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _STAT_H_
#define _STAT_H_

/***************************************************************************************** 
 * This file defines the stat related library.
 * All these functions make a system call to access kernel structures.
 * The user/kernel shared structures and mnemonics are defined in 
 * the <syscalls/shared_include/shared_stat.h> file.
 ****************************************************************************************/

#include <shared_stat.h>

/***************************************************************************************** 
 * This function creates a new directory in file system.
 *****************************************************************************************
 * @ pathname   : pathname (can be relative or absolute).
 * @ mode       : access rights (as defined in chmod).
 * @ return 0 if success / returns -1 if failure.
 ****************************************************************************************/
int mkdir( const char * pathname,
           int          mode );

/***************************************************************************************** 
 * This function creates a named FIFO file in the calling thread cluster.
 * The associated read and write file descriptors mut be be  explicitely created
 * using the open() system call.
 *****************************************************************************************
 * @ pathname   : pathname (can be relative or absolute).
 * @ mode       : access rights (as defined in chmod).
 * @ return 0 if success / returns -1 if failure.
 ****************************************************************************************/
int mkfifo( const char * pathname,
            int          mode );

/***************************************************************************************** 
 * This function change the acces rights for the file/dir identified by the
 * pathname argument.
 *****************************************************************************************
 * @ pathname   : pathname (can be relative or absolute).
 * @ rights     : acces rights.
 * @ return 0 if success / returns -1 if failure.
 ****************************************************************************************/
int chmod( char     * pathname,
           unsigned int   rights );

/***************************************************************************************** 
 * This function  returns in the <stat> structure, defined in the "shared_syscalls.h" 
 * file, various informations on the file/directory identified by the <pathname> argument.
 ***************************************************************************************** 
 * @ pathname  : user pointer on file pathname.
 * @ stat      : user pointer on the stat structure.
 * @ returns O if success / returns -1 if failure.
 ****************************************************************************************/
int stat( const char  * pathname,
          struct stat * stat );

#endif /* _SYS_STAT_H_ */
