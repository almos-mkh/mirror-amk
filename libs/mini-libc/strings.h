/*
 * strings.h - User level <strings> library definition.
 * 
 * Author     Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _STRINGS_H_
#define _STRINGS_H_

/***************************************************************************************** 
 * This file defines the user level, case insensitive string related <strings> library.
 * These functions do not use any syscall.
 ****************************************************************************************/


/********************************************************************************************
 * This function compares lexicographically the strings s1 and s2, ignoring case.
 ********************************************************************************************
 * @ s1   : pointer on string.
 * @ s2   : pointer on string.
 * @ return 0 if s1 == s2 / return 1 if s1 > s2 / return -1 if s1 < s2
 *******************************************************************************************/
int strcasecmp ( const char * s1,
                 const char * s2 );

#endif
