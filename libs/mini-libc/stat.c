/*
 * stat.c - User side file statistics related library implementation.
 * 
 * Author     Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <stat.h>
#include <hal_user.h>
#include <hal_shared_types.h>
#include <syscalls_numbers.h>

/////////////////////////////////
int mkdir( const char * pathname,
           int          mode )
{
    return hal_user_syscall( SYS_MKDIR,
                             (reg_t)pathname,
                             (reg_t)mode, 0, 0 );
}

//////////////////////////////////
int mkfifo( const char * pathname,
            int          mode )
{
    return hal_user_syscall( SYS_MKFIFO,
                             (reg_t)pathname,
                             (reg_t)mode, 0, 0 );
}

///////////////////////////////////
int chmod( char         * pathname,
           unsigned int   rights )
{
    return hal_user_syscall( SYS_CHMOD,
                             (reg_t)pathname,
                             (reg_t)rights, 0, 0 );
}
/////////////////////////////////
int stat( const char  * pathname,
          struct stat * stat )
{
    return hal_user_syscall( SYS_STAT,
                             (reg_t)pathname,
                             (reg_t)stat, 0, 0 );
}



