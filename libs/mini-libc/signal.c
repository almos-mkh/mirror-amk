/*
 * signal.c - User side signals related library implementation.
 * 
 * Author     Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <signal.h>
#include <hal_shared_types.h>
#include <hal_user.h>
#include <syscalls_numbers.h>

/////////////////////////////////
int signal( unsigned int   sigid,
            void         * handler )
{
    return hal_user_syscall( SYS_SIGNAL,
                             (reg_t)sigid,
                             (reg_t)handler, 0, 0 );
}

///////////////////////////
int kill( unsigned int pid,
          unsigned int sig_id )
{
    return hal_user_syscall( SYS_KILL,
                             (reg_t)pid,
                             (reg_t)sig_id, 0, 0 );
}


