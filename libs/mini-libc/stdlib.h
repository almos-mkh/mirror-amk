/*
 * stdlib.h - User level <stdlib> library definition.
 * 
 * Author     Alain Greiner (2016,2017)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _STDLIB_H_
#define _STDLIB_H

/***************************************************************************************** 
 * This file defines the user side <stdlib> library.
 * Some functions make a system call to access the kernel VFS.
 * The user/kernel shared structures and mnemonics are defined in 
 * the <syscalls/shared_include/shared_fcntl.h> file.
 ****************************************************************************************/

#include <shared_stdlib.h>

#define  RAND_MAX       0xFFFF

/***************************************************************************************** 
 * This function terminates a process.
 *****************************************************************************************
 * @ status   : terminaison status : 0 / EXIT_SUCCESS / EXIT_FAILURE.
 ****************************************************************************************/
void exit( int status );

/*********************************************************************************************
 * This function translates an ascii character string to an integer value.
 *********************************************************************************************
 * @ str   : pointer on charactter string.
 * @ return an integer value.
 ********************************************************************************************/
int atoi(const char * str );

/*********************************************************************************************
 * This function translates an ascii character string to a float value.
 *********************************************************************************************
 * @ str   : pointer on charactter string.
 * @ return a double value.
 ********************************************************************************************/
double atof(const char * str );

/*********************************************************************************************
 * This function sets the seed for a new sequence of pseudo-random numbers to be returned
 * by the rand function rand(). These sequences are repeatable by calling srand() with 
 * the same seed value.
 *********************************************************************************************
 * # seed  : seed value.
 ********************************************************************************************/
void srand( unsigned int seed );

/*********************************************************************************************
 * This function computes a sequence of pseudo-random integers in the range [0 to RAND_MAX].
 *********************************************************************************************
 * @ return an integer value between 0 and RAND_MAX.
 ********************************************************************************************/
int rand( void );

/*****************************************************************************************
 * This function allocates <size> bytes of memory in user space and returns a pointer
 * on the allocated buffer. The physical memory is allocated from store located in 
 * the calling core cluster.
 *****************************************************************************************
 * @ size    : number of requested bytes.
 * @ returns a pointer on the allocated buffer if success / returns NULL if failure
 ****************************************************************************************/
void * malloc( unsigned int size );

/*****************************************************************************************
 * This function releases the memory buffer identified by the <ptr> argument,
 * to the store located in the calling core cluster.
 * It displays an error message, but does nothing if the ptr is illegal.
 *****************************************************************************************
 * @ ptr   : pointer on the released buffer.
 ****************************************************************************************/
void free( void * ptr );

/*****************************************************************************************
 * This function releases the memory buffer identified by the <ptr> argument,
 * to the store located in the calling core cluster, and allocates a new buffer
 * containing <size> bytes from this store. 
 * The content of the old buffer is copied to the new buffer, up to <size> bytes.
 * It displays an error message, but does nothing if the ptr is illegal.
 *****************************************************************************************
 * @ ptr   : pointer on the released buffer.
 * @ size  : new buffer requested size (bytes).
 * @ return a pointer on allocated buffer if success / return NULL if failure
 ****************************************************************************************/
void * realloc( void        * ptr,
                unsigned int  size );


/*****************************************************************************************
 * This function allocates enough space for <count> objects that are <size> bytes
 * of memory each from the store located in the calling core cluster.
 * The allocated memory is filled with bytes of value zero.
 *****************************************************************************************
 * @ count   : number of requested objects.
 * @ size    : number of bytes per object.
 * @ returns a pointer on allocated buffer if success / returns NULL if failure
 ****************************************************************************************/
void * calloc( unsigned int count,
               unsigned int size );

#endif  // _STDLIB_H_
