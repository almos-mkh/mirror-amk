/*
 * strings.h - User level <strings> library implementation.
 * 
 * Author     Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <strings.h>
#include <ctype.h>

//////////////////////////////////
int strcasecmp( const char * str1,
                const char * str2 )
{
	char c1;
	char c2;

	do
    {
		c1 = (char)toupper( (int)*++str1 );
		c2 = (char)toupper( (int)*++str2 );
		c2 = toupper(*++str2);
	}
    while(c1 && c1 == c2);

	return (c1 - c2);
}
