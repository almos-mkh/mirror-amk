/*
 * dirent.c - User level <dirent> library implementation.
 * 
 * Author     Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <dirent.h>
#include <hal_shared_types.h>
#include <hal_user.h>
#include <syscalls_numbers.h>
#include <stdio.h>

//////////////////////////////////////
DIR * opendir( const char * pathname )
{
    DIR   * dirp; 
    int     error;
    error = hal_user_syscall( SYS_OPENDIR,
                              (reg_t)pathname,
                              (reg_t)&dirp, 0, 0 );
    if( error ) return NULL;
    else        return dirp;
}

/////////////////////////////////////
struct dirent * readdir( DIR * dirp )
{
    struct dirent * dentp;
    int             error;
    error = hal_user_syscall( SYS_READDIR,
                              (reg_t)dirp,
                              (reg_t)&dentp, 0, 0 );
    if( error ) return NULL;
    else        return dentp;
}

//////////////////////////
int closedir( DIR * dirp )
{
    return hal_user_syscall( SYS_CLOSEDIR,
                             (reg_t)dirp, 0, 0, 0 );
}


