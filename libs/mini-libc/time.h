/*
 * mman.h - User level <time> library definition.
 * 
 * Author     Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _TIME_H_
#define _TIME_H_

/***************************************************************************************** 
 * This file defines the user level, time of day related <time> library.
 * All these functions make a system call to access the calling thread VMM.
 * The user/kernel shared structures and mnemonics are defined in
 * the <syscalls/shared_include/shared_time.h> file.
 ****************************************************************************************/

#include <shared_time.h>

/***************************************************************************************** 
 * This function returns in the structure <tv>, defined in the time.h file,
 * the current time (in seconds & micro-seconds).
 * It is computed from the calling core descriptor.
 * The timezone is not supported.
 *****************************************************************************************
 * @ tv      : pointer on the timeval structure.
 * @ tz      : pointer on the timezone structure : must be NULL.       
 * @ return 0 if success / returns -1 if failure.
 ****************************************************************************************/
int gettimeofday( struct timeval  * tv,
                  struct timezone * tz );



#endif /* _TIME_H_ */
