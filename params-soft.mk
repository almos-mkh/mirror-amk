
# define absolute path to almos-mkh directory
ALMOSMKH_DIR = /Users/alain/soc/almos-mkh

# Select the libc
LIBC_NAME = mini-libc
#LIBC_NAME = newlib

# Select the target architecture
ARCH_NAME = tsar_mips32
#ARCH_NAME = x86_64

# check
ifeq ($(ALMOSMKH_DIR),)
$(error Please define ALMOSMKH_DIR parameter in params-soft.mk!)
endif

ifeq ($(ARCH_NAME),)
$(error Please define ARCH_NAME parameter in params-soft.mk!)
endif

ifeq ($(LIBC_NAME),)
$(error Please define LIBC_NAME parameter in params-soft.mk!)
endif

# define path for LIBC
ifeq ($(LIBC_NAME), mini-libc)
  LIBC_PATH    = $(ALMOSMKH_DIR)/libs/mini-libc/
  LIBC         = $(LIBC_PATH)/build/lib/
  LIBC_INCLUDE = $(LIBC_PATH)/build/include/
endif
ifeq ($(LIBC_NAME), newlib)
  ifeq ($(ARCH_NAME), tsar_mips32)
    ARCH_NEWLIB=mipsel
  endif
  #TODO for x86
  LIBC_PATH    = $(ALMOSMKH_DIR)/libs/newlib/
  LIBC         = $(LIBC_PATH)/build/$(ARCH_NEWLIB)-almosmkh/lib/
  LIBC_INCLUDE = $(LIBC_PATH)/build/$(ARCH_NEWLIB)-almosmkh/include/
endif

# define paths for LIBPTHREAD
LIBPTHREAD_PATH    = $(ALMOSMKH_DIR)/libs/libpthread/
LIBPTHREAD         = $(LIBPTHREAD_PATH)/build/lib/
LIBPTHREAD_INCLUDE = $(LIBPTHREAD_PATH)/build/include/

# define paths for LIBSEMAPHORE
LIBSEMAPHORE_PATH    = $(ALMOSMKH_DIR)/libs/libsemaphore
LIBSEMAPHORE         = $(LIBSEMAPHORE_PATH)/build/lib/
LIBSEMAPHORE_INCLUDE = $(LIBSEMAPHORE_PATH)/build/include/

# define paths for LIBMATH
LIBMATH_PATH    = $(ALMOSMKH_DIR)/libs/libmath
LIBMATH         = $(LIBMATH_PATH)/build/lib/
LIBMATH_INCLUDE = $(LIBMATH_PATH)/build/include/

# define paths for LIBALMOSMKH
LIBALMOSMKH_PATH    = $(ALMOSMKH_DIR)/libs/libalmosmkh
LIBALMOSMKH         = $(LIBALMOSMKH_PATH)/build/lib/
LIBALMOSMKH_INCLUDE = $(LIBALMOSMKH_PATH)/build/include/

# define paths for HAL
HAL            = $(ALMOSMKH_DIR)/hal
HAL_ARCH       = $(HAL)/$(ARCH_NAME)
HAL_INCLUDE    = $(HAL_ARCH)/core

# define paths for KERNEL
KERNEL         = $(ALMOSMKH_DIR)/kernel
SHARED_INCLUDE = $(KERNEL)/syscalls/shared_include/


ifeq ($(ARCH_NAME), tsar_mips32)
    $(info ARCH_NAME is tsar_mips32)
    export CFLAGS = -Wall -Wextra -ffreestanding -mno-gpopt -mips32 -g -ggdb -O2 \
                -fno-delete-null-pointer-checks -std=gnu89
    export CC = mipsel-unknown-elf-gcc
    export AS = mipsel-unknown-elf-as
    export LD = mipsel-unknown-elf-ld
    export DU = mipsel-unknown-elf-objdump
    export AR = mipsel-unknown-elf-ar
    export RANLIB = mipsel-unknown-elf-ranlib
endif

ifeq ($(ARCH_NAME), x86_64)
    $(info ARCH_NAME is x86_64)
    export CFLAGS = -Wall -ffreestanding -mno-red-zone -mno-mmx -mno-sse -mno-avx -g \
      -fno-delete-null-pointer-checks -mcmodel=large -std=gnu89
    export CC = gcc
    export AS = as
    export LD = ld
    export DU = objdump
    export AR = ar
    export RANLIB = ranlib
endif

BOOTLOADER_PATH = boot/$(ARCH_NAME)



