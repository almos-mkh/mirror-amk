/*
 * arch_info.c - Access functions to Hardware Architecture Information Structure
 *
 * Author  Alain Greiner (2016)
 *
 * Copyright (c)  UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH.is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH.is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH.; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

///////////////////////////////////////////////////////////////////////
archinfo_core_t * archinfo_get_core_base(archinfo_header_t * header) 
{
    return (archinfo_core_t *) ((char *) header +
            ARCHINFO_HEADER_SIZE);
}

//////////////////////////////////////////////////////////////////////
archinfo_cluster_t * archinfo_get_cluster_base(archinfo_header_t * header) 
{
    return (archinfo_cluster_t *) ((char *) header +
            ARCHINFO_HEADER_SIZE +
            ARCHINFO_CORE_SIZE * header->cores);
}

///////////////////////////////////////////////////////////////////////
archinfo_device_t *archinfo_get_device_base(archinfo_header_t * header) 
{
    return (archinfo_device_t *) ((char *) header +
            ARCHINFO_HEADER_SIZE +
            ARCHINFO_CORE_SIZE * header->cores +
            ARCHINFO_CLUSTER_SIZE * header->x_size * header->y_size);
}

////////////////////////////////////////////////////////////////////
archinfo_irq_t *archinfo_get_irq_base(archinfo_header_t * header) 
{
    return (archinfo_irq_t *) ((char *) header +
            ARCHINFO_HEADER_SIZE +
            ARCHINFO_CORE_SIZE * header->cores +
            ARCHINFO_CLUSTER_SIZE * header->x_size * header->y_size +
            ARCHINFO_DEVICE_SIZE * header->devices);
}
