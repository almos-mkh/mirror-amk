/*
 * archinfo.h - Hardware Architecture Information structures
 *
 * Author  Alain Greiner (2016)
 *
 * Copyright (c)  UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _ARCHINFO_H_
#define _ARCHINFO_H_

/****************************************************************************************
 * The ARCHINFO data structure describes a generic manycore hardware architecture:
 * - The number of cluster is variable (can be one). 
 * - The cluster topology is variable (2D mesh or vector)
 * - The number of cores per cluster is variable (can be zero). 
 * - The number of addressable component per cluster is variable.
 * - The size of the physical memory bank per cluster is variable.
 * An adressable device componentcan can be a physical memory bank or a peripheral.
 * Each cluster cover a fixed size segment in physical address space.
 *
 * It is loaded from the block device by the ALMOS-MKH bootloader as a BLOB.
 * The ARCHINFO structure has a three levels hierarchical organisation: 
 * - the architecture contains a variable number of clusters.
 * - each cluster contains a variable number of cores and a variable number of devices.
 * - some device contains a variable number of input IRQs.

 * The BLOB is organised as the concatenation of a fixed size header, 
 * and 4 variable size arrays of fixed size objects in the following order:
 * 1 : archinfo_core_t     core[]  
 * 2 : archinfo_cluster_t  cluster[]  
 * 3 : archinfo_device_t   device[]
 * 4 : archinfo_irq_t      irq[]   
 ***************************************************************************************/

#include <hal_kernel_types.h>

#define ARCHINFO_HEADER_SIZE   sizeof(archinfo_header_t)
#define ARCHINFO_CLUSTER_SIZE  sizeof(archinfo_cluster_t)
#define ARCHINFO_CORE_SIZE     sizeof(archinfo_core_t)
#define ARCHINFO_IRQ_SIZE      sizeof(archinfo_irq_t)
#define ARCHINFO_DEVICE_SIZE   sizeof(archinfo_device_t)

#define ARCHINFO_SIGNATURE   0xBABE2016

/****************************************************************************************
 * This enum defines the supported device types.
 * The 16 MSB bits define the functionnal type.
 * The 16 LSB bits define the implementation type.
 * It must be consistent with values defined in file arch_class.py
 ***************************************************************************************/

enum device_types_e
{
    DEV_TYPE_RAM_SCL   = 0x00000000,
    DEV_TYPE_ROM_SCL   = 0x00010000,
    DEV_TYPE_FBF_SCL   = 0x00020000,
    DEV_TYPE_FBF_LTI   = 0x00020001,
    DEV_TYPE_IOB_TSR   = 0x00030000,
    DEV_TYPE_IOC_BDV   = 0x00040000,
    DEV_TYPE_IOC_HBA   = 0x00040001,
    DEV_TYPE_IOC_SDC   = 0x00040002,
    DEV_TYPE_IOC_SPI   = 0x00040003,
    DEV_TYPE_IOC_RDK   = 0x00040004,
    DEV_TYPE_MMC_TSR   = 0x00050000,
    DEV_TYPE_DMA_SCL   = 0x00060000,
    DEV_TYPE_NIC_CBF   = 0x00070000,
    DEV_TYPE_TIM_SCL   = 0x00080000,
    DEV_TYPE_TXT_TTY   = 0x00090000,
    DEV_TYPE_TXT_RS2   = 0x00090001,
    DEV_TYPE_TXT_MTY   = 0x00090002,
    DEV_TYPE_ICU_XCU   = 0x000A0000,
    DEV_TYPE_PIC_TSR   = 0x000B0000,
}; 

/****************************************************************************************
 * This structure defines the ARCHINFO header.
 * WARNING: the size of this structure (128 bytes) is used by the ALMOS-MKH bootloader.
 ***************************************************************************************/

typedef struct __attribute__((packed))  archinfo_header_s 
{
    uint32_t    signature;       // must contain ARCH_INFO_SIGNATURE
    uint32_t    x_size;          // number of clusters in a row
    uint32_t    y_size;          // number of clusters in a column
    uint32_t    paddr_width;     // number of bits for physical address
    uint32_t    x_width;         // number of bits for x coordinate
    uint32_t    y_width;         // number of bits for y coordinate
    uint32_t    cores_max;       // max number of cores per cluster
    uint32_t    devices_max;     // max number of devices per cluster

    uint32_t    cores;           // total number of cores 
    uint32_t    devices;         // total number of devices 
    uint32_t    irqs;            // total number of irqs 
    uint32_t    io_cxy;          // io_cluster identifier
    uint32_t    boot_cxy;        // boot_cluster identifier
    uint32_t    irqs_per_core;   // number of IRQs per core
    uint32_t    cache_line_size; // number of bytes
    uint32_t    reserved;        // reserved

    char        name[64];        // architecture name
} 
archinfo_header_t;


/****************************************************************************************
 * This structure defines the ARCHINFO cluster.
 ***************************************************************************************/

typedef struct __attribute__((packed))  archinfo_cluster_s 
{
    uint32_t    cxy;             // cluster identifier
    uint32_t    cores;           // number of cores in cluster
    uint32_t    core_offset;     // global index of first core in cluster
    uint32_t    devices;         // number of devices in cluster
    uint32_t    device_offset;   // global index of first device in cluster
} 
archinfo_cluster_t;

/****************************************************************************************
 * This structure defines the ARCHINFO core descriptor.
 * WARNING: the size of this structure (8 bytes) is used by the ALMOS-MKH bootloader.
 ***************************************************************************************/

typedef struct __attribute__((packed))  archinfo_core_s 
{
    uint32_t    gid;             // core hardware identifier
    uint16_t    cxy;             // cluster identifier         
    uint16_t    lid;             // core local index in cluster
} 
archinfo_core_t;

/****************************************************************************************
 * This structure defines the ARCHINFO device descriptor.
 ***************************************************************************************/

typedef struct __attribute__((packed))  archinfo_device_s
{
    uint64_t    base;            // base address in physical space
    uint64_t    size;            // channel size (bytes)
    uint32_t    type;            // supported values defined above
    uint32_t    channels;        // number of channels
    uint32_t    arg0;            // semantic depends on device type
    uint32_t    arg1;            // semantic depends on device type
    uint32_t    arg2;            // semantic depends on device type
    uint32_t    arg3;            // semantic depends on device type
    uint32_t    irqs;            // number of input IRQs (for ICU or PIC)
    uint32_t    irq_offset;      // global index of first IRQ 
} 
archinfo_device_t;  

/****************************************************************************************
 * This structure defines the ARCHINFO input IRQs for XCU or PIC components.
 * It describes the hardware connection from one device output IRQ (identified 
 * by the source device type, channel, and direction) to a PIC or XCU iput port.
 ***************************************************************************************/

typedef struct __attribute__((packed))  archinfo_irq_s 
{
    uint32_t    dev_type;        // source device type index
    uint8_t     channel;         // source device channel
    uint8_t     is_rx;           // source device direction 
    uint8_t     port;            // input IRQ index (in XCU/PIC)
    uint8_t     reserved;        // padding
} 
archinfo_irq_t; 

/****************************************************************************
 * These functions allows the boot-loader to get to the starting address    *
 * of various ARCHINFO tables.                                              *
 ****************************************************************************/

inline archinfo_core_t* archinfo_get_core_base(archinfo_header_t* header)
{
    return (archinfo_core_t*)((char*)header                                 + 
                              ARCHINFO_HEADER_SIZE);
}

inline archinfo_cluster_t* archinfo_get_cluster_base(archinfo_header_t* header)
{
    return (archinfo_cluster_t*)((char*)header                              + 
                                 ARCHINFO_HEADER_SIZE                       +
                                 ARCHINFO_CORE_SIZE * header->cores); 
}
                                 
inline archinfo_device_t* archinfo_get_device_base(archinfo_header_t* header)
{
    return (archinfo_device_t*)((char*)header                               + 
                                 ARCHINFO_HEADER_SIZE                       +
                                 ARCHINFO_CORE_SIZE * header->cores         + 
                                 ARCHINFO_CLUSTER_SIZE * header->x_size * 
                                                         header->y_size);
}

inline archinfo_irq_t* archinfo_get_irq_base(archinfo_header_t* header)
{
    return (archinfo_irq_t*)((char*)header                                  + 
                             ARCHINFO_HEADER_SIZE                           +
                             ARCHINFO_CORE_SIZE * header->cores             + 
                             ARCHINFO_CLUSTER_SIZE * header->x_size * 
                                                     header->y_size         +
                             ARCHINFO_DEVICE_SIZE * header->devices);
}

#endif	/* _ARCHINFO_H_ */
