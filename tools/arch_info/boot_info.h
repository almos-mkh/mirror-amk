/*
 * boot_info.h - informations passed by the bootloader to the kernel in each cluster.
 *
 * Author  Alain Greiner (june 2016,2017)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH
 *
 * ALMOS-kernel is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-kernel is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-kernel; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _BOOT_INFO_H_
#define _BOOT_INFO_H_

#include <hal_kernel_types.h>
#include <kernel_config.h>

/*********************************************************************************************
 * Boot-Info binary file signature
 ********************************************************************************************/

#define BOOT_INFO_SIGNATURE    0x12344321

/*********************************************************************************************
 * This structure defines the mapping between the hard-wired core identifier,
 * and the composite index (cluster_identifier , local index).
 ********************************************************************************************/

typedef struct boot_core_s
{
    gid_t    gid;                     /*! hardware identifier                               */
    lid_t    lid;                     /*! local index in cluster                            */
    cxy_t    cxy;                     /*! cluster identifier                                */
}
boot_core_t;

/*********************************************************************************************
 * This structure defines the hardware connexion from one source device (identified by
 * the tuple (dev_type/channel/is_rx), to a PIC or XCU component (input IRQ index).
 ********************************************************************************************/

typedef struct boot_irq_s 
{
    uint32_t    dev_type;             /*! source device functional type                     */
    uint8_t     channel;              /*! source device channel index                       */
    uint8_t     is_rx;                /*! source device direction                           */
    uint8_t     valid;                /*! Boolean :input IRQ connected                      */
    uint8_t     reserved;             /*! padding                                           */
} 
boot_irq_t; 

/*********************************************************************************************
 * This structure defines all informations associated to a device in a given cluster.
 * There is one device descriptor per peripheral channel.
 * The base address can be physical or virtual depending on the hardware architecture.
 ********************************************************************************************/

typedef struct boot_device_s
{
    uint64_t    base;                 /*! segment base address                              */
    uint32_t    type;                 /*! peripheral type (func | impl)                     */
    uint32_t    channels;             /*! number of channels                                */
    uint32_t    param0;               /*! semantic depends on peripherat type               */
    uint32_t    param1;               /*! semantic depends on peripherat type               */
    uint32_t    param2;               /*! semantic depends on peripherat type               */
    uint32_t    param3;               /*! semantic depends on peripherat type               */
    uint32_t    irqs;                 /*! number of input IRQs (PIC only)                   */
    boot_irq_t  irq[32];              /*! array of input IRQS (PIC only)                    */
}
boot_device_t;

/*********************************************************************************************
 * This structure defines a reserved zone in the physical address space of a given cluster.
 * A reserved zone is described as a set of contiguous small pages (4 Kbytes) covering the
 * reserved zone, that must be considered already allocated.
 ********************************************************************************************/

typedef struct boot_rsvd_s
{
    ppn_t      first_page;            /*! first physical page index in cluster              */ 
    uint32_t   npages;                /*! number of small pages                             */
}
boot_rsvd_t;

/*********************************************************************************************
 * This structure defines the interface between the boot-loader and the kernel.
 * In each cluster, the boot core build a local boot_info_t structure containing
 * both global (trans-clusters) informations, and cluster specific informations.
 ********************************************************************************************/

typedef struct boot_info_s
{
    uint32_t      signature;                     /*! boot info signature                    */

    // global platform parameters

    uint32_t      paddr_width;                   /*! number of bits in physical address     */
    uint32_t      x_width;                       /*! number of bits to code X coordinate    */
    uint32_t      y_width;                       /*! number of bits to code Y coordinate    */
    uint32_t      x_size;                        /*! number of cluster in a row             */
    uint32_t      y_size;                        /*! number of cluster in a column          */
    uint32_t      io_cxy;                        /*! IO cluster identifier                  */

    uint8_t       cluster_info[CONFIG_MAX_CLUSTERS_X][CONFIG_MAX_CLUSTERS_Y]; 

    // shared resources

    uint32_t      ext_dev_nr;                    /*! number of external peripherals         */
    boot_device_t ext_dev[CONFIG_MAX_EXT_DEV];   /*! array of external peripherals          */

    // private resources (per cluster)

    uint32_t      cxy;                           /*! cluster identifier                     */
    uint32_t      cores_nr;                      /*! number of local cores in               */
    boot_core_t   core[CONFIG_MAX_LOCAL_CORES];  /*! array of core descriptors              */
    uint32_t      rsvd_nr;                       /*! number of reserved zones               */
    boot_rsvd_t   rsvd[CONFIG_PPM_MAX_RSVD];     /*! array of reserved zones                */

    uint32_t      int_dev_nr;                    /*! number of internal peripherals         */
    boot_device_t int_dev[CONFIG_MAX_INT_DEV];   /*! array of internal peripherals          */

    uint32_t      pages_offset;                  /*! first free page index (after kernel)   */
    uint32_t      pages_nr;                      /*! total number of physical pages in RAM  */

    // kernel segments

    intptr_t      kcode_base;                    /*! kernel code base paddr                 */
    intptr_t      kcode_size;                    /*! kernel code size                       */
    intptr_t      kdata_base;                    /*! kernel data base paddr                 */
    intptr_t      kdata_size;                    /*! kernel data size                       */
    intptr_t      kentry_base;                   /*! kernel entry base paddr                */
    intptr_t      kentry_size;                   /*! kernel entry size                      */
}
boot_info_t;

#endif	/* _BOOT_INFO_H_ */
