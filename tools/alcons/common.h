/*
 *******************************************************************************
 * Authors:
 *     Maxime Villard, 2017
 *     Jankovic Marco, 01/07/2014
 *******************************************************************************
 */

#define XOPEN_SOURCE

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <sys/time.h>
#include <pthread.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <time.h>
#include <sys/types.h>

#include <err.h>

#include "config.h"

#define READ_LEN	64
#define FIFO_LIMIT_SIZE	512

#define XON		0x11
#define XOFF	0x13

#define UNUSED(x)	(void)(x) /* for unused variables */

/*
 * Encoding for the multiplexer: we use packets, and their size is at most 2
 * bytes.
 *
 * If [the first byte of the packet has bit7==1]
 * --> bits [0;6] are the TTY number
 * --> the next byte is the character (char, as usual)
 * Else If [the first byte of the packet == 0x11]
 * --> it's XON
 * Else If [the first byte of the packet == 0x13]
 * --> it's XOFF
 * Else
 * --> unknown packet, drop it
 */
#define PKT_TTY	0
#define PKT_DAT	1
#define PACKET_SIZE	2

void *thread_mux_demux(void *a);
void *thread_read_rx(void *a);
void *thread_write_tx(void *a);

/******************************************************************************/

extern int slave_fd[NB_CHANNELS];
extern int master_fd[NB_CHANNELS];
extern int fifo_rx_fd;
extern int fifo_tx_fd;
extern int device_fd;
extern int test_rx_fd;
extern int test_tx_fd;

/******************************************************************************/

