/*
 *******************************************************************************
 * Authors:
 *     Maxime Villard, 2017
 *     Jankovic Marco, 01/07/2014
 *******************************************************************************
 */

/* ---------------------------- Main paramameters --------------------------- */
#define NB_CHANNELS 4

#define DEVICE_PATH "/dev/pts/2"
#define FIFO_RX "/tmp/fifo_rx"
#define FIFO_TX "/tmp/fifo_tx"
/*
 * see man termios.h for BAUD RATE selection
 */
#define BAUDRATE B19200

/* --------------------------------- DEBUG ---------------------------------- */
#define TEST_BENCH	0

#define DEBUG_MUX	0
#define DEBUG_WRITE	0
#define DEBUG_READ	0

/*
 * set the delay to 1 second for debug
 */
#define TIMEOUT_DELAY_MUX_S		0
#define TIMEOUT_DELAY_MUX_US	100

#define TIMEOUT_DELAY_READ_S	0
#define TIMEOUT_DELAY_READ_US	100

#define TIMEOUT_DELAY_WRITE_S	0
#define TIMEOUT_DELAY_WRITE_US	100

