/*
 * soclib_dma.c - soclib Multi Channels DMA driver implementation
 *
 * Author     Alain Greiner (2017)

 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <chdev.h>
#include <dev_dma.h>
#include <thread.h>
#include <soclib_dma.h>

///////////////////////////////////////
void soclib_dma_init( chdev_t * chdev )
{
    // get hardware device cluster and local pointer
    cxy_t      dma_cxy  = GET_CXY( chdev->base );
    uint32_t * dma_ptr  = (uint32_t *)GET_PTR( chdev->base );

    // set driver specific fields in chdev descriptor
    chdev->cmd = &soclib_dma_cmd;
    chdev->isr = &soclib_dma_isr;

    // enable interrupts
	hal_remote_s32( XPTR( dma_cxy , dma_ptr + DMA_IRQ_DISABLED ) , 0 );

} // soclib_dma_init()

//////////////////////////////////////////////////////////////////
void __attribute__ ((noinline)) soclib_dma_cmd( xptr_t thread_xp )
{
    xptr_t     dev_xp;       // extended pointer on DMA devive
    xptr_t     dst_xp;       // extended pointer on destination buffer
    xptr_t     src_xp;       // extended pointer on source buffer
    uint32_t   size;         // buffer size

    // get client thread cluster and local pointer
    cxy_t      thread_cxy = GET_CXY( thread_xp );
    thread_t * thread_ptr = (thread_t *)GET_PTR( thread_xp );

    // get command arguments and extended pointer on DMA device
    dev_xp = (xptr_t)hal_remote_l64( XPTR( thread_cxy , &thread_ptr->dma_cmd.dev_xp ) );
    dst_xp = (xptr_t)hal_remote_l64( XPTR( thread_cxy , &thread_ptr->dma_cmd.dst_xp ) );
    src_xp = (xptr_t)hal_remote_l64( XPTR( thread_cxy , &thread_ptr->dma_cmd.src_xp ) );
    size   =         hal_remote_l32 ( XPTR( thread_cxy , &thread_ptr->dma_cmd.size   ) );

    // get DMA device cluster and local pointer
    cxy_t     dev_cxy = GET_CXY( dev_xp );
    chdev_t * dev_ptr = (chdev_t *)GET_PTR( dev_xp );

    // get extended pointer on SOCLIB-DMA peripheral
    xptr_t     dma_xp = hal_remote_l32( XPTR( dev_cxy , &dev_ptr->base ) );

    // get SOCLIB_DMA device cluster and local pointer
    cxy_t      dma_cxy = GET_CXY( dma_xp );
    uint32_t * dma_ptr = (uint32_t *)GET_PTR( dma_xp );

    // get DMA channel index and channel base address
    uint32_t * base = dma_ptr + DMA_SPAN * dev_ptr->channel;

    // split dst and src buffers addresses in two 32 bits words
    uint32_t   dst_lsb = (uint32_t)(dst_xp);
    uint32_t   dst_msb = (uint32_t)(dst_xp>>32);
    uint32_t   src_lsb = (uint32_t)(src_xp);
    uint32_t   src_msb = (uint32_t)(src_xp>>32);

    // set SOCLIB_DMA registers to start tranfer operation
    hal_remote_s32( XPTR( dma_cxy , base + DMA_SRC     ) , src_lsb ); 
    hal_remote_s32( XPTR( dma_cxy , base + DMA_SRC_EXT ) , src_msb ); 
    hal_remote_s32( XPTR( dma_cxy , base + DMA_DST     ) , dst_lsb );
    hal_remote_s32( XPTR( dma_cxy , base + DMA_DST_EXT ) , dst_msb );
    hal_remote_s32( XPTR( dma_cxy , base + DMA_LEN     ) , size    );

    // Block and deschedule server thread
    thread_block( XPTR( local_cxy , CURRENT_THREAD ) , THREAD_BLOCKED_ISR );
    sched_yield("blocked on ISR");
    
} // soclib_dma_cmd()

/////////////////////////////////////////////////////////////////
void __attribute__ ((noinline)) soclib_dma_isr( chdev_t * chdev )
{
    // get extended pointer on client thread
    xptr_t root      = XPTR( local_cxy , &chdev->wait_root );
    xptr_t client_xp = XLIST_FIRST( root , thread_t , wait_list );

    // get extended pointer on server thread
    xptr_t server_xp = XPTR( local_cxy , &chdev->server );

    // get client thread cluster and local pointer
    cxy_t      client_cxy = GET_CXY( client_xp );
    thread_t * client_ptr = (thread_t *)GET_PTR( client_xp );

    // get SOCLIB_DMA peripheral cluster and local pointer
    cxy_t      dma_cxy  = GET_CXY( chdev->base );
    uint32_t * dma_ptr  = (uint32_t *)GET_PTR( chdev->base );

    // get DMA channel base address
    uint32_t * base = dma_ptr + (DMA_SPAN * chdev->channel);

    // get DMA status register 
	uint32_t status = hal_remote_l32( XPTR( dma_cxy , base + DMA_LEN ) );   

    // acknowledge IRQ
    hal_remote_s32( XPTR( dma_cxy , base + DMA_RESET ) , 0 );

    // set operation status in command
	error_t  error = ( status != DMA_SUCCESS ); 
    hal_remote_s32( XPTR( client_cxy , &client_ptr->dma_cmd.error ) , error );

    // unblock server thread 
    thread_unblock( server_xp , THREAD_BLOCKED_ISR );

    // unblock client thread
    thread_unblock( client_xp , THREAD_BLOCKED_IO );

} // soclib_dma_isr()



