/*
 * soclib_dma.h - soclib Multi Channels DMA driver definition.
 *
 * Author     Alain Greiner (2017)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _SOCLIB_DMA_H_
#define _SOCLIB_DMA_H_

/********************************************************************************************
 * This driver supports the SocLib VciBlockDevice component, that is a simgle channel,
 * block oriented, external storage controler, supporting only one I/O transaction,
 * at a given time.
 *******************************************************************************************/

/********************************************************************************************
 *       SOCLIB_DMA registers offset 
 *******************************************************************************************/

enum SoclibDmaRegisters 
{
    DMA_SRC		     = 0,    /*! source buffer 32 LSB address bits                         */
    DMA_DST		     = 1,    /*! source buffer 32 LSB address bits                         */
    DMA_LEN		     = 2,    /*! number of bytes (on write) / transfer status (on read)    */
    DMA_RESET		 = 3,    /*! desactivate channel (can be usde to acknowledge IRQ)      */
    DMA_IRQ_DISABLED = 4,    /*! no IRQ generated if non zero                              */
    DMA_SRC_EXT      = 5,    /*! source buffer 32 MSB address bits                         */
    DMA_DST_EXT      = 6,    /*! source buffer 32 MSB address bits                         */

    DMA_SPAN		 = 8,	 /*! number of registers per channel                           */
};

/********************************************************************************************
 *       SOCLIB_DMA status values
 *******************************************************************************************/

#define DMA_SUCCESS       0
#define DMA_IDLE          2
#define DMA_ERROR_READ    1
#define DMA_ERROR_WRITE   3
#define DMA_BUSY          4    // or any value larger than 3


/********************************************************************************************
 * This function access the SOCLIB_DMA hardware register to enable interrupts.
 ********************************************************************************************
 * @ chdev     : pointer on DMA chdev descriptor.
 *******************************************************************************************/
extern void soclib_dma_init( chdev_t * chdev );

/********************************************************************************************
 * This function is called by the server thread associated to the DMA device.
 * It access the command embedded in the calling thread descriptor, (format defined in the 
 * dev_dma.h file) and access the SOCLIB_DMA hardware registers to start command execution.
 * Then it blocks on the THREAD_BLOCKED_DEV_ISR and deschedules, because each DMA channel 
 * can only execute one command at a given time. 
 * It is re-activated by the ISR signaling the transfer completion.
 ********************************************************************************************
 * @ thread_xp  : extended pointer on the client thread.
 *******************************************************************************************/
extern void soclib_dma_cmd( xptr_t thread_xp );

/********************************************************************************************
 * This Interrupt Service Routine is executed when the IRQ signaling the completion of 
 * a DMA command is received by a core. It acknowledge the IRQ by accessing the proper
 * SOCLIB_DMA register, unblock the client thread, and unblock the server thread that
 * can starts execution of a new command if the waiting queue is not emppty.
 ********************************************************************************************
 * @ chdev     : pointer on DMA chdev descriptor.
 *******************************************************************************************/
extern void soclib_dma_isr( chdev_t * chdev );


#endif	/* _SOCLIB_DMA_H_ */
