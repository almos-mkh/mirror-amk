/*
 * soclib_mmc.c - soclib L2 cache controller driver implementation.
 *
 * Author     Alain Greiner (2016)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH..
 *
 * ALMOS-MKH. is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH. is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH.; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */


#include <hal_kernel_types.h>
#include <chdev.h>
#include <dev_mmc.h>
#include <soclib_mmc.h>
#include <thread.h>
#include <printk.h>


///////////////////////////////////////
void soclib_mmc_init( chdev_t * chdev )
{
    // get pointer on MMC segment base
    uint32_t * base = (uint32_t *)GET_PTR( chdev->base );

    // set driver specific fields in device descriptor
    chdev->cmd = &soclib_mmc_cmd;
    chdev->isr = &soclib_mmc_isr;

    // enable MMC IRQ
    *(base + (SOCLIB_MMC_ERROR_FUNC << 7) + SOCLIB_MMC_ERROR_IRQ_ENABLE) = 1;
} 


//////////////////////////////////////////////////////////////
void __attribute__ ((noinline)) soclib_mmc_cmd( xptr_t th_xp )
{
    xptr_t     dev_xp;       // extended pointer on MMC device
    uint32_t   type;         // MMC command : type
    void     * buf_ptr;      // MMC command : buffer pointer 
    uint32_t   buf_size;     // MMC command : buffer size
    uint32_t   reg_index;    // MMC command : register index in MMC peripheral
    uint32_t * reg_ptr;      // MMC command : pointer on dst/src buffer in client cluster 

    // get client thread cluster and local pointer
    cxy_t      th_cxy = GET_CXY( th_xp );
    thread_t * th_ptr = GET_PTR( th_xp );

    // get command type and extended pointer on MMC device
    type   =         hal_remote_l32 ( XPTR( th_cxy , &th_ptr->mmc_cmd.type   ) );
    dev_xp = (xptr_t)hal_remote_l64( XPTR( th_cxy , &th_ptr->mmc_cmd.dev_xp ) );

    // get MMC device cluster and local pointer
    cxy_t     dev_cxy = GET_CXY( dev_xp );
    chdev_t * dev_ptr = GET_PTR( dev_xp );

    // get cluster and pointers for SOCLIB_MMC peripheral segment base
    xptr_t     seg_xp = (xptr_t)hal_remote_l64( XPTR( dev_cxy , &dev_ptr->base ) );
    cxy_t      seg_cxy = GET_CXY( seg_xp );
    uint32_t * seg_ptr = GET_PTR( seg_xp );

    if( (type == MMC_CC_INVAL) || (type == MMC_CC_SYNC) )
    {
        // get buffer pointer and size
        buf_ptr  = hal_remote_lpt( XPTR( th_cxy , &th_ptr->mmc_cmd.buf_ptr ) );
        buf_size = hal_remote_l32 ( XPTR( th_cxy , &th_ptr->mmc_cmd.buf_size ) );

        // set command type
        uint32_t   cc_cmd;
        if( type == MMC_CC_INVAL )  cc_cmd = SOCLIB_MMC_CC_INVAL;
        else                        cc_cmd = SOCLIB_MMC_CC_SYNC; 

        // set SOCLIB_MMC registers to start INVAL/SYNC operation
        hal_remote_s32( XPTR( seg_cxy , seg_ptr + SOCLIB_MMC_ADDR_LO    ) , (uint32_t)buf_ptr );
        hal_remote_s32( XPTR( seg_cxy , seg_ptr + SOCLIB_MMC_ADDR_HI    ) , (uint32_t)dev_cxy );
        hal_remote_s32( XPTR( seg_cxy , seg_ptr + SOCLIB_MMC_BUF_LENGTH ) , buf_size ); 
        hal_remote_s32( XPTR( seg_cxy , seg_ptr + SOCLIB_MMC_CMD_TYPE   ) , cc_cmd ); 
    }
    else  // (type == MMC_GET_ERROR) or (type == MMC_GET_ERROR) pr (type == MMC_GET_INSTRU )
    {
        // get src/dst buffer local pointer and register index
        reg_ptr   = (uint32_t *)hal_remote_lpt( XPTR( th_cxy , &th_ptr->mmc_cmd.reg_ptr ) );
        reg_index = hal_remote_l32( XPTR( th_cxy , &th_ptr->mmc_cmd.reg_index ) );

        // move register to/from local buffer 
        if( (type == MMC_GET_ERROR) || (type == MMC_GET_INSTRU) )
        {
            *reg_ptr =  hal_remote_l32( XPTR( seg_cxy , seg_ptr + reg_index ) );
        }
        else  // type == MMC_SET_ERROR
        {
            hal_remote_s32( XPTR( seg_cxy , seg_ptr + reg_index ) , *reg_ptr );
        }
    }
} // end soclib_mmc_cmd()


////////////////////////////////////////////////////////////////
void __attribute__ ((noinline)) soclib_mmc_isr( chdev_t * chdev )
{
    // get pointer on MMC segment base
    uint32_t * base = (uint32_t *)GET_PTR( chdev->base );

    // get faulty ADDRESS and SRCID from MMC registers
    uint32_t paddr_lo = *(base + (SOCLIB_MMC_ERROR_FUNC << 7) + SOCLIB_MMC_ERROR_ADDR_LO);
    uint32_t paddr_hi = *(base + (SOCLIB_MMC_ERROR_FUNC << 7) + SOCLIB_MMC_ERROR_ADDR_HI);
    uint32_t srcid    = *(base + (SOCLIB_MMC_ERROR_FUNC << 7) + SOCLIB_MMC_ERROR_SRCID);

    paddr_t paddr = (((paddr_t)paddr_hi)<<32) + ((paddr_t)paddr_lo);
    
    // reset MMC IRQ
    *(base + (SOCLIB_MMC_ERROR_FUNC << 7) + SOCLIB_MMC_ERROR_IRQ_RESET) = 0;
    
    // print an error message on kernel terminal TODO : should be improved
    printk("\n[ERROR] reported from MMC in cluster %x : faulty address = %l / srcid = %x\n",
           paddr , srcid );

} // end soclib_mmc_isr()



