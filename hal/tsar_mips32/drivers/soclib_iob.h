/*
 * soclib_iob.h - soclib IOB driver API definition.
 *
 * Alain Greiner    (2016)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _SOCLIB_IOB_H_
#define _SOCLIB_IOB_H_

#include <hal_kernel_types.h>
#include <chdev.h>

/******************************************************************************************
 *    SOCLIB_IOB  Registers Mnemonics
 *****************************************************************************************/

enum IOB_registers 
{
    IOB_IOMMU_PTPR       = 0,      // R/W : Page Table Pointer Register
    IOB_IOMMU_ACTIVE     = 1,      // R/W : IOMMU activated if not 0
    IOB_IOMMU_BVAR       = 2,      // R   : Bad Virtual Address (unmapped) 
    IOB_IOMMU_ERROR      = 3,      // R   : Error Type 
    IOB_IOMMU_SRCID      = 4,      // R   : Faulty Peripheral Index (SRCID)
    IOB_INVAL_PTE        = 5,      // W   : Invalidate a PTE (virtual address)
    IOB_WTI_ENABLE       = 6,      // R/W : Enable WTIs (both IOMMU and peripherals)
    IOB_WTI_ADDR_LO      = 7,      // W/R : 32 LSB bits for IOMMU WTI
    IOB_WTI_ADDR_HI      = 8,      // W/R : 32 MSB bits for IOMMU WTI
};

enum mmu_error_type_e 
{
    MMU_NONE                      = 0x0000, // None
    MMU_WRITE_ACCES_VIOLATION     = 0x0008, // Write access to a non writable page 
    MMU_WRITE_PT1_ILLEGAL_ACCESS  = 0x0040, // Write Bus Error accessing Table 1       
    MMU_READ_PT1_UNMAPPED 	      = 0x1001, // Read  Page fault on Page Table 1  	
    MMU_READ_PT2_UNMAPPED 	      = 0x1002, // Read  Page fault on Page Table 2  
    MMU_READ_PT1_ILLEGAL_ACCESS   = 0x1040, // Read  Bus Error in Table1 access      
    MMU_READ_PT2_ILLEGAL_ACCESS   = 0x1080, // Read  Bus Error in Table2 access 	
    MMU_READ_DATA_ILLEGAL_ACCESS  = 0x1100, // Read  Bus Error in cache access 
};


/******************************************************************************************
 * This function initializes the SOCLIB IOB component:
 ******************************************************************************************
 * @ chdev   : local pointer on IOB chdev descriptor.
 *****************************************************************************************/
void soclib_iob_init( chdev_t * chdev );

/******************************************************************************************
 * This function set a new value in the IOB_IOMMU_ACTIVE register.
 ******************************************************************************************
 * @ iob_xp     : extended pointer on the remote XCU device descriptor.
 * @ value      : value to be written in register.
 *****************************************************************************************/
void soclib_iob_set_active( xptr_t   iox_xp,
                            uint32_t value );

/******************************************************************************************
 * This function set a new value in the IOB_IOMMU_PTPR register.
 ******************************************************************************************
 * @ iob_xp     : extended pointer on the remote XCU device descriptor.
 * @ value      : value to be written in register IOMMU.
 *****************************************************************************************/
void soclib_iob_set_ptpr( xptr_t   iob_xp,
                          uint32_t value );

/******************************************************************************************
 * This function invaldates a TLB entry identified by its vpn.
 ******************************************************************************************
 * @ iox_xp     : extended pointer on the remote XCU device descriptor.
 * @ vpn        : virtual page number.
 *****************************************************************************************/
void soclib_iob_inval_page( xptr_t  iox_xp,
                            vpn_t   vpn );

/******************************************************************************************
 * This function returns the content of IOB_IOMMU_BVAR register.
 ******************************************************************************************
 * @ iox_xp     : extended pointer on the remote XCU device descriptor.
 * @ return registered bad virtual address (after error reported by IOMMU).
 *****************************************************************************************/
uint32_t soclib_iob_get_bvar( xptr_t  iox_xp );

/******************************************************************************************
 * This function returns the content of IOB_IOMMU_SRCID register.
 ******************************************************************************************
 * @ iox_xp     : extended pointer on the remote XCU device descriptor.
 * @ return registered faulty peripheral index (after error reported by IOMMU).
 *****************************************************************************************/
uint32_t soclib_iob_get_srcid( xptr_t  iox_xp );

/******************************************************************************************
 * This function returns the content of IOB_IOMMU_ERROR register.
 ******************************************************************************************
 * @ iox_xp     : extended pointer on the remote XCU device descriptor.
 * @ return registered error type (after error reported by IOMMU).
 *****************************************************************************************/
uint32_t soclib_iob_get_error( xptr_t  iox_xp );



#endif

// Local Variables:
// tab-width: 4
// c-basic-offset: 4
// c-file-offsets:((innamespace . 0)(inline-open . 0))
// indent-tabs-mode: nil
// End:
// vim: filetype=c:expandtab:shiftwidth=4:tabstop=4:softtabstop=4

