/*
 * soclib_tty.c - soclib_tty driver definition (used in the TSAR_IOB architecture).
 *
 * Author  Alain Greiner (2016,2017,2018)
 *
 * Copyright (c)  UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-kernel; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <dev_txt.h>
#include <chdev.h>


/****************************************************************************************
 * This driver supports the vci_tty_tsar component.
 *
 * 1) This hardware component handles several hardware channels, supporting directly
 *    several kernel TXT channels.
 * 2) For received characters, the hardware support one RX_IRQ, and one bit 
 *    in the STATUS register to signal that the READ register is full. 
 * 3) For transmitted characters, the hardware supports one RX_IRQ, and one bit
 *    in STATUS register to signal that the WRITE register is empty.
 * 
 * It implements the generic TXT device API:
 * - transfer one single character from TTY to command "buffer" if to_mem is non-zero.
 * - transfer "count" characters from command "buffer" to TTY if to_mem is zero.
 *
 * It handles asynchronous control characters (^C / ^Z), that are translated to signals
 * transmited to the TXT owner process (foreground process).
 *
 * This driver implements one TX_FIFO for the transmited characters, writen by the "cmd"
 * function, and read by the "isr" function).
 * This driver implements one RX_FIFO for the received characters, writen by the "isr"
 * function, and read by the "cmd" function).
 ***************************************************************************************/

/****************************************************************************************
 *     SOCLIB_TTY registers offsets
 ***************************************************************************************/

#define TTY_WRITE              0
#define TTY_STATUS             1
#define TTY_READ               2
#define TTY_RX_IRQ_ENABLE      3
#define TTY_TX_IRQ_ENABLE      4

#define TTY_SPAN               8       // number of registers per channel

/****************************************************************************************
 * masks for TTY_STATUS_REG
 ***************************************************************************************/

#define TTY_STATUS_RX_FULL     1       // TTY_READ_REG full if 1
#define TTY_STATUS_TX_FULL     2       // TTY_WRITE_REG full if 1

/****************************************************************************************
 * This structure is used for both the RX_FIFO and the TX_FIFO.
 ***************************************************************************************/

#define TTY_FIFO_DEPTH  128

typedef struct tty_fifo_s 
{
    char          data[TTY_FIFO_DEPTH];   // one char per slot
    unsigned int  ptr;                    // next free slot index
    unsigned int  ptw;                    // next full slot index
    unsigned int  sts;                    // number of full slots
} 
tty_fifo_t;

/****************************************************************************************
 * This function masks both the TTY_RX and TTY_TX IRQs.
 * These IRQs are unmasked by the soclib_tty_cmd() function.
 ****************************************************************************************
 * @ chdev     : pointer on the TXT chdev descriptor.
 ***************************************************************************************/
void soclib_tty_init( chdev_t * chdev );

/****************************************************************************************
 * This function implements both the TXT_READ & TXT_WRITE commands registered in the
 * client thread descriptor (in the txt_cmd field), even if ALMOS-MKH defines two
 * different chdevs (and consequently two diffeerent server threads) for the RX and TX
 * directions. The client thread is identified by the <thread_xp> argument.
 * These functions are supposed to be called by the server thread associated at a
 * given TXT channel for a given direction (TX or RX).
 * Depending on the command type, it access the TX_FIFO or RX_FIFO, and blocks the TXT
 * device server thread on the THREAD_BLOCKED_DEV_ISR, if the RX_FIFO is empty (for a
 * READ), or if the TX_FIFO is full for a WRITE). 
 * The actual transfer between the FIFOs and the TTY device registers is done by the ISR.
 * ****************************************************************************************
 * @ thread_xp : extended pointer on client thread descriptor.
 ***************************************************************************************/
void soclib_tty_cmd( xptr_t thread_xp );

/****************************************************************************************
 * This function implements the TXT_SYNC_WRITE command registered in the txt_aux_t
 * structure, using a busy waiting policy, without using the TTY IRQ.
 * It is used by the kernel do display debug messages on TXT0 terminal, without
 * interference with another TXT access to another terminal done by the same thread.
 ****************************************************************************************
 * @ thread_xp : pointer on the txt_aux_t structure containing the arguments.
 ***************************************************************************************/
void soclib_tty_aux( void * args );

/****************************************************************************************
 * This ISR is executed to handle both the TTY_TX_IRQ and the TTY_RX_IRQ, even if
 * there is two different channel devices for TX and RX transfers.
 *
 * As the <chdev> argument is a local pointer, this ISR is always executed by a core
 * that is in the same cluster as the core running the DEV server thread.
 *
 * - The RX_IRQ is activated as soon as the TTY_STATUS_RX_FULL bit is 1 in the
 *   TTY_STATUS register, when the TTY_RX_IRQ_ENABLE is non zero, indicating that
 *   the TTY_READ buffer is full and can be read.
 * - The TX_IRQ is activated as soon as the TTY_STATUS_TX_FULL bit is 0 in the
 *   TTY_STATUS register, when the TTY_TX_IRQ_ENABLE is non zero, indicating that
 *   the TTY_WRITE buffer is empty, and can be written.
 *
 * The RX_IRQ is always enabled to catch the control characters (^Z / ^C),
 * but the TX_IRQ is dynamically enabled by the TXT_WRITE command, and disabled when
 * the command is completed.
 *
 * For normal characters The ISR uses two private TTY_RX and TTY_TX software FIFOs
 * (two FIFOs per channel) to communicates with the DEV server thread executing the 
 * soclib_tty_cmd() function.

 * For an RX transfer, this ISR executes a while loop moving bytes until the source
 * TTY_READ buffer is empty:
 *   1) The ISR read one byte from the TTY_READ register and acknowledge the RX_IRQ.
 *   2) if the byte is a ^Z character, it uses a multicast RPC to block all treads of
 *      the TXT owner process, and transfer TXT ownership to another process of the group.
 *   3) if the byte is a ^C character, it removes the process from the TXT group, and send
 *      a multicast RPC to delete all threads of the TXT owner process.
 *   4) if the byte is a normal character and the destination TTY_RX_FIFO is not full,
 *      it writes the byte to this FIFO, unblock the TXT_RX server thread, and send an IPI
 *      to this server thread (only if it is running on another core than the ISR). 
 *   5) It discards the byte if the TTY_RX_FIFO is full, with a warning message on TXT0.
 *
 * For a TX transfer, this ISR executes a while loop moving bytes until the source
 * TTY_TX_FIFO is empty:
 *   1) if the destination TTY_WRITE register is not full, it moves the byte from the
 *      TTY_TX_FIFO to the TTY_WRITE register.
 *   2) if the TTY_WRITE register is full, it polls (busy waiting) the TTY_STATUS register,
 *      until the TTY_WRITE register is empty.
 *   3) when the source TTY_TX_FIFO is empty, this ISR disable the TTY_TX_IRQ, unblock
 *      the TXT_TX server thread, and send an IPI to this server thread (only if it is
 *      running on another core than the ISR). 
 ****************************************************************************************
 * @ chdev     : local pointer on TXT chdev descriptor.
 ***************************************************************************************/
void soclib_tty_isr( chdev_t * chdev );

