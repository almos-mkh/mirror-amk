/*
 * soclib_tty.c - soclib tty driver implementation.
 *
 * Author  Alain Greiner (2016,2017,2018)
 *
 * Copyright (c)  UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH. is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */


#include <hal_kernel_types.h>
#include <dev_txt.h>
#include <chdev.h>
#include <soclib_tty.h>
#include <thread.h>
#include <printk.h>
#include <hal_special.h>

#if (DEBUG_SYS_READ & 1)
extern uint32_t  enter_tty_cmd_read;
extern uint32_t  exit_tty_cmd_read;

extern uint32_t  enter_tty_isr_read;
extern uint32_t  exit_tty_isr_read;
#endif

#if (DEBUG_SYS_WRITE & 1)
extern uint32_t  enter_tty_cmd_write;
extern uint32_t  exit_tty_cmd_write;

extern uint32_t  enter_tty_isr_write;
extern uint32_t  exit_tty_isr_write;
#endif

////////////////////////////////////////////////////////////////////////////////////
// These global variables implement the TTY_RX  FIFOs (one per channel)
////////////////////////////////////////////////////////////////////////////////////
// Implementation note:
// We allocate - in each cluster - two arrays of FIFOs containing as many entries
// as the total number of TXT channels, but all entries are not used in all
// clusters: for a given cluster K, a given entry corresponding to a given channel
// and a given direction is only used if the associated chdev is in cluster K.
// With this policy, the driver can ignore the actual placement of chdevs.
////////////////////////////////////////////////////////////////////////////////////

__attribute__((section(".kdata")))
tty_fifo_t  tty_rx_fifo[CONFIG_MAX_TXT_CHANNELS];

__attribute__((section(".kdata")))
tty_fifo_t  tty_tx_fifo[CONFIG_MAX_TXT_CHANNELS];

///////////////////////////////////////
void soclib_tty_init( chdev_t * chdev )
{
    xptr_t reg_xp;

    // initialise function pointers in chdev
    chdev->cmd = &soclib_tty_cmd;
    chdev->isr = &soclib_tty_isr;
    chdev->aux = &soclib_tty_aux;

    // get TTY channel and extended pointer on TTY peripheral base address
    xptr_t   tty_xp  = chdev->base;
    uint32_t channel = chdev->channel;
    bool_t   is_rx   = chdev->is_rx;

    // get SOCLIB_TTY device cluster and local pointer
    cxy_t      tty_cxy = GET_CXY( tty_xp );
    uint32_t * tty_ptr = GET_PTR( tty_xp );

    // set TTY_RX_IRQ_ENABLE 
    reg_xp = XPTR( tty_cxy , tty_ptr + (channel * TTY_SPAN) + TTY_RX_IRQ_ENABLE );
    hal_remote_s32( reg_xp , 1 );

    // reset TTY_TX_IRQ_ENABLE
    reg_xp = XPTR( tty_cxy , tty_ptr + (channel * TTY_SPAN) + TTY_TX_IRQ_ENABLE );
    hal_remote_s32( reg_xp , 0 );

    // reset relevant FIFO
    if( is_rx )
    {
        tty_rx_fifo[channel].sts = 0;
        tty_rx_fifo[channel].ptr = 0;
        tty_rx_fifo[channel].ptw = 0;
    }
    else
    {
        tty_tx_fifo[channel].sts = 0;
        tty_tx_fifo[channel].ptr = 0;
        tty_tx_fifo[channel].ptw = 0;
    }
}  // end soclib_tty_init()

//////////////////////////////////////////////////////////////
void __attribute__ ((noinline)) soclib_tty_cmd( xptr_t th_xp )
{
    tty_fifo_t * fifo;     // TTY_RX or TTY_TX FIFO
    char         byte;     // byte value
    uint32_t     done;     // number of bytes moved

    // get client thread cluster and local pointer
    cxy_t      th_cxy = GET_CXY( th_xp );
    thread_t * th_ptr = GET_PTR( th_xp );

    // get command arguments
    uint32_t type     = hal_remote_l32 ( XPTR( th_cxy , &th_ptr->txt_cmd.type   ) );
    xptr_t   buf_xp   = hal_remote_l64( XPTR( th_cxy , &th_ptr->txt_cmd.buf_xp ) );
    uint32_t count    = hal_remote_l32 ( XPTR( th_cxy , &th_ptr->txt_cmd.count  ) );
    xptr_t   error_xp = XPTR( th_cxy , &th_ptr->txt_cmd.error );

#if (DEBUG_SYS_READ & 1)
if( type == TXT_READ) enter_tty_cmd_read = (uint32_t)hal_get_cycles();
#endif

#if (DEBUG_SYS_WRITE & 1)
if( type == TXT_WRITE) enter_tty_cmd_write = (uint32_t)hal_get_cycles();
#endif

#if( DEBUG_HAL_TXT_TX || DEBUG_HAL_TXT_RX )
thread_t * this = CURRENT_THREAD;
#endif

    // get TXT device cluster and pointers
    xptr_t     dev_xp = (xptr_t)hal_remote_l64( XPTR( th_cxy , &th_ptr->txt_cmd.dev_xp ) );
    cxy_t      dev_cxy = GET_CXY( dev_xp );
    chdev_t  * dev_ptr = GET_PTR( dev_xp );

    // get cluster and pointers for SOCLIB_TTY peripheral base segment
    xptr_t     tty_xp = (xptr_t)hal_remote_l64( XPTR( dev_cxy , &dev_ptr->base ) );
    cxy_t      tty_cxy = GET_CXY( tty_xp );
    uint32_t * tty_ptr = GET_PTR( tty_xp );

    // get TTY channel index and channel base address
    uint32_t   channel = hal_remote_l32( XPTR( dev_cxy , &dev_ptr->channel ) );
    uint32_t * base    = tty_ptr + TTY_SPAN * channel;

    ///////////////////////
    if( type == TXT_WRITE )         // write bytes to TTY_TX FIFO
    {
        fifo = &tty_tx_fifo[channel];

        done = 0;

        while( done < count )
        {
            if( fifo->sts < TTY_FIFO_DEPTH )   // put one byte to FIFO if TX_FIFO not full
            {
                // get one byte from command buffer
                byte = hal_remote_lb( buf_xp + done );

#if DEBUG_HAL_TXT_TX
uint32_t   tx_cycle = (uint32_t)hal_get_cycles();
if( DEBUG_HAL_TXT_TX < tx_cycle )
printk("\n[%s] thread[%x,%x] put character <%c> to TXT%d_TX fifo / cycle %d\n",
__FUNCTION__, this->process->pid, this->trdid, byte, channel, tx_cycle );
#endif
                // write byte to FIFO
                fifo->data[fifo->ptw] = byte;

                // prevent race
                hal_fence();

                // update FIFO state
                fifo->ptw = (fifo->ptw + 1) % TTY_FIFO_DEPTH;
                hal_atomic_add( &fifo->sts , 1 );

                // udate number of bytes moved
                done++;

                // enable TX_IRQ 
                hal_remote_s32( XPTR( tty_cxy , base + TTY_TX_IRQ_ENABLE ) , 1 );
            }
            else                                // block & deschedule if TX_FIFO full
            {
                // block on ISR
                thread_block( XPTR( local_cxy , CURRENT_THREAD ) , THREAD_BLOCKED_ISR );

                // deschedule
                sched_yield( "TTY_TX_FIFO full" ); 
            }
        }

        // set error status in command and return
        hal_remote_s32( error_xp , 0 );
    }
    ///////////////////////////
    else if( type == TXT_READ )       // read several bytes from TTY_RX FIFO   
    {
        fifo = &tty_rx_fifo[channel];

        done = 0;

        while( done < count )
        {
            if( fifo->sts > 0 )               // get byte from FIFO if not empty
            {
                // get one byte from FIFO
                char byte = fifo->data[fifo->ptr];

#if DEBUG_HAL_TXT_RX
uint32_t rx_cycle = (uint32_t)hal_get_cycles();
if( DEBUG_HAL_TXT_RX < rx_cycle )
printk("\n[%s] thread[%x,%x] get character <%c> from TXT%d_RX fifo / cycle %d\n",
__FUNCTION__, this->process->pid, this->trdid, byte, channel, rx_cycle );
#endif
                // update FIFO state
                fifo->ptr = (fifo->ptr + 1) % TTY_FIFO_DEPTH;
                hal_atomic_add( &fifo->sts , -1 );

                // set byte to command buffer
                hal_remote_sb( buf_xp + done , byte );

                // udate number of bytes
                done++;
            }
            else                             //  deschedule if FIFO empty
            {
                // block on ISR
                thread_block( XPTR( local_cxy , CURRENT_THREAD ) , THREAD_BLOCKED_ISR );
    
                // deschedule
                sched_yield( "TTY_RX_FIFO empty" );
            }
        }  // end while

        // set error status in command
        hal_remote_s32( error_xp , 0 );
    }
    else
    {
        assert( false , "illegal TXT command\n" );
    }

#if (DEBUG_SYS_READ & 1)
if( type == TXT_READ ) exit_tty_cmd_read = (uint32_t)hal_get_cycles();
#endif

#if (DEBUG_SYS_WRITE & 1)
if( type == TXT_WRITE ) exit_tty_cmd_write = (uint32_t)hal_get_cycles();
#endif

}  // end soclib_tty_cmd()

/////////////////////////////////////////////////////////////////
void __attribute__ ((noinline)) soclib_tty_isr( chdev_t * chdev )
{
    thread_t   * server;            // pointer on TXT chdev server thread
    lid_t        server_lid;        // local index of core running the server thread
    uint32_t     channel;           // TXT chdev channel
    bool_t       is_rx;             // TXT chdev direction
    char         byte;              // byte value
    xptr_t       owner_xp;          // extended pointer on TXT owner process
    cxy_t        owner_cxy;         // TXT owner process cluster
    process_t  * owner_ptr;         // local pointer on TXT owner process
    pid_t        owner_pid;         // TXT owner process identifier
    tty_fifo_t * fifo;              // pointer on TTY_TX or TTY_RX FIFO
    cxy_t        tty_cxy;           // soclib_tty cluster
    uint32_t   * tty_ptr;           // soclib_tty segment base address
    uint32_t   * base;              // soclib_tty channel base address
    xptr_t       status_xp;         // extended pointer on TTY_STATUS register
    xptr_t       write_xp;          // extended pointer on TTY_WRITE register
    xptr_t       read_xp;           // extended pointer on TTY_READ register
    xptr_t       parent_xp;         // extended pointer on parent process
    cxy_t        parent_cxy;        // parent process cluster
    process_t  * parent_ptr;        // local pointer on parent process
    thread_t   * parent_main_ptr;   // extended pointer on parent process main thread
    xptr_t       parent_main_xp;    // local pointer on parent process main thread

    // get TXT chdev channel, direction, server thread, and server core
    channel    = chdev->channel;
    is_rx      = chdev->is_rx;
    server     = chdev->server;
    server_lid = server->core->lid;

#if (DEBUG_SYS_READ & 1)
if( is_rx ) enter_tty_isr_read = (uint32_t)hal_get_cycles();
#endif

#if (DEBUG_SYS_WRITE & 1)
if( is_rx == 0 ) enter_tty_isr_write = (uint32_t)hal_get_cycles();
#endif

#if DEBUG_HAL_TXT_RX
uint32_t rx_cycle = (uint32_t)hal_get_cycles();
#endif

#if DEBUG_HAL_TXT_TX
uint32_t tx_cycle = (uint32_t)hal_get_cycles();
#endif

    // get SOCLIB_TTY peripheral cluster and local pointer
    tty_cxy = GET_CXY( chdev->base );
    tty_ptr = GET_PTR( chdev->base );

    // get channel base address
    base    = tty_ptr + TTY_SPAN * channel;

    // get extended pointer on TTY registers
    status_xp = XPTR( tty_cxy , base + TTY_STATUS );
    write_xp  = XPTR( tty_cxy , base + TTY_WRITE );
    read_xp   = XPTR( tty_cxy , base + TTY_READ );

    /////////////////////////// handle RX //////////////////////
    if( is_rx )
    {
        fifo = &tty_rx_fifo[channel];

        // try to move bytes until TTY_READ register empty
        while( hal_remote_l32( status_xp ) & TTY_STATUS_RX_FULL )   
        {
            // get one byte from TTY_READ register & acknowledge RX_IRQ
            byte = (char)hal_remote_lb( read_xp );

            // filter special character ^Z  => block TXT owner process
            if( byte == 0x1A ) 
            {

#if DEBUG_HAL_TXT_RX
if( DEBUG_HAL_TXT_RX < rx_cycle )
printk("\n[%s] read ^Z character from TXT%d\n", __FUNCTION__, channel );
#endif
                // get pointers on TXT owner process in owner cluster
                owner_xp  = process_txt_get_owner( channel );
               
                // check process exist
                assert( (owner_xp != XPTR_NULL) ,
                "TXT owner process not found\n" );

                // get relevant infos on TXT owner process
                owner_cxy = GET_CXY( owner_xp );
                owner_ptr = GET_PTR( owner_xp );
                owner_pid = hal_remote_l32( XPTR( owner_cxy , &owner_ptr->pid ) );

                // block TXT owner process only if it is not the INIT process 
                if( owner_pid != 1 )
                {
                    // get parent process descriptor pointers
                    parent_xp  = hal_remote_l64( XPTR( owner_cxy , &owner_ptr->parent_xp ) );
                    parent_cxy = GET_CXY( parent_xp );
                    parent_ptr = GET_PTR( parent_xp );

                    // get pointers on the parent process main thread
                    parent_main_ptr = hal_remote_lpt(XPTR(parent_cxy,&parent_ptr->th_tbl[0])); 
                    parent_main_xp  = XPTR( parent_cxy , parent_main_ptr );

                    // transfer TXT ownership 
                    process_txt_transfer_ownership( owner_xp );

                    // block all threads in all clusters, but the main thread
                    process_sigaction( owner_pid , BLOCK_ALL_THREADS );

                    // block the main thread
                    xptr_t main_xp = XPTR( owner_cxy , &owner_ptr->th_tbl[0] );
                    thread_block( main_xp , THREAD_BLOCKED_GLOBAL );

                    // atomically update owner process termination state
                    hal_remote_atomic_or( XPTR( owner_cxy , &owner_ptr->term_state ) ,
                                          PROCESS_TERM_STOP );

                    // unblock the parent process main thread 
                    thread_unblock( parent_main_xp , THREAD_BLOCKED_WAIT );

                    return;
                }
            }

            // filter special character ^C  => kill TXT owner process
            if( byte == 0x03 )
            {

#if DEBUG_HAL_TXT_RX
if( DEBUG_HAL_TXT_RX < rx_cycle )
printk("\n[%s] read ^C character from TXT%d\n", __FUNCTION__, channel );
#endif
                // get pointer on TXT owner process in owner cluster
                owner_xp  = process_txt_get_owner( channel );

                // check process exist
                assert( (owner_xp != XPTR_NULL) ,
                "TXT owner process not found\n" );

                // get relevant infos on TXT owner process
                owner_cxy = GET_CXY( owner_xp );
                owner_ptr = GET_PTR( owner_xp );
                owner_pid = hal_remote_l32( XPTR( owner_cxy , &owner_ptr->pid ) );

                // kill TXT owner process only if it is not the INIT process
                if( owner_pid != 1 )
                {
                    // get parent process descriptor pointers
                    parent_xp  = hal_remote_l64( XPTR( owner_cxy , &owner_ptr->parent_xp ) );
                    parent_cxy = GET_CXY( parent_xp );
                    parent_ptr = GET_PTR( parent_xp );

                    // get pointers on the parent process main thread
                    parent_main_ptr = hal_remote_lpt(XPTR(parent_cxy,&parent_ptr->th_tbl[0])); 
                    parent_main_xp  = XPTR( parent_cxy , parent_main_ptr );

                    // remove process from TXT list
                    process_txt_detach( owner_xp );

                    // mark for delete all thread in all clusters, but the main
                    process_sigaction( owner_pid , DELETE_ALL_THREADS );
                
                    // block main thread
                    xptr_t main_xp = XPTR( owner_cxy , &owner_ptr->th_tbl[0] );
                    thread_block( main_xp , THREAD_BLOCKED_GLOBAL );

                    // atomically update owner process termination state
                    hal_remote_atomic_or( XPTR( owner_cxy , &owner_ptr->term_state ) ,
                                          PROCESS_TERM_KILL );

                    // unblock the parent process main thread 
                    thread_unblock( parent_main_xp , THREAD_BLOCKED_WAIT );

                    return;
                }
            }

            // write byte in TTY_RX FIFO if not full / discard byte if full
            if ( fifo->sts < TTY_FIFO_DEPTH )
            {

#if DEBUG_HAL_TXT_RX
if( DEBUG_HAL_TXT_RX < rx_cycle )
printk("\n[%s] put character <%c> to TXT%d_RX fifo\n",
__FUNCTION__, byte, channel );
#endif
                // store byte into FIFO 
                fifo->data[fifo->ptw] = (char)byte; 

                // avoid race 
                hal_fence();

                // update RX_FIFO state
                fifo->ptw = (fifo->ptw + 1) % TTY_FIFO_DEPTH;
                hal_atomic_add( &fifo->sts , 1 );

                // unblock TXT_RX server thread 
                thread_unblock( XPTR( local_cxy , server ) , THREAD_BLOCKED_ISR );

                // send IPI to core running server thread if required
                if( server_lid != CURRENT_THREAD->core->lid )
                {
                    dev_pic_send_ipi( local_cxy , server_lid );
                }
            }
            else
            {
                printk("\n[WARNING] %s : TTY_RX_FIFO[%d] full => discard character <%x>\n",
                __FUNCTION__, channel, (uint32_t)byte );
            }
        }  // end while TTY_READ register full

    }  // end RX

    ///////////////////////  handle TX  /////////////////////////////
    else
    {
        fifo = &tty_tx_fifo[channel];

        // try to move bytes until TX_FIFO empty
        while( fifo->sts > 0 )
        {
            // write one byte to TTY_WRITE register if empty / exit loop if full
            if( (hal_remote_l32( status_xp ) & TTY_STATUS_TX_FULL) == 0 ) 
            {
                // get one byte from TX_FIFO
                byte = fifo->data[fifo->ptr];

#if DEBUG_HAL_TXT_TX
if( DEBUG_HAL_TXT_TX < tx_cycle )
printk("\n[%s] get character <%c> from TXT%d_TX fifo\n",
__FUNCTION__, byte, channel );
#endif
                // update TX_FIFO state
                fifo->ptr = (fifo->ptr + 1) % TTY_FIFO_DEPTH;
                hal_atomic_add( &fifo->sts , -1 );

                // write byte to TTY_WRITE register & acknowledge TX_IRQ
                hal_remote_sb( write_xp , byte );
            }
        }

        // disable TX_IRQ 
        hal_remote_s32( XPTR( tty_cxy , base + TTY_TX_IRQ_ENABLE ) , 0 );

        // unblock TXT_TX server thread
        thread_unblock( XPTR( local_cxy , server ) , THREAD_BLOCKED_ISR );

        // send IPI to core running server thread if required
        if( server_lid != CURRENT_THREAD->core->lid )
        {
            dev_pic_send_ipi( local_cxy , server_lid );
        }

    }  // end TX

    hal_fence();

#if (DEBUG_SYS_READ & 1)
if( is_rx ) exit_tty_isr_read = (uint32_t)hal_get_cycles();
#endif

#if (DEBUG_SYS_WRITE & 1)
if( is_rx == 0 ) exit_tty_isr_write = (uint32_t)hal_get_cycles();
#endif

}  // end soclib_tty_isr()

/////////////////////////////////////////////////////////////
void __attribute__ ((noinline)) soclib_tty_aux( void * args )
{
    uint32_t   status;
    bool_t     empty;
    uint32_t   i;

    xptr_t       dev_xp = ((txt_sync_args_t *)args)->dev_xp;
    const char * buffer = ((txt_sync_args_t *)args)->buffer;
    uint32_t     count  = ((txt_sync_args_t *)args)->count;
    
    // get chdev cluster and local pointer
    cxy_t     dev_cxy = GET_CXY( dev_xp );
    chdev_t * dev_ptr = GET_PTR( dev_xp );

    // get extended pointer on TTY channel base address
    xptr_t tty_xp = (xptr_t)hal_remote_l64( XPTR( dev_cxy , &dev_ptr->base ) );

    // get TTY channel segment cluster and local pointer
    cxy_t      tty_cxy = GET_CXY( tty_xp );
    uint32_t * tty_ptr = GET_PTR( tty_xp );

    // get extended pointers on TTY_WRITE & TTY_STATUS registers
    xptr_t write_xp  = XPTR( tty_cxy , tty_ptr + TTY_WRITE );
    xptr_t status_xp = XPTR( tty_cxy , tty_ptr + TTY_STATUS );

    // loop on characters 
    for( i = 0 ; i < count ; i++ )
    {
        // busy waiting policy on TTY_STATUS register
        do
        {
            // get TTY_STATUS
            status = hal_remote_l32( status_xp );
            empty  = ( (status & TTY_STATUS_TX_FULL) == 0 );

            // transfer one byte if TX buffer empty
            if ( empty )  hal_remote_sb( write_xp , buffer[i] );
        }
        while ( empty == false );
    }
}  // end soclib_tty_aux()



