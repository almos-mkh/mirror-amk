/*
 * hal_irqmask.c - implementation of Generic IRQ Masking API for TSAR-MIPS32
 * 
 * Author  Ghassan Almaless (2008,2009,2010,2011,2012)
 *         Alain Greiner    (2016)
 *
 * Copyright (c) UPMC Sorbonne Universites
 * 
 * This file is part of ALMOS-MKH..
 *
 * ALMOS-MKH. is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH. is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH.; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_shared_types.h>

//////////////////////////////////////////
inline void hal_disable_irq( reg_t * old )
{
	register uint32_t sr;
	
	__asm__ volatile 
		(".set noat                          \n"
         "mfc0   $1,     $12                 \n"
		 "or     %0,     $0,     $1          \n"
		 "srl    $1,     $1,     1           \n"
		 "sll    $1,     $1,     1           \n"
		 "mtc0   $1,     $12                 \n"
         ".set at                            \n"
		 : "=&r" (sr) );

	if( old ) *old = sr;
}

/////////////////////////////////////////
inline void hal_enable_irq( reg_t * old )
{
	register uint32_t sr;
 
	__asm__ volatile 
		(".set noat                          \n"
		 "mfc0   $1,     $12                 \n"
		 "or     %0,     $0,     $1          \n"
		 "ori    $1,     $1,     0xFF01      \n"
		 "mtc0   $1,     $12                 \n"
         ".set at                            \n"
		 : "=&r" (sr) );
  
	if( old ) *old = sr;
}

////////////////////////////////////////
inline void hal_restore_irq( reg_t old )
{
	__asm__ volatile 
		( "mtc0    %0,    $12" : : "r" (old) );
}


