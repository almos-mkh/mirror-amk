/*
 * hal_shared_types.h - Data types shared by kernel & libraries for TSAR-MIPS32.
 *
 * Author  Alain Greiner (2016)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef HAL_SHARED_TYPES_H_
#define HAL_SHARED_TYPES_H_

/*******************i***************************************************************
 * This file defines - for the TSAR_MIPS32 architecture - types that can be used
 * by both the kernel and the user applications.
 *
 * - the <reg_t> type, is used by the hal_user_syscall() function, called by 
 *   several user-level libraries to pass syscall arguments to the  kernel, 
 *   and used by the do_syscall() kernel function, to analyse arguments.
 *   It is also used by the hal_*_irq() kernel functions to save/restore 
 *   the SR register value.
 **********************************************************************************/

typedef unsigned long int     reg_t;    // core register

#endif
