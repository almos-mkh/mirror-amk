/*
 * hal_special.c - implementation of Generic Special Register Access API for TSAR-MIPS32
 * 
 * Author    Alain Greiner (2016,2017)
 *
 * Copyright (c) UPMC Sorbonne Universites
 * 
 * This file is part of ALMOS-MKH..
 *
 * ALMOS-MKH. is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH. is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH.; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */


#include <hal_kernel_types.h>
#include <hal_special.h>
#include <hal_exception.h>
#include <core.h>
#include <thread.h>

/****  Forward declarations ****/

struct thread_s;


//////////////////////////////////////////////////////////////////////////////////
//   Extern global variables
//////////////////////////////////////////////////////////////////////////////////

extern cxy_t local_cxy;
extern void  hal_kentry_enter( void );

/////////////////////////////////////////////////////////////////////////////////
// For the TSAR architecture, this function register the physical address of
// the first level page table (PT1) in the PTPR register.
// It activates the intructions MMU, and de-activates the data MMU.
/////////////////////////////////////////////////////////////////////////////////
void hal_mmu_init( gpt_t * gpt )
{

    // set PT1 base address in mmu_ptpr register
    uint32_t ptpr = (((uint32_t)gpt->ptr) >> 13) | (local_cxy << 19);
    asm volatile ( "mtc2   %0,   $0         \n" : : "r" (ptpr) );

    // set ITLB | ICACHE | DCACHE bits in mmu_mode register 
    asm volatile ( "ori    $26,  $0,  0xB   \n" 
                   "mtc2   $26,  $1         \n" );
}

////////////////////////////////////////////////////////////////////////////////
// For the TSAR architecture, this function registers the address of the
// hal_kentry_enter() function in the MIPS32 cp0_ebase register.
////////////////////////////////////////////////////////////////////////////////
void hal_set_kentry( void )
{
    uint32_t kentry = (uint32_t)(&hal_kentry_enter);

    asm volatile("mtc0   %0,  $15,  1" : : "r" (kentry) );
}

////////////////////////////////
inline gid_t hal_get_gid( void )
{
	uint32_t proc_id;

	asm volatile ("mfc0    %0,  $15, 1" : "=&r" (proc_id));

	return (proc_id & 0x3FF);  // 4/4/2 format for TSAR
}

///////////////////////////////////
inline reg_t hal_time_stamp( void )
{
    reg_t count;

	asm volatile ("mfc0   %0,  $9" : "=&r" (count));

    return count;
}

///////////////////////////////
inline reg_t hal_get_sr( void )
{
    reg_t sr;

	asm volatile ("mfc0    %0,    $12" : "=&r" (sr));

	return sr;
}

///////////////////////////////
uint64_t hal_get_cycles( void )
{
	uint64_t cycles;                // absolute time to be returned
    uint32_t last_count;            // last registered cycles count
    uint32_t current_count;         // current cycles count
	uint32_t elapsed;

    core_t * core = CURRENT_THREAD->core;

    // get last registered time stamp
	last_count = core->time_stamp;

    // get current time stamp from hardware register
	current_count = hal_time_stamp();

	// compute number of elapsed cycles, taking into account 32 bits register wrap
	if(current_count < last_count) elapsed = (0xFFFFFFFF - last_count) + current_count;
	else                           elapsed = current_count - last_count;

    // compute absolute time
	cycles = core->cycles + elapsed;

	// update core time
	core->time_stamp = current_count;
	core->cycles     = cycles;

	hal_fence();

	return cycles;
}

///////////////////////////////////////////////////////
inline struct thread_s * hal_get_current_thread( void )
{
	void * thread_ptr;
 
	asm volatile ("mfc0    %0,  $4,  2" : "=&r" (thread_ptr));

	return thread_ptr;
}

///////////////////////////////////////////////////////
void hal_set_current_thread( struct thread_s * thread )
{ 
	asm volatile ("mtc0    %0,  $4,  2" : : "r" (thread));
}

///////////////////////////
void hal_fpu_enable( void )
{
    // set CU1 bit (FPU enable) in c0_sr
	asm volatile 
	( ".set noat                         \n"
      "lui    $27,    0x2000             \n"
      "mfc0   $1,     $12                \n"
      "or     $27,    $1,    $27         \n"
      "mtc0   $27,    $12                \n"
      ".set at                           \n" );

    // set CU1 bit in calling thread UZONE
    uint32_t * uzone = CURRENT_THREAD->uzone_current;
    uzone[34] |= 0x20000000;
}

////////////////////////////
void hal_fpu_disable( void )
{
    // reset CU1 bit (FPU enable) in c0_sr
	asm volatile 
	( ".set noat                         \n"
      "lui    $27,    0xDFFF             \n"
	  "ori    $27,    $27,   0xFFFF      \n"
      "mfc0   $1,     $12                \n"
      "and    $27,    $1,    $27         \n"
      "mtc0   $27,    $12                \n"
	  ".set at                           \n");

    // reset CU1 bit in calling thread UZONE
    uint32_t * uzone = CURRENT_THREAD->uzone_current;
    uzone[34] &= 0xDFFFFFFF;
}

///////////////////////////
uint32_t hal_get_sp( void )
{
	register uint32_t sp;
  
	asm volatile ("or    %0,   $0,   $29" : "=&r" (sp));
  
	return sp;
}

/////////////////////////////////////
uint32_t hal_set_sp( void * new_val )
{
	register uint32_t sp;
  
	asm volatile
	( "or    %0,   $0,      $29   \n"
	  "or    $29,  $0,      %1    \n"
	  : "=&r" (sp) : "r" (new_val)  );
  
	return sp;
}

///////////////////////////
uint32_t hal_get_ra( void )
{
	register uint32_t ra;
  
	asm volatile ("or    %0,   $0,   $31" : "=&r" (ra));
  
	return ra;
}

//////////////////////////////////
uint32_t hal_get_bad_vaddr( void )
{
	register uint32_t bad_va;

	asm volatile
    ( "mfc0    %0,  $8  \n"
      : "=&r" (bad_va) );

	return bad_va;
}

////////////////////////////////////////////
uint32_t hal_uncached_read( uint32_t * ptr )
{
	register uint32_t val;

	asm volatile
	( "ll    %0,     (%1)  \n"
      : "=&r"(val) : "r" (ptr) );

	return val;
}

//////////////////////////////////////////
void hal_invalid_dcache_line( void * ptr )
{
	asm volatile
	( "cache    %0,     (%1)              \n"
	  "sync                               \n"
	  : : "i" (0x11) , "r" (ptr) );
}

/////////////////////////////
inline void hal_fence( void )
{
	asm volatile ("sync");
}

/////////////////////////////
inline void hal_rdbar( void )
{
	asm volatile( "" ::: "memory" );
}

///////////////////////////
void hal_core_sleep( void )
{
	while( 1 ) asm volatile ("wait");
}

//////////////////////////////////////
void hal_fixed_delay( uint32_t delay )
{ 
    asm volatile
    ( ".set noreorder        \n"
      "or    $27,  %0,  $0   \n"
      "1:                    \n"
      "addi  $27, $27,  -1   \n"
      "nop                   \n"
      "bne   $27,  $0,  1b   \n"
      "nop                   \n"
      ".set reorder          \n"
      : : "r" (delay>>2) : "$27" );
}

//////////////////////////////////////////////////
void hal_get_mmu_excp( intptr_t * mmu_ins_excp_code,
                       intptr_t * mmu_ins_bad_vaddr,
                       intptr_t * mmu_dat_excp_code,
                       intptr_t * mmu_dat_bad_vaddr )
{
    asm volatile
    ( "mfc2   %0,    $11        \n"
      "mfc2   %1,    $13        \n"
      "mfc2   %2,    $12        \n"
      "mfc2   %3,    $14        \n"
      : "=&r"(*mmu_ins_excp_code),
        "=&r"(*mmu_ins_bad_vaddr),
        "=&r"(*mmu_dat_excp_code),
        "=&r"(*mmu_dat_bad_vaddr) );
}

