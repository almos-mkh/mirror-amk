/*
 * hal_interrupt.c - implementation of interrupt handler for TSAR-MIPS32
 * 
 * Author   Alain Greiner (2016,2017)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_special.h>
#include <hal_interrupt.h>
#include <kernel_config.h>
#include <thread.h>
#include <printk.h>
#include <soclib_pic.h>

///////////////////////
void hal_do_interrupt( void )
{
    // ALMOS-MKH does not define a generic interrupt handler.
    // we call the specific TSAR IRQ handler to select the relevant ISR
    soclib_pic_irq_handler();
}
