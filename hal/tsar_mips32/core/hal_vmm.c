/*
 * hal_vmm.c - Virtual Memory Manager Initialisation for TSAR
 *
 * Authors  Alain Greiner (2016,2017,2018,2019)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <kernel_config.h>
#include <hal_kernel_types.h>
#include <hal_vmm.h>
#include <hal_gpt.h>
#include <process.h>
#include <thread.h>
#include <vseg.h>
#include <xlist.h>
#include <vmm.h>
#include <remote_rwlock.h>

//////////////////////////////////////////////////////////////////////////////////////////
// This file contains the TSAR specific code used to initialize the kernel process VMM,
// or to update an user process VMM with informations related to the kernel vsegs.
// As the TSAR architure does not use the DATA MMU, but use only the DATA extension
// address register to access local and remote kernel data, the kernel VSL contains only
// one "kcode" segment, and the kernel GPT contains only one big page in PT1[0] slot.
//////////////////////////////////////////////////////////////////////////////////////////

// extern global variables
extern process_t            process_zero;
extern chdev_directory_t    chdev_dir;

//////////////////////////////////////////////////////////////////////////////////////////
// This function is called by the process_zero_init() function during kernel_init.
// It initializes the VMM of the kernel proces_zero (containing all kernel threads)
// in the local cluster: it registers one "kcode" vseg in kernel VSL, and registers 
// one big page in slot[0] of kernel GPT.
//////////////////////////////////////////////////////////////////////////////////////////
error_t  hal_vmm_kernel_init( boot_info_t * info )
{
    error_t   error;

    // get pointer on kernel GPT
    gpt_t * gpt = &process_zero.vmm.gpt;

    // get cluster identifier
    cxy_t cxy = local_cxy;

    // allocate memory for kernel GPT
    error = hal_gpt_create( gpt );

    if( error )
    {
        printk("\n[PANIC] in %s : cannot allocate kernel GPT in cluster %x\n",
        __FUNCTION__ , cxy );
        hal_core_sleep();
    }

#if DEBUG_HAL_VMM
thread_t * this = CURRENT_THREAD;
printk("\n[%s] thread[%x,%x] enter in cluster %x / gpt %x\n", 
__FUNCTION__, this->process->pid, this->trdid, local_cxy, gpt );
#endif

    // compute attr and ppn for one PTE1
    uint32_t attr = GPT_MAPPED | GPT_READABLE | GPT_CACHABLE | GPT_EXECUTABLE | GPT_GLOBAL;
    uint32_t ppn  = cxy << 20;    

    // register PTE1  in slot[0] of kernel GPT
    error = hal_gpt_set_pte( XPTR( cxy , gpt ) , 0 , attr , ppn );

    if( error )
    {
        printk("\n[PANIC] in %s : cannot initialize kernel GPT in cluster %x\n",
        __FUNCTION__ , cxy );
        hal_core_sleep();
    }

#if DEBUG_HAL_VMM
printk("\n[%s] thread[%x,%x] created PT1[0] : ppn %x / attr %x\n", 
__FUNCTION__, this->process->pid, this->trdid, ppn, attr );
#endif

    // create kcode vseg and register it in kernel VSL
    vseg_t * vseg = vmm_create_vseg( &process_zero,
                                     VSEG_TYPE_KCODE,
                                     info->kcode_base,
                                     info->kcode_size,
                                     0, 0,                  // file ofset and file size (unused)
                                     XPTR_NULL,             // no mapper
                                     local_cxy );
    if( vseg == NULL )
    {
        printk("\n[PANIC] in %s : cannot register vseg to VSL in cluster %x\n",
        __FUNCTION__ , cxy );
        hal_core_sleep();
    }

#if DEBUG_HAL_VMM
printk("\n[%s] thread[%x,%x] registered kcode vseg[%x,%x]\n",
__FUNCTION__, this->process->pid, this->trdid, info->kcode_base, info->kcode_size );
hal_vmm_display( &process_zero , true );
#endif

    return 0;

}  // end hal_kernel_vmm_init()

//////////////////////////////////////////////////////////////////////////////////////////
// This function is called by the vmm_init() function to update the VMM of an user
// process identified by the <process> argument.
// It registers in the user VSL the "kcode" vseg, registered in the local kernel VSL,
// and register in the user GPT the big page[0] mapped in the local kernel GPT. 
//////////////////////////////////////////////////////////////////////////////////////////
error_t hal_vmm_kernel_update( process_t * process )
{
    error_t error;
    uint32_t attr;
    uint32_t ppn;

#if DEBUG_HAL_VMM
thread_t * this = CURRENT_THREAD;
printk("\n[%s] thread[%x,%x] enter in cluster %x \n", 
__FUNCTION__, this->process->pid, this->trdid, local_cxy );
hal_vmm_display( process , true );
hal_vmm_display( &process_zero , true );
#endif

    // get cluster identifier
    cxy_t cxy = local_cxy;

    // get extended pointer on kernel GPT
    xptr_t k_gpt_xp = XPTR( cxy , &process_zero.vmm.gpt );

    // get ppn and attributes from slot[0] of kernel GPT
    hal_gpt_get_pte( k_gpt_xp , 0 , &attr , &ppn );

#if DEBUG_HAL_VMM
printk("\n[%s] thread[%x,%x] get PT1[0] ( ppn %x / attr %x ) from kernel  GPT\n", 
__FUNCTION__, this->process->pid, this->trdid, ppn, attr );
#endif

    // get extended pointer on user GPT
    xptr_t u_gpt_xp = XPTR( cxy , &process->vmm.gpt );

    // update user GPT : set PTE1 in slot[0]
    error = hal_gpt_set_pte( u_gpt_xp , 0 , attr , ppn );

    if( error )
    {
        printk("\n[ERROR] in %s : cannot update user GPT in cluster %x\n",
        __FUNCTION__ , cxy );
        return -1;
    }

#if DEBUG_HAL_VMM
printk("\n[%s] thread[%x,%x] registered PT1[0] ( ppn %x / attr %x ) to user GPT\n", 
__FUNCTION__, this->process->pid, this->trdid, ppn, attr );
#endif

    // get pointer on the unique vseg registered in kernel VSL
    xptr_t   root_xp = XPTR( cxy , &process_zero.vmm.vsegs_root );
    xptr_t   vseg_xp = XLIST_FIRST( root_xp , vseg_t , xlist );
    vseg_t * vseg    = GET_PTR( vseg_xp );

// check vsegs_nr 
assert( (process_zero.vmm.vsegs_nr == 1 ) ,
"bad vsegs number in kernel VSL = %d\n", process_zero.vmm.vsegs_nr );

    // update user VSL : register one new vseg for kcode
    vseg_t * new = vmm_create_vseg( process,
                                    vseg->type,
                                    vseg->min,
                                    vseg->max - vseg->min,
                                    0, 0,                  // file ofset and file size (unused)
                                    XPTR_NULL,             // no mapper
                                    local_cxy );
    if( new == NULL )
    {
        printk("\n[ERROR] in %s : cannot update user VSL in cluster %x\n",
        __FUNCTION__ , cxy );
        return -1;
    }

#if DEBUG_HAL_VMM
printk("\n[%s] thread[%x,%x] created vseg %s ( base %x / size %x ) to user VSL\n", 
__FUNCTION__, this->process->pid, this->trdid,
vseg_type_str(vseg->type) , vseg->min, (vseg->max - vseg->min) );
hal_vmm_display( process , true );
#endif

    return 0;

}  // end hal_vmm_kernel_update()

//////////////////////////////////////////
void hal_vmm_display( process_t * process,
                      bool_t      mapping )
{
    vmm_t * vmm = &process->vmm;
    gpt_t * gpt = &vmm->gpt;

    // get pointers on TXT0 chdev
    xptr_t    txt0_xp  = chdev_dir.txt_tx[0];
    cxy_t     txt0_cxy = GET_CXY( txt0_xp );
    chdev_t * txt0_ptr = GET_PTR( txt0_xp );

    // get extended pointer on remote TXT0 lock
    xptr_t  lock_xp = XPTR( txt0_cxy , &txt0_ptr->wait_lock );

    // get locks protecting the VSL and the GPT
    remote_rwlock_rd_acquire( XPTR( local_cxy , &vmm->vsegs_lock ) );
    remote_rwlock_rd_acquire( XPTR( local_cxy , &vmm->gpt_lock ) );

    // get TXT0 lock
    remote_busylock_acquire( lock_xp );

    nolock_printk("\n***** VSL and GPT for process %x in cluster %x\n",
    process->pid , local_cxy );

    // scan the list of vsegs
    xptr_t         root_xp = XPTR( local_cxy , &vmm->vsegs_root );
    xptr_t         iter_xp;
    xptr_t         vseg_xp;
    vseg_t       * vseg;
    XLIST_FOREACH( root_xp , iter_xp )
    {
        vseg_xp = XLIST_ELEMENT( iter_xp , vseg_t , xlist );
        vseg    = GET_PTR( vseg_xp );

        nolock_printk(" - %s : base = %X / size = %X / npages = %d\n",
        vseg_type_str( vseg->type ) , vseg->min , vseg->max - vseg->min , vseg->vpn_size );

        if( mapping )
        {
            vpn_t    vpn     = vseg->vpn_base;
            vpn_t    vpn_max = vpn + vseg->vpn_size;
            ppn_t    ppn;
            uint32_t attr;

            while( vpn < vpn_max )
            {
                hal_gpt_get_pte( XPTR( local_cxy , gpt ) , vpn , &attr , &ppn );

                if( attr & GPT_MAPPED )
                {
                    if( attr & GPT_SMALL )
                    {
                        nolock_printk("    . SMALL : vpn = %X / attr = %X / ppn = %X\n",
                        vpn , attr , ppn );
                        vpn++;
                    }
                    else
                    {
                        nolock_printk("    . BIG   : vpn = %X / attr = %X / ppn = %X\n",
                        vpn , attr , ppn );
                        vpn += 512;
                    }
                }
                else
                {
                    vpn++;
                }
            }
        }
    }

    // release TXT0 lock
    remote_busylock_release( lock_xp );

    // release the VSK and GPT locks
    remote_rwlock_rd_release( XPTR( local_cxy , &vmm->vsegs_lock ) );
    remote_rwlock_rd_release( XPTR( local_cxy , &vmm->gpt_lock ) );

}  // hal_vmm_display()

