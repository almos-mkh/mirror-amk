/*
 * hal_syscall.c - Implementation of syscall handler for TSAR-MIPS32.
 * 
 * Author    Alain Greiner (2016,2017)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_syscall.h>
#include <do_syscall.h>
#include <thread.h>
#include <printk.h>
#include <hal_kentry.h>

/////////////////////
void hal_do_syscall( void )
{
    thread_t    * this;

    uint32_t    * enter_uzone;
    uint32_t    * exit_uzone;

	uint32_t      arg0;
	uint32_t      arg1;
	uint32_t      arg2;
	uint32_t      arg3;
	uint32_t      service_num;
	uint32_t      retval;
  
    // get pointer on enter_thread uzone
    this        = CURRENT_THREAD;
    enter_uzone = (uint32_t *)this->uzone_current;

    // get syscall arguments from uzone
	service_num = enter_uzone[UZ_V0];
	arg0        = enter_uzone[UZ_A0];
	arg1        = enter_uzone[UZ_A1];
	arg2        = enter_uzone[UZ_A2];
	arg3        = enter_uzone[UZ_A3];
 
    // call architecture independant syscall handler
	retval = do_syscall( this,
                         arg0,
                         arg1,
                         arg2,
                         arg3,
                         service_num );

    // get pointer on exit_thread uzone, because
    // exit_thread can be different from enter_thread
    this       = CURRENT_THREAD;
    exit_uzone = (uint32_t *)this->uzone_current;

    // set return value to uzone 
	exit_uzone[UZ_V0] = retval;

    // update EPC in uzone
	exit_uzone[UZ_EPC] += 4;

    hal_fence();
}
