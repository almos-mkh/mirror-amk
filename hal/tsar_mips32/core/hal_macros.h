/*
 * hal_macros.h - User-side, architecture specific macros.
 *
 * Author      Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _HAL_MACROS_H_
#define _HAL_MACROS_H_

//////////////////////////////////////////////////////////////////////////////////////////
//     User-side, hardware dependant, macros definition.
//
//   Any architecture specific implementation must implement these macros.
//////////////////////////////////////////////////////////////////////////////////////////

#define HAL_CXY_FROM_XY( x , y )  ((x<<4) + y)

#define HAL_X_FROM_CXY( cxy )     ((cxy>>4) & 0xF)

#define HAL_Y_FROM_CXY( cxy )     (cxy & 0xF)


#endif
