/*
 * hal_kentry.h - MIPS32 registers mnemonics
 * 
 * Copyright (c) 2008,2009,2010,2011,2012 Ghassan Almaless
 * Copyright (c) 2011,2012 UPMC Sorbonne Universites
 * 
 * This file is part of ALMOS-kernel.
 *
 * ALMOS-kernel is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-kernel is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-kernel; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _HAL_KENTRY_H_
#define _HAL_KENTRY_H_

//////////////////////////////////////////////////////////////////////////////////////////
// This file defines the MIPS32 specific mnemonics to access the "uzone", that is
// a fixed size array of 32 bits integers, used by the kentry function to save/restore 
// the MIPS32 CPU registers, at each exception / interruption / syscall.
// It also defines several initial values for the SR register.
//
// This file is included in the hal_kentry.S, hal_syscall.c, hal_exception.c,
// and hal_context.c files.
//////////////////////////////////////////////////////////////////////////////////////////


/****************************************************************************************
 * This structure defines the cpu_uzone dynamically allocated in the kernel stack
 * by the hal_kentry assembly code for the TSAR_MIPS32 architecture.
 * WARNING : It is replicated in hal_kentry.S file.
 ***************************************************************************************/

#define      UZ_MODE         0    /* c2_mode */             
#define      UZ_AT           1    /* at_01   */
#define      UZ_V0           2    /* v0_02   */
#define      UZ_V1           3    /* v1_03   */
#define      UZ_A0           4    /* a0_04   */
#define      UZ_A1           5    /* a1_05   */
#define      UZ_A2           6    /* a2_06   */
#define      UZ_A3           7    /* a3_07   */
#define      UZ_T0           8    /* t0_08   */
#define      UZ_T1           9    /* t1_09   */
#define      UZ_T2           10   /* t2_10   */
#define      UZ_T3           11   /* t3_11   */
#define      UZ_T4           12   /* t4_12   */
#define      UZ_T5           13   /* t5_13   */
#define      UZ_T6           14   /* t6_14   */
#define      UZ_T7           15   /* t7_15   */
#define      UZ_S0           16   /* s0_16   */
#define      UZ_S1           17   /* s1_17   */
#define      UZ_S2           18   /* s2_18   */
#define      UZ_S3           19   /* s3_19   */
#define      UZ_S4           20   /* s4_20   */
#define      UZ_S5           21   /* s5_21   */
#define      UZ_S6           22   /* s6_22   */
#define      UZ_S7           23   /* s7_23   */
#define      UZ_T8           24   /* t8_24   */
#define      UZ_T9           25   /* t9_25   */

#define      UZ_LO           26
#define      UZ_HI           27

#define      UZ_GP           28   /* gp_28   */
#define      UZ_SP           29   /* sp_29   */
#define      UZ_S8           30   /* s8_30   */
#define      UZ_RA           31   /* ra_31   */

#define      UZ_PTPR         32   /* c2_ptpr */
#define      UZ_EPC          33   /* c0_epc  */
#define      UZ_SR           34   /* c0_sr   */
#define      UZ_TH           35   /* c0_th   */
#define      UZ_CR           36   /* c0_cr   */

#define      UZ_REGS         37

/*************************************************************************************
 * The hal_kentry_enter() function is the unique kernel entry point in case of 
 * exception, interrupt, or syscall for the TSAR_MIPS32 architecture.  
 * It can be executed by a core in user mode (in case of exception or syscall),
 * or by a core already in kernel mode (in case of interrupt).
 *
 * In both cases it allocates an "uzone" space in the kernel stack to save the
 * CPU registers values, desactivates the MMU, and calls the relevant handler 
 * (exception/interrupt/syscall) 
 *
 * After handler execution, it restores the CPU context from the uzone and jumps 
 * to address contained in EPC calling hal_kentry_eret()
 ************************************************************************************/
void hal_kentry_enter( void );

/*************************************************************************************
 * The hal_kentry_eret() function contains only the assembly "eret" instruction,
 * that reset the EXL bit in the c0_sr register, and jump to the address 
 * contained in the c0_epc register. 
 * ************************************************************************************/
void hal_kentry_eret( void );

#endif	/* _HAL_KENTRY_H_ */
