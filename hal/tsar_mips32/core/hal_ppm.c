/*
 * hal_ppm.c - Generic Physical Page Manager API implementation for TSAR
 *
 * Authors  Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <kernel_config.h>
#include <hal_kernel_types.h>
#include <hal_ppm.h>
#include <hal_special.h>
#include <printk.h>
#include <busylock.h>
#include <process.h>
#include <ppm.h>
#include <thread.h>
#include <cluster.h>
#include <page.h>

//////////////////////////////////////////////////////////////////////////////////////////
// This file contains the TSAR specific code for the physical memory
// allocators initialisation.
//
// For The TSAR architecture, the kernel pointers are identity mapped:
// - the 32 bits PTR value is identical to the 32 bits LPA value,
// - the 64 bits XPTR value is identical to the 64 bits PADDR value.
// This hal_ppm_init() function initializes the pages_tbl[] array used by the generic
// kmem memory allocator in the local cluster. This array starts in first free page
// after kernel code, as defined by the 'pages_offset' field in boot_info.
//////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////
error_t  hal_ppm_init( boot_info_t * info )
{
	uint32_t i;

	// get relevant info from boot_info structure
	uint32_t pages_nr         = info->pages_nr;
	uint32_t pages_tbl_offset = info->pages_offset;

	// get pointer on local Physical Page Manager
	ppm_t * ppm = &LOCAL_CLUSTER->ppm;

	// initialize lock protecting the free_pages[] lists
	busylock_init( &ppm->free_lock , LOCK_PPM_FREE );

	// initialize lock protecting the dirty_pages list
	remote_queuelock_init( XPTR( local_cxy , &ppm->dirty_lock ) , LOCK_PPM_DIRTY );

	// initialize all free_pages[] lists as empty
	for( i = 0 ; i < CONFIG_PPM_MAX_ORDER ; i++ )
	{
		list_root_init( &ppm->free_pages_root[i] );
		ppm->free_pages_nr[i] = 0;
	}

	// initialize dirty_list as empty
	list_root_init( &ppm->dirty_root );

	// compute size of pages_tbl[] array rounded to an integer number of pages
	uint32_t bytes = ARROUND_UP( pages_nr * sizeof(page_t), CONFIG_PPM_PAGE_SIZE );

	// compute number of pages required to store page descriptor array
	uint32_t pages_tbl_nr = bytes >> CONFIG_PPM_PAGE_SHIFT;

	// compute total number of reserved pages (kernel code & pages_tbl[])
	uint32_t reserved_pages = pages_tbl_offset + pages_tbl_nr;

	// initialize pages_nr, pages_tbl, and vaddr_base pointers
	// (TSAR uses identity mapping for kernel pointers)
	// x86 architectures should use vaddr_base = 0xFFFF8000000000 + (cxy << 36)
	ppm->pages_nr   = pages_nr;
	ppm->vaddr_base = NULL;
	ppm->pages_tbl  = (page_t*)( ppm->vaddr_base +
	                             (pages_tbl_offset << CONFIG_PPM_PAGE_SHIFT) );

	// initialize all page descriptors in pages_tbl[]
	for( i = 0 ; i < pages_nr ; i++ )
	{
		page_init( &ppm->pages_tbl[i] );

		// TODO optimisation for this enormous loop on small pages:
		// make only a partial init with a memset, and complete the
		// initialisation when page is allocated [AG]
	}

	// - set PG_RESERVED flag for reserved pages (kernel code & pages_tbl[])
	// - release all other pages to populate the free lists
	for( i = 0 ; i < reserved_pages ; i++)
	{
		page_set_flag( &ppm->pages_tbl[i] , PG_RESERVED );
	}
	for( i = reserved_pages ; i < pages_nr ; i++ )
	{
		ppm_free_pages_nolock( &ppm->pages_tbl[i] );

		// TODO optimisation : decompose this enormous set of small pages
		// to several sets of big pages with various order values [AG]
	}

	// check consistency
	return ppm_assert_order( ppm );

}  // end hal_ppm_init()


