/*
 * hal_remote.h - Generic Remote Access API definition.
 *
 * Authors   Mohamed Karaoui  (2015)
 *           Alain Greiner    (2016)
 *
 * Copyright (c)  UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef  _HAL_REMOTE_H_
#define  _HAL_REMOTE_H_

#include <hal_kernel_types.h>

//////////////////////////////////////////////////////////////////////////////////////////
//          Generic Remote Access API (implementation in hal_remote.c)
//
// Kernel accesses to local memory bank and peripherals can use normal C pointers.
// kernel accesses to remote memory banks or peripherals must use the following
// dedicated functions, because implementation depends on architectures.
/////////////////////////////////////////////////////////////////////////////////////////

/*****************************************************************************************
 * This function writes a single byte in a remote cluster.
 *****************************************************************************************
 * @ xp      : extended pointer to remote cluster
 * @ data    : value to be written
 ****************************************************************************************/
void hal_remote_sb( xptr_t xp,
                    uint8_t  data );

/*****************************************************************************************
 * This function writes an aligned 32 bits word in a remote cluster.
 *****************************************************************************************
 * @ xp      : extended pointer to remote cluster
 * @ data    : value to be written
 ****************************************************************************************/
void hal_remote_s32( xptr_t   xp,
                    uint32_t data );

/*****************************************************************************************
 * This function writes an aligned 64 bits word in a remote cluster.
 *****************************************************************************************
 * @ xp      : extended pointer to remote cluster
 * @ data    : value to be written
 ****************************************************************************************/
void hal_remote_s64( xptr_t   xp,
                     uint64_t data );

/*****************************************************************************************
 * This function writes a pointer (32 or 64 bits) in a remote cluster.
 *****************************************************************************************
 * @ xp      : extended pointer to remote cluster
 * @ pt      : value to be written
 ****************************************************************************************/
void hal_remote_spt( xptr_t   xp,
                     void   * pt );

/*****************************************************************************************
 * This function reads a single byte in a remote cluster.
 *****************************************************************************************
 * @ xp      : extended pointer to remote data
 * @ return read value
 ****************************************************************************************/
uint8_t hal_remote_lb( xptr_t  xp );

/*****************************************************************************************
 * This function reads an aligned 32 bits word in a remote cluster.
 *****************************************************************************************
 * @ xp      : extended pointer to remote data
 * @ return read value
 ****************************************************************************************/
uint32_t hal_remote_l32( xptr_t  xp );

/*****************************************************************************************
 * This function reads an aligned 64 bits word in a remote cluster.
 *****************************************************************************************
 * @ xp      : extended pointer to remote data
 * @ return read value
 ****************************************************************************************/
uint64_t hal_remote_l64( xptr_t  xp );

/*****************************************************************************************
 * This function reads a pointer (can be 32 or 64 bits) in a remote cluster.
 *****************************************************************************************
 * @ xp      : extended pointer to remote data
 * @ return read value
 ****************************************************************************************/
void * hal_remote_lpt( xptr_t  xp );

/*****************************************************************************************
 * This non blocking function makes an atomic Compare-And-Swap in a remote cluster.
 *****************************************************************************************
 * @ xp      : extended pointer to remote data
 * @ old     : expected value
 * @ new     : new value to be written
 * @ return true if success / return false if failure
 ****************************************************************************************/
bool_t hal_remote_atomic_cas( xptr_t   xp,
                              uint32_t old,
                              uint32_t new );

/*****************************************************************************************
 * This blocking function adds atomically an increment to the current value of
 * a 32 bits integer in a remote cluster. Returns only after success.
 *****************************************************************************************
 * @ xp      : extended pointer to remote data
 * @ incr    : increment value.
 * @ return old value (before increment) of the remote integer
 ****************************************************************************************/
uint32_t hal_remote_atomic_add( xptr_t    xp,
                                uint32_t  incr );

/*****************************************************************************************
 * This blocking function makes an atomic "and" between a local mask value and
 * a 32 bits integer in a remote cluster. Returns only after success.
 *****************************************************************************************
 * @ xp      : extended pointer to remote data
 * @ mask    : local mask value.
 * @ return old value (before mask) of the remote integer
 ****************************************************************************************/
uint32_t hal_remote_atomic_and( xptr_t    xp,
                                uint32_t  mask );

/*****************************************************************************************
 * This blocking function makes an atomic "or" between a local mask value and
 * a 32 bits integer in a remote cluster. Returns only after success.
 *****************************************************************************************
 * @ xp      : extended pointer to remote data
 * @ mask    : local mask value.
 * @ return old value (before mask) of the remote integer
 ****************************************************************************************/
uint32_t hal_remote_atomic_or( xptr_t    xp,
                               uint32_t  mask );

/*****************************************************************************************
 * This non blocking function tries to make an atomic increment to the current
 * value of a 32 bits integer in a remote cluster.
 *****************************************************************************************
 * @ xp      : extended pointer to remote data
 * @ incr    : increment value.
 * @ old     : local buffer address for the read value (before increment)
 * @ return 0 if atomic / return non-zero if failure
 ****************************************************************************************/
error_t hal_remote_atomic_try_add( xptr_t     xp,
                                   uint32_t   incr,
                                   uint32_t * old );

/*****************************************************************************************
 * This function makes a memcpy from a source remote buffer in kernel space to another
 * destination remote buffer in kernel space.
 *****************************************************************************************
 * @ dst_xp  : extended pointer to destination buffer
 * @ src_xp  : extended pointer to source buffer
 * @ size    : number of bytes to move
 ****************************************************************************************/
void hal_remote_memcpy( xptr_t   dst_xp,
                        xptr_t   src_xp,
                        uint32_t size );

/*****************************************************************************************
 * This function makes a copy from a source character string to another destination
 * character string, including the NUL terminating character.
 *****************************************************************************************
 * @ dst     : extended pointer to destination char array.
 * @ src     : extended pointer to source char array.
 ****************************************************************************************/
void hal_remote_strcpy( xptr_t dst,
                        xptr_t src );

/*****************************************************************************************
 * This function makes a memset to a remote buffer in kernel space.
 *****************************************************************************************
 * @ buf_xp  : extended pointer to destination buffer.
 * @ byte    : Rvalue to be set in all buffer slots.
 * @ size    : number of bytes to move.
 ****************************************************************************************/
void hal_remote_memset( xptr_t   buf_xp,
                        uint8_t  byte,
                        uint32_t size );

#endif	/* _HAL_REMOTE_H_ */
