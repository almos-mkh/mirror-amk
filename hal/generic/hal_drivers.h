/*
 * hal_drivers.h - Function definitions for driver initializers
 *
 * Copyright (c) 2017 Maxime Villard
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _HAL_DRIVERS_H_
#define _HAL_DRIVERS_H_

#include <chdev.h>

void hal_drivers_txt_init(chdev_t *txt, uint32_t impl);

void hal_drivers_pic_init(chdev_t *pic, uint32_t impl);

void hal_drivers_iob_init(chdev_t *iob, uint32_t impl);

void hal_drivers_ioc_init(chdev_t *ioc, uint32_t impl);

void hal_drivers_mmc_init(chdev_t *mmc, uint32_t impl);

void hal_drivers_nic_init(chdev_t *nic, uint32_t impl);

void hal_drivers_dma_init(chdev_t *dma, uint32_t impl);

#endif	/* HAL_DRIVERS_H_ */

