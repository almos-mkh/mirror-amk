/*
 * hal_vmm.h - Kernel Virtual Memory Manager initialisation
 *
 * Authors  Alain Greiner (2016,2017,2018,2019)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _HAL_VMM_H_
#define _HAL_VMM_H_

#include <hal_kernel_types.h>
#include <boot_info.h>

/////////////////////////////////////////////////////////////////////////////////////////
//    Kernel Virtual Memory Manager initialisation (implementation in hal_vmm.c)
//
// Any arch-specific implementation must implement this API.
/////////////////////////////////////////////////////////////////////////////////////////

/****  Forward declarations  ****/

struct process_s;
struct boot_info_s;

/****************************************************************************************
 * Depending on the hardware architecture, this function creates (i.e. allocates memory
 * and initializes) the VSL (Virtual segments List) and the GPT (Generic Page Table),
 * for all vsegs required by the kernel process. 
 ****************************************************************************************
 * @ info  : local pointer on boot_info (for kernel segments base & size).
 * @ return 0 if success / return ENOMEM if failure.
 ***************************************************************************************/
error_t hal_vmm_kernel_init( struct boot_info_s * info );

/****************************************************************************************
 * Depending on the hardware architecture, this function updates the VMM of an user
 * process identified by the <process> argument. It registers in VSL and GPT all
 * kernel vsegs required by this architecture.
 ****************************************************************************************
 * @ process   : local pointer on user process descriptor.
 * @ return 0 if success / return ENOMEM if failure.
 ***************************************************************************************/
error_t hal_vmm_kernel_update( struct process_s * process );

#endif	/* HAL_VMM_H_ */
