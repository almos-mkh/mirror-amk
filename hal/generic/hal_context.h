/*
 * hal_context.h - Generic Thread Context Access API definition.
 * 
 * Author  Alain Greiner    (2016)
 *
 * Copyright (c) UPMC Sorbonne Universites
 * 
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef  _HAL_CONTEXT_H_
#define  _HAL_CONTEXT_H_

//////////////////////////////////////////////////////////////////////////////////////////
//        Generic Thread Context API definition (implementation in hal_context.c) 
//
// A thread context is defined by the two (core specific) structures hal_cpu_context_t
// and hal_fpu_context_t, defined in hal_context.c file, that are accessed with generic
// void* pointers stored in the thread descriptor.
// - the "hal_context_t" struct is used for the CPU registers values at context switch.
// - the "hal_fpu_context_t" struct is used for the FPU registers when required.
//////////////////////////////////////////////////////////////////////////////////////////

/**** Forward declarations ****/

struct thread_s;

/****************************************************************************************
 * This function allocates memory for a CPU context and links it to the thread
 * identified by the <thread> argument. The context is not initialised.
 ****************************************************************************************
 * @ thread  : pointer on the thread descriptor.
 * @ return 0 if success / return -1 if failure.
 ***************************************************************************************/
error_t hal_cpu_context_alloc( struct thread_s * thread );

/****************************************************************************************
 * This function initializes a CPU context from scratch.
 ****************************************************************************************
 * @ thread  : pointer on the thread descriptor.
 ***************************************************************************************/
void hal_cpu_context_init( struct thread_s * thread );

/****************************************************************************************
 * This function is used to implement the fork() system call. 
 * 1) It saves in a remote (child) thread CPU context the current CPU registers values.
 *    Three slots are not simple copies of the parent registers values :
 *    - the thread pointer is set to the child thread local pointer. 
 *    - the stack pointer is set to parrent SP + (child_base - parent_base).
 *    - the status register is set to kernel mode with IRQ disabled.
 * 2) It copies the content of the calling (parent) thread kernel_stack,
 *    to the remote (child) thread kernel_stack.
 ****************************************************************************************
 * @ thread_xp  : extended pointer on the remote thread descriptor.
 ***************************************************************************************/
void hal_cpu_context_fork( xptr_t    thread_xp );

/****************************************************************************************
 * This function is used to implement the exec() system call. 
 * 1) It initialize the relevant slots of the the calling thread CPU context.
 * 2) It call the hal_do_cpu_restore() function to return to user mode and start
 *    execution of the new process.
 ****************************************************************************************
 * @ thread  : pointer on the thread descriptor.
 ***************************************************************************************/
void hal_cpu_context_exec( struct thread_s * thread );

/****************************************************************************************
 * This function display some slots of the CPU context.
 ****************************************************************************************
 * @ thread_xp  : extended pointer on the thread descriptor.
 ***************************************************************************************/
void hal_cpu_context_display( xptr_t  thread_xp );

/****************************************************************************************
 * This function releases the physical memory allocated for a thread CPU context.
 ****************************************************************************************
 * @ thread  : pointer on the thread descriptor.
 ***************************************************************************************/
void hal_cpu_context_destroy( struct thread_s * thread );







/****************************************************************************************
 * This function allocates memory for a FPU context, reset all entries,
 * and links it to the thread identified by the <thread> argument.
 ****************************************************************************************
 * @ thread  : pointer on the thread descriptor.
 * @ return 0 if success / return -1 if failure.
 ***************************************************************************************/
error_t hal_fpu_context_alloc( struct thread_s * thread );

/****************************************************************************************
 * This function initializes a FPU context from scratch.
 ****************************************************************************************
 * @ thread  : pointer on the thread descriptor.
 ***************************************************************************************/
void hal_fpu_context_init( struct thread_s * thread );

/****************************************************************************************
 * This function copies a FPU context defined by the <src> argument to the FPU context
 * defined by the <dst> argument. It is used by the fork system call.
 ****************************************************************************************
 * @ dst  : pointer on the destination thread descriptor.
 * @ src  : pointer on the source thread descriptor.
 ***************************************************************************************/
void hal_fpu_context_copy( struct thread_s * dst,
                           struct thread_s * src );

/****************************************************************************************
 * This function releases the physical memory allocated for a FPU context.
 ****************************************************************************************
 * @ thread  : pointer on the thread descriptor.
 ***************************************************************************************/
void hal_fpu_context_destroy( struct thread_s * thread );

/****************************************************************************************
 * This function is used to implement the fork() system call. 
 * It saves in a remote thread FPU context the current FPU registers values.
 ****************************************************************************************
 * @ thread_xp  : extended pointer on the remote thread descriptor.
 ***************************************************************************************/
void hal_fpu_context_save( xptr_t thread_xp );

/****************************************************************************************
 * This function restores from the calling thread FPU context the FPU registers values.
 ****************************************************************************************
 * @ thread  : pointer on the thread descriptor.
 ***************************************************************************************/
void hal_fpu_context_restore( struct thread_s * thread );

#endif	/* _HAL_CONTEXT_H_ */
