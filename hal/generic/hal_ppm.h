/*
 * hal_ppm.h - Generic Physical Page Manager API definition.
 *
 * Authors  Alain Greiner (2016,2017)
 *          Maxime Villard (2017)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _HAL_PPM_H_
#define _HAL_PPM_H_

#include <hal_kernel_types.h>
#include <boot_info.h>

/////////////////////////////////////////////////////////////////////////////////////////
//           Generic Physical Page Manager API (implementation in hal_ppm.c)
//
// Any arch-specific implementation must implement this API.
/////////////////////////////////////////////////////////////////////////////////////////

/****  Forward declarations  ****/

struct page_s;


/****************************************************************************************
 * This function initializes the Physical Memory Manager in a given cluster.
 * It allocates physical memory to store the physical page descriptors
 * array (ppm_tbl), and initializes it using the <pages_nr> field in boot_info.
 * As the physical memory allocator is not available yet, this physical memory is
 * allocated on top of the kernel code, using the <pages_offset> field in boot_info.
 * The pages containing the kernel code, and the pages marked as <reserved> by the
 * boot-loader in the boot_info structure are removed from the free-lists.
 ****************************************************************************************
 * @ info       : pointer on the boot_info structure.
 * @ return 0 if success / return ENOMEM if failure.
 ***************************************************************************************/
error_t hal_ppm_init( boot_info_t * info );

#endif	/* HAL_PPM_H_ */
