/*
 * hal_irqmask.h - Generic Interrupt Masking API definition
 *
 * Authors   Ghassan Almaless (2008,2009,2010,2011,2012)
 *           Alain Greiner    (2016)
 *
 * Copyright (c)  UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef  _HAL_IRQMASK_H_
#define  _HAL_IRQMASK_H_

#include <hal_shared_types.h>


//////////////////////////////////////////////////////////////////////////////////////////
//       Generic IRQ Masking API (implementation in hal_irqmask.c)
//
// Interrupt Enabling / disabling for a given core depends on the hardware architecture.
// ALMOS-MKH uses the following API. 
//////////////////////////////////////////////////////////////////////////////////////////

/*****************************************************************************************
 * This function disables all IRQs, and saves the CPU SR state if required.
 *****************************************************************************************
 * @ old    : address of buffer to save the SR (no save if NULL).
 ****************************************************************************************/
extern inline void hal_disable_irq( reg_t * old );

/*****************************************************************************************
 * This function enables all IRQs, and saves the CPU SR state if required.
 *****************************************************************************************
 * @ old    : address of buffer to save the SR (no save if NULL).
 ****************************************************************************************/
extern inline void hal_enable_irq( reg_t * old );

/*****************************************************************************************
 * This function restores a previously saved SR state.
 *****************************************************************************************
 * @ old    : value to be written in CPU SR register
 ****************************************************************************************/
extern inline void hal_restore_irq( reg_t old );


#endif	/* _HAL_IRQ_MASK_H_ */
