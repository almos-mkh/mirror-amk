/*
 * hal_uspace.h - Generic User Space Access API definition
 *
 * Authors    Alain Greiner (2016,2017)
 *
 * Copyright (c)  UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef  _HAL_USPACE_H_
#define  _HAL_USPACE_H_

#include <hal_kernel_types.h>

//////////////////////////////////////////////////////////////////////////////////////////
//           User space access API (implementation in hal_uspace.c)
//
// When moving data between user space and kernel space, the user address is always
// a virtual address, but the kernel address can be a physical address, on 32 bits
// architectures, and require MMU dynamic activation/deactivation. 
// For sake of portability, user/kernel data transfers must use the following API.
//////////////////////////////////////////////////////////////////////////////////////////


/*****************************************************************************************
 * This function tranfers a data buffer from the user space to the kernel space.
 * If the kernel uses physical addresses, it activates the MMU to access the user buffer.
 *****************************************************************************************
 * @ k_dst     : destination address in kernel space.
 * @ u_src     : source buffer address in user space.
 * @ size      : size (number of bytes).
 ****************************************************************************************/
extern void hal_copy_from_uspace( void     * k_dst,
                                  void     * u_src,
                                  uint32_t   size );

/*****************************************************************************************
 * This function tranfers a data buffer from the kernel space to the user space.
 * If the kernel uses physical addresses, it activates the MMU to access the user buffer.
 *****************************************************************************************
 * @ u_dst     : destination buffer address in user space.
 * @ k_src     : source address in kernel space.
 * @ size      : size (number of bytes).
 ****************************************************************************************/
extern void hal_copy_to_uspace( void     * u_dst,
                                void     * k_src,
                                uint32_t   size );

/*****************************************************************************************
 * This function tranfers a string from the user space to the kernel space.
 * The transfer stops after the first encountered NUL character, and no more than
 * <max_size> characters are actually copied to target buffer.
 * If the kernel uses physical addresses, it activates the MMU to access the user buffer.
 *****************************************************************************************
 * @ u_dst     : destination buffer address in user space.
 * @ k_src     : source address in kernel space.
 * @ max_size  : max number of characters to be copied.
 ****************************************************************************************/
extern void hal_strcpy_from_uspace( char     * k_dst,
                                    char     * u_src, 
                                    uint32_t   max_size );

/*****************************************************************************************
 * This function tranfers a string from the kernel space to the user space.
 * The transfer stops after the first encountered NUL character, and no more than
 * <max_size> characters are actually copied to target buffer.
 * If the kernel uses physical addresses, it activates the MMU to access the user buffer.
 *****************************************************************************************
 * @ u_dst     : destination buffer address in user space.
 * @ k_src     : source address in kernel space.
 * @ max_size  : max number of characters to be copied.
 ****************************************************************************************/
extern void hal_strcpy_to_uspace( char     * u_dst,
                                  char     * k_src,
                                  uint32_t   max_size );

/*****************************************************************************************
 * This function computes the length of a string in user space.
 * If the kernel uses physical addresses, it activates the MMU to access the user buffer.
 *****************************************************************************************
 * @ string     : string in user space.
 * @ return length of the string.
 ****************************************************************************************/
uint32_t hal_strlen_from_uspace( char * string );


#endif	/* _HAL_USPACE_H_ */
