/*
 * hal_user.h - User-side, architecture specific API definition.
 *
 * Author      Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _HAL_USER_H_
#define _HAL_USER_H_

#include <hal_shared_types.h>

//////////////////////////////////////////////////////////////////////////////////////////
//     User-side, hardware dependant functions API definition.
//
//   Any architecture specific implementation must implement this API.
//////////////////////////////////////////////////////////////////////////////////////////

/*****************************************************************************************
 * This function implements the ALMOS-MKH user-side syscall handler.
 * The syscall index and the arguments values are written in the proper core registers
 * to enter the kernel as required by the specific hardware architecture.
 *****************************************************************************************
 * @ service_num  : syscall index (defined in the "shared_sycalls.h" file.
 * @ arg0         : first  syscall argument (semantice depends on syscall index).
 * @ arg1         : second syscall argument (semantice depends on syscall index).
 * @ arg2         : third  syscall argument (semantice depends on syscall index).
 * @ arg3         : fourth syscall argument (semantice depends on syscall index).
 * @ returned value semantic depends on the syscall index.
 ****************************************************************************************/
int hal_user_syscall( reg_t service_num,
                      reg_t arg0,
                      reg_t arg1,
                      reg_t arg2,
                      reg_t arg3 );

/*****************************************************************************************
 * This blocking function atomically adds a positive or negative value to an int
 * shared variable, returning only when atomic add is successful.
 * This function does not use a syscall.
 *****************************************************************************************
 * @ ptr     : pointer on the shared variable.
 * @ val     : signed value to add
 * @ return shared variable value before add.
 ****************************************************************************************/
int hal_user_atomic_add( int * ptr,
                         int   val );

/*****************************************************************************************
 * This blocking function implement a memory fence. 
 * It actually flush the calling core write buffer.
 ****************************************************************************************/
void hal_user_fence( void );


#endif   // _HAL_USER_H_
