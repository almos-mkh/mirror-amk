/*
 * hal_kernel_syscall.h - Architecture specific kernel_side syscall handler API definition.
 *
 * Author      Alain Greiner (2016,2017)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _HAL_KERNEL_SYSCALL_H_
#define _HAL_KERNEL_SYSCALL_H_

#include <hal_kernel_types.h>

///////////////////////////////////////////////////////////////////////////////////////
//     Kernel-side syscall handler API
//
// This hal_do_syscall() function extract from the calling thread uzone array
// the syscall index, and the four syscall arguments. 
// Then it calls the generic do_syscall() function,
// that calls itself the relevant kernel function, depending on the syscall index.
//
// When the generic do_syscall function returns, it saves the return value
// in the UZ_VO slot of the returning thread uzone, and update the UZ_EPC slot.
// 
// WARNING: The returning thread can be different from the entering thread 
// in the case of a sys_fork() system call.
///////////////////////////////////////////////////////////////////////////////////////


/**************************************************************************************
 * This function implements the syscall handler for the TSAR architecture.
 *************************************************************************************/
void hal_do_syscall( void );


#endif   // _HAL_KERNEL_SYSCALL_H_
