/*
 * hal_special.h - Generic Special Registers Access API definition.
 *
 * Authors   Alain Greiner    (2016,2017)
 *
 * Copyright (c)  UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef  _HAL_CPU_H_
#define  _HAL_CPU_H_

#include <hal_shared_types.h>
#include <hal_kernel_types.h>

/****  Forward declarations  ***/

struct thread_s;
struct gpt_s;

///////////////////////////////////////////////////////////////////////////////////////////
//      Generic Special Registers Access API (implementation in hal_special.c)
//
// ALMOS-MKH uses the following API to access the core protected registers.
///////////////////////////////////////////////////////////////////////////////////////////

/*****************************************************************************************
 * This function initialise - for architectures requiring it - the protected register(s)
 * containing the kernel_entry adresse(s) for interrupts / exceptions / syscalls.
 ****************************************************************************************/
void hal_set_kentry( void );

/*****************************************************************************************
 * This function initializes - for architectures requiring it - the MMU registers of the
 * calling core to use the the kernel page table identified by the <gpt> argument for
 * all threads attached to kernel process_zero. 
 * It is called by all cores in the kernel_init() function.
 *****************************************************************************************
 * @ gpt :  local pointer on the kernel page table descriptor.
 ****************************************************************************************/
void hal_mmu_init( struct gpt_s * gpt );

/*****************************************************************************************
 * This function returns the calling core status register value.
 ****************************************************************************************/
inline reg_t hal_get_sr( void );

/*****************************************************************************************
 * This function returns the global core identifier from the calling core register.
 ****************************************************************************************/
inline gid_t hal_get_gid( void );

/*****************************************************************************************
 * This function returns the current value of the hardware cycles counter.
 * This cycle counter is reset when the core is initialised (at each boot).
 ****************************************************************************************/
inline reg_t hal_time_stamp( void );

/*****************************************************************************************
 * This function returns the content of the calling core private cycles counter,
 * taking into account overflow if the core hardware register is not 64 bits.
 * This cycle counter is reset when the core is initialised (at each boot).
 ****************************************************************************************/
cycle_t hal_get_cycles( void );

/*****************************************************************************************
 * This function returns the current thread pointer from the calling core register.
 ****************************************************************************************/
inline struct thread_s * hal_get_current_thread( void );

/*****************************************************************************************
 * This function registers a thread pointer in the calling core register.
 ****************************************************************************************/
void hal_set_current_thread( struct thread_s * thread );

/*****************************************************************************************
 * This function writes into the proper core register to enable the floating point unit.
 ****************************************************************************************/
void hal_fpu_enable( void );

/*****************************************************************************************
 * This function writes into the proper core register to disable the floating point unit.
 ****************************************************************************************/
void hal_fpu_disable( void );

/*****************************************************************************************
 * This function returns the current value of stack pointer from core register.
 ****************************************************************************************/
uint32_t hal_get_sp( void );

/*****************************************************************************************
 * This function returns the current value of the return adddress from core register.
 ****************************************************************************************/
uint32_t hal_get_ra( void );

/*****************************************************************************************
 * This function registers a new value in the core stack pointer and returns previous one.
 ****************************************************************************************/
inline uint32_t hal_set_sp( void * new_val );

/*****************************************************************************************
 * This function returns the faulty address in case of address exception.
 ****************************************************************************************/
uint32_t hal_get_bad_vaddr( void );

/*****************************************************************************************
 * This function makes an uncachable read to a 32 bits variable in local memory.
 *****************************************************************************************
 * @ ptr     : pointer on the variable
 * @ returns the value
 ****************************************************************************************/
uint32_t hal_uncached_read( uint32_t * ptr );

/*****************************************************************************************
 * This function invalidates the cache line containing a given address.
 * @ ptr     : address in local memory
 ****************************************************************************************/
void hal_invalid_dcache_line( void * ptr );

/*****************************************************************************************
 * This blocking function flushes the write buffer to synchronize all pending writes.
 ****************************************************************************************/
void hal_fence( void );

/*****************************************************************************************
 * This forbids code reordering accross this barrier by the compiler.
 ****************************************************************************************/
void hal_rdbar( void );

/*****************************************************************************************
 * This function forces the calling core in idle-low-power mode.
 ****************************************************************************************/
void hal_core_sleep( void ) __attribute__((__noreturn__));

/*****************************************************************************************
 * This function returns after approximately <delay> cycles.
 * @ delay  : number of cycles.
 ****************************************************************************************/
void hal_fixed_delay( uint32_t delay );

/*****************************************************************************************
 * This function returns information on MMU exceptions :
 *****************************************************************************************
 * @ mmu_ins_excp_code : [out] instruction fetch exception code
 * @ mmu_ins_bad_vaddr : [out] instruction fetch faulty virtual address
 * @ mmu_dat_excp_code : [out] data access exception code
 * @ mmu_dat_bad_vaddr : [out] data access faulty virtual address
 ****************************************************************************************/
void hal_get_mmu_excp( intptr_t * mmu_ins_excp_code,
                       intptr_t * mmu_ins_bad_vaddr,
                       intptr_t * mmu_dat_excp_code,
                       intptr_t * mmu_dat_bad_vaddr );


#endif	/* _HAL_SPECIAL_H_ */
