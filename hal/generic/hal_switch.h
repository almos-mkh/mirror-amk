/*
 * hal_switch.h - Generic architecture context switch function
 * 
 * Authorg   Alain Greiner  (2017)
 *
 * Copyright (c) UPMC Sorbonne Universites
 * 
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _HAL_SWITCH_H_
#define _HAL_SWITCH_H_

struct thread_s;

/*************************************************************************************
 * This assembly level function is called by the sched_yield() function,
 * to make a CPU context switch. 
 * The current thread CPU context is identified by the <ctx_old> pointer.
 * The new thread CPU context is identified by the <ctx_new> pointer.
 * The architecture specific hal_cpu_context_t structure used to store a CPU context
 * is defined in the architecture specific hal_context.c file.
 * This function does NOT modify any register before saving values into context.
 * When the switch is completed, it jumps to address contained in the relevant
 * register of the new thread CPU context.
 *************************************************************************************
 * @ ctx_current  : local pointer on current thread CPU context.
 * @ ctx_next     : local pointer on new thread CPU context.
 ************************************************************************************/
void hal_do_cpu_switch( void * ctx_old,
                        void * ctx_new );

/*************************************************************************************
 * This assembly level function is called by the hal_cpu_context_fork() function.
 * It save the calling CPU register values to a CPU context identified by <ctx>.
 * This function does NOT modify any register before saving values into context.
 * The architecture specific hal_cpu_context_t structure used to store a CPU context
 * is defined in the architecture specific hal_context.c file.
 * When the save is completed, it simply returns to the calling function.
 *************************************************************************************
 * @ ctx     : local pointer on CPU context.
 ************************************************************************************/
void hal_do_cpu_save( void * ctx );

/*************************************************************************************
 * This assembly level function, is called by the hal_cpu_context_exec() function.
 * It restore the calling CPU register values from a CPU context identified by <ctx>.
 * The architecture specific hal_cpu_context_t structure used to store a CPU context
 * is defined in the architecture specific hal_context.c file.
 * When the restore is completed, it simply jumps to the address contained in ra_31.
 * In ALMOS-MKH, ra_31 must contain a pointer on the eret() function, and c0_epc
 * must contain the new main thread entry point.
 *************************************************************************************
 * @ ctx     : local pointer on CPU context.
 ************************************************************************************/
void hal_do_cpu_restore( void * ctx );

#endif	/* _HAL_SWITCH_H_ */
