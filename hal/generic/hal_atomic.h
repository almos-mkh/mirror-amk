/*
 * hal_atomic.h - Generic Atomic Operations API definition.
 *
 * Authors   Alain Greiner    (2016)
 *
 * Copyright (c)  UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef  _HAL_ATOMIC_H_
#define  _HAL_ATOMIC_H_

#include <kernel_config.h>
#include <hal_kernel_types.h>

//////////////////////////////////////////////////////////////////////////////////////////
//            Generic Atomic Operations API (implementation in hal_atomic.c)
//
// Atomic read-then-write operations depend on the CPU instruction set.
// ALMOS-MKH uses the following generic API.
//////////////////////////////////////////////////////////////////////////////////////////

/*****************************************************************************************
 * This blocking function makes an atomic "and" between a 32 bits mask, and a 32 bits
 * unsigned int shared variable, returning only when atomic operation is successful.
 *****************************************************************************************
 * @ ptr     : pointer on the shared variable
 * @ val     : mask value
 ****************************************************************************************/
void hal_atomic_and( uint32_t * ptr,
                     uint32_t   val );

/*****************************************************************************************
 * This blocking function makes an atomic "or" between a 32 bits mask, and a 32 bits
 * unsigned int shared variable, returning only when atomic operation is successful.
 *****************************************************************************************
 * @ ptr     : pointer on the shared variable
 * @ val     : mask value
 ****************************************************************************************/
void hal_atomic_or( uint32_t * ptr,
                    uint32_t   val );

/*****************************************************************************************
 * This blocking function atomically adds a positive or negative value to a 32 bits
 * signed or unsigned int shared variable, returning only when atomic add is successful.
 *****************************************************************************************
 * @ ptr     : pointer on the shared variable (signed or unsigned)
 * @ val     : value to add
 * @ return shared variable value before add
 ****************************************************************************************/
int32_t hal_atomic_add( void     * ptr,
                        int32_t    val );

/*****************************************************************************************
 * This NON blocking function makes an atomic Compare-And-Swap on a 32 bits unsigned int
 * shared variable, returning a Boolean to indicate success.
 *****************************************************************************************
 * @ ptr     : pointer on the shared variable
 * @ old     : expected value for the shared variable
 * @ new     : value to be written if success
 * @ return true if success.
 ****************************************************************************************/
bool_t hal_atomic_cas( uint32_t * ptr,
                       uint32_t   old,
                       uint32_t   new );

/*****************************************************************************************
 * This non blocking function makes an atomic Test-if-zero-And-Set on a 32 bits unsigned
 * int shared variable, returning a Boolean to indicate both success and atomicity.
 *****************************************************************************************
 * @ ptr     : pointer on the shared variable
 * @ val     : value to be written if success
 * @ return true if (current == 0) and (access is atomic)
 ****************************************************************************************/
bool_t hal_atomic_test_set( uint32_t * ptr,
                            uint32_t   val );

#endif	/* _HAL_ATOMIC_H_ */
