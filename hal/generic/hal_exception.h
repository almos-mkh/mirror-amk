/*
 * hal_exception.c - Architecture specific exception handler API definition
 * 
 * Authors    Alain Greiner (2016,2017)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _HAL_EXCEPTION_H_
#define _HAL_EXCEPTION_H_

#include <hal_kernel_types.h>
#include <thread.h>

//////////////////////////////////////////////////////////////////////////////////////////
//     Architecture specific exception handler API
//
// ALMOS-MKH defines three classes of exceptions:
//
// - NON_FATAL : exceptions such as "page unmapped" or "FPU unusable" can be non fatal.
//   => The hal_do_exception() function call the generic vmm_handle_page_fault(),
//      or the fpu_handle_exception() function, and the calling thread resumes execution
//      when the exception can be  sucessfully handled.
//
// - USER_ERROR : exceptions such a "illegal vaddr" or "illegal write access" are fatal.
//   => The hal_do_exception() function send a kill signal to the calling thread process,
//      and displays an error message on TXT0.
//
// - KERNEL_PANIC : "kernel mistakes" are abnormal events.
//   => The hal_do_exception() function calls the generic panic() function, to display
//      a message on TXT0, disable IRQs and call the hal_core_sleep() function.
//
// For all exceptions, the faulty core registers have been saved in the "uzone"
// that can be accessed through the "uzone" pointer stored in thread descriptor.
//////////////////////////////////////////////////////////////////////////////////////////


/*****************************************************************************************
 * This enum defines the global exception types after analysis by the exception handler.
 ****************************************************************************************/

typedef enum
{
    EXCP_NON_FATAL,
    EXCP_USER_ERROR,
    EXCP_KERNEL_PANIC,

}
exception_handling_type_t;


/*****************************************************************************************
 * This function is called by the hal_kentry() function when an exception is detected by
 * the hardware for a given thread running on a given core.
 ****************************************************************************************/
void hal_do_exception( void );


#endif  // _HAL_EXCEPTION_H_
