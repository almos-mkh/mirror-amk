/*
 * hal_interrupt.h - Architecture specific interrupt handler API definition.
 *
 * Author   Alain Greiner (2016,2017)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _HAL_INTERRUPT_H_
#define _HAL_INTERRUPT_H_

#include <hal_kernel_types.h>

///////////////////////////////////////////////////////////////////////////////////////
//     Architecture specific interrupt handler API
//
// The interrupted thread context (CPU registers) has been saved by the hal_kentry
// function, in the uzone array, that can be accessed through the "uzone" pointer
// stored in the thread descriptor.
///////////////////////////////////////////////////////////////////////////////////////


/**************************************************************************************
 * This function implements the TSAR_MIPS32 specific interrupt handler.
 *************************************************************************************/
void hal_do_interrupt( void );


#endif   // _HAL_INTERRUPT_H_
