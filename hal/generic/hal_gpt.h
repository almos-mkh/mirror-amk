/*
 * hal_gpt.h - Generic Page Table API definition.
 *
 * Authors  Alain Greiner (2016)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _GPT_H_
#define _GPT_H_

#include <hal_kernel_types.h>

/////////////////////////////////////////////////////////////////////////////////////////
//           Generic Page Table Definition (implementation in hal_gpt.c)
//
// It is specified as a simple (one dimensional) array indexed by the VPN (vpn_t type),
// even if implementations can use a more sophisticated organisation (two-levels or more).
// - The number of entries (number of pages in a virtual space) is architecture
//   dependent, and is defined as (CONFIG_USER_SPACE_SIZE / CONFIG_PPM_PAGE_SIZE).
// - Each entry contains a Physical Page Number (ppn_t type), and a set of attributes,
//   defined as a 32 bits-vector.
//
// Any arch-specific implementation must implement this API.
/////////////////////////////////////////////////////////////////////////////////////////

/****  Forward declarations  ****/

struct page_s;
struct process_s;

/****************************************************************************************
 * These macros define the masks for the Generic Page Table Entry attributes.
 ***************************************************************************************/

#define GPT_MAPPED      0x0001       /*! PTE is mapped                                 */
#define GPT_SMALL       0x0002       /*! PTE is a small page                           */
#define GPT_READABLE    0x0004       /*! PTE is readable                               */
#define GPT_WRITABLE    0x0008       /*! PTE is writable                               */
#define GPT_EXECUTABLE  0x0010       /*! PTE is executable                             */
#define GPT_CACHABLE    0x0020       /*! PTE can be cached                             */
#define GPT_USER        0x0040       /*! PTE is user accessible                        */
#define GPT_DIRTY       0x0080       /*! PTE has been written                          */
#define GPT_ACCESSED    0x0100       /*! PTE has been "recently" accessed              */
#define GPT_GLOBAL      0x0200       /*! PTE is kept in TLB at context switch          */
#define GPT_COW         0x0400       /*! PTE must be copied on write                   */
#define GPT_SWAP        0x0800       /*! PTE swapped on disk (not implemented yet)     */
#define GPT_LOCKED      0x1000       /*! PTE is protected against concurrent access    */

/****************************************************************************************
 * This structure defines the Generic Page Table descriptor.
 ***************************************************************************************/

typedef struct gpt_s
{
	void           * ptr;    /*! local pointer on GPT root                             */
	ppn_t            ppn;    /*! PPN of GPT root                                       */
}
gpt_t;


/****************************************************************************************
 * This function allocates physical memory for first level page table (PT1),
 * and initializes the GPT descriptor, creating an empty GPT.
 ****************************************************************************************
 * @ gpt     : pointer on generic page table descriptor.
 * @ returns 0 if success / returns ENOMEM if error.
 ***************************************************************************************/
error_t hal_gpt_create( gpt_t * gpt );

/****************************************************************************************
 * This function releases all memory dynamically allocated for a generic page table.
 * For a multi-levels radix tree implementation, it includes all nodes in the tree.
 * If the calling thread is running in the reference cluster, it checks that user PTE
 * entries are unmapped, and releases the mapped physical pages.
 * The kernel pages are not released.
 ****************************************************************************************
 * @ gpt     : pointer on generic page table descriptor.
 ***************************************************************************************/
void hal_gpt_destroy( gpt_t * gpt);

/****************************************************************************************
 * This function prints on the kernel terminal the content of a generic page table.
 ****************************************************************************************
 * @ process : pointer on local process descriptor.
 ***************************************************************************************/
void hal_gpt_display( struct process_s * process );

/****************************************************************************************
 * This blocking function gets a lock on a PTE (Page Table Entry) identified
 * by its VPN, and returns only when the PTE has been successfully locked.
 * If the target PTE is not present, it allocates and maps a physical page.
 * A big page cannot be locked.
 ****************************************************************************************
 * @ gpt     : pointer on the generic page table
 * @ vpn     : virtual page number of the target PTE.
 * @ returns 0 if success / return ENOMEM or EINVAL if error.
 ***************************************************************************************/
error_t hal_gpt_lock_pte( gpt_t * gpt,
                          vpn_t   vpn );

/****************************************************************************************
 * This function releases the lock on a PTE identified by its VPN.
 ****************************************************************************************
 * @ gpt     : pointer on the generic page table
 * @ vpn     : virtual page number of the target PTE.
 * @ returns 0 if success / returns EINVAL if error.
 ***************************************************************************************/
error_t hal_gpt_unlock_pte( gpt_t * gpt,
                            vpn_t   vpn );

/****************************************************************************************
 * This function maps in a - local or remote - GPT identified by the <gpt_xp> argument
 * an entry identified by the <vpn> argument, as defined by <ppn> and <attr> arguments.
 * It allocates physical memory for the GPT PT2, using a RPC_PMEM_GET_PAGES if required.
 ****************************************************************************************
 * @ gpt_xp    : [in] pointer on the page table
 * @ vpn       : [in] virtual page number
 * @ attr      : [in] generic attributes
 * @ ppn       : [in] physical page number
 * @ returns 0 if success / returns ENOMEM if error
 ***************************************************************************************/
error_t hal_gpt_set_pte( xptr_t     gpt_xp,
                         vpn_t      vpn,
                         uint32_t   attr,
                         ppn_t      ppn );

/****************************************************************************************
 * This function unmaps all pages identified by the <vpn> argument from the local GPT
 * identified by the <gpt> argument.
 * It does NOT release the physical memory allocated for the unmapped pages.
 ****************************************************************************************
 * @ gpt      : [in] pointer on the local page table
 * @ vpn      : [in] page index in virtual space
 ***************************************************************************************/
void hal_gpt_reset_pte( gpt_t * gpt,
                        vpn_t   vpn );

/****************************************************************************************
 * This function returns in the <attr> and <ppn> arguments the current values stored
 * in a - local or remote - GPT entry, identified by the <gpt> and <vpn> arguments.
 ****************************************************************************************
 * @ gpt_xp    : [in]  extended pointer on the page table
 * @ vpn       : [in]  virtual page number
 * @ attr      : [out] generic attributes
 * @ ppn       : [out] physical page number
 ***************************************************************************************/
void hal_gpt_get_pte( xptr_t     gpt_xp,
                      vpn_t      vpn,
                      uint32_t * attr,
                      ppn_t    * ppn );

/****************************************************************************************
 * This function is used to implement the "fork" system call: It copies one GPT entry
 * identified by the <vpn> argument, from a remote <src_gpt_xp> to a local <dst_gpt>. 
 * It does nothing if the source PTE is not MAPPED and SMALL.
 * It optionnally activates the "Copy on Write" mechanism: when the <cow> argument is
 * true: the GPT_WRITABLE flag is reset, and the GPT_COW flag is set.
 * A new second level PT2(s) is allocated for destination GPT if required.
 * It returns in the <ppn> and <mapped> arguments the PPN value for the copied PTE,
 * and a boolean indicating if the PTE is mapped and small, and was actually copied.
 ****************************************************************************************
 * @ dst_gpt      : [in]  local pointer on the local destination GPT.
 * @ src_gpt_xp   : [in]  extended pointer on the remote source GPT.
 * @ vpn_base     : [in]  vpn defining the PTE to be copied.
 * @ cow          : [in]  activate the COPY-On-Write mechanism if true.
 * @ ppn          : [out] PPN value (only if mapped is true).
 * @ mapped       : [out] true if src_gpt[vpn] actually copied to dst_gpt[vpn].
 * @ return 0 if success / return -1 if no memory for a new PT2.
 ***************************************************************************************/
error_t hal_gpt_pte_copy( gpt_t    * dst_gpt,
                          xptr_t     src_gpt_xp,
                          vpn_t      vpn,
                          bool_t     cow,
                          ppn_t    * ppn,
                          bool_t   * mapped );

/****************************************************************************************
 * This function returns true if the MAPPED and SMALL flags are both set
 * for a PTE defined by <gpt> and <vpn> arguments.
 ****************************************************************************************
 * @ gpt       : [in]  pointer on the page table
 * @ vpn       : [in]  virtual page number
 * @ returns true if MAPPED is set.
 ***************************************************************************************/
bool_t hal_gpt_pte_is_mapped( gpt_t * gpt,
                              vpn_t   vpn );

/****************************************************************************************
 * This function returns true if the MAPPED, SMALL, and COW flags are all set
 * for a PTE defined by <gpt> and <vpn> arguments.
 ****************************************************************************************
 * @ gpt       : [in]  pointer on the page table
 * @ vpn       : [in]  virtual page number
 * @ returns true if COW is set.
 ***************************************************************************************/
bool_t hal_gpt_pte_is_cow( gpt_t * gpt,
                           vpn_t   vpn );

/****************************************************************************************
 * This function atomically set the COW flag and reset the WRITABLE flag for all PTEs
 * of a remote GPT identified by the <gpt_xp>, <vpn_base>, and <vpn_size arguments.
 * It does nothing if the remote PTE is not MAPPED and SMALL.
 ****************************************************************************************
 * @ gpt_xp    : [in]  extended pointer on the remote GPT.
 * @ vpn_base  : [in]  first virtual page.
 * @ vpn_size  : [in]  number of pages.
 ***************************************************************************************/
void hal_gpt_set_cow( xptr_t  gpt_xp,
                      vpn_t   vpn_base,
                      vpn_t   vpn_size );

/****************************************************************************************
 * This function is used to maintain coherence amongst the multiple GPT copies.
 * It modifies an existing entry identified by the <vpn> argument in a remote GPT
 * identified by the <gpt_xp> argument, using remote accesses.
 * It cannot fail, because only MAPPED & SMALL entries are modified.
 ****************************************************************************************
 * @ gpt_xp    : [in] extended pointer on the page table
 * @ vpn       : [in] virtual page number
 * @ attr      : [in] generic attributes
 * @ ppn       : [in] physical page number
 ***************************************************************************************/
void hal_gpt_update_pte( xptr_t     gpt_xp,
                         vpn_t      vpn,
                         uint32_t   attr,
                         ppn_t      ppn );


#endif	/* _GPT_H_ */

