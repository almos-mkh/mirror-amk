/*
 * hal_register.h - Values available in some x86 registers
 *
 * Copyright (c) 2017 Maxime Villard
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*
 * %cr0
 */
#define CR0_PE	0x00000001	/* Protected mode Enable */
#define CR0_MP	0x00000002	/* "Math" Present (NPX or NPX emulator) */
#define CR0_EM	0x00000004	/* EMulate non-NPX coproc. (trap ESC only) */
#define CR0_TS	0x00000008	/* Task Switched (if MP, trap ESC and WAIT) */
#define CR0_ET	0x00000010	/* Extension Type (387 (if set) vs 287) */
#define CR0_NE	0x00000020	/* Numeric Error enable (EX16 vs IRQ13) */
#define CR0_WP	0x00010000	/* Write Protect (honor PG_RW in all modes) */
#define CR0_AM	0x00040000	/* Alignment Mask (set to enable AC flag) */
#define CR0_NW	0x20000000	/* Not Write-through */
#define CR0_CD	0x40000000	/* Cache Disable */
#define CR0_PG	0x80000000	/* PaGing enable */

/*
 * %cr4
 */
#define CR4_VME		0x00000001 /* virtual 8086 mode extension enable */
#define CR4_PVI		0x00000002 /* protected mode virtual interrupt enable */
#define CR4_TSD		0x00000004 /* restrict RDTSC instruction to cpl 0 */
#define CR4_DE		0x00000008 /* debugging extension */
#define CR4_PSE		0x00000010 /* large (4MB) page size enable */
#define CR4_PAE		0x00000020 /* physical address extension enable */
#define CR4_MCE		0x00000040 /* machine check enable */
#define CR4_PGE		0x00000080 /* page global enable */
#define CR4_PCE		0x00000100 /* enable RDPMC instruction for all cpls */
#define CR4_OSFXSR	0x00000200 /* enable fxsave/fxrestor and SSE */
#define CR4_OSXMMEXCPT	0x00000400 /* enable unmasked SSE exceptions */
#define CR4_UMIP	0x00000800 /* user-mode instruction prevention */
#define CR4_VMXE	0x00002000 /* enable VMX operations */
#define CR4_SMXE	0x00004000 /* enable SMX operations */
#define CR4_FSGSBASE	0x00010000 /* enable *FSBASE and *GSBASE instructions */
#define CR4_PCIDE	0x00020000 /* enable Process Context IDentifiers */
#define CR4_OSXSAVE	0x00040000 /* enable xsave and xrestore */
#define CR4_SMEP	0x00100000 /* enable SMEP support */
#define CR4_SMAP	0x00200000 /* enable SMAP support */
#define CR4_PKE		0x00400000 /* protection key enable */

/*
 * MSRs
 */
#define MSR_EFER	0xc0000080		/* Extended feature enable */
#define 	EFER_SCE	0x00000001	/* SYSCALL extension */
#define 	EFER_LME	0x00000100	/* Long Mode Active */
#define 	EFER_LMA	0x00000400	/* Long Mode Enabled */
#define 	EFER_NXE	0x00000800	/* No-Execute Enabled */
#define 	EFER_SVME	0x00001000	/* Secure Virtual Machine En. */
#define 	EFER_LMSLE	0x00002000	/* Long Mode Segment Limit E. */
#define 	EFER_FFXSR	0x00004000	/* Fast FXSAVE/FXRSTOR En. */
#define 	EFER_TCE	0x00008000	/* Translation Cache Ext. */

#define MSR_STAR	0xc0000081		/* 32 bit syscall gate addr */
#define MSR_LSTAR	0xc0000082		/* 64 bit syscall gate addr */
#define MSR_CSTAR	0xc0000083		/* compat syscall gate addr */
#define MSR_SFMASK	0xc0000084		/* flags to clear on syscall */

#define MSR_FSBASE	0xc0000100		/* 64bit offset for fs: */
#define MSR_GSBASE	0xc0000101		/* 64bit offset for gs: */
#define MSR_KERNELGSBASE 0xc0000102		/* storage for swapgs ins */

#define MSR_APICBASE		0x01b
#define 	APICBASE_BSP		0x00000100	/* boot processor */
#define 	APICBASE_EXTD		0x00000400	/* x2APIC mode */
#define 	APICBASE_EN		0x00000800	/* software enable */
#define 	APICBASE_PHYSADDR	0xfffff000	/* physical address */

/*
 * CPUID
 */
/* Fn00000001 %edx features */
#define CPUID_FPU	0x00000001	/* processor has an FPU? */
#define CPUID_VME	0x00000002	/* has virtual mode (%cr4's VME/PVI) */
#define CPUID_DE	0x00000004	/* has debugging extension */
#define CPUID_PSE	0x00000008	/* has 4MB page size extension */
#define CPUID_TSC	0x00000010	/* has time stamp counter */
#define CPUID_MSR	0x00000020	/* has mode specific registers */
#define CPUID_PAE	0x00000040	/* has phys address extension */
#define CPUID_MCE	0x00000080	/* has machine check exception */
#define CPUID_CX8	0x00000100	/* has CMPXCHG8B instruction */
#define CPUID_APIC	0x00000200	/* has enabled APIC */
#define CPUID_B10	0x00000400	/* reserved, MTRR */
#define CPUID_SEP	0x00000800	/* has SYSENTER/SYSEXIT extension */
#define CPUID_MTRR	0x00001000	/* has memory type range register */
#define CPUID_PGE	0x00002000	/* has page global extension */
#define CPUID_MCA	0x00004000	/* has machine check architecture */
#define CPUID_CMOV	0x00008000	/* has CMOVcc instruction */
#define CPUID_PAT	0x00010000	/* Page Attribute Table */
#define CPUID_PSE36	0x00020000	/* 36-bit PSE */
#define CPUID_PN	0x00040000	/* processor serial number */
#define CPUID_CFLUSH	0x00080000	/* CLFLUSH insn supported */
#define CPUID_B20	0x00100000	/* reserved */
#define CPUID_DS	0x00200000	/* Debug Store */
#define CPUID_ACPI	0x00400000	/* ACPI performance modulation regs */
#define CPUID_MMX	0x00800000	/* MMX supported */
#define CPUID_FXSR	0x01000000	/* fast FP/MMX save/restore */
#define CPUID_SSE	0x02000000	/* streaming SIMD extensions */
#define CPUID_SSE2	0x04000000	/* streaming SIMD extensions #2 */
#define CPUID_SS	0x08000000	/* self-snoop */
#define CPUID_HTT	0x10000000	/* Hyper-Threading Technology */
#define CPUID_TM	0x20000000	/* thermal monitor (TCC) */
#define CPUID_IA64	0x40000000	/* IA-64 architecture */
#define CPUID_SBF	0x80000000	/* signal break on FERR */

