/*
 * hal_interrupt.c - Implementation of interrupt handler for x86_64
 *
 * Copyright (c) 2017 Maxime Villard
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <kernel_config.h>
#include <thread.h>
#include <do_interrupt.h>
#include <hal_interrupt.h>

#include <hal_kentry.h>
#include <hal_apic.h>
#include <hal_internal.h>
#include <hal_special.h>
#include <hal_segmentation.h>

/* -------------------------------------------------------------------------- */

/*
 * Timer interrupt
 */
void hal_timer_intr(hal_trapframe_t *tf)
{
	core_t *core = CURRENT_THREAD->core;

	if (hal_get_gid() == 0) {
		/* print the message only for cpu0 */
		x86_printf("-> got timer: cpu%z rip=%Z (th=%Z)\n", hal_get_gid(),
		    tf->tf_rip, hal_get_current_thread());
	}

	core_clock(core);
}

/* -------------------------------------------------------------------------- */

/*
 * Serial Port (COM1) interrupt
 */
void hal_com1_intr(hal_trapframe_t *tf)
{
	static char prev;
	uint8_t chan;
	char c;

	if (prev & 0x80) {
		/* the previous char was the channel number (tty) */
		c = hal_com_read();
		chan = prev & 0x7F;
		x86_printf("-> got com [%z,'%c']\n", (uint64_t)chan, c);
		prev = 0;
	} else {
		prev = hal_com_read();
	}

	return;
}

/* -------------------------------------------------------------------------- */

#define PS2_DATA	0x60

#define PS2_STATUS	0x64
#	define STATUS_OUT_FULL	0x00
#	define STATUS_IN_FULL	0x01
#	define STATUS_SYS_FLAG	0x02

#define PS2_CMD	0x64

/*
 * US scancode table, found on the internet.
 */
unsigned char scancode_US[128] =
{
	 0,     27, '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=',
	'\b', '\t', 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']',
	'\n',
	 0,	/* 29   - Control */
	'a',   's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '\'', '`',
	 0,	/* Left shift */
	'\\',  'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '/',
	 0,	/* Right shift */
	'*',
	 0,	/* Alt */
	' ',	/* Space bar */
	 0,	/* Caps lock */
	 0,	/* 59 - F1 key ... > */
	 0,   0,   0,   0,   0,   0,   0,   0,
	 0,	/* < ... F10 */
	 0,	/* 69 - Num lock*/
	 0,	/* Scroll Lock */
	 0,	/* Home key */
	 0,	/* Up Arrow */
	 0,	/* Page Up */
	'-',
	 0,	/* Left Arrow */
	 0,
	 0,	/* Right Arrow */
	'+',
	 0,	/* 79 - End key*/
	 0,	/* Down Arrow */
	 0,	/* Page Down */
	 0,	/* Insert Key */
	 0,	/* Delete Key */
	 0,   0,   0,
	 0,	/* F11 Key */
	 0,	/* F12 Key */
	 0,	/* All other keys are undefined */
};	

/*
 * Keyboard interrupt (8042 PS/2)
 */
void hal_keyboard_intr(hal_trapframe_t *tf)
{
	uint64_t val;

	do {
		val = in8(PS2_STATUS);
	} while ((val & STATUS_IN_FULL) == 0);

	val = in8(PS2_DATA);

	x86_printf("-> got keyboard: cpu%z rip=%Z key=%c ", hal_get_gid(),
	    tf->tf_rip, scancode_US[val]);

	if (val & 0x80)
		x86_printf("[released]\n");
	else
		x86_printf("[pressed]\n");

	return;
}


/* -------------------------------------------------------------------------- */

void hal_do_interrupt( thread_t * this,
		               reg_t    * regs_tbl )
{
	x86_panic((char *)__func__);
}
