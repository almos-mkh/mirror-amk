/*
 * hal_uspace.c - implementation of Generic User Space Access API for MIPS32
 *
 * Author  Mohamed Karaoui (2015)
 *         Alain Greiner   (2016)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH..
 *
 * ALMOS-MKH. is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH. is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH.; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_uspace.h>
#include <hal_irqmask.h>

#include <hal_internal.h>

void hal_copy_from_uspace(void *k_dst, void *u_src, uint32_t size)
{
	x86_panic((char *)__func__);
}

void hal_copy_to_uspace(void *u_dst, void *k_src, uint32_t size)
{
	x86_panic((char *)__func__);
}

uint32_t hal_strlen_from_uspace( char * u_str )
{
	x86_panic((char *)__func__);
	return 0;
}

void hal_strcpy_to_uspace(char *u_dst, char *k_src, uint32_t max_size)
{
	x86_panic((char *)__func__);
	return 0;
}

void hal_strcpy_from_uspace(char *k_dst, char *u_src, uint32_t max_size)
{
	x86_panic((char *)__func__);
	return 0;
}

