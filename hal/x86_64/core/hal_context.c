/*
 * hal_context.c - Implementation of Thread Context API for x86_64
 *
 * Copyright (c) 2017 Maxime Villard
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <memcpy.h>
#include <thread.h>
#include <string.h>
#include <process.h>
#include <printk.h>
#include <vmm.h>
#include <core.h>
#include <cluster.h>

#include <hal_context.h>
#include <hal_kentry.h>
#include <hal_internal.h>
#include <hal_segmentation.h>

error_t hal_cpu_context_create(struct thread_s *thread)
{
	hal_trapframe_t *tf;
	hal_cpu_context_t *ctx;
	uint64_t kstacktop;
	kmem_req_t req;

	kstacktop = (uint64_t)thread->k_stack_base + thread->k_stack_size;

	/* allocate memory for cpu_context */
	req.type  = KMEM_CPU_CTX;
	req.size  = sizeof(hal_cpu_context_t);
	req.flags = AF_KERNEL | AF_ZERO;

	ctx = (hal_cpu_context_t *)kmem_alloc(&req);
	if (ctx == NULL)
		return ENOMEM;

	/* set cpu context pointer in thread */
	thread->cpu_context = (void *)ctx;

	/*
	 * Build the context
	 */
	ctx->ctx_rsp0 = kstacktop & ~0xF;
	ctx->ctx_tf = (uint64_t)&ctx->ctx_hidden_tf;
	ctx->ctx_intr = INTRS_ENABLED;

	/*
	 * Build the trap frame
	 */
	tf = &ctx->ctx_hidden_tf;
	tf->tf_rdi = (uint64_t)thread->entry_args;
	tf->tf_gs = GDT_FIXED_SEL(GDT_UDATA_SEL, SEL_UPL);
	tf->tf_fs = GDT_FIXED_SEL(GDT_UDATA_SEL, SEL_UPL);
	tf->tf_es = GDT_FIXED_SEL(GDT_UDATA_SEL, SEL_UPL);
	tf->tf_ds = GDT_FIXED_SEL(GDT_UDATA_SEL, SEL_UPL);
	tf->tf_rip = (uint64_t)thread->entry_func;
	tf->tf_rflags = PSL_USERSET;

	if (thread->type == THREAD_USER) {
		tf->tf_cs = GDT_FIXED_SEL(GDT_UCODE_SEL, SEL_UPL);
		tf->tf_ss = GDT_FIXED_SEL(GDT_UDATA_SEL, SEL_UPL);
		tf->tf_rsp = ((uint64_t)thread->u_stack_base) +
		    thread->u_stack_size;
	} else {
		tf->tf_cs = GDT_FIXED_SEL(GDT_KCODE_SEL, SEL_KPL);
		tf->tf_ss = GDT_FIXED_SEL(GDT_KDATA_SEL, SEL_KPL);
		tf->tf_rsp = kstacktop;
	}

	return 0;
}

error_t hal_cpu_context_copy(thread_t *dst, thread_t *src)
{
	x86_panic((char *)__func__);
	return 0;
}

void hal_cpu_context_destroy(thread_t *thread)
{
	x86_panic((char *)__func__);
}

void hal_cpu_context_switch(thread_t *old, thread_t *new)
{
	process_t *oldproc = old->process;
	process_t *newproc = new->process;

	curtls()->tls_thr = new;

	/* Switch the VM space */
	if (newproc != oldproc) {
		lcr3((uint64_t)newproc->vmm.gpt.ppn << CONFIG_PPM_PAGE_SHIFT);
	}

	/* Switch the CPU context */
	cpu_context_switch(old->cpu_context, new->cpu_context);
}

void hal_cpu_context_load( thread_t * thread )
{
	x86_panic((char *)__func__);
}

error_t hal_fpu_context_create( thread_t * thread )
{
	/* FPU not implemented yet */
	return 0;
}

error_t hal_fpu_context_copy( thread_t * dst,
                              thread_t * src )
{
	x86_panic((char *)__func__);
	return 0;
}

void hal_fpu_context_destroy( thread_t * thread )
{
	x86_panic((char *)__func__);
}

void hal_fpu_context_save( thread_t * thread )
{
	x86_panic((char *)__func__);
}

void hal_fpu_context_restore( thread_t * thread )
{
	x86_panic((char *)__func__);
}

void hal_fpu_context_dup( xptr_t dst,
                          xptr_t src )
{
	x86_panic((char *)__func__);
}

