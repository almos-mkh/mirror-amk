/*
 * hal_ppm.c - Generic Physical Page Manager API implementation for x86
 *
 * Copyright (c) 2017 Maxime Villard
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <kernel_config.h>
#include <hal_kernel_types.h>
#include <hal_ppm.h>
#include <hal_special.h>
#include <printk.h>
#include <spinlock.h>
#include <process.h>
#include <ppm.h>
#include <thread.h>
#include <cluster.h>
#include <page.h>

#include <hal_boot.h>
#include <hal_internal.h>

error_t hal_ppm_init(boot_info_t *info)
{
	boot_rsvd_t *rsvd;
	size_t i, j;

	// get relevant info from boot_info structure
	uint32_t pages_nr         = info->pages_nr;
	uint32_t pages_tbl_offset = info->pages_offset;
	uint32_t rsvd_nr          = info->rsvd_nr;

	// get pointer on local Physical Page Manager
	ppm_t * ppm = &LOCAL_CLUSTER->ppm;

	// initialize lock protecting the free_pages[] lists
	spinlock_init( &ppm->free_lock );

	// initialize lock protecting the dirty_pages list
	spinlock_init( &ppm->dirty_lock );

	// initialize all free_pages[] lists as empty
	for( i = 0 ; i < CONFIG_PPM_MAX_ORDER ; i++ )
	{
		list_root_init( &ppm->free_pages_root[i] );
		ppm->free_pages_nr[i] = 0;
	}

	// initialize dirty_list as empty
	list_root_init( &ppm->dirty_root );

	// compute size of pages_tbl[] array rounded to an integer number of pages
	uint32_t bytes = ARROUND_UP( pages_nr * sizeof(page_t), CONFIG_PPM_PAGE_SIZE );

	// compute number of pages required to store page descriptor array
	uint32_t pages_tbl_nr = bytes >> CONFIG_PPM_PAGE_SHIFT;

	// compute total number of reserved pages (kernel code & pages_tbl[])
	uint32_t reserved_pages = pages_tbl_offset + pages_tbl_nr;

	// initialize pages_nr, pages_tbl, and vaddr_base pointers
	ppm->pages_nr   = pages_nr;
	ppm->vaddr_base = (void *)CLUSTER_MIN_VA(0);
	ppm->pages_tbl  = (page_t *)hal_gpt_bootstrap_valloc(pages_tbl_nr);

	// make sure we respect the rule [VA = PA + constant_offset]
	XASSERT(ppm->pages_tbl == ppm->vaddr_base + pages_tbl_offset * PAGE_SIZE);

	// initialize all page descriptors in pages_tbl[]
	for( i = 0 ; i < pages_nr ; i++ )
	{
		page_init( &ppm->pages_tbl[i] );

		// TODO optimisation for this enormous loop on small pages:
		// make only a partial init with a memset, and complete the
		// initialisation when page is allocated [AG]
	}

	/*
	 * Set the PG_RESERVED flag for reserved pages (kernel, pages_tbl[] and
	 * memory holes).
	 */
	for (i = 0; i < reserved_pages; i++) {
		page_set_flag(&ppm->pages_tbl[i], PG_RESERVED);
	}
	for (i = 0; i < rsvd_nr; i++) {
		rsvd = &info->rsvd[i];
		for (j = 0; j < rsvd->npages; j++) {
			page_set_flag(&ppm->pages_tbl[rsvd->first_page + j], PG_RESERVED);
		}
	}

	/* Release all other pages to populate the free lists */
	for (i = reserved_pages; i < pages_nr; i++) {
		if (!page_is_flag(&ppm->pages_tbl[i], PG_RESERVED))
			ppm_free_pages_nolock(&ppm->pages_tbl[i]);
	}

	/* Check consistency */
	return ppm_assert_order(ppm);
}

