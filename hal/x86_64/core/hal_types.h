/*
 * hal_kernel_types.h - common kernel types for x86_64
 *
 * Author  Alain Greiner (2016)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef HAL_TYPES_H_
#define HAL_TYPES_H_

#include <kernel_config.h>

#define HAL_64BIT

#ifndef NULL
#define NULL (void*)        0
#endif

#define	__packed	__attribute__((__packed__))
#define __arraycount(a) (sizeof(a) / sizeof(*(a)))
#define __in_kdata	__attribute__((section(".kdata")))

#define __HAL_x86_64__ 1

/**************************************************************************
 *                      Exact-width integer types.
 **************************************************************************/

typedef   signed           char      int8_t;
typedef unsigned           char     uint8_t;

typedef   signed           short    int16_t;
typedef unsigned           short   uint16_t;

typedef          int                int32_t;
typedef unsigned int               uint32_t;

typedef   signed long long int      int64_t;
typedef unsigned long long int     uint64_t;

typedef uint64_t                     size_t;

typedef uint64_t                    vaddr_t; // XXX
typedef uint64_t                 pt_entry_t; // XXX

/***************************************************************************
 * Boolean type and macros
 **************************************************************************/

typedef uint32_t      bool_t;

#define false         0
#define true          1

/***************************************************************************
 * Kernel error type : signed
 **************************************************************************/

typedef int32_t       error_t;

/***************************************************************************
 * Time related type
 **************************************************************************/

typedef uint64_t      cycle_t;   // for cycle counters

/***************************************************************************
 * Global Process identifier type / fixed format
 * 16 MSB = cluster XY identifier
 * 16 LSB = local process index
 **************************************************************************/

typedef uint32_t      pid_t;

/***************************************************************************
 * Local Process index
 **************************************************************************/

typedef uint16_t      lpid_t;

/***************************************************************************
 * Thread  identifier type / fixed format
 * 16 MSB = cluster XY identifier
 * 16 LSB = local thread index
 **************************************************************************/

typedef uint32_t      trdid_t;

/***************************************************************************
 * Local Thread index
 **************************************************************************/

typedef uint16_t      ltid_t;

/***************************************************************************
 * User identifier type
 **************************************************************************/

typedef uint32_t      uid_t;

/***************************************************************************
 * CPU identifier types
 **************************************************************************/

typedef uint16_t      lid_t;    // local index in cluster
typedef uint32_t      gid_t;    // global (hardware) identifier

/***************************************************************************
 * File Descriptor Index in File Descriptor Array
 **************************************************************************/

typedef uint32_t      fdid_t;

/***************************************************************************
 * This structure defines a single 32 bits integer alone in a cache line.
 **************************************************************************/

typedef struct cache_line_s
{
	union
	{
		uint32_t values[CONFIG_CACHE_LINE_SIZE>>2];
		uint32_t value;
	};
}
__attribute__((packed)) cacheline_t;

#define CACHELINE_ALIGNED __attribute__((aligned(CONFIG_CACHE_LINE_SIZE)))

/***************************************************************************
 *  Address types and macros        !!! hardware dependent !!!
 ***************************************************************************
 * An extended pointer is a 64 bits integer, structured in two fields :
 * - cxy : cluster identifier.
 * - ptr : pointer in the virtual space of a single cluster.
 *
 * In Intel 64 bits, the kernel virtual space has 64 Gbytes per cluster
 * - the cxy field occupies bits[43:36]
 * - the ptr field occupies bits[35:0]
 ***************************************************************************
 * A physical address is a 64 bits integer, structured in two fields :
 * - cxy : cluster identifier.
 * - lpa : local physical address inside cluster.
 *
 * In Intel 64 bits, the physical space has 8 Gbytes per cluster.
 * - the cxy field occupies bits[40:33]
 * - the lpa field occupies bits[32:0]
 **************************************************************************/

typedef uint64_t               reg_t;          // core register

typedef uint64_t               xptr_t;         // extended pointer

typedef uint16_t               cxy_t;          // cluster identifier

typedef uint64_t               paddr_t;        // global physical address (64 bits)

typedef uint64_t               lpa_t;          // local physical address

typedef uint64_t               intptr_t;       // local pointer stored as integer

typedef uint64_t               ppn_t;          // Physical Page Number

typedef uint64_t               vpn_t;          // Virtual Page number

#define XPTR_NULL              0

/* virtual */
#define HAL_VA_BASE            0xffff800000000000ULL
#define PTR_MASK               0x0000000FFFFFFFFFULL
#define CXY_MASK               0x00000FF000000000ULL
#define PTR_SHIFT              36
#define GET_CXY(xp)            ((cxy_t)(((xp) & CXY_MASK) >> PTR_SHIFT))

#define GET_PTR(xp)            ((void *)((uint64_t)HAL_VA_BASE | \
                                ((uint64_t)(local_cxy) << PTR_SHIFT) | \
                                (((uint64_t)(xp)) & PTR_MASK)))

#define XPTR(cxy,ptr)          ((uint64_t)HAL_VA_BASE | \
                                ((uint64_t)(cxy) << PTR_SHIFT) | \
                                (((uint64_t)(ptr)) & PTR_MASK))

/* physical */
#define LPA_MASK               0x00000001FFFFFFFFULL
#define LPA_SHIFT              33
#define CXY_FROM_PADDR(paddr)  ((cxy_t)((paddr) >> LPA_SHIFT))
#define LPA_FROM_PADDR(paddr)  (lpa_t)(paddr & LPA_MASK)
#define PADDR(cxy,lpa)         (((uint64_t)(cxy) << LPA_SHIFT) | (((uint64_t)(lpa)) & LPA_MASK))

#endif	/* HAL_TYPES_H_ */
