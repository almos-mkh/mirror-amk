/*
 * hal_remote.c - implementation of Generic Remote Access API x86
 *
 * Copyright (c) 2017 Maxime Villard
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_internal.h>

#include <string.h>

void hal_remote_sb(xptr_t xp, uint8_t data)
{
	*(uint8_t *)xp = data;
}

void hal_remote_s32(xptr_t xp, uint32_t data)
{
	*(uint32_t *)xp = data;
}

void hal_remote_s64(xptr_t xp, uint64_t data)
{
	*(uint64_t *)xp = data;
}

void hal_remote_spt(xptr_t xp, void *pt)
{
	hal_remote_s64(xp, (uint64_t)pt);
}

uint8_t hal_remote_lb(xptr_t xp)
{
	return *(uint8_t *)xp;
}

uint32_t hal_remote_l32(xptr_t xp)
{
	return *(uint32_t *)xp;
}

uint64_t hal_remote_l64(xptr_t xp)
{
	return *(uint64_t *)xp;
}

void *hal_remote_lpt(xptr_t xp)
{
	return (void *)hal_remote_l64(xp);
}

bool_t hal_remote_atomic_cas(xptr_t xp, uint32_t old, uint32_t new)
{
	return (atomic_cas_32((volatile uint32_t *)xp, old, new) == old);
}

uint32_t hal_remote_atomic_add(xptr_t xp, uint32_t incr)
{
	return atomic_add_32((volatile uint32_t *)xp, incr);
}

uint32_t hal_remote_atomic_and(xptr_t xp, uint32_t mask)
{
	return atomic_and_32((volatile uint32_t *)xp, mask);
}

uint32_t hal_remote_atomic_or(xptr_t xp, uint32_t mask)
{
	return atomic_or_32((volatile uint32_t *)xp, mask);
}

error_t hal_remote_atomic_try_add(xptr_t xp, uint32_t incr, uint32_t *old)
{
	x86_panic((char *)__func__);
	return 0;
}

void hal_remote_memcpy(xptr_t dst, xptr_t src, uint32_t size)
{
	memcpy((void *)dst, (void *)src, size);
}

void hal_remote_strcpy(xptr_t dst, xptr_t src)
{
	strcpy((void *)dst, (void *)src);
}

void hal_remote_memset(xptr_t buf_xp, uint8_t byte, uint32_t size)
{
	memset((void *)buf_xp, byte, size);
}

