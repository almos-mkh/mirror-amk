/*
 * hal_irqmask.c - implementation of Generic IRQ Masking API for x86
 *
 * Copyright (c) 2017 Maxime Villard
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_internal.h>
#include <hal_segmentation.h>

inline void hal_disable_irq(reg_t *old)
{
	tls_t *tls = curtls();

	if (old)
		*old = tls->tls_intr;
	tls->tls_intr = INTRS_DISABLED;

	cli();
}

inline void hal_enable_irq(reg_t *old)
{
	tls_t *tls = curtls();

	if (old)
		*old = tls->tls_intr;
	tls->tls_intr = INTRS_ENABLED;

	sti();
}

inline void hal_restore_irq(reg_t old)
{
	tls_t *tls = curtls();

	tls->tls_intr = old;

	if (old == INTRS_ENABLED)
		sti();
	else if(old == INTRS_DISABLED)
		cli();
	else
		x86_panic("hal_restore_irq: not reachable");
}

