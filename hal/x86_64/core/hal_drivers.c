/*
 * hal_drivers.c - Driver initializers for x86_64
 *
 * Copyright (c) 2017 Maxime Villard
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <chdev.h>
#include <hal_drivers.h>
#include <hal_internal.h>

#include <ioc_ata.h>
#include <pic_apic.h>
#include <txt_rs232.h>

void hal_drivers_txt_init(chdev_t *txt, uint32_t impl)
{
	if (impl != IMPL_TXT_RS2) {
		x86_panic("undefined TXT device implementation");
	}

	txt_rs232_init(txt);
}

/* -------------------------------------------------------------------------- */

void hal_drivers_pic_init(chdev_t *pic, uint32_t impl)
{
	if (impl != IMPL_PIC_I86) {
		x86_panic("undefined PIC device implementation");
	}

	pic_apic_init(pic);

	/* update the PIC chdev extension */
	pic->ext.pic.enable_timer = &pic_apic_enable_timer;
	pic->ext.pic.enable_ipi   = &pic_apic_enable_ipi;
	pic->ext.pic.enable_irq   = &pic_apic_enable_irq;
	pic->ext.pic.disable_irq  = &pic_apic_disable_irq;
	pic->ext.pic.bind_irq     = &pic_apic_bind_irq;
	pic->ext.pic.send_ipi     = &pic_apic_send_ipi;
	pic->ext.pic.extend_init  = &pic_apic_extend_init;
}

/* -------------------------------------------------------------------------- */

void hal_drivers_iob_init(chdev_t *iob, uint32_t impl)
{
	x86_panic("IOB driver not implemented");
}

/* -------------------------------------------------------------------------- */

void hal_drivers_ioc_init(chdev_t *ioc, uint32_t impl)
{
	ioc_ata_init(ioc);
}

/* -------------------------------------------------------------------------- */

void hal_drivers_mmc_init(chdev_t *mmc, uint32_t impl)
{
	x86_panic("MMC driver not implemented");
}

/* -------------------------------------------------------------------------- */

void hal_drivers_nic_init(chdev_t *nic, uint32_t impl)
{
	x86_panic("NIC driver not implemented");
}

/* -------------------------------------------------------------------------- */

void hal_drivers_dma_init(chdev_t *dma, uint32_t impl)
{
	x86_panic("DMA driver not implemented");
}

