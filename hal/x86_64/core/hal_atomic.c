/*
 * hal_atomic.c - implementation of Generic Atomic Operations API for x86
 *
 * Copyright (c) 2017 Maxime Villard
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_internal.h>

void hal_atomic_and(uint32_t *ptr, int32_t val)
{
	atomic_and_32((volatile uint32_t *)ptr, val);
}

void hal_atomic_or(uint32_t *ptr, int32_t val)
{
	atomic_or_32((volatile uint32_t *)ptr, val);
}

uint32_t hal_atomic_add(void *ptr, int32_t val)
{
	return atomic_add_32((volatile uint32_t *)ptr, val);
}

bool_t hal_atomic_cas(uint32_t *ptr, uint32_t old, uint32_t new)
{
	return (atomic_cas_32((volatile uint32_t *)ptr, old, new) == old);
}

bool_t hal_atomic_test_set(uint32_t *ptr, int32_t val)
{
	x86_panic((char *)__func__);
	return 0;
}

