/*
 * pic_apic.c - APIC PIC driver definitions
 *
 * Copyright (c) 2017 Maxime Villard
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _PIC_APIC_H_
#define _PIC_APIC_H_

#include <hal_kernel_types.h>

void pic_apic_init(chdev_t *pic);
void pic_apic_extend_init(uint32_t *xcu_base);
void pic_apic_bind_irq(lid_t lid, chdev_t *src_chdev);
void pic_apic_enable_irq(lid_t lid, xptr_t src_chdev_xp);
void pic_apic_disable_irq(lid_t lid, xptr_t src_chdev_xp);
void pic_apic_enable_timer(uint32_t period);
void pic_apic_enable_ipi( void );
void pic_apic_send_ipi(cxy_t cxy, lid_t lid);

#endif	/* _PIC_APIC_H_ */
