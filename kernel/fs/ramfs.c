/*
 * ramfs.c  RAMFS file system API implementation.
 *
 * Authors   Mohamed Lamine Karaoui (2014,2015)
 *           Alain Greiner (2016,2017)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */


#include <hal_kernel_types.h>
#include <hal_special.h>
#include <vfs.h>
#include <cluster.h>
#include <ramfs.h>


//////////////////////////////////////////////
error_t ramfs_mount( xptr_t   parent_inode_xp,
                     char   * ramfs_root_name )
{
    xptr_t        dentry_xp;     // unused but required by vfs_add_child_in_parent() 
    xptr_t        inode_xp;
    vfs_inode_t * inode_ptr;
 
    cxy_t     cxy = cluster_random_select();

    // create VFS dentry and VFS inode for RAMFS root directory
    return  vfs_add_child_in_parent( cxy,
                                     FS_TYPE_RAMFS,
                                     parent_inode_xp,
                                     ramfs_root_name,
                                     &dentry_xp,
                                     &inode_xp );
    // update inode type field
    inode_ptr = GET_PTR( inode_xp );
    inode_ptr->type = INODE_TYPE_DIR;
}

