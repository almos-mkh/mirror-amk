/*
 * devfs.h - DEVFS File System API definition                                
 *
 * Authors       Mohamed Lamine Karaoui (2014,2015)
 *               Alain Greiner (2016,2017)
 *
 * Copyright (c) 2011,2012 UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _DEVFS_H_
#define _DEVFS_H_

//////////////////////////////////////////////////////////////////////////////////////////
// The DEVFS File System contains inodes and dentries associated to all chdev descriptors 
// availables in the architecture. 
//
// It is structured as a three levels tree structure :
// - The "dev" directory inode is stored in cluster_IO. It is the root of the DEVFS
//   file system. The parent inode is the "/" VFS root.
// - There is one "internal_cxy" directory inode per cluster, that is the parent of
//   the local children inodes associated to the local, internal chdev descriptors. 
//   The parent dentry is stored in cluster_IO.
// - The I/O cluster contains one "external" directory inode, that is the parent of 
//   the remote children inodes associated to the remote external chdev descriptors.
//   The parent dentry is stored in cluster_IO.
//
// The DEVFS extensions to the generic VFS are the following:
// 1) The vfs_ctx_t "extend" void* field is pointing on the devfs_ctx_t structure.
//    This structure contains two extended pointers on the DEVFS "dev" directory inode,
//    and on the "external" directory inode.
// 2) The vfs_inode_t "extend" void* field is pointing on the chdev descriptor.
//////////////////////////////////////////////////////////////////////////////////////////

/*****************************************************************************************
 * This structure defines the DEVFS context extension.
 ****************************************************************************************/

typedef struct devfs_ctx_s
{
    xptr_t   dev_inode_xp;               /*! extended pointer on DEVFS root inode       */ 
    xptr_t   external_inode_xp;          /*! extended pointer on DEVFS external inode   */ 
}
devfs_ctx_t;


/*****************************************************************************************
 * This fuction allocates memory from local cluster for a DEVFS context descriptor.
 *****************************************************************************************
 * @ return a pointer on the created context / return NULL if failure.
 ****************************************************************************************/
devfs_ctx_t * devfs_ctx_alloc( void );

/*****************************************************************************************
 * This function initialises the DEVFS context from arguments values, and link it
 * to the relevant VFS context in the local cluster.
 *****************************************************************************************
 * @ devfs_ctx               : local pointer on DEVFS context.
 * @ devfs_dev_inode_xp      : [out] extended pointer on  <dev> inode.
 * @ devfs_external_inode_xp : [out] extended pointer on  <external> inode.
 ****************************************************************************************/
void devfs_ctx_init( devfs_ctx_t * devfs_ctx,
                     xptr_t        devfs_dev_inode_xp,
                     xptr_t        devfs_external_inode_xp );

/*****************************************************************************************
 * This function releases memory dynamically allocated for the DEVFS context.
 *****************************************************************************************
 * @ devfs_ctx   : local pointer on DEVFS context.
 ****************************************************************************************/
void devfs_ctx_destroy( devfs_ctx_t * devfs_ctx );

/*****************************************************************************************
 * This function start to create the DEVFS subtree. 
 * This function should be called once in the cluster containing the VFS parent inode.
 * More precisely, it creates in cluster 0 the "dev" and "external" DEVFS directories.
 * For each one, it creates the inode and link the associated dentry to parent inode.
 * The DEVFS root inode is linked to the VFS parent inode identified by <parent_inode_xp>.
 *****************************************************************************************
 * @ parent_inode_xp         : extended pointer on the parent VFS inode.
 * @ devfs_dev_inode_xp      : [out] extended pointer on created <dev> inode.
 * @ devfs_external_inode_xp : [out] extended pointer on created <external> inode.
 ****************************************************************************************/
void devfs_global_init( xptr_t   parent_inode_xp,
                        xptr_t * devfs_dev_inode_xp,
                        xptr_t * devfs_external_inode_xp );

/*****************************************************************************************
 * This function completes the initialisation of the DEVFS subtree.
 * It should be called once in each cluster.
 * 1. In each cluster (i), it creates the "internal" directory, 
 *    linked to the DEVFS "dev" parent directory.
 * 2. In each cluster (i), it creates - for each external chdev in cluster (i) - 
 *    a pseudo-file, linked to the DEVFS "external" parent directory.
 * 3. In each cluster (i), it creates - for each internal chdev in cluster (i) -
 *    a pseudo-file, linked to the DEVFS "internal" parent directory.
 *****************************************************************************************
 * @ devfs_dev_inode_xp      : extended pointer on DEVFS root inode.
 * @ devfs_external_inode_xp : extended pointer on DEVFS external inode.
 * @ devfs_internal_inode_xp : [out] extended pointer on created <internal> inode.
 ****************************************************************************************/
void devfs_local_init( xptr_t   devfs_dev_inode_xp,
                       xptr_t   devfs_external_inode_xp,
                       xptr_t * devfs_internal_inode_xp );
                       
/****************************************************************************************** 
 * This function is called by the sys_read() and sys_write() functions.
 * It moves <size> bytes between a TXT device, and an user space <u_buf> buffer. 
 * It uses the <file_xp> and <to_buffer> arguments, to call the relevant device function.
 * It uses a kernel buffer allocated in the calling thread kernel stack.
 * If the <size> argument is larger than CONFIG_TXT_KBUF_SIZE, the user buffer is split
 * in smaller chunks to be copied from the user buffer to this kernel buffer.
 ******************************************************************************************
 * @ to_buffer : device -> buffer if true / buffer -> device if false. 
 * @ file_xp   : extended pointer on the remote file descriptor.
 * @ u_buf     : user space buffer (virtual address).
 * @ size      : requested number of bytes from offset.
 * @ returns number of bytes actually moved if success / -1 if error.
 *****************************************************************************************/
int devfs_user_move( bool_t   to_buffer,
                     xptr_t   file_xp, 
                     char   * u_buf,
                     uint32_t size );

/******************************************************************************************
 * As the DEVFS is dynamically build in memory during kernel init, all relevant
 * information is contained in the Inode Tree. Therefore, this function simply
 * access the xhtab contained in the <inode> descriptor, to get the  dentries 
 * to be copied in the dirent <array>.
 ******************************************************************************************
 * @ inode      : [in]  local pointer on directory inode.
 * @ array      : [in]  local pointer on array of dirents.
 * @ max_dirent : [in]  max number of slots in dirent array.
 * @ min_dentry : [in]  index of first dentry to be copied into array.
 * @ detailed   : [in]  dynamic inode creation if true.
 * @ entries    : [out] number of dentries actually copied into array.
 * @ done       : [out] Boolean true when last entry found.
 * @ return 0 if success / return -1 if failure.
 *****************************************************************************************/
error_t devfs_get_user_dir( struct vfs_inode_s * inode,
                            struct dirent      * array,
                            uint32_t             max_dirent,
                            uint32_t             min_dentry,
                            bool_t               detailed,
                            uint32_t           * entries,
                            bool_t             * done );


#endif	/* _DEVFS_H_ */
