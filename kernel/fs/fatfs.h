/*
 * fatfs.h - FATFS file system API definition.
 *
 * Author    Mohamed Lamine Karaoui (2014,2015)
 *           Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _FATFS_H_
#define _FATFS_H_

#include <hal_kernel_types.h>
#include <remote_queuelock.h>
#include <vfs.h>
#include <dev_ioc.h>


///////////////////////////////////////////////////////////////////////////////////////////
// The FATFS File System implements a FAT32 read/write file system.
//
// The FATFS extensions to the generic VFS are the following:
//
// 1) The vfs_ctx_t "extend" field is a void* pointing on the fatfs_ctx_t structure.
//    This structure contains various general informations such as the total
//    number of sectors in FAT region, the number of bytes per sector, the number 
//    of sectors per cluster, the lba of FAT region, the lba of data region, or the
//    cluster index for the root directory. It contains also an extended pointer
//    on the FAT mapper.
//
// 2) The vfs_inode_t "extend" contains, for each inode,
//    the first FAT cluster index (after cast to intptr).
//
// 3) The vfs_dentry_t "extend" field contains, for each dentry, the entry index
//    in the FATFS directory (32 bytes per FATFS entry). 
///////////////////////////////////////////////////////////////////////////////////////////

/*************** Partition Boot Sector Format **********************************/
//                                     offset |  length
#define BS_JMPBOOT                          0 ,  3
#define BS_OEMNAME                          3 ,  8
#define BPB_BYTSPERSEC                     11 ,  2
#define BPB_SECPERCLUS                     13 ,  1
#define BPB_RSVDSECCNT                     14 ,  2
#define BPB_NUMFATS                        16 ,  1
#define BPB_ROOTENTCNT                     17 ,  2
#define BPB_TOTSEC16                       19 ,  2
#define BPB_MEDIA                          21 ,  1
#define BPB_FATSZ16                        22 ,  2
#define BPB_SECPERTRK                      24 ,  2
#define BPB_NUMHEADS                       26 ,  2
#define BPB_HIDDSEC                        28 ,  4
#define BPB_TOTSEC32                       32 ,  4
#define BPB_PARTITION_TABLE               446 , 64 

// FAT 32
#define BPB_FAT32_FATSZ32                  36 ,  4
#define BPB_FAT32_EXTFLAGS                 40 ,  2
#define BPB_FAT32_FSVER                    42 ,  2
#define BPB_FAT32_ROOTCLUS                 44 ,  4
#define BPB_FAT32_FSINFO                   48 ,  2
#define BPB_FAT32_BKBOOTSEC                50 ,  2
#define BS_FAT32_DRVNUM                    64 ,  1
#define BS_FAT32_BOOTSIG                   66 ,  1
#define BS_FAT32_VOLID                     67 ,  4
#define BS_FAT32_VOLLAB                    71 , 11
#define BS_FAT32_FILSYSTYPE                82 ,  8

// Partitions
#define FIRST_PARTITION_ACTIVE            446 ,  8
#define FIRST_PARTITION_BEGIN_LBA         454 ,  4
#define FIRST_PARTITION_SIZE              458 ,  4 
#define SECOND_PARTITION_ACTIVE           462 ,  8
#define SECOND_PARTITION_BEGIN_LBA        470 ,  4
#define SECOND_PARTITION_SIZE             474 ,  4
#define THIRD_PARTITION_ACTIVE            478 ,  8
#define THIRD_PARTITION_BEGIN_LBA         486 ,  4
#define THIRD_PARTITION_SIZE              490 ,  4
#define FOURTH_PARTITION_ACTIVE           494 ,  8
#define FOURTH_PARTITION_BEGIN_LBA        502 ,  4
#define FOURTH_PARTITION_SIZE             506 ,  4    
/*******************************************************************************/

#define MBR_SIGNATURE_POSITION            510 , 2
#define MBR_SIGNATURE_VALUE               0xAA55  

/************** FAT_FS_INFO SECTOR  ********************************************/
#define FS_SIGNATURE_VALUE_1              0x52526141
#define FS_SIGNATURE_VALUE_2              0x72724161
#define FS_SIGNATURE_VALUE_3              0x000055AA  
#define FS_SIGNATURE_POSITION_1           0   , 4  
#define FS_SIGNATURE_POSITION_2           484 , 4
#define FS_SIGNATURE_POSITION_3           508 , 4  
#define FS_FREE_CLUSTERS                  488 , 4
#define FS_FREE_CLUSTER_HINT              492 , 4
/*******************************************************************************/

#define DIR_ENTRY_SIZE          32
                   
#define NAME_MAX_SIZE           31

/******* SFN Directory Entry Structure (32 bytes) ******************************/
//                            offset | length
#define DIR_NAME                   0 , 11   // dir_entry name
#define DIR_ATTR                  11 ,  1   // attributes
#define DIR_NTRES                 12 ,  1   // reserved for the OS        
#define DIR_CRT_TIMES_TENTH       13 ,  1 
#define DIR_FST_CLUS_HI           20 ,  2   // cluster index 16 MSB bits
#define DIR_WRT_TIME              22 ,  2   // time of last write
#define DIR_WRT_DATE              24 ,  2   // date of last write
#define DIR_FST_CLUS_LO           26 ,  2   // cluster index 16 LSB bit
#define DIR_FILE_SIZE             28 ,  4   // dir_entry size (up to 4 Gbytes)
/*******************************************************************************/

/******* LFN Directory Entry Structure  (32 bytes) *****************************/
//                            offset | length
#define LDIR_ORD                   0 ,  1   // Sequence number (from 0x01 to 0x0f)    
#define LDIR_NAME_1                1 , 10   // name broken into 3 parts 
#define LDIR_ATTR                 11 ,  1   // attributes (must be 0x0F) 
#define LDIR_TYPE                 12 ,  1   // directory type (must be 0x00)
#define LDIR_CHKSUM               13 ,  1   // checksum of name in short dir  
#define LDIR_NAME_2               14 , 12 
#define LDIR_RSVD                 26 ,  2   // artifact of previous fat (must be 0)
#define LDIR_NAME_3               28 ,  4   
/*******************************************************************************/

/***********************  DIR_ATTR values  (attributes) ************************/
#define ATTR_READ_ONLY            0x01
#define ATTR_HIDDEN               0x02
#define ATTR_SYSTEM               0x04
#define ATTR_VOLUME_ID            0x08
#define ATTR_DIRECTORY            0x10
#define ATTR_ARCHIVE              0x20
#define ATTR_LONG_NAME_MASK       0x0f      // READ_ONLY|HIDDEN|SYSTEM|VOLUME_ID
/*******************************************************************************/

/********************* DIR_ORD special values **********************************/
#define FREE_ENTRY                0xE5     // this entry is free in the directory
#define NO_MORE_ENTRY             0x00     // no more entry in the directory
/*******************************************************************************/

/******************** CLuster Index Special Values *****************************/
#define FREE_CLUSTER              0x00000000
#define RESERVED_CLUSTER          0x00000001
#define BAD_CLUSTER               0x0FFFFFF7
#define END_OF_CHAIN_CLUSTER_MIN  0x0ffffff8
#define END_OF_CHAIN_CLUSTER_MAX  0x0fffffff
/*******************************************************************************/

/****  Forward declarations  ****/

struct mapper_s;
struct page_s;
struct vfs_ctx_s;
struct vfs_inode_s;
struct vfs_dentry_s;

/*****************************************************************************************
 * This structure defines a FATFS specific context (extension to VFS context).
 * This extension is replicated in all clusters.
 *
 * WARNING : Almost all fields are constant values, but the <free_cluster_hint> and
 * <free_clusters> are shared variables. All kernel instances use the variables
 * in cluster 0, using the <free_lock> remote busy_lock for exclusive access.
 ****************************************************************************************/

typedef struct fatfs_ctx_s
{
    uint32_t            fat_sectors_count;     /*! number of sectors in FAT region      */
    uint32_t            bytes_per_sector;      /*! number of bytes per sector           */
    uint32_t            sectors_per_cluster;   /*! number of sectors per cluster        */
    uint32_t            fat_begin_lba;         /*! lba of FAT region                    */
    uint32_t            cluster_begin_lba;     /*! lba of data region                   */
    uint32_t            fs_info_lba;           /*! lba of FS_INFO sector                */
    uint32_t            root_dir_cluster;      /*! cluster index for  root directory    */
    xptr_t              fat_mapper_xp;         /*! extended pointer on FAT mapper       */
    uint32_t            free_cluster_hint;     /*! start point to search free cluster   */
    uint32_t            free_clusters;         /*! free clusters number                 */
    remote_queuelock_t  free_lock;             /*! exclusive access to hint & number    */
}
fatfs_ctx_t;

//////////////////////////////////////////////////////////////////////////////////////////
//              FATFS specific extern functions  
//////////////////////////////////////////////////////////////////////////////////////////

/*****************************************************************************************
 * This function access the FAT (File Allocation Table), stored in the FAT mapper, and
 * returns in <searched_cluster> the FATFS cluster index for a given page of a given 
 * inode identified by the <first_cluster> and <page_id> arguments.
 * It can be called by a thread running in any cluster, as it uses remote access
 * primitives when the FAT mapper is remote.
 * The FAT is actually an array of uint32_t slots. Each slot in this array contains the 
 * index of another slot in this array, to form one linked list for each file stored on
 * device in the FATFS file system. This index in the FAT array is also the index of the
 * FATFS cluster on the device. One FATFS cluster is supposed to contain one PPM page. 
 * For a given file, the entry point in the FAT is simply the index of the FATFS cluster 
 * containing the first page of the file. The FAT mapper being a cache, this function 
 * updates the FAT mapper from informations stored on IOC device in case of miss.
 *****************************************************************************************
 * @ first_cluster   	 : [in]  index of first FATFS cluster allocated to the file.
 * @ page_id             : [in]  index of searched page in file.
 * @ searched_cluster    : [out] found FATFS cluster index.
 * @ return 0 if success / return -1 if a FAT mapper miss cannot be solved.
 ****************************************************************************************/
error_t fatfs_get_cluster( uint32_t   first_cluster,
                           uint32_t   page_id,
                           uint32_t * searched_cluster );

/*****************************************************************************************
 * This function display the content of the FATFS context.
 ****************************************************************************************/
void fatfs_ctx_display( void );

/*****************************************************************************************
 * This function displays the content of a part of the File Allocation Table.
 * It loads the requested page fom device to mapper if required.
 *****************************************************************************************
 * @ page_id   : page index in FAT mapper (one page is 4 Kbytes).
 * @ nentries  : number of entries (one entry is 4 bytes).
 ****************************************************************************************/
void fatfs_display_fat( uint32_t  page_id,
                        uint32_t  nentries );


//////////////////////////////////////////////////////////////////////////////////////////
// Generic API: These functions are called by the kernel VFS,
//              and must be implemented by all File Systems.
//////////////////////////////////////////////////////////////////////////////////////////

/*****************************************************************************************
 * This fuction allocates memory from local cluster for a FATFS context descriptor.
 *****************************************************************************************
 * @ return a pointer on the created context / return NULL if failure.
 ****************************************************************************************/
fatfs_ctx_t * fatfs_ctx_alloc( void );

/*****************************************************************************************
 * This function access the boot device, and initialises the local FATFS context
 * from informations contained in the boot record. 
 *****************************************************************************************
 * @ vfs_ctx   : local pointer on VFS context for FATFS.
 ****************************************************************************************/
void fatfs_ctx_init( fatfs_ctx_t * fatfs_ctx );

/*****************************************************************************************
 * This function releases memory dynamically allocated for the FATFS context extension.
 *****************************************************************************************
 * @ vfs_ctx   : local pointer on VFS context.
 ****************************************************************************************/
void fatfs_ctx_destroy( fatfs_ctx_t * fatfs_ctx );

/*****************************************************************************************
 * This function implements the generic vfs_fs_add_dentry() function for the FATFS.
 *****************************************************************************************
 * This function updates a directory identified by the <inode> argument
 * to add a new directory entry identified by the <dentry> argument.
 * All modified pages in directory mapper are synchronously updated on IOC device.
 * It must be called by a thread running in the cluster containing the inode.
 *
 * Implementation note : this function works in two steps:
 * - It scan the set of 32 bytes FATFS directry entries, using two embedded loops  
 *   to find the end of directory (NO_MORE_ENTRY marker).
 * - Then it writes 3, 4, or 5 directory entries (depending on the name length), using 
 *   a 5 steps FSM (one state per entry to be written), updates on IOC device the 
 *   modified pages, and updates the dentry extension field, that must contain 
 *   the dentry index in FATFS directory.
 *****************************************************************************************
 * @ inode    : local pointer on directory inode.
 * @ dentry   : local pointer on dentry.
 * @ return 0 if success / return ENOENT if not found, or EIO if no access to IOC device.
 ****************************************************************************************/
error_t fatfs_add_dentry( struct vfs_inode_s  * inode,
                          struct vfs_dentry_s * dentry );

/*****************************************************************************************
 * This function implements the generic vfs_fs_remove_dentry() function for the FATFS.
 *****************************************************************************************
 * This function updates a directory identified by the <inode> argument
 * to remove a directory entry identified by the <dentry> argument.
 * All modified pages in directory mapper are synchronously updated on IOC device.
 * It must be called by a thread running in the cluster containing the inode.
 *
 * Implementation note: this function uses the dentry extension to directly access 
 * the NORMAL directory entry and invalidate all involved LFN entries. Then it
 * updates the modified pages on IOC device.
 *****************************************************************************************
 * @ inode    : local pointer on directory inode.
 * @ dentry   : local pointer on dentry.
 * @ return 0 if success / return ENOENT if not found, or EIO if no access to IOC device.
 ****************************************************************************************/
error_t fatfs_remove_dentry( struct vfs_inode_s  * inode,
                             struct vfs_dentry_s * dentry );

/*****************************************************************************************
 * This function implements the generic vfs_fs_new_dentry() function for the FATFS.
 *****************************************************************************************
 * It initializes a new inode/dentry couple in Inode Tree, attached to the directory
 * identified by the <parent_inode> argument. The new directory entry is identified
 * by the <name> argument. The child inode descriptor identified by the <child_inode_xp>
 * argument, and the dentry descriptor must have been previously allocated.
 * It scan the parent mapper to find the <name> argument.
 * It set the "type", "size", and "extend" fields in inode descriptor.
 * It set the " extend" field in dentry descriptor.
 * It must be called by a thread running in the cluster containing the parent inode.
 *****************************************************************************************
 * @ parent_inode    : local pointer on parent inode (directory).
 * @ name            : child name.
 * @ child_inode_xp  : extended pointer on remote child inode (file or directory).
 * @ return 0 if success / return ENOENT if child not found.
 ****************************************************************************************/
error_t fatfs_new_dentry( struct vfs_inode_s * parent_inode,
                          char               * name,
                          xptr_t               child_inode_xp );

/*****************************************************************************************
 * This function implements the generic vfs_fs_update_dentry() function for the FATFS.
 *****************************************************************************************
 * It update the size of a directory entry identified by the <dentry> argument in
 * the mapper of a directory identified by the <inode> argument, as defined by the <size>
 * argument.
 * It scan the mapper to find the entry identified by the dentry "name" field.
 * It set the "size" field in the in the directory mapper AND marks the page as DIRTY.
 * It must be called by a thread running in the cluster containing the directory inode.
 *****************************************************************************************
 * @ inode        : local pointer on inode (directory).
 * @ dentry       : local pointer on dentry (for name).
 * @ size         : new size value.
 * @ return 0 if success / return ENOENT if child not found.
 ****************************************************************************************/
error_t fatfs_update_dentry( struct vfs_inode_s  * inode,
                             struct vfs_dentry_s * dentry,
                             uint32_t              size );

/*****************************************************************************************
 * This function implements the generic vfs_fs_get_user_dir() function for the FATFS.
 *****************************************************************************************
 * It is called by the remote_dir_create() function to scan the mapper of a directory 
 * identified by the <inode> argument, and copy up to <max_dirent> valid dentries to a 
 * local dirent array, defined by the <array> argument. The <min_dentry> argument defines
 * the index of the first dentry to be copied to the target dirent array.
 * This function returns in the <entries> buffer the number of dentries actually written,
 * and signals in the <done> buffer when the last valid entry has been found.
 * If the <detailed> argument is true, a dentry/inode couple that does not exist in
 * the Inode Tree is dynamically created, and all dirent fields are documented in the
 * dirent array. Otherwise, only the dentry name is documented.
 * It must be called by a thread running in the cluster containing the directory inode.
 *****************************************************************************************
 * @ inode      : [in]  local pointer on directory inode.
 * @ array      : [in]  local pointer on array of dirents.
 * @ max_dirent : [in]  max number of slots in dirent array.
 * @ min_dentry : [in]  index of first dentry to be copied into array.
 * @ detailed   : [in]  dynamic inode creation if true.
 * @ entries    : [out] number of dentries actually copied into array.
 * @ done       : [out] Boolean true when last entry found.
 * @ return 0 if success / return -1 if failure.
 ****************************************************************************************/
error_t fatfs_get_user_dir( struct vfs_inode_s * inode,
                            struct dirent      * array, 
                            uint32_t             max_dirent,
                            uint32_t             min_dentry,
                            bool_t               detailed,
                            uint32_t           * entries,
                            bool_t             * done );

/*****************************************************************************************
 * This function implements the generic vfs_fs_sync_inode() function for the FATFS.
 *****************************************************************************************
 * It updates the FATFS on the IOC device for a given inode identified by 
 * the <inode> argument. It scan all pages registered in the associated mapper,
 * and copies from mapper to device each page marked as dirty.
 * WARNING : The target <inode> cannot be a directory, because all modifications in a 
 * directory are synchronously done on the IOC device by the two fatfs_add_dentry() 
 * and fatfs_remove_dentry() functions.
 *****************************************************************************************
 * @ inode   : local pointer on inode.
 * @ return 0 if success / return EIO if failure during device access. 
 ****************************************************************************************/
error_t fatfs_sync_inode( struct vfs_inode_s * inode );

/*****************************************************************************************
 * This function implements the generic vfs_fs_sync_fat() function for the FATFS.
 *****************************************************************************************
 * It updates the FATFS on the IOC device for the FAT itself.
 * It scan all clusters registered in the FAT mapper, and copies from mapper to device 
 * each page marked as dirty.
 *
 * TODO : the current implementation check ALL pages in the FAT region, even if most
 * pages are empty, and not copied in mapper. It is sub-optimal.
 * - A first solution is to maintain in the FAT context two "dirty_min" and "dirty_max"
 *  variables defining the smallest/largest dirty page index in FAT mapper... 
 *****************************************************************************************
 * @ return 0 if success / return EIO if failure during device access.
 ****************************************************************************************/
error_t fatfs_sync_fat( void );

/*****************************************************************************************
 * This function implements the generic vfs_fs_sync_fsinfo() function for the FATFS.
 *****************************************************************************************
 * It updates the FS_INFO sector on the IOC device. 
 * It copies the <free_cluster_hint> and <free_clusters> variables from
 * the FATFS context in cluster 0 to the FS_INFO sector on device. 
 *****************************************************************************************
 * @ return 0 if success / return EIO if failure during device access.
 ****************************************************************************************/
error_t fatfs_sync_free_info( void );

/*****************************************************************************************
 * This function implements the generic vfs_fs_cluster_alloc() function for the FATFS.
 *****************************************************************************************
 * It access the FAT (File allocation table), stored in the FAT mapper, and returns
 * in <searched_cluster> the FATFS cluster index of a free cluster.
 * It can be called by a thread running in any cluster, as it uses remote access
 * primitives when the FAT mapper is remote. It takes the "free_lock" stored in the 
 * FATFS context located in the same cluster as the FAT mapper itself, to get exclusive
 * access to the FAT. It uses (and updates) the <free_cluster_hint> and <free_clusters> 
 * shared variables in this FATFS context.
 * It updates the FAT mapper, and synchronously updates the FAT region on IOC device. 
 * The FAT mapper being a cache, this function updates the FAT mapper from informations
 * stored on IOC device in case of miss.
 *****************************************************************************************
 * @ searched_cluster    : [out] found FATFS cluster index.
 * @ return 0 if success / return -1 if no more free clusters on IOC device.
 ****************************************************************************************/
error_t fatfs_cluster_alloc( uint32_t * searched_cluster );

/*****************************************************************************************
 * This function implements the generic vfs_fs_release_inode() function for the FATFS.
 *****************************************************************************************
 * It releases all clusters allocated to a file/directory identified by the <inode_xp>
 * argument. All released clusters are marked FREE_CLUSTER in the FAT mapper. 
 * This function calls the recursive function fatfs_cluster_release() to release 
 * the clusters in reverse order of the linked list (from last to first). 
 * When the FAT mapper has been updated, it calls the fatfs_sync_fat() function to
 * synchronously update all dirty pages in the FAT mapper to the IOC device.
 * Finally the FS-INFO sector on the IOC device is updated.
 *****************************************************************************************
 * @ inode_xp   : extended pointer on inode.
 * @ return 0 if success / return EIO if failure during device access.
 ****************************************************************************************/
error_t fatfs_release_inode( xptr_t inode_xp );

/*****************************************************************************************
 * This function implements the generic vfs_fs_move_page() function for the FATFS.
 *****************************************************************************************
 * This function moves a page from/to the mapper to/from the FATFS file system on device.
 * The page must have been previously allocated and registered in the mapper.   
 * The page - and the mapper - can be located in another cluster than the calling thread.
 * The pointer on the mapper and the page index in file are found in the page descriptor.
 * It is used for both a regular file/directory mapper, and the FAT mapper.
 * For the FAT mapper, it access the FATFS to get the location on IOC device.
 * For a regular file, it access the FAT mapper to get the cluster index on IOC device.
 * It can be called by any thread running in any cluster.
 *
 * WARNING : For the FAT mapper, the inode field in the mapper MUST be NULL, as this
 * is used to indicate that the corresponding mapper is the FAT mapper.
 *****************************************************************************************
 * @ page_xp   : extended pointer on page descriptor.
 * @ cmd_type  : IOC_READ / IOC_WRITE / IOC_SYNC_READ / IOC_SYNC_WRITE
 * @ return 0 if success / return EIO if error during device access. 
 ****************************************************************************************/
error_t fatfs_move_page( xptr_t      page_xp,
                         cmd_type_t  cmd_type );






#endif	/* _FATFS_H_ */
