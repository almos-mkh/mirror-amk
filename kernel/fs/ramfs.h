/*
 * ramfs.h  RAMFS file system API definition.
 *
 * Authors   Mohamed Lamine Karaoui (2014,2015)
 *           Alain Greiner (2016,2017)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _RAMFS_H_
#define _RAMFS_H_

///////////////////////////////////////////////////////////////////////////////////////////
// The RAMFS File System does not uses any external device to store data.
// It stores the dynamically created files and directories in the VFS mappers.
// The ramfs_read_page() and ramfs_write_page() functions should never be used.
// The RAMFS cannot be used as the root File System.
//
// There is no RAMFS context extension, and no RAMFS inode extension.
///////////////////////////////////////////////////////////////////////////////////////////



/******************************************************************************************
 * This function mount a RAMFS on a given inode of the root FS.
 * It actually creates a new VFS dentry in the cluster containing the parent inode,
 * and create a new VFS inode in another cluster.
 ******************************************************************************************
 * @ parent_inode_xp : extended pointer on the parent inode in VFS.
 * @ ramfs_root_name : RAMFS root directory name.
 *****************************************************************************************/
error_t ramfs_mount( xptr_t   parent_inode_xp,
                     char   * ramfs_root_name );

#endif	/* _RAMFS_H_ */
