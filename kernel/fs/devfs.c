/*
 * devfs.c - DEVFS File system API implementation.
 *
 * Author   Mohamed Lamine Karaoui (2014,2015)
 *          Alain Greiner (2016,2017,2018,2019)
 *
 * Copyright (c) Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_special.h>
#include <hal_uspace.h>
#include <printk.h>
#include <string.h>
#include <chdev.h>
#include <thread.h>
#include <dev_txt.h>
#include <cluster.h>
#include <vfs.h>
#include <kmem.h>
#include <devfs.h>

/////////////////////////////////////////////////////////////////////////////////////////
//     Extern variables
/////////////////////////////////////////////////////////////////////////////////////////

extern vfs_ctx_t            fs_context[];   // allocated in kernel_init.c
extern chdev_directory_t    chdev_dir;      // allocated in kernel_init.c

#if (DEBUG_SYS_READ & 1)
extern uint32_t  enter_devfs_read;
extern uint32_t  exit_devfs_read;
#endif

#if (DEBUG_SYS_WRITE & 1)
extern uint32_t  enter_devfs_write;
extern uint32_t  exit_devfs_write;
#endif

/////////////////////////////////////
devfs_ctx_t * devfs_ctx_alloc( void )
{
    kmem_req_t    req;

	req.type    = KMEM_DEVFS_CTX;
	req.size    = sizeof(devfs_ctx_t);
    req.flags   = AF_KERNEL | AF_ZERO;

	return (devfs_ctx_t *)kmem_alloc( &req );
}

/////////////////////////////////////////////
void devfs_ctx_init( devfs_ctx_t * devfs_ctx,
                     xptr_t        devfs_dev_inode_xp,
                     xptr_t        devfs_external_inode_xp )
{
    devfs_ctx->dev_inode_xp      = devfs_dev_inode_xp;
    devfs_ctx->external_inode_xp = devfs_external_inode_xp;

    fs_context[FS_TYPE_DEVFS].extend = devfs_ctx;
}

/////////////////////////////////////////////////
void devfs_ctx_destroy( devfs_ctx_t * devfs_ctx )
{
    kmem_req_t    req;

    req.type = KMEM_DEVFS_CTX;
    req.ptr  = devfs_ctx;
    kmem_free( &req );
}

/////////////////////////////////////////////////
void devfs_global_init( xptr_t   root_inode_xp,
                        xptr_t * devfs_dev_inode_xp,
                        xptr_t * devfs_external_inode_xp )
{
    error_t       error;
    xptr_t        unused_xp;   // required by vfs_add_child_in_parent()
    vfs_inode_t * inode;

    // create DEVFS "dev" inode in cluster 0
    error = vfs_add_child_in_parent( 0,                // cxy
                                     FS_TYPE_DEVFS,
                                     root_inode_xp,
                                     "dev",
                                     &unused_xp,
                                     devfs_dev_inode_xp );

    // update inode "type" field
    inode = GET_PTR( *devfs_dev_inode_xp );
    inode->type = INODE_TYPE_DIR;
 
    // create dentries <.> and <..> in <dev>
    error |= vfs_add_special_dentries( *devfs_dev_inode_xp,
                                       root_inode_xp );

    if( error )
    {
        printk("\n[PANIC] in %s : cannot create <dev> directory\n", __FUNCTION__ );
        hal_core_sleep();
    }

#if DEBUG_DEVFS_GLOBAL_INIT
uint32_t   cycle = (uint32_t)hal_get_cycles();
thread_t * this  = CURRENT_THREAD;
if( DEBUG_DEVFS_GLOBAL_INIT < cycle )
printk("\n[%s] thread[%x,%x] created <dev> inode / cycle %d\n",
__FUNCTION__, this->process->pid, this->trdid, cycle );
#endif

    // create DEVFS "external" inode in cluster 0
    error = vfs_add_child_in_parent( 0,               // cxy
                                     FS_TYPE_DEVFS,
                                     *devfs_dev_inode_xp,
                                     "external",
                                     &unused_xp,
                                     devfs_external_inode_xp );

    // update inode "type" field
    inode = GET_PTR( *devfs_external_inode_xp );
    inode->type = INODE_TYPE_DIR;
 
    // create dentries <.> and <..> in <external>
    error |= vfs_add_special_dentries( *devfs_external_inode_xp,
                                       *devfs_dev_inode_xp );

    if( error )
    {
        printk("\n[PANIC] in %s : cannot create <external> directory\n", __FUNCTION__ );
        hal_core_sleep();
    }

#if DEBUG_DEVFS_GLOBAL_INIT
cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEVFS_GLOBAL_INIT < cycle )
printk("\n[%s] thread[%x,%x] created <external> inode / cycle %d\n",
__FUNCTION__, this->process->pid, this->trdid, cycle );
#endif

}  // end devfs_global_init()

///////////////////////////////////////////////////
void devfs_local_init( xptr_t   devfs_dev_inode_xp,
                       xptr_t   devfs_external_inode_xp,
                       xptr_t * devfs_internal_inode_xp )
{
    char          node_name[16];
    xptr_t        chdev_xp;
    cxy_t         chdev_cxy;
    chdev_t     * chdev_ptr;
    xptr_t        inode_xp;
    vfs_inode_t * inode_ptr;
    uint32_t      channel;
    xptr_t        unused_xp;    // required by add_child_in_parent()
    error_t       error;

#if DEBUG_DEVFS_LOCAL_INIT 
uint32_t   cycle = (uint32_t)hal_get_cycles();
thread_t * this  = CURRENT_THREAD;
if( DEBUG_DEVFS_LOCAL_INIT < cycle )
printk("\n[%s] thread[%x,%x] enter / cycle %d\n",
__FUNCTION__, this->process->pid, this->trdid, cycle );
#endif

    // create "internal" directory 
    snprintf( node_name , 16 , "internal_%x" , local_cxy );

    error = vfs_add_child_in_parent( local_cxy,
                                     FS_TYPE_DEVFS,
                                     devfs_dev_inode_xp,
                                     node_name,
                                     &unused_xp,
                                     devfs_internal_inode_xp );

    // set inode "type" field
    inode_ptr = GET_PTR( *devfs_internal_inode_xp );
    inode_ptr->type = INODE_TYPE_DEV;
 
    // create dentries <.> and <..> in <internal>
    error |= vfs_add_special_dentries( *devfs_internal_inode_xp,
                                       devfs_dev_inode_xp );

    if( error )
    {
        printk("\n[PANIC] in %s : cannot create <internal> directory\n", __FUNCTION__ );
        hal_core_sleep();
    }

#if DEBUG_DEVFS_LOCAL_INIT 
cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEVFS_LOCAL_INIT < cycle )
printk("\n[%s] thread[%x,%x] created <%s> inode in cluster %x / cycle %d\n",
__FUNCTION__, this->process->pid, this->trdid, node_name, local_cxy, cycle );
#endif

    // create MMC chdev inode
    chdev_xp  = chdev_dir.mmc[local_cxy];
    if( chdev_xp != XPTR_NULL)
    {
        chdev_ptr = GET_PTR( chdev_xp );
        chdev_cxy = GET_CXY( chdev_xp );

        if( chdev_cxy != local_cxy )
        {
            printk("\n[PANIC] in %s : illegal MMC chdev in cluster %x\n",
            __FUNCTION__, local_cxy );
            hal_core_sleep();
        }

        error = vfs_add_child_in_parent( local_cxy,
                                         FS_TYPE_DEVFS,
                                         *devfs_internal_inode_xp,
                                         chdev_ptr->name,
                                         &unused_xp,
                                         &inode_xp );

        if( error )
        {
            printk("\n[PANIC] in %s : cannot create MMC inode in cluster %x\n",
            __FUNCTION__, local_cxy );
            hal_core_sleep();
        }

        // update child inode "extend" and "type" fields
        inode_ptr = GET_PTR( inode_xp );
        inode_ptr->extend = chdev_ptr;
        inode_ptr->type   = INODE_TYPE_DEV;
        
#if DEBUG_DEVFS_LOCAL_INIT
cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEVFS_LOCAL_INIT < cycle )
printk("\n[%s] thread[%x,%x] created <mmc> inode in cluster %x\n",
__FUNCTION__, this->process->pid, this->trdid, local_cxy, cycle );
#endif

    }

    // create DMA chdev inodes (one DMA channel per core)
    for( channel = 0 ; channel < LOCAL_CLUSTER->cores_nr ; channel++ )
    {
        chdev_xp  = chdev_dir.dma[channel];
        if( chdev_xp != XPTR_NULL)
        {
            chdev_ptr = GET_PTR( chdev_xp );
            chdev_cxy = GET_CXY( chdev_xp );

            if( chdev_cxy != local_cxy )
            {
                printk("\n[PANIC] in %s : illegal DMA chdev in cluster %x\n",
                __FUNCTION__, local_cxy );
                hal_core_sleep();
            }

            error = vfs_add_child_in_parent( local_cxy,
                                             FS_TYPE_DEVFS,
                                             *devfs_internal_inode_xp,
                                             chdev_ptr->name,
                                             &unused_xp,
                                             &inode_xp );
            if( error )
            {
                printk("\n[PANIC] in %s : cannot create DMA inode in cluster %x\n",
                __FUNCTION__, local_cxy );
                hal_core_sleep();
            }

            // update child inode "extend" and "type" fields
            inode_ptr = GET_PTR( inode_xp );
            inode_ptr->extend = chdev_ptr;
            inode_ptr->type   = INODE_TYPE_DEV;
        
#if DEBUG_DEVFS_LOCAL_INIT
cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEVFS_LOCAL_INIT < cycle )
printk("\n[%s] thread [%x,%x] created <dma[%d]> inode in cluster %x\n",
__FUNCTION__, this->process->pid, this->trdid, channel, local_cxy, cycle );
#endif
        }
    }

    // create an IOB inode in cluster containing IOB chdev
    chdev_xp = chdev_dir.iob;
    if( chdev_xp != XPTR_NULL )
    {
        chdev_cxy = GET_CXY( chdev_xp );
        chdev_ptr = GET_PTR( chdev_xp );

        if( chdev_cxy == local_cxy )
        {
            error = vfs_add_child_in_parent( local_cxy,
                                             FS_TYPE_DEVFS,
                                             devfs_external_inode_xp,
                                             chdev_ptr->name,
                                             &unused_xp,
                                             &inode_xp );
            if( error )
            {
                printk("\n[PANIC] in %s : cannot create IOB inode in cluster %x\n",
                __FUNCTION__, local_cxy );
                hal_core_sleep();
            }

            // update child inode "extend" and "type" fields
            inode_ptr = GET_PTR( inode_xp );
            inode_ptr->extend = chdev_ptr;
            inode_ptr->type   = INODE_TYPE_DEV;
        
#if DEBUG_DEVFS_LOCAL_INIT
cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEVFS_LOCAL_INIT < cycle )
printk("\n[%s] thread[%x,%x] created <iob> inode in cluster %x\n",
__FUNCTION__, this->process->pid, this->trdid, local_cxy, cycle );
#endif
        }
    }
        
    // create a PIC inode in cluster containing PIC chdev
    chdev_xp = chdev_dir.pic;
    if( chdev_xp != XPTR_NULL )
    {
        chdev_cxy = GET_CXY( chdev_xp );
        chdev_ptr = GET_PTR( chdev_xp );

        if( chdev_cxy == local_cxy )
        {
            error = vfs_add_child_in_parent( local_cxy,
                                             FS_TYPE_DEVFS,
                                             devfs_external_inode_xp,
                                             chdev_ptr->name,
                                             &unused_xp,
                                             &inode_xp );

            if( error )
            {
                printk("\n[PANIC] in %s : cannot create PIC inode in cluster %x\n",
                __FUNCTION__, local_cxy );
                hal_core_sleep();
            }

            // update child inode "extend" field
            inode_ptr = GET_PTR( inode_xp );
            inode_ptr->extend = chdev_ptr;
            inode_ptr->type   = INODE_TYPE_DEV;
        
#if DEBUG_DEVFS_LOCAL_INIT
cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEVFS_LOCAL_INIT < cycle )
printk("\n[%s] thread[%x,%x] created <pic> inode in cluster %x\n",
__FUNCTION__, this->process->pid, this->trdid, local_cxy, cycle );
#endif
        }
    }

    // create a TXT_RX inode in each cluster containing a TXT_RX chdev
    for( channel = 0 ; channel < CONFIG_MAX_TXT_CHANNELS ; channel++ )
    {
        chdev_xp = chdev_dir.txt_rx[channel];
        if( chdev_xp != XPTR_NULL )
        {
            chdev_cxy = GET_CXY( chdev_xp );
            chdev_ptr = GET_PTR( chdev_xp );

            if( chdev_cxy == local_cxy )
            {
                error = vfs_add_child_in_parent( local_cxy,
                                                 FS_TYPE_DEVFS,
                                                 devfs_external_inode_xp,
                                                 chdev_ptr->name,
                                                 &unused_xp,
                                                 &inode_xp );

                if( error )
                {
                    printk("\n[PANIC] in %s : cannot create TXT_RX inode in cluster %x\n",
                    __FUNCTION__, local_cxy );
                    hal_core_sleep();
                }

                // update child inode "extend" and "type" fields
                inode_ptr = GET_PTR( inode_xp );
                inode_ptr->extend = chdev_ptr;
                inode_ptr->type   = INODE_TYPE_DEV;
        
#if DEBUG_DEVFS_LOCAL_INIT
cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEVFS_LOCAL_INIT < cycle )
printk("\n[%s] thread[%x,%x] created <txt_rx[%d]> inode in cluster %x\n",
__FUNCTION__, this->process->pid, this->trdid, channel, local_cxy, cycle );
#endif
            }
        }
    }

    // create a TXT_TX inode in each cluster containing a TXT_TX chdev
    for( channel = 0 ; channel < CONFIG_MAX_TXT_CHANNELS ; channel++ )
    {
        chdev_xp = chdev_dir.txt_tx[channel];
        if( chdev_xp != XPTR_NULL )
        {
            chdev_cxy = GET_CXY( chdev_xp );
            chdev_ptr = GET_PTR( chdev_xp );

            if( chdev_cxy == local_cxy )
            {
                error = vfs_add_child_in_parent( local_cxy,
                                                 FS_TYPE_DEVFS,
                                                 devfs_external_inode_xp,
                                                 chdev_ptr->name,
                                                 &unused_xp,
                                                 &inode_xp );
                if( error )
                {
                    printk("\n[PANIC] in %s : cannot create TXT_TX inode in cluster %x\n",
                    __FUNCTION__, local_cxy );
                    hal_core_sleep();
                }

                // update child inode "extend" and "type" fields
                inode_ptr = GET_PTR( inode_xp );
                inode_ptr->extend = chdev_ptr;
                inode_ptr->type   = INODE_TYPE_DEV;
        
#if DEBUG_DEVFS_LOCAL_INIT 
cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEVFS_LOCAL_INIT < cycle )
printk("\n[%s] thread[%x,%x] created <txt_tx[%d]> inode in cluster %x\n",
__FUNCTION__, this->process->pid, this->trdid, channel, local_cxy, cycle );
#endif
            }
        }
    }

    // create an IOC inode in each cluster containing an IOC chdev 
    for( channel = 0 ; channel < CONFIG_MAX_IOC_CHANNELS ; channel++ )
    {
        chdev_xp = chdev_dir.ioc[channel];
        if( chdev_xp != XPTR_NULL )
        {
            chdev_cxy = GET_CXY( chdev_xp );
            chdev_ptr = GET_PTR( chdev_xp );

            if( chdev_cxy == local_cxy )
            {
                error = vfs_add_child_in_parent( local_cxy,
                                                 FS_TYPE_DEVFS,
                                                 devfs_external_inode_xp,
                                                 chdev_ptr->name,
                                                 &unused_xp,
                                                 &inode_xp );
                if( error )
                {
                    printk("\n[PANIC] in %s : cannot create IOC inode in cluster %x\n",
                    __FUNCTION__, local_cxy );
                    hal_core_sleep();
                }

                // update child inode "extend" and "type" fields
                inode_ptr = GET_PTR( inode_xp );
                inode_ptr->extend = chdev_ptr;
                inode_ptr->type   = INODE_TYPE_DEV;
        
#if DEBUG_DEVFS_LOCAL_INIT
cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEVFS_LOCAL_INIT < cycle )
printk("\n[%s] thread[%x,%x] created <ioc[%d]> inode in cluster %x\n",
__FUNCTION__, this->process->pid, this->trdid, channel, local_cxy, cycle );
#endif
            }
        }
    }

    // create a FBF inode in each cluster containing a FBF chdev
    for( channel = 0 ; channel < CONFIG_MAX_FBF_CHANNELS ; channel++ )
    {
        chdev_xp = chdev_dir.fbf[channel];
        if( chdev_xp != XPTR_NULL )
        {
            chdev_cxy = GET_CXY( chdev_xp );
            chdev_ptr = GET_PTR( chdev_xp );

            if( chdev_cxy == local_cxy )
            {
                error = vfs_add_child_in_parent( local_cxy,
                                                 FS_TYPE_DEVFS,
                                                 devfs_external_inode_xp,
                                                 chdev_ptr->name,
                                                 &unused_xp,
                                                 &inode_xp );
                if( error )
                {
                    printk("\n[PANIC] in %s : cannot create FBF inode in cluster %x\n",
                    __FUNCTION__, local_cxy );
                    hal_core_sleep();
                }

                // update child inode "extend" and "type" fields
                inode_ptr = GET_PTR( inode_xp );
                inode_ptr->extend = chdev_ptr;
                inode_ptr->type   = INODE_TYPE_DEV;
        
#if DEBUG_DEVFS_LOCAL_INIT
cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEVFS_LOCAL_INIT < cycle )
printk("\n[%s] thread[%x,%x] created <fbf[%d]> inode in cluster %x\n",
__FUNCTION__, this->process->pid, this->trdid, channel, local_cxy, cycle );
#endif
            }
        }
    }

    // create a NIC_RX inode in each cluster containing a NIC_RX chdev 
    for( channel = 0 ; channel < CONFIG_MAX_NIC_CHANNELS ; channel++ )
    {
        chdev_xp = chdev_dir.nic_rx[channel];
        if( chdev_xp != XPTR_NULL )
        {
            chdev_cxy = GET_CXY( chdev_xp );
            chdev_ptr = GET_PTR( chdev_xp );

            if( chdev_cxy == local_cxy )
            {
                error = vfs_add_child_in_parent( local_cxy,
                                                 FS_TYPE_DEVFS,
                                                 devfs_external_inode_xp,
                                                 chdev_ptr->name,
                                                 &unused_xp,
                                                 &inode_xp );
                if( error )
                {
                    printk("\n[PANIC] in %s : cannot create NIC_RX inode in cluster %x\n",
                    __FUNCTION__, local_cxy );
                    hal_core_sleep();
                }

                // update child inode "extend" and "type" fields
                inode_ptr = GET_PTR( inode_xp );
                inode_ptr->extend = chdev_ptr;
                inode_ptr->type   = INODE_TYPE_DEV;
 
#if DEBUG_DEVFS_LOCAL_INIT
cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEVFS_LOCAL_INIT < cycle )
printk("\n[%s] thread[%x,%x] created <nic_rx[%d]> inode in cluster %x\n",
__FUNCTION__, this->process->pid, this->trdid, channel, local_cxy, cycle );
#endif
            }
        }
    }

    // create a NIC_TX inode in each cluster containing a NIC_TX chdev 
    for( channel = 0 ; channel < CONFIG_MAX_NIC_CHANNELS ; channel++ )
    {
        chdev_xp = chdev_dir.nic_tx[channel];
        if( chdev_xp != XPTR_NULL )
        {
            chdev_cxy = GET_CXY( chdev_xp );
            chdev_ptr = GET_PTR( chdev_xp );

            if( chdev_cxy == local_cxy )
            {
                error = vfs_add_child_in_parent( local_cxy,
                                                 FS_TYPE_DEVFS,
                                                 devfs_external_inode_xp,
                                                 chdev_ptr->name,
                                                 &unused_xp,
                                                 &inode_xp );
                if( error )
                {
                    printk("\n[PANIC] in %s : cannot create NIC_TX inode in cluster %x\n",
                    __FUNCTION__, local_cxy );
                    hal_core_sleep();
                }

                // update child inode "extend" and "type" fields
                inode_ptr = GET_PTR( inode_xp );
                inode_ptr->extend = chdev_ptr;
                inode_ptr->type   = INODE_TYPE_DEV;
        
#if DEBUG_DEVFS_LOCAL_INIT
cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEVFS_LOCAL_INIT < cycle )
printk("\n[%s] thread[%x,%x] created <nic_tx[%d]> inode in cluster %x\n",
__FUNCTION__, this->process->pid, this->trdid, channel, local_cxy, cycle );
#endif
            }
        }
    }
}  // end devfs_local_init()

//////////////////////////////////////////
int devfs_user_move( bool_t     to_buffer,
                     xptr_t     file_xp,
                     char     * u_buf,
                     uint32_t   size )
{
    xptr_t           chdev_xp;
    cxy_t            chdev_cxy;
    chdev_t        * chdev_ptr;                    // associated chdev type
    uint32_t         func;                         // chdev functionnal type
    uint32_t         channel;                      // chdev channel index
    uint32_t         burst;                        // number of bytes in a burst
    uint32_t         todo;                         // number of bytes not yet moved
    error_t          error;
    uint32_t         i;

    char             k_buf[CONFIG_TXT_KBUF_SIZE];  // local kernel buffer

assert( ( file_xp != XPTR_NULL ) , "file_xp == XPTR_NULL" );

#if (DEBUG_SYS_READ & 1)
enter_devfs_read = hal_time_stamp();
#endif

#if (DEBUG_SYS_WRITE & 1)
enter_devfs_write = hal_time_stamp();
#endif

#if DEBUG_DEVFS_MOVE
uint32_t   cycle = (uint32_t)hal_get_cycles();
thread_t * this  = CURRENT_THREAD;
if( DEBUG_DEVFS_MOVE < cycle )
printk("\n[%s] thread[%x,%x] enter / to_mem %d / cycle %d\n",
__FUNCTION__, this->process->pid, this->trdid, to_buffer, cycle );
#endif

    // get extended pointer on chdev_xp
    chdev_xp = chdev_from_file( file_xp );

    // get cluster and local pointer on chdev
    chdev_cxy  = GET_CXY( chdev_xp );
    chdev_ptr  = (chdev_t *)GET_PTR( chdev_xp );

    // get chdev functionnal type and channel
    func    = hal_remote_l32( XPTR( chdev_cxy , &chdev_ptr->func ) );
    channel = hal_remote_l32( XPTR( chdev_cxy , &chdev_ptr->channel ) );

// Only TXT devices are associated to a pseudo-file
assert( ( func == DEV_FUNC_TXT ) , __FUNCTION__, "illegal device func_type");

    // initialise number of bytes to move
    todo = size;

    /////////////// TXT read
    if( to_buffer )
    { 
        while( todo )
        {
            // set burst size
            if( todo > CONFIG_TXT_KBUF_SIZE ) burst = CONFIG_TXT_KBUF_SIZE;
            else                              burst = todo;

            // read burst bytes from TXT device to kernel buffer
            for( i = 0 ; i < burst ; i++ )
            {
                error = dev_txt_read( channel , &k_buf[i] );

                if( error )  return -1;
            }

            // move burst bytes from k_buf to u_buf                   
            hal_strcpy_to_uspace( u_buf , k_buf , burst );

            // update loop variables
            todo  -= burst;
            u_buf += burst;
        }

#if DEBUG_DEVFS_MOVE
cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEVFS_MOVE < cycle )
printk("\n[%s] thread[%x,%x] exit / to_mem %d / cycle %d\n",
__FUNCTION__, this->process->pid, this->trdid, to_buffer, cycle );
#endif

#if (DEBUG_SYS_READ & 1)
exit_devfs_read = hal_time_stamp();
#endif
            return size;
    } 
    ///////////// TXT write  
    else                
    {
        while( todo )
        {
            // set burst size
            if( todo > CONFIG_TXT_KBUF_SIZE ) burst = CONFIG_TXT_KBUF_SIZE;
            else                              burst = todo;

            // move burst bytes from u_buf to k_buf
            hal_strcpy_from_uspace( k_buf , u_buf , burst );

            // write burst bytes from kernel buffer to TXT device
            error = dev_txt_write( channel , k_buf , burst );

            if( error ) return -1;

            // update loop variables
            todo  -= burst;
            u_buf += burst;
        } 

#if DEBUG_DEVFS_MOVE
cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEVFS_MOVE < cycle )
printk("\n[%s] thread[%x,%x] exit / to_mem %d / cycle %d\n",
__FUNCTION__, this->process->pid, this->trdid, to_buffer, cycle );
#endif

#if (DEBUG_SYS_WRITE & 1)
exit_devfs_write = hal_time_stamp();
#endif
            return size;
    }

}  // end devfs_user_move()

///////////////////////////////////////////////////////
error_t devfs_get_user_dir( struct vfs_inode_s * inode,
                            struct dirent      * array,
                            uint32_t             max_dirent,
                            uint32_t             min_dentry,
                            bool_t               detailed,
                            uint32_t           * entries,
                            bool_t             * done )
{
    xptr_t         xhtab_xp;    // extended pointer on inode xhtab (children dentries)
    xptr_t         dentry_xp;   // extended pointer on current dentry
    vfs_dentry_t * dentry_ptr;  // local pointer on current dentry
    uint32_t       dentry_id;   // dentry index in set of children dentry    
    uint32_t       dirent_id;   // dirent index in array of dirent

// detailed argument unused
assert( (detailed == false) , "detailed argument not supported\n");
 
    // One loop to scan the target inode xhtab containing the set of dentries
    // exit loop if no more dentries, or dirent array full 

#if DEBUG_DEVFS_GET_USER_DIR
char       inode_name[CONFIG_VFS_MAX_NAME_LENGTH];
uint32_t   cycle = (uint32_t)hal_get_cycles();
thread_t * this  = CURRENT_THREAD;
vfs_inode_get_name( XPTR( local_cxy , inode ) , inode_name );
if( DEBUG_DEVFS_GET_USER_DIR < cycle )
printk("\n[%s]  thread[%x,%x] enter for inode <%s> / cycle %d\n",
__FUNCTION__, this->process->pid, this->trdid, inode_name , cycle );
#endif

    // get extended pointer on inode xhtab 
    xhtab_xp  = XPTR( local_cxy , &inode->children );

    // initialize loop variables
    dentry_xp = xhtab_get_first( xhtab_xp );
    dentry_id = 0;
    dirent_id = 0;

    while( (dentry_xp != XPTR_NULL ) && (dirent_id < max_dirent) )
    {
        if( dentry_id >= min_dentry )
        {
            // copy name into dirent array
            dentry_ptr = GET_PTR( dentry_xp );
            strcpy( array[dirent_id].d_name , dentry_ptr->name ); 

            // increment dirent_id
            dirent_id++;
        }
         
        // update loop variables
        dentry_xp = xhtab_get_next( xhtab_xp );
        dentry_id++;
    }

    // return results of scan
    *done    = (dentry_xp == XPTR_NULL);
    *entries = dirent_id;

#if DEBUG_DEVFS_GET_USER_DIR
cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEVFS_GET_USER_DIR < cycle )
printk("\n[%s]  thread[%x,%x] exit for inode <%s> / %d entries / cycle %d\n",
__FUNCTION__, this->process->pid, this->trdid, inode_name, entries, cycle );
#endif

    return 0;
    
}  // en devfs_get_user_dir()

