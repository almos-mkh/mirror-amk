/*
 * do_syscall.h - generic syscall handler.
 * 
 * Authors   Ghassan Almaless (2008,2009,2010,2011,2012)
 *           Mohamed Lamine Karaoui (2015)
 *           Alain Greiner (2016)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _DO_SYSCALL_H_
#define _D0_SYSCALL_H_

#include <hal_kernel_types.h>
#include <thread.h>

/**************************************************************************************
 * This function calls the kernel function defined by the <service_num> argument.
 * The possible values for service_num are defined in the syscalls/syscalls.h file.
 * It does NOT enable interrupts, that must be enabled by the kernel function
 * depending on the implemented service. 
 **************************************************************************************
 * @ this        : pointer on calling thread descriptor
 * @ arg0        : kernel function argument 0
 * @ arg1        : kernel function argument 1
 * @ arg2        : kernel function argument 2
 * @ arg3        : kernel function argument 3
 * @ service_num : kernel service index
 * @ return 0 if success / return non zero if failure.
 *************************************************************************************/
reg_t do_syscall( thread_t * this,
                  reg_t      arg0,
		          reg_t      arg1,
		          reg_t      arg2,
		          reg_t      arg3,
		          reg_t      service_num );

#endif  // _DO_SYSCALL_H_
