/*
 * thread.h -  Thread and related operations definition.
 *
 * Author  Ghassan Almaless (2008,2009,2010,2011,2012)
 *         Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _THREAD_H_
#define _THREAD_H_

#include <hal_kernel_types.h>
#include <shared_syscalls.h>
#include <hal_special.h>
#include <xlist.h>
#include <list.h>
#include <hal_context.h>
#include <remote_busylock.h>
#include <core.h>
#include <chdev.h>
#include <cluster.h>
#include <process.h>
#include <dev_ioc.h>
#include <dev_nic.h>
#include <dev_txt.h>
#include <dev_mmc.h>
#include <dev_dma.h>

/***************************************************************************************
 * These macros are used to compose or decompose the global thread identifier (TRDID)
 * to or from cluster identifier / local thread index (CXY , LTID)
 **************************************************************************************/

#define LTID_FROM_TRDID( trdid )   (ltid_t)(trdid & 0x0000FFFF)
#define CXY_FROM_TRDID( trdid )    (cxy_t)(trdid >> 16)
#define TRDID( cxy , ltid )        (trdid_t)((cxy << 16) | ltid )

/***************************************************************************************
 * This enum defines the thread types.
 **************************************************************************************/

typedef enum
{
	THREAD_USER    = 0,          /*! user thread (pthread)                            */
	THREAD_RPC     = 1,          /*! kernel thread executing pending RPCs             */
	THREAD_DEV     = 2,          /*! kernel thread executing I/O device commands      */
	THREAD_IDLE    = 3,          /*! kernel idle thread                               */
}
thread_type_t;

/***************************************************************************************
 * This defines the thread flags bit-vector.
 **************************************************************************************/

#define THREAD_FLAG_DETACHED     0x0001  /*! This thread is detached from parent      */
#define THREAD_FLAG_JOIN_DONE    0x0002  /*! Parent thread made a join request        */
#define THREAD_FLAG_KILL_DONE    0x0004  /*! This thread received a kill request      */
#define THREAD_FLAG_REQ_ACK      0x0010  /*! Acknowledge required from scheduler      */
#define THREAD_FLAG_REQ_DELETE   0x0020  /*! Destruction required from scheduler      */

/***************************************************************************************
 * This defines the thread blocking causes bit-vector.
 **************************************************************************************/

#define THREAD_BLOCKED_GLOBAL    0x0001  /*! thread deactivated / wait activation     */
#define THREAD_BLOCKED_IO        0x0002  /*! thread wait IO operation completion      */
#define THREAD_BLOCKED_MAPPER    0x0004  /*! thread wait mapper                       */
#define THREAD_BLOCKED_EXIT      0x0008  /*! thread blocked in join / wait exit       */
#define THREAD_BLOCKED_JOIN      0x0010  /*! thread blocked in exit / wait join       */
#define THREAD_BLOCKED_SEM       0x0020  /*! thread wait semaphore                    */
#define THREAD_BLOCKED_PAGE      0x0040  /*! thread wait page access                  */
#define THREAD_BLOCKED_IDLE      0x0080  /*! thread RPC wait RPC_FIFO non empty       */
#define THREAD_BLOCKED_USERSYNC  0x0100  /*! thread wait (cond/mutex/barrier)         */
#define THREAD_BLOCKED_RPC       0x0200  /*! thread wait RPC completion               */
#define THREAD_BLOCKED_ISR       0x0400  /*! thread DEV wait ISR                      */
#define THREAD_BLOCKED_WAIT      0x0800  /*! thread wait child process termination    */
#define THREAD_BLOCKED_LOCK      0x1000  /*! thread wait queuelock or rwlock          */

/***************************************************************************************
 * This structure defines thread instrumentation informations.
 **************************************************************************************/

typedef struct thread_info_s
{
	uint32_t              pgfault_nr;    /*! cumulated number of page fault           */
	uint32_t              sched_nr;      /*! TODO ???  [AG]                           */
	uint32_t              u_err_nr;      /*! TODO ???  [AG]                           */
	uint32_t              m_err_nr;      /*! TODO ???  [AG]                           */
	cycle_t               last_cycle;    /*! last cycle counter value (date)          */
	cycle_t               usr_cycles;    /*! user execution duration (cycles)         */
	cycle_t               sys_cycles;    /*! system execution duration (cycles)       */
}
thread_info_t;

/***************************************************************************************
 * This structure defines a thread descriptor.
 * It is used for both the user threads and the kernel threads.
 * In a process, a user thread is identified by a unique TRDID (thread identifier):
 * - The TRDID 16 LSB bits contain the LTID (Local Thread Index).
 * - The TRDID 16 MSB bits contain the CXY of cluster containing the thread.
 * The main thread LTID value is always 0. 
 * The LTID is used to index the th_tbl[] array in the local process descriptor.
 * This TRDID is computed by the process_register_thread() function, when the user
 * thread is registered in the local copy of the process descriptor.
 *
 * WARNING (1) Don't modify the first 4 fields order, as this order is used by the
 *             hal_kentry assembly code for some architectures (TSAR).
 *
 * WARNING (2) Most of the thread state is private and accessed only by this thread,
 *             but some fields are shared, and can be modified by other threads.
 *             - the "blocked" bit_vector can be modified by another thread
 *               running in another cluster (using atomic instructions),
 *               to change this thread scheduling status.
 *             - the "flags" bit_vector can be modified by another thread
 *               running in another cluster (using atomic instructions),
 *               to register requests such as ACK or DELETE.
 *             - the "join_xp" field can be modified by the joining thread,
 *               and this rendez-vous is protected by the dedicated "join_lock".
 *
 * WARNING (3) When this thread is blocked on a shared resource (queuelock, condvar,
 *             or chdev), it registers in the associated waiting queue, using the
 *             "wait_list" (local list) or "wait_xlist" (trans-cluster list) fields. 
 **************************************************************************************/

#define THREAD_SIGNATURE    0xDEADBEEF

typedef struct thread_s
{
	void              * cpu_context;     /*! CPU context used by sched_yield          */
	void              * fpu_context;     /*! FPU context used by sched_yield          */
    void              * uzone_current;   /*! used by hal_do_syscall & hal_do_except   */
    void              * uzone_previous;  /*! used by hal_do_syscall & hal_do_except   */

	intptr_t            k_stack_base;    /*! kernel stack base address                */
	uint32_t            k_stack_size;    /*! kernel stack size (bytes)                */

	uint32_t            trdid;           /*! thread index (cxy.ltid)                  */
	thread_type_t       type;            /*! thread type                              */
	uint32_t            quantum;         /*! number of clock ticks given to thread    */
	uint32_t            ticks_nr;        /*! number of ticks used                     */
	uint32_t            time_last_check; /*! last cpu_time_stamp                      */
	core_t            * core;            /*! pointer to the owner core                */
	process_t         * process;         /*! pointer on local process descriptor      */
    xptr_t              parent;          /*! extended pointer on parent thread        */

    remote_busylock_t   join_lock;       /*! lock protecting the join/exit            */
    xptr_t              join_xp;         /*! joining/killer thread extended pointer   */

    uint32_t          * ack_rsp_count;   /*! pointer on acknowledge response counter  */

	intptr_t            u_stack_base;    /*! user stack base address                  */
	uint32_t            u_stack_size;    /*! user stack size (bytes)                  */

    void              * entry_func;      /*! pointer on entry function                */
    void              * entry_args;      /*! pointer on entry function arguments      */
    uint32_t            main_argc;       /*! main thread number of arguments          */
    char             ** main_argv;       /*! main thread array of strings arguments   */

    uint32_t            flags;           /*! bit vector of flags                      */
    uint32_t            blocked;         /*! bit vector of blocking causes            */

	error_t             errno;           /*! errno value set by last system call      */
    uint32_t            utls;            /*! user thread local storage                */

    bool_t              fork_user;       /*! user defined placement for next fork()   */
    cxy_t               fork_cxy;        /*! target cluster  for next fork()          */

	list_entry_t        sched_list;      /*! member of threads attached to same core  */

    chdev_t           * chdev;           /*! chdev pointer (for a DEV thread only)    */

    reg_t               save_sr;         /*! used by sched_yield() function           */

    ioc_command_t       ioc_cmd;         /*! IOC device generic command               */
    txt_command_t       txt_cmd;         /*! TXT device generic command               */
    nic_command_t       nic_cmd;         /*! NIC device generic command               */
    mmc_command_t       mmc_cmd;         /*! MMC device generic command               */
    dma_command_t       dma_cmd;         /*! DMA device generic command               */

  	xptr_t              rpc_client_xp;   /*! client thread (for a RPC thread only)    */

    list_entry_t        wait_list;       /*! member of a local waiting queue          */
    xlist_entry_t       wait_xlist;      /*! member of a trans-cluster waiting queue  */

	uint32_t            busylocks;	     /*! number of taken busylocks                */

#if DEBUG_BUSYLOCK
    xlist_entry_t       busylocks_root;  /*! root of xlist of taken busylocks         */
#endif

	thread_info_t       info;            /*! embedded thread_info_t                   */

	uint32_t            signature;       /*! for kernel stack overflow detection      */
}
thread_t;

/***************************************************************************************
 * This macro returns a pointer on the calling thread from the core hardware register.
 **************************************************************************************/

#define CURRENT_THREAD  (hal_get_current_thread())

/***************************************************************************************
 * This function returns a printable string for a thread type.
 ***************************************************************************************
 * @ type    : thread type.
 * returns pointer on string.
 **************************************************************************************/
const char * thread_type_str( thread_type_t type );

/***************************************************************************************
 * This function is used by the pthread_create() system call to create a "new" thread
 * in an existing process. It allocates memory for an user thread descriptor in the
 * local cluster, and initializes it from information contained in the arguments.
 * The CPU context is initialized from scratch. 
 * It is registered in the local process descriptor specified by the <pid> argument.
 * The THREAD_BLOCKED_GLOBAL bit is set => the thread must be activated by the caller
 * to start at the next scheduling point.
 ***************************************************************************************
 * @ pid          : process identifier.
 * @ start_func   : pointer on entry function.
 * @ start_args   : pointer on function argument (can be NULL).
 * @ attr         : pointer on pthread attributes descriptor.
 * @ new_thread   : [out] address of buffer for new thread descriptor pointer.
 * @ returns 0 if success / returns ENOMEM if error.
 **************************************************************************************/
error_t thread_user_create( pid_t             pid,
                            void            * start_func,
                            void            * start_arg,
                            pthread_attr_t  * attr,
                            thread_t       ** new_thread );

/***************************************************************************************
 * This function is used by the sys_fork() system call to create the "child" thread
 * in the local cluster. It allocates memory for a thread descriptor, and initializes
 * it from the "parent" thread descriptor defined by the <parent_thread_xp> argument.
 * The new thread is attached to the core that has the lowest load in local cluster.
 * It is registered in the "child" process defined by the <child_process> argument.
 * This new thread inherits its user stack from the parent thread, as it uses the
 * Copy-On-Write mechanism to get a private stack when required.
 * The content of the parent kernel stack is copied into the child kernel stack, as 
 * the Copy-On-Write mechanism cannot be used for kernel segments (because kernel 
 * uses physical addressing on some architectures).
 * The CPU and FPU execution contexts are created and linked to the new thread.
 * but the actual context copy is NOT done, and must be done by by the sys_fork(). 
 * The THREAD_BLOCKED_GLOBAL bit is set => the thread must be activated to start.
 ***************************************************************************************
 * @ parent_thread_xp  : extended pointer on parent thread descriptor.
 * @ child_process     : local pointer on child process descriptor.
 * @ child_thread      : [out] address of buffer for child thread descriptor pointer.
 * @ returns 0 if success / returns -1 if error.
 **************************************************************************************/
error_t thread_user_fork( xptr_t      parent_thread_xp,
                          process_t * child_process,
                          thread_t ** child_thread );

/***************************************************************************************
 * This function is called by the process_make_exec() function to re-initialise the
 * thread descriptor of the calling thread (that will become the new process main
 * thread), and immediately jump to user code without returning to kentry!!!
 * It must be called by the main thread of the calling process.
 * - A new user stack vseg is created and initialised.
 * - The kernel stack (currently in use) is not modified.  
 * - The function calls the hal_cpu_context_exec() to re-initialize the CPU context
 *   an jump to user code.  
 ***************************************************************************************
 * @ entry_func : main thread entry point.
 * @ argc       : number of main thread arguments.
 * @ argv       : array of pointers on stringarguments.
 * @ returns 0 if success / returns ENOMEM if error.
 **************************************************************************************/
error_t thread_user_exec( void     * entry_func,
                          uint32_t   argc,
                          char    ** argv);

/***************************************************************************************
 * This function allocates memory for a kernel thread descriptor in the local cluster,
 * and initializes it from arguments values. 
 * It is called by kernel_init() to statically create all DEV server threads
 * It is also called to dynamically create RPC threads when required.
 * The THREAD_BLOCKED_GLOBAL bit is set, and the thread must be activated to start.
 ***************************************************************************************
 * @ new_thread   : address of buffer for new thread pointer.
 * @ type         : kernel thread type.
 * @ func         : pointer on function.
 * @ args         : function arguments.
 * @ core_lid     : local core index.
 * @ returns 0 if success / returns ENOMEM if error
 **************************************************************************************/
error_t thread_kernel_create( thread_t     ** new_thread,
                              thread_type_t   type,
                              void          * func,
                              void          * args,
                              lid_t           core_lid );

/***************************************************************************************
 * This function is called by the kernel_init() function to initialize the IDLE thread
 * descriptor from arguments values.
 * The THREAD_BLOCKED_GLOBAL bit is set, and the thread must be activated to start.
 * It returns a kernel panic if failure.
 ***************************************************************************************
 * @ thread   : pointer on existing thread descriptor.
 * @ type     : kernel thread type.
 * @ func     : pointer on function.
 * @ args     : function arguments.
 * @ core_lid : local core index.
 **************************************************************************************/
void thread_idle_init( thread_t      * thread,
                       thread_type_t   type,
                       void          * func,
                       void          * args,
                       lid_t           core_lid );

/***************************************************************************************
 * This low-level function is called by the sched_handle_signals() function to releases 
 * the physical memory allocated for a thread in a given cluster, when this thread 
 * is marked for delete. This include the thread descriptor itself, the associated 
 * CPU and FPU context, and the physical memory allocated for an user thread local stack.
 ***************************************************************************************
 * @ thread  : pointer on the thread descriptor to release.
 * @ return true, if the thread was the last registerd thread in local process.
 **************************************************************************************/
void thread_destroy( thread_t * thread );

/***************************************************************************************
 * This function defines the code of the thread executed by all cores after kernel_init,
 * or when no other thread is runnable for a given core.
 * It enter and infinite loop in wich:
 * - it unmask the IRQs
 * - it optionally calls the hal_core_sleep() function to reduce the power consumption
 *   (this behavior is controlled by the CONFIG_THREAD_IDLE_MODE_SLEEP flag).
 * - it call the sched_yield() function to find another runnable thread.
 *
 * TODO: In the TSAR architecture the hal_core_sleep() function forces the core to
 * low-power mode. Any IRQ will force the core to exit this low-power mode, but no ISR
 * is executed. We must analyse if we have the same behaviour for I86 architectures...
 **************************************************************************************/
void thread_idle_func( void );

/***************************************************************************************
 * This function is used by a "blocker" thread running in the same cluster as a "target"
 * thread to request the scheduler of the target thread to acknowledge that the target
 * thread is blocked and not running, at the next context switch.
 * This function executes atomically the following actions :
 * - it set the request_pending boolean in the target scheduler descriptor.
 * - it set the REQ_ACK flag in the "flags" field of the target thread descriptor.
 * - It registers the responses counter pointer in the target thread descriptor.
 * The request_pending flag is handled as a set/reset flip-flop by the "blocker" thread
 * and by the "target" scheduler. 
 ***************************************************************************************
 * @ target        : local pointer on target thread.
 * @ ack_rsp_count : local pointer on responses counter.
 **************************************************************************************/
void thread_set_req_ack( thread_t * target,
                         uint32_t * ack_rsp_count );

/***************************************************************************************
 * This function is used by the sched_handle_signal() function executed by the 
 * scheduler of a "target" thread to reset a "blocked not running" acknowledge request
 * in both the target thread descriptor, and in the target  thread scheduler.
 ***************************************************************************************
 * @ target    : local pointer on target thread.
 **************************************************************************************/
void thread_reset_req_ack( thread_t * target );

/***************************************************************************************
 * This function is used by the four sys_thread_cancel(), sys_thread_exit(),
 * sys_kill() and sys_exit() system calls to mark for delete a given thread. 
 * It set the THREAD_BLOCKED_GLOBAL bit and set the the THREAD_FLAG_REQ_DELETE bit
 * in the thread descriptor identified by the <thread_xp> argument, to ask the scheduler 
 * to asynchronously delete the target thread, at the next scheduling point. 
 * The calling thread can run in any cluster, as it uses remote accesses, but
 * the target thread cannot be the main thread of the process identified by the <pid>
 * argument, because the main thread must be deleted by the parent process argument.
 * If the target thread is running in "attached" mode, and the <is_forced> argument
 * is false, this function implements the required sychronisation with the joining 
 * thread, blocking the killer thread until the pthread_join() syscall is executed
 * by the joining thread.
 ***************************************************************************************
 * @ thread_xp   : extended pointer on the target thread.
 * @ pid         : process identifier (to get the owner cluster identifier).
 * @ is_forced   : the deletion does not depends on the attached mode. 
 **************************************************************************************/
void thread_delete( xptr_t  thread_xp,
                    pid_t   pid,
                    bool_t  is_forced );

/***************************************************************************************
 * This function registers a blocking cause defined by the <cause> argument 
 * in a remote thread descriptor identified by the <thread_xp> argument.
 * We need an extended pointer, because this function can be called by another thread
 * than the target thread, executing the sys_kill() function.
 * WARNING : this function does not deschedule the target thread, and the descheduling
 * must be explicitely forced by a sched_yield().
 ***************************************************************************************
 * @ thread_xp   : extended pointer on remote thread descriptor.
 * @ cause       : mask defining the cause (one hot).
 **************************************************************************************/
void thread_block( xptr_t   thread_xp,
                   uint32_t cause );

/***************************************************************************************
 * This function resets the bit identified by the <cause> argument in a remote
 * thread descriptor identified by the <thread_xp> argument.
 * We need an extended pointer, because the client thread of an I/O operation on a
 * given device is generally not in the same cluster as the associated server thread.
 * WARNING : this function does not reschedule the remote thread.
 * The scheduling can be forced by sending an IPI to the core running the remote thread.
 ***************************************************************************************
 * @ thread_xp   : extended pointer the remote thread.
 * @ cause       : mask defining the cause (one hot).
 * @ return non zero if the bit-vector was actually modified / return 0 otherwise
 **************************************************************************************/
uint32_t thread_unblock( xptr_t   thread_xp,
                         uint32_t cause );

/***************************************************************************************
 * This function updates the calling thread user_time or kernel_time counters. 
 ***************************************************************************************
 * @ thread   : local pointer on target thread.
 * @ is_user  : update user time if true / update kernel time if false
 **************************************************************************************/
void thread_time_update( thread_t * thread,
                         bool_t     is_user );

/***************************************************************************************
 * This function returns the extended pointer on a thread descriptor identified
 * by its thread identifier, and process identifier.
 * It can be called by any thread running in any cluster.
 ***************************************************************************************
 * @ pid     : process identifier.
 * @ trdid   : thread identifier.
 * @ return the extended pointer if thread found / return XPTR_NULL if not found.
 **************************************************************************************/
xptr_t thread_get_xptr( pid_t    pid,
                        trdid_t  trdid );

/***************************************************************************************
 * This function checks that the thread identified by the <thread> argument does hold
 * any busylock (local or remote).
 * If the xlist of taken busylocks is not empty, it displays the set of taken locks,
 * and makes a kernel panic.  
 ***************************************************************************************
 * @ thread    : local pointer on target thread.
 * @ func_str  : faulty function name.
 **************************************************************************************/
void thread_assert_can_yield( thread_t    * thread,
                              const char  * func_str );

/***************************************************************************************
 * This debug function display the list of busylocks (local or remote) 
 * currently owned by a the thread identified by the <thead_xp> argument.
 * The <string> argument is printed in header (can be the calling function name). 
 * WARNING : the DEBUG_BUSYLOCK parameter must be set in the kernel_config.h file.
 ***************************************************************************************
 * @ thread_xp  : extended pointer on target thread.
 * @ string     : defines the calling context.
 **************************************************************************************/
void thread_display_busylocks( xptr_t       thread_xp,
                               const char * string );



#endif	/* _THREAD_H_ */
