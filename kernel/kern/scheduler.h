/*
 * scheduler.h - Core scheduler definition.
 * 
 * Author    Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _SCHEDULER_H_
#define _SCHEDULER_H_

#include <hal_kernel_types.h>
#include <list.h>
#include <busylock.h>

/****  Forward declarations  ****/

struct core_s;
struct thread_s;

/*********************************************************************************************
 * This structure define the scheduler associated to a given core.
 ********************************************************************************************/

typedef struct scheduler_s
{
    busylock_t        lock;            /*! lock protecting scheduler state                  */
    uint16_t          u_threads_nr;    /*! total number of attached user threads            */
    uint16_t          k_threads_nr;    /*! total number of attached kernel threads          */
    list_entry_t      u_root;          /*! root of list of user threads                     */
    list_entry_t      k_root;          /*! root of list of kernel threads                   */
    list_entry_t    * u_last;          /*! pointer on list_entry for last executed k_thread */
    list_entry_t    * k_last;          /*! pointer on list entry for last executed u_thread */
    struct thread_s * idle;            /*! pointer on idle thread                           */
    struct thread_s * current;         /*! pointer on current running thread                */
    volatile bool_t   req_ack_pending; /*! sequencialize ack requests when true             */
    bool_t            trace;           /*! context switches trace activated if true         */
}
scheduler_t;

/*********************************************************************************************
 *  This function initialises the scheduler for a given core.
 ********************************************************************************************/  
void sched_init( struct core_s * core );

/*********************************************************************************************
 * This function atomically register a new thread in a given core scheduler.
 * Note: There is no specific sched_remove_thread(), as a thread is always deleted
 * by the ched_handle_signals() function, called by the sched_yield() function.
 *********************************************************************************************
 * @ core    : local pointer on the core descriptor.
 * @ thread  : local pointer on the thread descriptor.
 ********************************************************************************************/  
void sched_register_thread( struct core_s   * core,
                            struct thread_s * thread );

/********************************************************************************************* 
 * This function is the only method to make a context switch. It is called in cas of TICK,
 * or when a thread explicitely requires to be descheduled.
 * It takes the scheduler busylock to atomically update the scheduled state.
 * It calls the sched_select() private function to select a new thread. After switch, it 
 * calls the sched_handle_signals() private function to handle the pending REQ_ACK and 
 * REQ_DELETE flagss for all threads attached to the scheduler: it deletes all threads 
 * marked for delete (and the process descriptor when the deleted thread is the main thread).
 * As the REQ_DELETE flag can be asynchronously set (between the select and the handle),
 * the sched_handle-signals() function check that the thread to delete is not the new thread,
 * because a thread cannot delete itself.
 * The cause argument is only used for debug by the sched_display() functions, and indicates
 * the scheduling cause.
 *********************************************************************************************
 * @ cause    : character string defining the scheduling cause.
 ********************************************************************************************/
void sched_yield( const char * cause );

/********************************************************************************************* 
 * This debug function displays on TXT0 the internal state of a local scheduler, 
 * identified by the core local index <lid>. It must be called by a local thread.
 *********************************************************************************************
 * @ lid      : local index of target core.
 ********************************************************************************************/
void sched_display( lid_t lid );

/********************************************************************************************* 
 * This debug function displays on TXT0 the internal state of a scheduler, 
 * identified by the target cluster identifier <cxy> and the core local index <lid>.
 * It can be called by a thread running in any cluster, as it uses remote accesses,
 * to scan the scheduler lists of threads.
 *********************************************************************************************
 * @ cxy      : target cluster identifier
 * @ lid      : local index of target core.
 ********************************************************************************************/
void sched_remote_display( cxy_t  cxy,
                           lid_t  lid );

#endif	/* _SCHEDULER_H_ */
