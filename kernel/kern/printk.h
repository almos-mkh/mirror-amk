/*
 * printk.h - Kernel Log & debug messages API definition.
 *
 * authors  Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

///////////////////////////////////////////////////////////////////////////////////
// The printk.c and printk.h files define the functions used by the kernel
// to display messages on the kernel terminal TXT0, using a busy waiting policy. 
// It calls synchronously the TXT0 driver, without descheduling.
///////////////////////////////////////////////////////////////////////////////////

#ifndef _PRINTK_H
#define _PRINTK_H

#include <hal_kernel_types.h>
#include <stdarg.h>

#include <hal_special.h> 

/**********************************************************************************
 * This function build a formatted string.
 * The supported formats are defined below :
 *   %b : exactly 2 digits hexadecimal integer (8 bits)
 *   %c : single ascii character (8 bits)
 *   %d : up to 10 digits decimal integer (32 bits)
 *   %u : up to 10 digits unsigned decimal (32 bits) 
 *   %x : up to 8 digits hexadecimal integer (32 bits) 
 *   %X : exactly 8 digits hexadecimal integer (32 bits) 
 *   %l : up to 16 digits hexadecimal integer (64 bits)
 *   %L : exactly 16 digits hexadecimal integer (64 bits)
 *   %s : NUL terminated character string
 **********************************************************************************
 * @ string     : pointer on target buffer (allocated by caller).
 * @ length     : target buffer length (number of bytes).
 * @ format     : format respecting the printf syntax.
 * @ returns the string length (including NUL) if success / return -1 if error.
 *********************************************************************************/
uint32_t snprintf( char     * string,
                   uint32_t   length,
                   char     * format, ... );

/**********************************************************************************
 * This function displays a formatted string on the kernel terminal TXT0,
 * after taking the TXT0 lock.
 * It uses a busy waiting policy, calling directly the relevant TXT driver,
 **********************************************************************************
 * @ format     : formatted string.
 *********************************************************************************/
void printk( char* format, ... );

/**********************************************************************************
 * This function displays a formatted string on the kernel terminal TXT0,
 * without taking the TXT0 lock.
 * It uses a busy waiting policy, calling directly the relevant TXT driver,
 **********************************************************************************
 * @ format     : formatted string.
 *********************************************************************************/
void nolock_printk( char* format, ... );


/**********************************************************************************
 * This function is called in case of kernel panic. It printt a detailed message
 * on the TXT0 terminal after taking the TXT0 lock, and call the hal_core_sleep()
 * function to block the calling core.  It is used by the assert macro (below).
 **********************************************************************************
 * @ file_name     : File where the assert macro was invoked
 * @ function_name : Name of the calling function.
 * @ line          : Line number where the assert macro was invoked
 * @ cycle         : Cycle where the macro was invoked
 * @ format        : Formatted string
 * ...             : arguments of the format string
 *
 * See assert macro documentation for information about printed information.
 *********************************************************************************/
void panic( const char * function_name,
            uint32_t     line,
            cycle_t      cycle,
            const char * format,
            ... ) __attribute__((__noreturn__));

/**********************************************************************************
 * This macro displays a formated message on kernel TXT0 terminal,
 * and forces the calling core in sleeping mode if a Boolean condition is false.
 * Actually used to debug the kernel.
 *
 * Extra information printed by assert:
 * - Current thread, process, and core
 * - Function name / line number in file / cycle
 **********************************************************************************
 * @ condition     : condition that must be true.
 * @ format        : formatted string
 *********************************************************************************/
#define assert( expr, format, ... )                                               \
{                                                                                 \
    uint32_t __line_at_expansion = __LINE__;                                      \
    const volatile cycle_t __assert_cycle = hal_get_cycles();                     \
    if ( ( expr ) == false )                                                      \
    {                                                                             \
        panic( __FUNCTION__,                                                      \
               __line_at_expansion,                                               \
               __assert_cycle,                                                    \
               ( format ), ##__VA_ARGS__ );                                       \
    }                                                                             \
}

/**********************************************************************************
 * This function displays a non-formated message on kernel TXT0 terminal.
 * This function is actually used to debug the assembly level kernel functions.
 **********************************************************************************
 * @ string   : non-formatted string.
 *********************************************************************************/
void puts( char * string );

/**********************************************************************************
 * This function displays a 32 bits value in hexadecimal on kernel TXT0 terminal.
 * This function is actually used to debug the assembly level kernel functions.
 **********************************************************************************
 * @ val   : 32 bits unsigned value.
 *********************************************************************************/
void putx( uint32_t val );

/**********************************************************************************
 * This function displays a 64 bits value in hexadecimal on kernel TXT0 terminal.
 * This function is actually used to debug the assembly level kernel functions.
 **********************************************************************************
 * @ val   : 64 bits unsigned value.
 *********************************************************************************/
void putl( uint64_t val );

/**********************************************************************************
 * This debug function displays on the kernel TXT0 terminal the content of an
 * array of bytes defined by <buffer> and <size> arguments (16 bytes per line).
 * The <string> argument is displayed before the buffer content.
 * The line format is an address folowed by 16 (hexa) bytes.
 **********************************************************************************
 * @ string   : buffer name or identifier.
 * @ buffer   : local pointer on bytes array.
 * @ size     : number of bytes bytes to display.
 *********************************************************************************/
void putb( char     * string,
           uint8_t  * buffer,
           uint32_t   size );



#endif  // _PRINTK_H

// Local Variables:
// tab-width: 4
// c-basic-offset: 4
// c-file-offsets:((innamespace . 0)(inline-open . 0))
// indent-tabs-mode: nil
// End:
// vim: filetype=c:expandtab:shiftwidth=4:tabstop=4:softtabstop=4

