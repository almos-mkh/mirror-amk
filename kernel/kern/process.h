/*
 * process.h - process related functions definition.
 *
 * Authors  Ghassan Almaless (2008,2009,2010,2011,2012)
 *          Mohamed Lamine Karaoui (2015)
 *          Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _PROCESS_H_
#define _PROCESS_H_

#include <kernel_config.h>
#include <errno.h>
#include <hal_kernel_types.h>
#include <list.h>
#include <xlist.h>
#include <bits.h>
#include <busylock.h>
#include <rwlock.h>
#include <queuelock.h>
#include <remote_queuelock.h>
#include <remote_rwlock.h>
#include <hal_atomic.h>
#include <vmm.h>
#include <cluster.h>
#include <vfs.h>

/****  Forward declarations  ****/

struct thread_s;

/*********************************************************************************************
 * These macros are used to compose or decompose global process identifier (PID)
 * to or from cluster identifier / local process index (CXY , LPID)
 ********************************************************************************************/

#define LPID_FROM_PID( pid )       (lpid_t)(pid & 0x0000FFFF)
#define CXY_FROM_PID( pid )        (cxy_t)(pid >> 16)
#define PID( cxy , lpid )          (pid_t)((cxy << 16) | lpid )

/*********************************************************************************************
 * This enum defines the actions that can be executed by the process_sigaction() function.
 ********************************************************************************************/

typedef enum 
{
    BLOCK_ALL_THREADS    = 0x11,
    UNBLOCK_ALL_THREADS  = 0x22,
    DELETE_ALL_THREADS   = 0x33, 
} 
process_sigactions_t;

/*********************************************************************************************
 * This structure defines an array of extended pointers on the open file descriptors
 * for a given process. We use an extended pointer because the open file descriptor
 * is always stored in the same cluster as the inode associated to the file.
 * A free entry in this array contains the XPTR_NULL value.
 * The array size is defined by the CONFIG_PROCESS_FILE_MAX_NR parameter.
 *
 * NOTE: - Only the fd_array[] in the reference process contains a complete list of open
 *         files, and is protected by the lock against concurrent access.
 *       - the fd_array[] in a process copy is simply a cache containing a subset of the
 *         open files to speed the fdid to xptr translation, but the "lock" and "current
 *         fields are not used.
 *       - all modifications made by the process_fd_remove() are done in reference cluster
 *         and reported in all process_copies.
 ********************************************************************************************/

typedef struct fd_array_s
{
	remote_queuelock_t lock;                              /*! lock protecting fd_array      */
    uint32_t           current;                           /*! current number of open files  */
	xptr_t             array[CONFIG_PROCESS_FILE_MAX_NR]; /*! open file descriptors         */
}
fd_array_t;

/*********************************************************************************************
 * This structure defines a process descriptor.
 * A process is identified by a unique PID (process identifier):
 * - The PID 16 LSB bits contain the LPID (Local Process Index)
 * - The PID 16 MSB bits contain the owner cluster CXY.
 * In each cluster, the process manager allocates  the LPID values for the process that
 * are owned by this cluster.
 * The process descriptor is replicated in all clusters containing at least one thread 
 * of the PID process, with the following rules :
 * 1) The <pid>, <ppid>, <ref_xp>, <owner_xp>, <vfs_root_xp>, <vfs_bin_xp>  fields are 
 *    defined in all process descriptor copies.
 * 2) The <vfs_cwd_xp> and associated <cwd_lock>, that can be dynamically modified,
 *    are only defined in the reference process descriptor.
 * 2) The <vmm>, containing the VSL (list of registered vsegs), and the GPT (generic
 *    page table), are only complete in the reference process cluster, other copies 
 *    are actually use as read-only caches.
 * 3) the <fd_array>, containing extended pointers on the open file descriptors, is only
 *    complete in the reference process cluster, other copies are read-only caches.
 * 4) The <sem_root>, <mutex_root>, <barrier_root>, <condvar_root>, and the associated
 *    <sync_lock>, dynamically allocated, are only defined in the reference cluster.
 * 5) The <children_root>, <children_nr>, <children_list>, and <txt_list> fields are only
 *    defined in the reference cluster, and are undefined in other clusters.
 * 6) The <local_list>, <copies_list>, <th_tbl>, <th_nr>, <u_th_lock> or <k_th_lock> fields
 *    are specific in each cluster, and are defined in all process descriptors copies.
 * 7) The termination <flags> and <exit_status> are only defined in the reference cluster.
 *    (The term_state format is defined in the shared_syscalls.h file ).
 ********************************************************************************************/

typedef struct process_s
{
	vmm_t              vmm;              /*! embedded virtual memory manager                 */

	fd_array_t         fd_array;         /*! embedded open file descriptors array            */

	xptr_t             vfs_root_xp;      /*! extended pointer on VFS root inode              */
	xptr_t             vfs_bin_xp;       /*! extended pointer on .elf file descriptor        */
	pid_t              pid;              /*! process identifier                              */
    xptr_t             ref_xp;           /*! extended pointer on reference process           */
    xptr_t             owner_xp;         /*! extended pointer on owner process               */
    xptr_t             parent_xp;        /*! extended pointer on parent process              */

	xptr_t             cwd_xp;           /*! extended pointer on current working dir inode   */
	remote_busylock_t  cwd_lock;         /*! lock protecting working directory changes       */

	xlist_entry_t      children_root;    /*! root of the children process xlist              */
    remote_queuelock_t children_lock;    /*! lock protecting children process xlist          */
    uint32_t           children_nr;      /*! number of children processes                    */

	xlist_entry_t      children_list;    /*! member of list of children of same parent       */
    xlist_entry_t      local_list;       /*! member of list of process in same cluster       */
    xlist_entry_t      copies_list;      /*! member of list of copies of same process        */
    xlist_entry_t      txt_list;         /*! member of list of processes sharing same TXT    */

	struct thread_s  * th_tbl[CONFIG_THREADS_MAX_PER_CLUSTER];       /*! local threads       */

	uint32_t           th_nr;            /*! number of threads in this cluster               */
    rwlock_t           th_lock;          /*! lock protecting th_tbl[]  i                     */ 

    xlist_entry_t      sem_root;         /*! root of the user defined semaphore list         */
    xlist_entry_t      mutex_root;       /*! root of the user defined mutex list             */
    xlist_entry_t      barrier_root;     /*! root of the user defined barrier list           */
    xlist_entry_t      condvar_root;     /*! root of the user defined condvar list           */
    remote_queuelock_t sync_lock;        /*! lock protecting user defined synchro lists      */

    xlist_entry_t      dir_root;         /*! root of the user defined DIR list               */
    remote_queuelock_t dir_lock;         /*! lock protexting user defined DIR list           */

    uint32_t           term_state;       /*! termination status (flags & exit status)        */
}
process_t;

/*********************************************************************************************
 * This structure defines the information required by the process_make_exec() function
 * to create a new reference process descriptor, and the associated main thread.
 ********************************************************************************************/

typedef struct exec_info_s
{
    char               path[CONFIG_VFS_MAX_PATH_LENGTH];   /*!  .elf file path              */

    char            ** args_pointers;  /*! physical base address of array of pointers       */
    char             * args_buf_base;  /*! physical base address of kernel args buffer      */
    uint32_t           args_nr;        /*! actual number of arguments                       */

    char            ** envs_pointers;  /*! physical base address of array of pointers       */
    char             * envs_buf_base;  /*! physical base address of kernel args buffer      */
    char             * envs_buf_free;  /*! physical address of first free slot in envs_buf  */
    uint32_t           envs_nr;        /*! actual number of environment variables           */
}
exec_info_t;

/***************   Process Descriptor Operations    *****************************************/

/*********************************************************************************************
 * This function allocates memory in local cluster for a process descriptor.
 *********************************************************************************************
 * @ returns pointer on process descriptor if success / return NULL if failure
 ********************************************************************************************/
process_t * process_alloc( void );

/*********************************************************************************************
 * This function releases memory in local cluster for a process descriptor.
 *********************************************************************************************
 * @ process      : pointer on process descriptor to release.
 ********************************************************************************************/
void process_free( process_t * process );

/*********************************************************************************************
 * This function initialize, in each cluster, the kernel "process_zero", that contains
 * all kernel threads in a given cluster. It is called by the kernel_init() function.
 * The process_zero descriptor is allocated as a global variable in file kernel_init.c 
 * Both the PID and PPID fields are set to zero, the ref_xp is the local process_zero,
 * and the parent process is set to XPTR_NULL. The th_tbl[] is initialized as empty.
 * The process GPT is initialised as required by the target architecture.
 * The "kcode" and "kdata" segments are registered in the process VSL.
 *********************************************************************************************
 * @ process  : [in] pointer on process descriptor to initialize.
 * @ info     : pointer on local boot_info_t (for kernel segments base and size). 
 ********************************************************************************************/
void process_zero_create( process_t   * process,
                          boot_info_t * info );

/*********************************************************************************************
 * This function allocates memory and initializes the "process_init" descriptor and the
 * associated "thread_init" descriptor. It is called once at the end of the kernel 
 * initialisation procedure. Its local process identifier is 1, and parent process 
 * is the kernel process in cluster 0.
 * The "process_init" is the first user process, and all other user processes will be forked
 * from this process. The code executed by "process_init" is stored in a .elf file, whose
 * pathname is defined by the CONFIG_PROCESS_INIT_PATH configuration variable. 
 * The process_init does not use the [STDIN/STDOUT/STDERR] streams, but it is linked
 * to kernel TXT0, because these streams must be defined for all user processes.
 ********************************************************************************************/
void process_init_create( void );

/*********************************************************************************************
 * This function initializes a reference, user process descriptor from another process
 * descriptor, defined by the <parent_xp> argument. The <process> and <pid> arguments 
 * are previously allocated by the caller. This function can be called by two functions:
 * 1) process_init_create() : process is the INIT process; parent is process-zero.
 * 2) process_make_fork() : the parent process descriptor is generally remote.
 * The following fields are initialised :
 * - It set the pid / ppid / ref_xp / parent_xp / state fields.
 * - It initializes the VMM (register the kentry, args, envs vsegs in VSL)
 * - It initializes the FDT, defining the three pseudo files STDIN / STDOUT / STDERR.
 *   . if INIT process     => link to kernel TXT[0].
 *   . if KSH[i] process   => allocate a free TXT[i] and give TXT ownership.
 *   . if USER process     => same TXT[i] as parent process and give TXT ownership.
 * - It set the root_xp, bin_xp, cwd_xp fields.
 * - It reset the children list as empty, but does NOT register it in parent children list.
 * - It reset the TH_TBL list of threads as empty.
 * - It reset the semaphore / mutex / barrier / condvar lists as empty.
 * - It registers the process in the local_list, rooted in the local cluster manager.
 * - It registers the process in the copies_list, rooted in the owner cluster manager.
 * - It registers the process extended pointer in the local pref_tbl[] array.
 *********************************************************************************************
 * @ process      : [in] pointer on local process descriptor to initialize.
 * @ pid          : [in] process identifier.
 * @ parent_xp    : [in] extended pointer on parent process descriptor.
 ********************************************************************************************/
void process_reference_init( process_t * process,
                             pid_t       pid,
                             xptr_t      parent_xp );

/*********************************************************************************************
 * This function initializes a copy process descriptor, in the local cluster,
 * from information defined in the reference remote process descriptor.
 *********************************************************************************************
 * @ process              : [in] local pointer on process descriptor to initialize.
 * @ reference_process_xp : [in] extended pointer on reference process descriptor.
 * @ return 0 if success / return ENOMEM if failure
 ********************************************************************************************/
error_t process_copy_init( process_t * local_process,
                           xptr_t      reference_process_xp );

/*********************************************************************************************
 * This function releases all memory allocated for a process descriptor in the local cluster,
 * including memory allocated for embedded substructures (fd_array, vmm, etc).
 * The local th_tbl[] array must be empty.
 *********************************************************************************************
 * @ process     : pointer on the process descriptor.
 ********************************************************************************************/
void process_destroy( process_t * process );

/*********************************************************************************************
 * This debug function diplays on the kernel terminal TXT0 detailed informations on a 
 * process descriptor identified by the <process_xp> argument.
 * It can be called by a thread running in any cluster.
 * WARNING: this function uses the nolock_printk() function, and the  TXT0 lock MUST be
 * taken by the caller function. 
 *********************************************************************************************
 * @ process_xp : extended pointer on process descriptor.
 ********************************************************************************************/
void process_display( xptr_t process_xp );

/*********************************************************************************************
 * This function returns a printable string defining the sigaction type.
 *********************************************************************************************
 * @ sigaction_type   : BLOCK_ALL_THREADS / UNBLOCK_ALL_THREADS / DELETE_ALL_THREADS
 * @ return a string pointer.
 ********************************************************************************************/
const char * process_action_str( process_sigactions_t sigaction_type );

/*********************************************************************************************
 * This function allows a client thread running in any cluster to block, unblock or delete 
 * all threads of a process identified by the <pid> argument, depending on the 
 * <action_type> argument.
 *
 * It uses the multicast, non blocking rpc_process_sigaction_client() function to send
 * parallel requests to all remote clusters containing process copies. 
 * Then it blocks and deschedule to wait completion of these parallel requests.
 *
 * It is used by the sys_kill() & sys_exit() functions to handle the "kill" & "exit" syscalls.
 * It is also used by the process_make_exec() function to handle the "exec" syscall.
 * It is also called by the TXT device ISR to execute the ctrl C & ctrl Z commands.
 *
 * WARNING (1) the DELETE action is NOT executed on the target process main thread
 * (thread 0 in process owner cluster), and not executed on the client thread itself.
 *
 * WARNING (2) the BLOCK action is executed on all target process threads, and this function
 * returns only when all threads BUT the client thread are actually blocked and not running.
 * When the client thread is also a target thread, it is blocked but not descheduled. 
 * 
 * WARNING (3) the UNBLOCK action is executed on all target process threads, as the
 * client thread cannot be a target thread.
 *
 * Implementation note:
 * This function allocates a - shared - RPC descriptor in client thread stack, 
 * and initializes it. This RPC descriptor can be shared because all parallel,
 * non-blocking, RPC server threads use the same input arguments, including the
 * RPC responses counter field.
 *********************************************************************************************
 * @ pid         : target process identifier.
 * @ action_type : BLOCK_ALL_THREADS / UNBLOCK_ALL_THREADS / DELETE_ALL_THREADS
 ********************************************************************************************/
void process_sigaction( pid_t       pid,
                        uint32_t    action_type );

/*********************************************************************************************
 * This function marks for delete all threads for a given <process> in the local cluster.
 * It scan the list of local thread, and sets the THREAD_FLAG_REQ_DELETE bit for all
 * threads, BUT the main thread (thread 0 in owner cluster), and the client thread
 * identified by the <client_xp> argument. 
 * The actual delete will be done by the scheduler at the next scheduling point.
 *********************************************************************************************
 * @ process     : pointer on the process descriptor.
 * @ client_xp   : extended pointer on the client thread that should not be marked.
 ********************************************************************************************/
void process_delete_threads( process_t * process,
                             xptr_t      client_xp );

/*********************************************************************************************
 * This function blocks all threads for a given <process> in the local cluster. 
 * It scan the list of local thread, and sets the THREAD_BLOCKED_GLOBAL bit for all threads.
 * It request the relevant schedulers to acknowledge the blocking, using IPI if required, 
 * when the target thread is running on another core than the calling thread.
 * It returns only when all threads in cluster, including the caller are actually blocked.
 * The threads are not detached from the scheduler, and not detached from the local process.
 *********************************************************************************************
 * @ process     : pointer on the target process descriptor.
 ********************************************************************************************/
void process_block_threads( process_t * process );

/*********************************************************************************************
 * This function unblocks all threads of a given user process in a given cluster.
 *********************************************************************************************
 * @ process     : pointer on the process descriptor.
 ********************************************************************************************/
void process_unblock_threads( process_t * process );

/*********************************************************************************************
 * This function returns a pointer on the local copy of a process identified by its PID.
 * If this local copy does not exist yet, it is dynamically created, from the reference
 * process descriptor, registered in the global copies_list, and registered in the local_list.
 * This function is used by the thread_user_create() function.
 *********************************************************************************************
 * @ pid     : searched process identifier.
 * @ returns pointer on the local process descriptor if success / returns NULL if failure.
 ********************************************************************************************/
process_t * process_get_local_copy( pid_t pid );

/*********************************************************************************************
 * This function returns the parent process identifier for a remote process descriptor
 * identified by an extended pointer.
 *********************************************************************************************
 * @ process_xp   : extended pointer on remote process descriptor.
 * @ returns parent process dentifier.
 ********************************************************************************************/
pid_t process_get_ppid( xptr_t process_xp );

/*********************************************************************************************
 * This function implements the "exec" system call, and is called by the sys_exec() function.
 * The "new" process keep the "old" process PID and PPID, all open files, and env variables,
 * the vfs_root and vfs_cwd, but build a brand new memory image (new VMM from the new .elf).
 * It is executed in the local cluster, that becomes both the "owner" and the "reference"
 * cluster for the "new" process.
 *********************************************************************************************
 * @ exec_info   : [in]  pointer on the exec_info structure.
 * @ return 0 if success / return non-zero if error.
 ********************************************************************************************/
error_t process_make_exec( exec_info_t * exec_info );

/*********************************************************************************************
 * This function implements the "fork" system call, and is called by the sys_fork() function,
 * likely throuch the RPC_PROCESS_MAKE_FORK. 
 * It allocates memory and initializes a new "child" process descriptor, and the associated
 * "child" thread descriptor in local cluster. It involves up to three different clusters :
 * - the child (local) cluster can be any cluster selected by the sys_fork function.
 * - the parent cluster must be the reference cluster for the parent process.
 * - the client cluster containing the thread requesting the fork can be any cluster.
 * The new "child" process descriptor is initialised from informations found in the "parent"
 * reference process descriptor, containing the complete process description.
 * The new "child" thread descriptor is initialised from informations found in the "parent"
 * thread descriptor.
 *********************************************************************************************
 * @ parent_process_xp  : extended pointer on the reference parent process.
 * @ parent_thread_xp   : extended pointer on the parent thread requesting the fork.
 * @ child_pid          : [out] child process identifier.
 * @ child_thread_ptr   : [out] local pointer on child thread in target cluster.
 * @ return 0 if success / return non-zero if error.
 ********************************************************************************************/
error_t process_make_fork(  xptr_t             parent_process_xp,
                            xptr_t             parent_thread_xp,
                            pid_t            * child_pid, 
                            struct thread_s ** child_thread_ptr );


/********************   File Management Operations   ****************************************/

/*********************************************************************************************
 * This function initializes all entries of the local fd_array as empty.
 *********************************************************************************************
 * @ process  : pointer on the local process descriptor.
 ********************************************************************************************/
void process_fd_init( process_t * process );

/*********************************************************************************************
 * This function allocates a free slot in the fd_array of the reference process
 * identified by the <process_xp> argument, register the <file_xp> argument in the
 * allocated slot, and return the slot index in the <fdid> buffer.
 * It can be called by any thread in any cluster, because it uses remote access
 * primitives to access the reference process descriptor.
 * It takes the lock protecting the reference fd_array against concurrent accesses.
 *********************************************************************************************
 * @ process_xp : [in]  extended pointer on client reference process.
 * @ file_xp    : [in]  extended pointer on the file descriptor to be registered.
 * @ fdid       : [out] buffer for fd_array slot index.
 * @ return 0 if success / return EMFILE if array full.
 ********************************************************************************************/
error_t process_fd_register( xptr_t      process_xp,
                             xptr_t      file_xp,
                             uint32_t  * fdid );

/*********************************************************************************************
 * This function uses as many remote accesses as required, to reset an entry in fd_array[],
 * in all clusters containing a copy. The entry is identified by the <fdid> argument.
 * This function must be executed by a thread running in reference cluster, that contains
 * the complete list of process descriptors copies.
 * It takes the lock protecting the reference fd_array against concurrent accesses.
 * TODO this function is not implemented yet.
 *********************************************************************************************
 * @ process  : [in] pointer on the local process descriptor.
 * @ fdid     : [in] file descriptor index in the fd_array.
 ********************************************************************************************/
void process_fd_remove( process_t * process,
                        uint32_t    fdid );

/*********************************************************************************************
 * This function returns an extended pointer on a file descriptor identified by its index
 * in fd_array. It can be called by any thread running in any cluster.
 * It accesses first the local process descriptor. In case of local miss, it takes
 * the lock protecting the reference fd_array, and access the reference process descriptor.
 * It updates the local fd_array when the file descriptor exists in reference cluster.
 * It takes the lock protecting the reference fd_array against concurrent accesses.
 * The file descriptor refcount is not incremented.
 *********************************************************************************************
 * @ process  : pointer on the local process descriptor.
 * @ fdid     : file descriptor index in the fd_array.
 * @ return extended pointer on file descriptor if success / return XPTR_NULL if not found.
 ********************************************************************************************/
xptr_t process_fd_get_xptr( process_t * process,
                            uint32_t    fdid );

/*********************************************************************************************
 * This function copies all non-zero entries (other than the three first stdin/stdout/stderr)
 * from a remote <src_xp> fd_array, embedded in a process descriptor, to another remote 
 * <dst_xp> fd_array, embedded in another process descriptor. 
 * The calling thread can be running in any cluster.
 * It takes the lock protecting the reference fd_array against concurrent accesses.
 * For each involved file descriptor, the refcount is incremented.
 *********************************************************************************************
 * @ dst_xp   : extended pointer on the destination fd_array_t.
 * @ src_xp   : extended pointer on the source fd_array_t.
 ********************************************************************************************/
void process_fd_remote_copy( xptr_t dst_xp,
                             xptr_t src_xp );

/*********************************************************************************************
 * This function checks the current number of open files for a given process.
 * It can be called by any thread in any cluster, because it uses portable remote access
 * primitives to access the reference process descriptor.
 * It does not take the lock protecting the reference fd_array.
 *********************************************************************************************
 * @ returns true if file descriptor array full.
 ********************************************************************************************/
bool_t process_fd_array_full( void );



/********************   Thread Related Operations   *****************************************/

/*********************************************************************************************
 * This function atomically registers a new thread in the local process descriptor.
 * It checks that there is an available slot in the local th_tbl[] array, and allocates
 * a new LTID using the relevant lock depending on the kernel/user type.
 *********************************************************************************************
 * @ process  : pointer on the local process descriptor.
 * @ thread   : pointer on new thread to be registered.
 * @ trdid    : [out] buffer for allocated trdid.
 * @ returns 0 if success / returns non zero if no slot available.
 ********************************************************************************************/
error_t process_register_thread( process_t       * process,
                                 struct thread_s * thread,
                                 trdid_t         * trdid );


/***************   Terminals related operations  ********************************************/

/*********************************************************************************************
 * This function scan the set of user TXT terminals to find a free terminal
 * (the list of attached processes is empty for a free TXT terminal).
 * It is called only by the process_reference_init() function when creating a KSH process.
 * It makes a kernel panic if no free TXT terminal is found.
 * The allocated TXT terminal is only released when the KSH process is deleted.
 *********************************************************************************************
 * @ return TXT terminal index if succes / kernel panic if no terminal found.
 ********************************************************************************************/
uint32_t process_txt_alloc( void );

/*********************************************************************************************
 * This function attach a process, identified by the <process> argument to a TXT terminal,
 * identified by the <txt_id> channel index argument.
 * The process descriptor identified by the <process> argument must be in the owner cluster.  
 * It insert the process descriptor in the xlist rooted in the TXT_RX device.
 * It is called by the process_reference_init() function.
 *********************************************************************************************
 * @ process  : local pointer on process descriptor.
 * @ txt_id   : TXT channel index.
 ********************************************************************************************/
void process_txt_attach( process_t * process,
                         uint32_t    txt_id );

/*********************************************************************************************
 * This function detach a process, identified by the <process_xp> argument,
 * from the list of process attached to a given TXT terminal. It transfer the TXT ownership
 * to another process, if the detached process is the TXT owner.
 * The process descriptor identified by the <process_xp> argument must be in the owner
 * cluster, but the calling thread can be running in any cluster. 
 *********************************************************************************************
 * @ process_xp  : extended pointer on process descriptor.
 ********************************************************************************************/
void process_txt_detach( xptr_t  process_xp );                     

/*********************************************************************************************
 * This function gives a process identified by the <process_xp> argument the exclusive
 * ownership of its attached TXT_RX terminal (i.e. put the process in foreground).
 * It can be called by a thread running in any cluster, but the <process_xp> must be the
 * owner cluster process descriptor. 
 *********************************************************************************************
 * @ owner_xp  : extended pointer on process descriptor in owner cluster.
 ********************************************************************************************/
void process_txt_set_ownership( xptr_t process_xp );

/*********************************************************************************************
 * When the process identified by the <owner_xp> argument has the exclusive ownership of
 * the TXT_RX terminal, this function transfer this ownership to another attached process.
 * The process descriptor must be the process owner.
 * This function does nothing if the process identified by the <process_xp> is not 
 * the TXT owner.
 * - If the current owner is not the KSH process, the new owner is the KSH process.
 * - If the current owner is the KSH process, the new owner is another attached process.
 * - If there is no other attached process, the TXT has no more defined owner. 
 *********************************************************************************************
 * @ process_xp  : extended pointer on process descriptor in owner cluster.
 ********************************************************************************************/
void process_txt_transfer_ownership( xptr_t process_xp );

/*********************************************************************************************
 * This function returns true if the  process identified by the <process_xp> argument
 * is the TXT owner. It can be called by a thread running in any cluster, but the
 * process_xp must be the owner cluster process descriptor. 
 *********************************************************************************************
 * @ returns true if target process is TXT owner.
 ********************************************************************************************/
bool_t process_txt_is_owner( xptr_t process_xp );

/*********************************************************************************************
 * This function returns an extended ponter on the current TXT owner process, 
 * for the TXT terminal identified by the <channel> index. 
 *********************************************************************************************
 * @ channel : TXT channel.
 * @ return extended pointer on TXT owner process.
 ********************************************************************************************/
xptr_t process_txt_get_owner( uint32_t channel );

/*********************************************************************************************
 * This debug function diplays on the kernel terminal the list of processes attached
 * to a given terminal identified by the <txt_id> argument.
 *********************************************************************************************
 * @ txt_id  : TXT terminal channel.
 ********************************************************************************************/
void process_txt_display( uint32_t txt_id );


#endif	/* _PROCESS_H_ */
