/*
 * printk.c - Kernel Log & debug messages API implementation.
 * 
 * authors  Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH..
 *
 * ALMOS-MKH. is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH. is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH.; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_irqmask.h>
#include <hal_special.h>
#include <dev_txt.h>
#include <remote_busylock.h>
#include <cluster.h>
#include <thread.h>
#include <chdev.h>
#include <printk.h>
#include <shared_syscalls.h>

///////////////////////////////////////////////////////////////////////////////////
//      Extern variables
///////////////////////////////////////////////////////////////////////////////////

extern chdev_directory_t  chdev_dir;  // defined in chdev.h / allocated in kernel_init.c

/////////////////////////////////////
uint32_t snprintf( char     * string,
                   uint32_t   length,
                   char     * format, ... )
{

#define TO_STREAM(x) do { string[ps] = (x); ps++; if(ps==length) return -1; } while(0);

    va_list    args;      // printf arguments
    uint32_t   ps;        // pointer to the string buffer

    ps = 0;   
    va_start( args , format );

xprintf_text:

    while ( *format != 0 ) 
    {
        if (*format == '%')   // copy argument to string
        {
            format++;
            goto xprintf_arguments;
        }
        else                  // copy one char to string
        {
            TO_STREAM( *format );
            format++;
        }
    }

    va_end( args );
    
    // add terminating NUL chracter
    TO_STREAM( 0 );
    return ps;

xprintf_arguments:

    {
        char              buf[30];    // buffer to display one number
        char *            pbuf;       // pointer on first char to display
        uint32_t          len = 0;    // number of char to display
        static const char HexaTab[] = "0123456789ABCDEF";
        uint32_t          i;
        
        // Ignore fields width and precision 
        for ( ; (*format >= '0' && *format <= '9') || (*format == '.') ; format++ );

        switch (*format) 
        {
            case ('c'):             // char conversion 
            {
                int val = va_arg( args, int );
                buf[0] = val;
                pbuf   = buf;
                len    = 1;
                break;
            }
            case ('b'):             // excactly 2 digits hexadecimal integer
            {
                int  val = va_arg( args, int );
                int  val_lsb = val & 0xF;
                int  val_msb = (val >> 4) & 0xF;
                buf[0] = HexaTab[val_msb];
                buf[1] = HexaTab[val_lsb];
                len  = 2;
                pbuf = buf;
                break;
            }
            case ('d'):             // up to 10 digits decimal signed integer
            {
                int val = va_arg( args, int );
                if (val < 0) 
                {
                    TO_STREAM( '-' );
                    val = -val;
                }
                for(i = 0; i < 10; i++) 
                {
                    buf[9 - i] = HexaTab[val % 10];
                    if (!(val /= 10)) break;
                }
                len =  i + 1;
                pbuf = &buf[9 - i];
                break;
            }
            case ('u'):             // up to 10 digits decimal unsigned integer
            {
                uint32_t val = va_arg( args, uint32_t );
                for(i = 0; i < 10; i++) 
                {
                    buf[9 - i] = HexaTab[val % 10];
                    if (!(val /= 10)) break;
                }
                len =  i + 1;
                pbuf = &buf[9 - i];
                break;
            }
            case ('x'):             // up to 8 digits hexadecimal 
            case ('l'):             // up to 16 digits hexadecimal 
            {
                uint32_t imax;
                uint64_t val;
                
                if ( *format == 'l' )   // 64 bits
                {
                    val = va_arg( args, uint64_t);
                    imax = 16;
                }
                else                    // 32 bits
                {
                    val = va_arg( args, uint32_t);
                    imax = 8;
                }
                
                TO_STREAM( '0' );
                TO_STREAM( 'x' );
                
                for(i = 0; i < imax; i++) 
                {
                    buf[(imax-1) - i] = HexaTab[val % 16];
                    if (!(val /= 16))  break;
                }
                len =  i + 1;
                pbuf = &buf[(imax-1) - i];
                break;
            }
            case ('X'):             // exactly 8 digits hexadecimal
            {
                uint32_t val = va_arg( args , uint32_t );
                for(i = 0; i < 8; i++) 
                {
                    buf[7 - i] = HexaTab[val % 16];
                    val = (val>>4);
                }
                len =  8;
                pbuf = buf;
                break;
            }
            case ('s'):             /* string */
            {
                char* str = va_arg( args, char* );
                while (str[len]) { len++; }
                pbuf = str;
                break;
            }
            default:       // unsupported argument type
            {
                return -1;
            }
        }  // end switch on  argument type

        format++;

        // copy argument to string
        for( i = 0 ; i < len ; i++ )
        {
            TO_STREAM( pbuf[i] );
        }
        
        goto xprintf_text;
    }
} // end snprintf()

//////////////////////////////////////////////////////////////////////////////////////
// This static function is called by printk(), assert() and nolock_printk()
// to display a formated string on TXT0, using a busy waiting policy.
//////////////////////////////////////////////////////////////////////////////////////
// @ format    : printf like format.
// @ args      : va_list of arguments.
//////////////////////////////////////////////////////////////////////////////////////
static void kernel_printf( const char * format, 
                           va_list    * args ) 
{

printf_text:

    while (*format) 
    {
        uint32_t i;
        for (i = 0 ; format[i] && (format[i] != '%') ; i++);
        if (i) 
        {
            dev_txt_sync_write( format, i );
            format += i;
        }
        if (*format == '%') 
        {
            format++;
            goto printf_arguments;
        }
    }

    return;

printf_arguments:

    {
        char      buf[20];
        char    * pbuf = NULL;
        uint32_t  len  = 0;
        static const char HexaTab[] = "0123456789ABCDEF";
        uint32_t i;

        switch (*format++) 
        {
            case ('c'):             /* char conversion */
            {
                int  val = va_arg( *args , int );
                len = 1;
                buf[0] = (char)val;
                pbuf = &buf[0];
                break;
            }
            case ('b'):             // excactly 2 digits hexadecimal
            {
                int  val = va_arg( *args, int );
                int  val_lsb = val & 0xF;
                int  val_msb = (val >> 4) & 0xF;
                buf[0] = HexaTab[val_msb];
                buf[1] = HexaTab[val_lsb];
                len  = 2;
                pbuf = buf;
                break;
            }
            case ('d'):             /* up to 10 digits signed decimal */
            {
                int val = va_arg( *args , int );
                if (val < 0) 
                {
                    val = -val;
                    dev_txt_sync_write( "-" , 1 );
                }
                for(i = 0; i < 10; i++) 
                {
                    buf[9 - i] = HexaTab[val % 10];
                    if (!(val /= 10)) break;
                }
                len =  i + 1;
                pbuf = &buf[9 - i];
                break;
            }
            case ('u'):             /* up to 10 digits unsigned decimal */
            {
                uint32_t val = va_arg( *args , uint32_t );
                for(i = 0; i < 10; i++) 
                {
                    buf[9 - i] = HexaTab[val % 10];
                    if (!(val /= 10)) break;
                }
                len =  i + 1;
                pbuf = &buf[9 - i];
                break;
            }
            case ('x'):             /* up to 8 digits hexadecimal */
            {
                uint32_t val = va_arg( *args , uint32_t );
                dev_txt_sync_write( "0x" , 2 );
                for(i = 0; i < 8; i++) 
                {
                    buf[7 - i] = HexaTab[val & 0xF];
                    if (!(val = (val>>4)))  break;
                }
                len =  i + 1;
                pbuf = &buf[7 - i];
                break;
            }
            case ('X'):             /* exactly 8 digits hexadecimal */
            {
                uint32_t val = va_arg( *args , uint32_t );
                dev_txt_sync_write( "0x" , 2 );
                for(i = 0; i < 8; i++) 
                {
                    buf[7 - i] = HexaTab[val & 0xF];
                    val = (val>>4);
                }
                len =  8;
                pbuf = buf;
                break;
            }
            case ('l'):            /* up to 16 digits hexadecimal */
            {
                uint64_t val = va_arg( *args , uint64_t );
                dev_txt_sync_write( "0x" , 2 );
                for(i = 0; i < 16; i++) 
                {
                    buf[15 - i] = HexaTab[val & 0xF];
                    if (!(val = (val>>4)))  break;
                }
                len =  i + 1;
                pbuf = &buf[15 - i];
                break;
            }
            case ('L'):           /* exactly 16 digits hexadecimal */ 
            {
                uint64_t val = va_arg( *args , uint64_t );
                dev_txt_sync_write( "0x" , 2 );
                for(i = 0; i < 16; i++) 
                {
                    buf[15 - i] = HexaTab[val & 0xF];
                    val = (val>>4);
                }
                len =  16;
                pbuf = buf;
                break;
            }
            case ('s'):             /* string */
            {
                char* str = va_arg( *args , char* );
                while (str[len]) 
                {
                    len++;
                }
                pbuf = str;
                break;
            }
            default:
            {
                dev_txt_sync_write( "\n[PANIC] in kernel_printf() : illegal format\n", 45 );
            }
        }

        if( pbuf != NULL ) dev_txt_sync_write( pbuf, len );
        
        goto printf_text;
    }

}  // end kernel_printf()

//////////////////////////////////
void printk( char * format , ... )
{
    va_list       args;

    // get pointers on TXT0 chdev
    xptr_t    txt0_xp  = chdev_dir.txt_tx[0];
    cxy_t     txt0_cxy = GET_CXY( txt0_xp );
    chdev_t * txt0_ptr = GET_PTR( txt0_xp );

    // get extended pointer on remote TXT0 lock
    xptr_t  lock_xp = XPTR( txt0_cxy , &txt0_ptr->wait_lock );

    // get TXT0 lock
    remote_busylock_acquire( lock_xp );

    // display format on TXT0 in busy waiting mode
    va_start( args , format );
    kernel_printf( format , &args );
    va_end( args );

    // release TXT0 lock
    remote_busylock_release( lock_xp );
}

/////////////////////////////////////////
void nolock_printk( char * format , ... )
{
    va_list       args;

    // call kernel_printf on TXT0, in busy waiting mode
    va_start( args , format );
    kernel_printf( format , &args );
    va_end( args );
}

////////////////////////////////////
void panic( const char * function_name,
            uint32_t     line,
            cycle_t      cycle,
            const char * format,
            ... )
{
    // get pointers on TXT0 chdev
    xptr_t    txt0_xp = chdev_dir.txt_tx[0];
    cxy_t     txt0_cxy = GET_CXY(txt0_xp);
    chdev_t * txt0_ptr = GET_PTR(txt0_xp);

    // get extended pointer on remote TXT0 lock
    xptr_t lock_xp = XPTR( txt0_cxy , &txt0_ptr->wait_lock );

    // get TXT0 lock 
    remote_busylock_acquire( lock_xp );

    // get calling thread
    thread_t * current = CURRENT_THREAD;

    // print generic infos
    nolock_printk("\n[PANIC] in %s: line %d | cycle %d\n"
                  "core[%x,%d] | thread %x (%x) | process %x (%x)\n",
                  function_name, line, (uint32_t)cycle,
                  local_cxy, current->core->lid, 
                  current->trdid, current,
                  current->process->pid, current->process );

    // call kernel_printf to print format
    va_list args;
    va_start(args, format);
    kernel_printf(format, &args);
    va_end(args);

    // release TXT0 lock
    remote_busylock_release( lock_xp );

    // suicide
    hal_core_sleep();
}

//////////////////////////
void puts( char * string ) 
{
    uint32_t   n = 0;

    // compute string length
    while ( string[n] > 0 ) n++;

    // get pointers on TXT0 chdev
    xptr_t    txt0_xp  = chdev_dir.txt_tx[0];
    cxy_t     txt0_cxy = GET_CXY( txt0_xp );
    chdev_t * txt0_ptr = GET_PTR( txt0_xp );

    // get extended pointer on remote TXT0 lock
    xptr_t  lock_xp = XPTR( txt0_cxy , &txt0_ptr->wait_lock );

    // get TXT0 lock 
    remote_busylock_acquire( lock_xp );

    // display string on TTY0
    dev_txt_sync_write( string , n );

    // release TXT0 lock
    remote_busylock_release( lock_xp );
}


/////////////////////////
void putx( uint32_t val )
{
    static const char HexaTab[] = "0123456789ABCDEF";

    char      buf[10];
    uint32_t  c;

    buf[0] = '0';
    buf[1] = 'x';

    // build buffer
    for (c = 0; c < 8; c++) 
    { 
        buf[9 - c] = HexaTab[val & 0xF];
        val = val >> 4;
    }

    // get pointers on TXT0 chdev
    xptr_t    txt0_xp  = chdev_dir.txt_tx[0];
    cxy_t     txt0_cxy = GET_CXY( txt0_xp );
    chdev_t * txt0_ptr = GET_PTR( txt0_xp );

    // get extended pointer on remote TXT0 chdev lock
    xptr_t  lock_xp = XPTR( txt0_cxy , &txt0_ptr->wait_lock );

    // get TXT0 lock 
    remote_busylock_acquire( lock_xp );

    // display string on TTY0
    dev_txt_sync_write( buf , 10 );

    // release TXT0 lock
    remote_busylock_release( lock_xp );
}

/////////////////////////
void putl( uint64_t val )
{
    static const char HexaTab[] = "0123456789ABCDEF";

    char      buf[18];
    uint32_t  c;

    buf[0] = '0';
    buf[1] = 'x';

    // build buffer
    for (c = 0; c < 16; c++) 
    { 
        buf[17 - c] = HexaTab[(unsigned int)val & 0xF];
        val = val >> 4;
    }

    // get pointers on TXT0 chdev
    xptr_t    txt0_xp  = chdev_dir.txt_tx[0];
    cxy_t     txt0_cxy = GET_CXY( txt0_xp );
    chdev_t * txt0_ptr = GET_PTR( txt0_xp );

    // get extended pointer on remote TXT0 chdev lock
    xptr_t  lock_xp = XPTR( txt0_cxy , &txt0_ptr->wait_lock );

    // get TXT0 lock 
    remote_busylock_acquire( lock_xp );

    // display string on TTY0
    dev_txt_sync_write( buf , 18 );

    // release TXT0 lock
    remote_busylock_release( lock_xp );
}

/////////////////////////////
void putb( char     * string,
           uint8_t  * buffer,
           uint32_t   size )
{
    uint32_t line;
    uint32_t byte = 0;

    // get pointers on TXT0 chdev
    xptr_t    txt0_xp  = chdev_dir.txt_tx[0];
    cxy_t     txt0_cxy = GET_CXY( txt0_xp );
    chdev_t * txt0_ptr = GET_PTR( txt0_xp );

    // get extended pointer on remote TXT0 chdev lock
    xptr_t  lock_xp = XPTR( txt0_cxy , &txt0_ptr->wait_lock );

    // get TXT0 lock 
    remote_busylock_acquire( lock_xp );

    // display string on TTY0
    nolock_printk("\n***** %s *****\n", string );

    for ( line = 0 ; line < (size>>4) ; line++ )
    {
         nolock_printk(" %X | %b %b %b %b | %b %b %b %b | %b %b %b %b | %b %b %b %b \n",
         byte,
         buffer[byte+ 0],buffer[byte+ 1],buffer[byte+ 2],buffer[byte+ 3],
         buffer[byte+ 4],buffer[byte+ 5],buffer[byte+ 6],buffer[byte+ 7],
         buffer[byte+ 8],buffer[byte+ 9],buffer[byte+10],buffer[byte+11],
         buffer[byte+12],buffer[byte+13],buffer[byte+14],buffer[byte+15] );

         byte += 16;
    }

    // release TXT0 lock
    remote_busylock_release( lock_xp );
}



// Local Variables:
// tab-width: 4
// c-basic-offset: 4
// c-file-offsets:((innamespace . 0)(inline-open . 0))
// indent-tabs-mode: nil
// End:
// vim: filetype=c:expandtab:shiftwidth=4:tabstop=4:softtabstop=4

