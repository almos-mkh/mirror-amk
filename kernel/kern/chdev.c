/*
 * chdev.c - channel device descriptor operations implementation.
 *
 * Authors  Alain Greiner   (2016)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH.is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH.is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH.; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <kernel_config.h>
#include <hal_kernel_types.h>
#include <hal_special.h>
#include <hal_remote.h>
#include <hal_irqmask.h>
#include <printk.h>
#include <boot_info.h>
#include <xlist.h>
#include <kmem.h>
#include <scheduler.h>
#include <thread.h>
#include <rpc.h>
#include <chdev.h>
#include <devfs.h>

//////////////////////////////////////////////////////////////////////////////////////
// Extern global variables
//////////////////////////////////////////////////////////////////////////////////////

extern chdev_directory_t    chdev_dir;         // allocated in kernel_init.c


#if (DEBUG_SYS_READ & 1)
extern uint32_t enter_chdev_cmd_read;
extern uint32_t exit_chdev_cmd_read;
extern uint32_t enter_chdev_server_read;
extern uint32_t exit_chdev_server_read;
#endif

#if (DEBUG_SYS_WRITE & 1)
extern uint32_t enter_chdev_cmd_write;
extern uint32_t exit_chdev_cmd_write;
extern uint32_t enter_chdev_server_write;
extern uint32_t exit_chdev_server_write;
#endif

////////////////////////////////////////////
char * chdev_func_str( uint32_t func_type ) 
{
    switch ( func_type ) 
    {
        case DEV_FUNC_RAM: return "RAM";
        case DEV_FUNC_ROM: return "ROM";
        case DEV_FUNC_FBF: return "FBF";
        case DEV_FUNC_IOB: return "IOB";
        case DEV_FUNC_IOC: return "IOC";
        case DEV_FUNC_MMC: return "MMC";
        case DEV_FUNC_DMA: return "DMA";
        case DEV_FUNC_NIC: return "NIC";
        case DEV_FUNC_TIM: return "TIM";
        case DEV_FUNC_TXT: return "TXT";
        case DEV_FUNC_ICU: return "ICU";
        case DEV_FUNC_PIC: return "PIC";
        default:           return "undefined";
    }
}

/////////////////////////////////////////
chdev_t * chdev_create( uint32_t    func,
                        uint32_t    impl,
                        uint32_t    channel,
                        uint32_t    is_rx,
                        xptr_t      base )
{
    chdev_t    * chdev;
    kmem_req_t   req;

    // allocate memory for chdev
    req.type   = KMEM_DEVICE;
    req.flags  = AF_ZERO;
    chdev      = (chdev_t *)kmem_alloc( &req );

    if( chdev == NULL ) return NULL;

    // initialize lock
    remote_busylock_init( XPTR( local_cxy , &chdev->wait_lock ), LOCK_CHDEV_QUEUE );

    // initialise waiting queue
    xlist_root_init( XPTR( local_cxy , &chdev->wait_root ) );

    // initialize attributes
    chdev->func    =  func;
    chdev->impl    =  impl;
    chdev->channel =  channel;
    chdev->is_rx   =  is_rx;
    chdev->base    =  base; 

    return chdev;

}  // end chdev_create()

///////////////////////////////////
void chdev_print( chdev_t * chdev )
{
    printk("\n - func      = %s"
           "\n - channel   = %d"
           "\n - base      = %l"
           "\n - cmd       = %x"
           "\n - isr       = %x"
           "\n - chdev     = %x\n",
           chdev_func_str(chdev->func),
           chdev->channel,
           chdev->base,
           chdev->cmd,
           chdev->isr,
           chdev );
}

//////////////////////////////////////////////////
void chdev_register_command( xptr_t     chdev_xp )
{
    thread_t * server_ptr;    // local pointer on server thread associated to chdev
    xptr_t     server_xp;     // extended pointer on server thread
    core_t   * core_ptr;      // local pointer on core running the server thread
    uint32_t   server_lid;    // core running the server thread local index
    xptr_t     lock_xp;       // extended pointer on lock protecting the chdev state
    uint32_t   save_sr;       // for critical section

#if (DEBUG_SYS_READ & 1)
enter_chdev_cmd_read = (uint32_t)hal_get_cycles();
#endif

#if (DEBUG_SYS_WRITE & 1)
enter_chdev_cmd_write = (uint32_t)hal_get_cycles();
#endif

    thread_t * this = CURRENT_THREAD;

    // get chdev cluster and local pointer
    cxy_t     chdev_cxy = GET_CXY( chdev_xp );
    chdev_t * chdev_ptr = GET_PTR( chdev_xp );

    // check calling thread can yield
    thread_assert_can_yield( this , __FUNCTION__ );

    // get local and extended pointers on server thread
    server_ptr = (thread_t *)hal_remote_lpt( XPTR( chdev_cxy , &chdev_ptr->server) );
    server_xp  = XPTR( chdev_cxy , server_ptr );

    // get local pointer on core running the server thread
    core_ptr   = (core_t *)hal_remote_lpt( XPTR( chdev_cxy , &server_ptr->core ) );

    // get server core local index
    server_lid = hal_remote_l32( XPTR( chdev_cxy , &core_ptr->lid ) );

#if (DEBUG_CHDEV_CMD_RX || DEBUG_CHDEV_CMD_TX)
bool_t      is_rx        = hal_remote_l32( XPTR( chdev_cxy , &chdev_ptr->is_rx ) );
trdid_t     server_trdid = hal_remote_l32( XPTR( chdev_cxy , &server_ptr->trdid ) ); 
process_t * process_ptr  = hal_remote_lpt( XPTR( chdev_cxy , &server_ptr->process ) );
pid_t       server_pid   = hal_remote_l32( XPTR( chdev_cxy , &process_ptr->pid ) );
#endif
    
#if DEBUG_CHDEV_CMD_RX
uint32_t rx_cycle = (uint32_t)hal_get_cycles();
if( (is_rx) && (DEBUG_CHDEV_CMD_RX < rx_cycle) )
printk("\n[%s] client[%x,%x] enter for RX / server[%x,%x] / cycle %d\n",
__FUNCTION__, this->process->pid, this->trdid, server_pid, server_trdid, rx_cycle );
#endif

#if DEBUG_CHDEV_CMD_TX
uint32_t tx_cycle = (uint32_t)hal_get_cycles();
if( (is_rx == 0) && (DEBUG_CHDEV_CMD_TX < tx_cycle) )
printk("\n[%s] client[%x,%x] enter for TX / server[%x,%x] / cycle %d\n",
__FUNCTION__, this->process->pid, this->trdid, server_pid, server_trdid, tx_cycle );
#endif

    // build extended pointer on client thread xlist
    xptr_t  list_xp    = XPTR( local_cxy , &this->wait_list );

    // build extended pointer on chdev waiting queue root
    xptr_t  root_xp    = XPTR( chdev_cxy , &chdev_ptr->wait_root );

    // build extended pointer on server thread blocked state
    xptr_t  blocked_xp = XPTR( chdev_cxy , &server_ptr->blocked );

    // build extended pointer on lock protecting chdev waiting queue
    lock_xp            = XPTR( chdev_cxy , &chdev_ptr->wait_lock );

    // TODO the hal_disable_irq() / hal_restore_irq() 
    // in the sequence below is probably useless, as it is 
    // already done by the busylock_acquire() / busylock_release()
    // => remove it [AG] october 2018

    // critical section for the following sequence: 
    // (1) take the lock protecting the chdev state
    // (2) block the client thread
    // (3) unblock the server thread if required
    // (4) register client thread in server queue 
    // (5) send IPI to force server scheduling
    // (6) release the lock protecting waiting queue
    // (7) deschedule

    // enter critical section
    hal_disable_irq( &save_sr );

    // take the lock protecting chdev queue
    remote_busylock_acquire( lock_xp );

    // block current thread
    thread_block( XPTR( local_cxy , CURRENT_THREAD ) , THREAD_BLOCKED_IO );

#if (DEBUG_CHDEV_CMD_TX & 1)
if( (is_rx == 0) && (DEBUG_CHDEV_CMD_TX < tx_cycle) )
printk("\n[%s] client thread[%x,%x] blocked\n",
__FUNCTION__, this->process->pid, this->trdid );
#endif

#if (DEBUG_CHDEV_CMD_RX & 1)
if( (is_rx) && (DEBUG_CHDEV_CMD_RX < rx_cycle) )
printk("\n[%s] client thread[%x,%x] blocked\n",
__FUNCTION__, this->process_pid, this->trdid );
#endif

    // unblock server thread if required
    if( hal_remote_l32( blocked_xp ) & THREAD_BLOCKED_IDLE )
    thread_unblock( server_xp , THREAD_BLOCKED_IDLE );

#if (DEBUG_CHDEV_CMD_TX & 1)
if( (is_rx == 0) && (DEBUG_CHDEV_CMD_TX < tx_cycle) )
printk("\n[%s] TX server thread[%x,%x] unblocked\n",
__FUNCTION__, server_pid, server_trdid );
#endif

#if (DEBUG_CHDEV_CMD_RX & 1)
if( (is_rx) && (DEBUG_CHDEV_CMD_RX < rx_cycle) )
printk("\n[%s] RX server thread[%x,%x] unblocked\n",
__FUNCTION__, server_pid, server_trdid );
#endif

    // register client thread in waiting queue 
    xlist_add_last( root_xp , list_xp );

#if (DEBUG_CHDEV_CMD_TX & 1)
if( (is_rx == 0)  && (DEBUG_CHDEV_CMD_TX < tx_cycle) )
printk("\n[%s] client thread[%x,%x] registered write request in chdev\n",
__FUNCTION__, this->process->pid, this->trdid );
#endif
 
#if (DEBUG_CHDEV_CMD_RX & 1)
if( (is_rx)  && (DEBUG_CHDEV_CMD_RX < rx_cycle) )
printk("\n[%s] client thread[%x,%x] registered read request in chdev\n",
__FUNCTION__, this->process->pid, this->trdid );
#endif
 
    // send IPI to core running the server thread when server core != client core
    if( (server_lid != this->core->lid) || (local_cxy != chdev_cxy) )
    {
        dev_pic_send_ipi( chdev_cxy , server_lid ); 

#if (DEBUG_CHDEV_CMD_TX & 1)
if( (is_rx == 0)  && (DEBUG_CHDEV_CMD_TX < tx_cycle) )
printk("\n[%s] client thread[%x,%x] sent IPI to TX server thread[%x,%x]\n",
__FUNCTION__, this->process->pid, this->trdid, server_pid, server_trdid );
#endif

#if (DEBUG_CHDEV_CMD_RX & 1)
if( (is_rx)  && (DEBUG_CHDEV_CMD_RX < rx_cycle) )
printk("\n[%s] client thread[%x,%x] sent IPI to RX server thread[%x,%x]\n",
__FUNCTION__, this->process->pid, this->trdid, server_pid, server_trdid );
#endif

    }
 
    // release lock protecting chdev queue
    remote_busylock_release( lock_xp );

    // deschedule
    sched_yield("blocked on I/O");

    // exit critical section
    hal_restore_irq( save_sr );

#if DEBUG_CHDEV_CMD_RX
rx_cycle = (uint32_t)hal_get_cycles();
if( (is_rx) && (DEBUG_CHDEV_CMD_RX < rx_cycle) )
printk("\n[%s] client_thread[%x,%x] exit for RX / cycle %d\n",
__FUNCTION__, this->process->pid, this->trdid, rx_cycle );
#endif

#if DEBUG_CHDEV_CMD_TX
tx_cycle = (uint32_t)hal_get_cycles();
if( (is_rx == 0) && (DEBUG_CHDEV_CMD_TX < tx_cycle) )
printk("\n[%s] client_thread[%x,%x] exit for TX / cycle %d\n",
__FUNCTION__, this->process->pid, this->trdid, tx_cycle );
#endif

#if (DEBUG_SYS_READ & 1)
exit_chdev_cmd_read = (uint32_t)hal_get_cycles();
#endif

#if (DEBUG_SYS_WRITE & 1)
exit_chdev_cmd_write = (uint32_t)hal_get_cycles();
#endif

}  // end chdev_register_command()

/////////////////////////////////////////
void chdev_server_func( chdev_t * chdev )
{
    xptr_t          client_xp;    // extended pointer on waiting thread
    cxy_t           client_cxy;   // cluster of client thread
    thread_t      * client_ptr;   // local pointer on client thread
    thread_t      * server;       // local pointer on server thread
    xptr_t          root_xp;      // extended pointer on device waiting queue root
    xptr_t          lock_xp;      // extended pointer on lock ptotecting chdev queue

    server = CURRENT_THREAD;

    // build extended pointer on root of client threads queue
    root_xp = XPTR( local_cxy , &chdev->wait_root );

    // build extended pointer on lock protecting client threads queue
    lock_xp = XPTR( local_cxy , &chdev->wait_lock );

	// This infinite loop is executed by the DEV thread
    // to handle commands registered in the chdev queue.
    while( 1 )
    {

#if( DEBUG_CHDEV_SERVER_RX || DEBUG_CHDEV_SERVER_TX )
uint32_t rx_cycle = (uint32_t)hal_get_cycles();
if( (chdev->is_rx) && (DEBUG_CHDEV_SERVER_RX < rx_cycle) )
printk("\n[%s] DEV thread[%x,%x] check TXT_RX channel %d / cycle %d\n",
__FUNCTION__ , server->process->pid, server->trdid, chdev->channel, rx_cycle );
#endif

#if DEBUG_CHDEV_SERVER_TX
uint32_t tx_cycle = (uint32_t)hal_get_cycles();
if( (chdev->is_rx == 0) && (DEBUG_CHDEV_SERVER_TX < tx_cycle) )
printk("\n[%s] thread[%x,%x] check TXT_TX channel %d / cycle %d\n",
__FUNCTION__ , server->process->pid, server->trdid, chdev->channel, tx_cycle );
#endif

        // check server thread can yield
        thread_assert_can_yield( server , __FUNCTION__ );

        // get the lock protecting the waiting queue
        remote_busylock_acquire( lock_xp );

        // check waiting queue state
        if( xlist_is_empty( root_xp ) ) // waiting queue empty 
        {
            // release lock protecting the waiting queue
            remote_busylock_release( lock_xp );

#if DEBUG_CHDEV_SERVER_RX
rx_cycle = (uint32_t)hal_get_cycles();
if( (chdev->is_rx) && (DEBUG_CHDEV_SERVER_RX < rx_cycle) )
printk("\n[%s] thread[%x,%x] found RX queue empty => blocks / cycle %d\n",
__FUNCTION__ , server->process->pid, server->trdid, rx_cycle );
#endif

#if DEBUG_CHDEV_SERVER_TX
tx_cycle = (uint32_t)hal_get_cycles();
if( (chdev->is_rx == 0) && (DEBUG_CHDEV_SERVER_TX < tx_cycle) )
printk("\n[%s] thread[%x,%x] found TX queue empty => blocks / cycle %d\n",
__FUNCTION__ , server->process->pid, server->trdid, tx_cycle );
#endif
            // block 
            thread_block( XPTR( local_cxy , server ) , THREAD_BLOCKED_IDLE ); 

            // deschedule
            sched_yield("I/O queue empty");
        } 
        else                            // waiting queue not empty
        {
            // release lock protecting the waiting queue
            remote_busylock_release( lock_xp );

            // get extended pointer on first client thread
            client_xp = XLIST_FIRST( root_xp , thread_t , wait_list );

            // get client thread cluster and local pointer
            client_cxy = GET_CXY( client_xp );
            client_ptr = GET_PTR( client_xp );

#if( DEBUG_CHDEV_SERVER_TX || DEBUG_CHDEV_SERVER_RX )
process_t * process      = hal_remote_lpt( XPTR( client_cxy , &client_ptr->process ) );
pid_t       client_pid   = hal_remote_l32( XPTR( client_cxy , &process->pid ) );
trdid_t     client_trdid = hal_remote_l32( XPTR( client_cxy , &client_ptr->trdid ) );
#endif

#if DEBUG_CHDEV_SERVER_RX
rx_cycle = (uint32_t)hal_get_cycles();
if( (chdev->is_rx) && (DEBUG_CHDEV_SERVER_RX < rx_cycle) )
printk("\n[%s] thread[%x,%x] for RX get client thread[%x,%x] / cycle %d\n",
__FUNCTION__, server->process->pid, server->trdid, client_pid, client_trdid, rx_cycle );
#endif

#if DEBUG_CHDEV_SERVER_TX
tx_cycle = (uint32_t)hal_get_cycles();
if( (chdev->is_rx == 0) && (DEBUG_CHDEV_SERVER_TX < tx_cycle) )
printk("\n[%s] thread[%x,%x] for TX get client thread[%x,%x] / cycle %d\n",
__FUNCTION__, server->process->pid, server->trdid, client_pid, client_trdid, tx_cycle );
#endif

#if (DEBUG_SYS_READ & 1)
enter_chdev_server_read = (uint32_t)hal_get_cycles();
#endif

#if (DEBUG_SYS_WRITE & 1)
enter_chdev_server_write = (uint32_t)hal_get_cycles();
#endif

            // call the (blocking) driver command function
            // to launch I/O operation AND wait completion
            chdev->cmd( client_xp );
        
            // unblock client thread when driver returns
            thread_unblock( client_xp , THREAD_BLOCKED_IO );

            // get the lock protecting the waiting queue
            remote_busylock_acquire( lock_xp );

            // remove this client thread from chdev waiting queue
            xlist_unlink( XPTR( client_cxy , &client_ptr->wait_list ) );

            // release lock protecting the waiting queue
            remote_busylock_release( lock_xp );

#if DEBUG_CHDEV_SERVER_RX
rx_cycle = (uint32_t)hal_get_cycles();
if( (chdev->is_rx) && (DEBUG_CHDEV_SERVER_RX < rx_cycle) )
printk("\n[%s] thread[%x,%x] completes RX for client thread[%x,%x] / cycle %d\n",
__FUNCTION__, server->process->pid, server->trdid, client_pid, client_trdid, rx_cycle );
#endif

#if DEBUG_CHDEV_SERVER_TX
tx_cycle = (uint32_t)hal_get_cycles();
if( (chdev->is_rx == 0) && (DEBUG_CHDEV_SERVER_TX < tx_cycle) )
printk("\n[%s] thread[%x,%x] completes TX for client thread[%x,%x] / cycle %d\n",
__FUNCTION__, server->process->pid, server->trdid, client_pid, client_trdid, tX_cycle );
#endif

#if (DEBUG_SYS_READ & 1)
exit_chdev_server_read = (uint32_t)hal_get_cycles();
#endif

#if (DEBUG_SYS_WRITE & 1)
exit_chdev_server_write = (uint32_t)hal_get_cycles();
#endif

        }
    }  // end while
}  // end chdev_server_func()

////////////////////////////////////////
xptr_t chdev_from_file( xptr_t file_xp )
{
    cxy_t         file_cxy;
    vfs_file_t  * file_ptr;
    uint32_t      inode_type;
    vfs_inode_t * inode_ptr;
    chdev_t     * chdev_ptr;

    assert( (file_xp != XPTR_NULL) ,
    "file_xp == XPTR_NULL\n" );

    // get cluster and local pointer on remote file descriptor
    // associated inode and chdev are stored in same cluster as the file desc.
    file_cxy  = GET_CXY( file_xp );
    file_ptr  = (vfs_file_t *)GET_PTR( file_xp );

    // get inode type from file descriptor
    inode_type = hal_remote_l32( XPTR( file_cxy , &file_ptr->type ) );
    inode_ptr  = (vfs_inode_t *)hal_remote_lpt( XPTR( file_cxy , &file_ptr->inode ) );

    assert( (inode_type == INODE_TYPE_DEV) ,
    "inode type %d is not INODE_TYPE_DEV\n", inode_type );

    // get chdev local pointer from inode extension
    chdev_ptr = (chdev_t *)hal_remote_lpt( XPTR( file_cxy , &inode_ptr->extend ) );

    return XPTR( file_cxy , chdev_ptr );

}  // end chdev_from_file()

//////////////////////////////
void chdev_dir_display( void )
{
    uint32_t  i;
    cxy_t     cxy;
    chdev_t * ptr;
    uint32_t  base;

    // get pointers on TXT0 chdev
    xptr_t    txt0_xp  = chdev_dir.txt_tx[0];
    cxy_t     txt0_cxy = GET_CXY( txt0_xp );
    chdev_t * txt0_ptr = GET_PTR( txt0_xp );

    // get extended pointer on TXT0 lock
    xptr_t  lock_xp = XPTR( txt0_cxy , &txt0_ptr->wait_lock );

    // get TXT0 lock 
    remote_busylock_acquire( lock_xp );

    // header
    nolock_printk("\n***** external chdevs directory *****\n");

    // IOB
    if (chdev_dir.iob != XPTR_NULL )
    {
        cxy  = GET_CXY( chdev_dir.iob );
        ptr  = GET_PTR( chdev_dir.iob );
        base = (uint32_t)hal_remote_l64( XPTR( cxy , &ptr->base ) );
        nolock_printk("  - iob       : cxy = %X / ptr = %X / base = %X\n", cxy, ptr, base);
    }

    // PIC
    cxy  = GET_CXY( chdev_dir.pic );
    ptr  = GET_PTR( chdev_dir.pic );
    base = (uint32_t)hal_remote_l64( XPTR( cxy , &ptr->base ) );
    nolock_printk("  - pic       : cxy = %X / ptr = %X / base = %X\n", cxy, ptr, base);

    // TXT
    for( i = 0 ; i < LOCAL_CLUSTER->nb_txt_channels ; i++ )
    {
        cxy = GET_CXY( chdev_dir.txt_rx[i] );
        ptr = GET_PTR( chdev_dir.txt_rx[i] );
        base = (uint32_t)hal_remote_l64( XPTR( cxy , &ptr->base ) );
        nolock_printk("  - txt_rx[%d] : cxy = %X / ptr = %X / base = %X\n", i, cxy, ptr, base);

        cxy = GET_CXY( chdev_dir.txt_tx[i] );
        ptr = GET_PTR( chdev_dir.txt_tx[i] );
        base = (uint32_t)hal_remote_l64( XPTR( cxy , &ptr->base ) );
        nolock_printk("  - txt_tx[%d] : cxy = %X / ptr = %X / base = %X\n", i, cxy, ptr, base);
    }

    // IOC
    for( i = 0 ; i < LOCAL_CLUSTER->nb_ioc_channels ; i++ )
    {
        cxy = GET_CXY( chdev_dir.ioc[i] );
        ptr = GET_PTR( chdev_dir.ioc[i] );
        base = (uint32_t)hal_remote_l64( XPTR( cxy , &ptr->base ) );
        nolock_printk("  - ioc[%d]    : cxy = %X / ptr = %X / base = %X\n", i, cxy, ptr, base);
    }

    // FBF
    for( i = 0 ; i < LOCAL_CLUSTER->nb_fbf_channels ; i++ )
    {
        cxy  = GET_CXY( chdev_dir.fbf[i] );
        ptr  = GET_PTR( chdev_dir.fbf[i] );
        base = (uint32_t)hal_remote_l64( XPTR( cxy , &ptr->base ) );
        nolock_printk("  - fbf[%d]    : cxy = %X / ptr = %X / base = %X\n", i, cxy, ptr, base);
    }

    // NIC
    for( i = 0 ; i < LOCAL_CLUSTER->nb_nic_channels ; i++ )
    {
        cxy = GET_CXY( chdev_dir.nic_rx[i] );
        ptr = GET_PTR( chdev_dir.nic_rx[i] );
        base = (uint32_t)hal_remote_l64( XPTR( cxy , &ptr->base ) );
        nolock_printk("  - nic_rx[%d] : cxy = %X / ptr = %X / base = %X\n", i, cxy, ptr, base);

        cxy = GET_CXY( chdev_dir.nic_tx[i] );
        ptr = GET_PTR( chdev_dir.nic_tx[i] );
        base = (uint32_t)hal_remote_l64( XPTR( cxy , &ptr->base ) );
        nolock_printk("  - nic_tx[%d] : cxy = %X / ptr = %X / base = %X\n", i, cxy, ptr, base);
    }

    // release lock
    remote_busylock_release( lock_xp );

}  // end chdev_dir_display()

///////////////////////////////////////////
void chdev_queue_display( xptr_t chdev_xp )
{
    cxy_t       chdev_cxy;          // chdev cluster
    chdev_t   * chdev_ptr;          // chdev local pointer
    xptr_t      root_xp;            // extended pointer on waiting queuue root
    char        name[16];           // local copie of chdev name
    xptr_t      iter_xp;            // extended pointer on xlist_t field in waiting thread
    xptr_t      thread_xp;          // extended pointer on thread registered in queue
    cxy_t       thread_cxy;         // cluster identifier for waiting thread
    thread_t  * thread_ptr;         // local pointer on waiting thread
    trdid_t     trdid;              // waiting thread identifier
    process_t * process;            // waiting thread process descriptor
    pid_t       pid;                // waiting thread process identifier

    // get cluster and local pointer on chdev
    chdev_cxy = GET_CXY( chdev_xp );
    chdev_ptr = GET_PTR( chdev_xp );

    // get extended pointer on root of requests queue
    root_xp = XPTR( chdev_cxy , &chdev_ptr->wait_root );

    // get chdev name
    hal_remote_strcpy( XPTR( local_cxy , name ), XPTR( chdev_cxy , chdev_ptr->name ) );

    // get pointers on TXT0 chdev
    xptr_t    txt0_xp  = chdev_dir.txt_tx[0];
    cxy_t     txt0_cxy = GET_CXY( txt0_xp );
    chdev_t * txt0_ptr = GET_PTR( txt0_xp );

    // get extended pointer on TXT0 lock
    xptr_t  lock_xp = XPTR( txt0_cxy , &txt0_ptr->wait_lock );

    // get TXT0 lock 
    remote_busylock_acquire( lock_xp );

    // check queue empty
    if( xlist_is_empty( root_xp ) )
    {
        nolock_printk("\n***** Waiting queue empty for chdev %s\n", name ); 
    }
    else
    {
        nolock_printk("\n***** Waiting queue for chdev %s\n", name ); 

        // scan the waiting queue
        XLIST_FOREACH( root_xp , iter_xp )
        {
            thread_xp  = XLIST_ELEMENT( iter_xp , thread_t , wait_list );
            thread_cxy = GET_CXY( thread_xp );
            thread_ptr = GET_PTR( thread_xp );
            trdid      = hal_remote_l32 ( XPTR( thread_cxy , &thread_ptr->trdid   ) );
            process    = hal_remote_lpt( XPTR( thread_cxy , &thread_ptr->process ) );
			pid        = hal_remote_l32 ( XPTR( thread_cxy , &process->pid        ) );

            nolock_printk("- thread[%x,%x]\n", pid, trdid );
        }
    }

    // release TXT0 lock 
    remote_busylock_release( lock_xp );

}  // end chdev_queue_display()

