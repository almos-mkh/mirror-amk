/*
 * rpc.h - RPC (Remote Procedure Call) operations definition.
 *
 * Author  Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 * 
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _RPC_H_
#define _RPC_H_

#include <kernel_config.h>
#include <hal_kernel_types.h>
#include <hal_atomic.h>
#include <bits.h>
#include <vseg.h>
#include <remote_fifo.h>

/**** Forward declarations ****/

struct process_s;
struct page_s;
struct vseg_s;
struct exec_info_s;
struct pthread_attr_s;
struct remote_sem_s;
struct user_dir_s;
struct fragment_s;
struct vfs_inode_s;
struct vfs_dentry_s;
struct vfs_file_s;
struct thread_s;
struct mapper_s;


/**********************************************************************************/
/**************  structures for Remote Procedure Calls ****************************/
/**********************************************************************************/

/***********************************************************************************
 * This enum defines all RPC indexes.
 * It must be consistent with the rpc_server[] arrays defined in in the rpc.c file.
 **********************************************************************************/

typedef enum
{
    RPC_PMEM_GET_PAGES            = 0,
    RPC_PMEM_RELEASE_PAGES        = 1,
    RPC_UNDEFINED_2               = 2,      
    RPC_PROCESS_MAKE_FORK         = 3,
    RPC_USER_DIR_CREATE           = 4,
    RPC_USER_DIR_DESTROY          = 5,
    RPC_THREAD_USER_CREATE        = 6,
    RPC_THREAD_KERNEL_CREATE      = 7,
    RPC_VFS_FS_UPDATE_DENTRY      = 8,
    RPC_PROCESS_SIGACTION         = 9,

    RPC_VFS_INODE_CREATE          = 10,
    RPC_VFS_INODE_DESTROY         = 11,
    RPC_VFS_DENTRY_CREATE         = 12,
    RPC_VFS_DENTRY_DESTROY        = 13,
    RPC_VFS_FILE_CREATE           = 14,
    RPC_VFS_FILE_DESTROY          = 15,
    RPC_VFS_FS_GET_DENTRY         = 16,
    RPC_VFS_FS_ADD_DENTRY         = 17,
    RPC_VFS_FS_REMOVE_DENTRY      = 18,
    RPC_VFS_INODE_LOAD_ALL_PAGES  = 19,

    RPC_VMM_GET_VSEG              = 20,
    RPC_VMM_GLOBAL_UPDATE_PTE     = 21,
    RPC_KCM_ALLOC                 = 22,
    RPC_KCM_FREE                  = 23,
    RPC_MAPPER_SYNC               = 24,
    RPC_MAPPER_HANDLE_MISS        = 25,
    RPC_VMM_DELETE_VSEG           = 26,
    RPC_VMM_CREATE_VSEG           = 27,
    RPC_VMM_SET_COW               = 28,
    RPC_VMM_DISPLAY               = 29,

    RPC_MAX_INDEX                 = 30,
}
rpc_index_t;

/***********************************************************************************
 * This defines the prototype of the rpc_server functions,
 * defined by the rpc_server[] array in the rpc.c file. 
 **********************************************************************************/

typedef  void (rpc_server_t) ( xptr_t xp );

/***********************************************************************************
 *  This structure defines the RPC descriptor (100 bytes on a 32bits core)
 **********************************************************************************/

typedef struct rpc_desc_s
{
	rpc_index_t         index;       /*! index of requested RPC service      ( 4) */
	uint32_t          * rsp;         /*! local pointer ond responses counter ( 4) */
    struct thread_s   * thread;      /*! local pointer on client thread      ( 4) */
    uint32_t            lid;         /*! index of core running client thread ( 4) */ 
    bool_t              blocking;    /*! simple RPC mode when true           ( 4) */
    uint64_t            args[10];    /*! input/output arguments buffer       (80) */
} 
rpc_desc_t;

/**********************************************************************************/
/******* Generic functions supporting RPCs : client side **************************/
/**********************************************************************************/

/***********************************************************************************
 * This function is executed by the client thread in the client cluster. 
 * It puts one RPC descriptor defined by the <desc> argument in the remote fifo
 * defined by the <cxy> argument.  It sends an IPI to the server if fifo is empty.
 * The RPC descriptor must be allocated in the caller's stack, and initialised by
 * the caller. It exit with a Panic message if remote fifo is still full after 
 * (CONFIG_RPC_PUT_MAX_ITERATIONS) retries. 
 * - When the RPC <blocking> field is true, this function blocks and deschedule. 
 *   It returns only when the server acknowledges the RPC by writing in the RPC
 *   "response" field, and unblocks the client.
 * - When the <blocking> field is false, this function returns as soon as the RPC
 *   has been registered in the FIFO, and the server thread must directly signal
 *   completion to the client thread.
 ***********************************************************************************
 * @ cxy   : server cluster identifier
 * @ desc  : local pointer on RPC descriptor in client cluster
 **********************************************************************************/
void rpc_send( cxy_t        cxy,    
               rpc_desc_t * desc );



/**********************************************************************************/
/******* Generic functions supporting RPCs : server side **************************/
/**********************************************************************************/

/***********************************************************************************
 * This function contains the infinite loop executed by a RPC server thread,
 * to handle pending RPCs registered in the RPC fifo attached to a given core.
 * In each iteration in this loop, it try to handle one RPC request:
 * - it tries to take the RPC FIFO ownership,
 * - it consumes one request when the FIFO is not empty,
 * - it releases the FIFO ownership,
 * - it execute the requested service,
 * - it unblock and send an IPI to the client thread,
 * - it suicides if the number of RPC threads for this core is to large,
 * - it block on IDLE and deschedule otherwise.  
 **********************************************************************************/
void rpc_server_func( void );

/***********************************************************************************
 * This function is executed in case of illegal RPC index.
 **********************************************************************************/
void __attribute__((noinline)) rpc_undefined( xptr_t xp __attribute__ ((unused)) );



/**********************************************************************************/
/******* Marshalling functions attached to the various RPCs ***********************/
/**********************************************************************************/

/***********************************************************************************
 * [0] The RPC_PMEM_GET_PAGES allocates one or several pages in a remote cluster,
 * and returns the local pointer on the page descriptor.
 ***********************************************************************************
 * @ cxy     : server cluster identifier
 * @ order   : [in]  ln2( number of requested pages )
 * @ page    : [out] local pointer on page descriptor / NULL if failure
 **********************************************************************************/
void rpc_pmem_get_pages_client( cxy_t             cxy,
                                uint32_t          order,
                                struct page_s  ** page );

void rpc_pmem_get_pages_server( xptr_t xp );

/***********************************************************************************
 * [1] The RPC_PMEM_RELEASE_PAGES release one or several pages to a remote cluster.
 ***********************************************************************************
 * @ cxy     : server cluster identifier
 * @ page    : [in] local pointer on page descriptor to release.
 **********************************************************************************/
void rpc_pmem_release_pages_client( cxy_t            cxy,
                                    struct page_s  * page );

void rpc_pmem_release_pages_server( xptr_t xp );

/***********************************************************************************
 * [2] undefined slot
 **********************************************************************************/

/***********************************************************************************
 * [3] The RPC_PROCESS_MAKE_FORK creates a "child" process descriptor, and the
 * associated "child" thread descriptor in a target remote cluster that can be 
 * any cluster.  The child process is initialized from informations found in the 
 * "parent" process descriptor (that must be the parent reference cluster),
 * and from the "parent" thread descriptor that can be in any cluster.
 ***********************************************************************************
 * @ cxy              : server cluster identifier.
 * @ ref_process_xp   : [in]  extended pointer on reference parent process.
 * @ parent_thread_xp : [in]  extended pointer on parent thread.
 * @ child_pid        : [out] child process identifier.
 * @ child_thread_ptr : [out] local pointer on child thread.
 * @ error            : [out]  error status (0 if success).
 **********************************************************************************/
void rpc_process_make_fork_client( cxy_t              cxy,
                                   xptr_t             ref_process_xp,
                                   xptr_t             parent_thread_xp,
                                   pid_t            * child_pid,
                                   struct thread_s ** child_thread_ptr,
                                   error_t          * error );

void rpc_process_make_fork_server( xptr_t xp );

/***********************************************************************************
 * [4] The RPC_USER_DIR_CREATE allows a client thread to create an user_dir_t
 * structure and the associated array of dirents in a remote cluster containing
 * the target directory <inode>. It creates an ANON vseg in the user reference 
 * process VMM identified by the <ref_xp>. This reference cluster cluster can be
 * different from both the client and server clusters.
 * It is called by the sys_opendir() function.
 ***********************************************************************************
 * @ cxy        : server cluster identifier.
 * @ inode      : [in]   local pointer on inode in server cluster.
 * @ ref_xp     : [in]   extended pointer on user reference process descriptor.
 * @ dir        : [out]  local pointer on created user_dir structure.
 **********************************************************************************/
void rpc_user_dir_create_client( cxy_t                 cxy,
                                 struct vfs_inode_s  * inode,
                                 xptr_t                ref_xp,
                                 struct user_dir_s  ** dir );

void rpc_user_dir_create_server( xptr_t xp );

/***********************************************************************************
 * [5] The RPC_USER_DIR_DESTROY allows a client thread to delete an user_dir_t
 * structure and the associated array of dirents in a remote cluster containing
 * the target directory inode. It is called by the sys_closedir() function.
 ***********************************************************************************
 * @ cxy        : server cluster identifier.
 * @ dir        : [in]  local pointer on created user_dir structure.
 * @ ref_xp     : [in]   extended pointer on user reference process descriptor.
 **********************************************************************************/
void rpc_user_dir_destroy_client( cxy_t               cxy,
                                  struct user_dir_s * dir,
                                  xptr_t              ref_xp );

void rpc_user_dir_destroy_server( xptr_t xp );

/***********************************************************************************
 * [6] The RPC_THREAD_USER_CREATE creates an user thread in the server cluster,
 * as specified by the arguments. It returns an extended pointer on the new
 * thread descriptor in server cluster, and an error code.
 * It is called by the sys_thread_create() system call.
 ***********************************************************************************
 * @ cxy       : server cluster identifier.
 * @ attr      : [in]  local pointer on pthread_attr_t in client cluster.
 * @ thread_xp : [out] buffer for thread extended pointer. 
 * @ error     : [out] error status (0 if success).
 **********************************************************************************/
void rpc_thread_user_create_client( cxy_t                   cxy,
                                    pid_t                   pid,
                                    void                  * start_func,
                                    void                  * start_arg,
                                    pthread_attr_t        * attr,
                                    xptr_t                * thread_xp,
                                    error_t               * error );

void rpc_thread_user_create_server( xptr_t xp );

/***********************************************************************************
 * [7] The RPC_THREAD_KERNEL_CREATE creates a kernel thread in the server cluster,
 * as specified by the type, func and args arguments. It returns the local pointer
 * on the thread descriptor in server cluster and an error code.
 * It is used by the dev_init() function to create the device server thread.
 ***********************************************************************************
 * @ cxy       : server cluster identifier.
 * @ type      : [in]  type of kernel thread.
 * @ func      : [in]  local pointer on thread function.
 * @ args      : [in]  local pointer on function arguments.
 * @ thread_xp : [out] pointer on buffer for thread extended pointer. 
 * @ error     : [out] error status (0 if success).
 **********************************************************************************/
void rpc_thread_kernel_create_client( cxy_t     cxy,
                                      uint32_t  type,
                                      void    * func,
                                      void    * args,
                                      xptr_t  * thread_xp,
                                      error_t * error );

void rpc_thread_kernel_create_server( xptr_t xp );

/***********************************************************************************
 * [8] The RPC_VFS_FS_UPDATE_DENTRY allows a client thread to request a remote
 * cluster to update the <size> field of a directory entry in the mapper of a
 * remote directory inode, identified by the <inode> local pointer.
 * The target entry name is identified by the <dentry> local pointer. 
 ***********************************************************************************
 * @ cxy     : server cluster identifier.
 * @ inode   : [in] local pointer on remote directory inode.
 * @ dentry  : [in] local pointer on remote dentry.
 * @ size    : [in] new size value.
 * @ error   : [out] error status (0 if success).
 **********************************************************************************/
void rpc_vfs_fs_update_dentry_client( cxy_t                 cxy,
                                      struct vfs_inode_s  * inode,
                                      struct vfs_dentry_s * dentry,
                                      uint32_t              size,
                                      error_t             * error );

void rpc_vfs_fs_update_dentry_server( xptr_t xp );

/***********************************************************************************
 * [9] The RPC_PROCESS_SIGACTION allows a client thread to request a remote cluster
 * to execute a given sigaction, defined by the <action_type> for a given process,
 * identified by the <pid> argument.
 ***********************************************************************************
 * @ cxy     : server cluster identifier.
 * @ pid     : [in] target process identifier.
 * @ action  : [in] sigaction index.
 **********************************************************************************/
void rpc_process_sigaction_client( cxy_t     cxy,
                                   pid_t     pid,
                                   uint32_t  action );
                             
void rpc_process_sigaction_server( xptr_t xp );

/*********************************************************************************** 
 * [10] The RPC_VFS_INODE_CREATE creates an inode and the associated mapper in a
 * remote cluster. The parent dentry must have been previously created.
 * It returns an extended pointer on the created inode.
 ***********************************************************************************
 * @ cxy        :  server cluster identifier.
 * @ fs_type    : [in]  file system type.
 * @ inode_type : [in]  file system type.
 * @ attr       : [in]  inode attributes.
 * @ rights     : [in]  access rights
 * @ uid        : [in]  user ID
 * @ gid        : [in]  group ID
 * @ inode_xp   : [out] buffer for extended pointer on created inode.
 * @ error      : [out] error status (0 if success).
 **********************************************************************************/
void rpc_vfs_inode_create_client( cxy_t      cxy,
                                  uint32_t   fs_type,
                                  uint32_t   attr,    
                                  uint32_t   rights,  
                                  uint32_t   uid,
                                  uint32_t   gid,
                                  xptr_t   * inode_xp,
                                  error_t  * error );

void rpc_vfs_inode_create_server( xptr_t xp );

/*********************************************************************************** 
 * [11] The RPC_VFS_INODE_DESTROY releases memory allocated for an inode descriptor
 * and for the associated mapper in a remote cluster. 
 ***********************************************************************************
 * @ cxy       :  server cluster identifier
 * @ inode     : [in]  local pointer on inode.
 **********************************************************************************/
void rpc_vfs_inode_destroy_client( cxy_t                cxy,
                                   struct vfs_inode_s * inode );

void rpc_vfs_inode_destroy_server( xptr_t xp );

/*********************************************************************************** 
 * [12] The RPC_VFS_DENTRY_CREATE creates a dentry in a remote cluster. 
 * It returns an extended pointer on the created dentry.
 ***********************************************************************************
 * @ cxy        :  server cluster identifier
 * @ type       : [in]  file system type.
 * @ name       : [in]  directory entry name.
 * @ dentry_xp  : [out] buffer for extended pointer on created dentry.
 * @ error      : [out] error status (0 if success).
 **********************************************************************************/
void rpc_vfs_dentry_create_client( cxy_t                  cxy,
                                   uint32_t               type,
                                   char                 * name,    
                                   xptr_t               * dentry_xp,
                                   error_t              * error );

void rpc_vfs_dentry_create_server( xptr_t xp );

/*********************************************************************************** 
 * [13] The RPC_VFS_DENTRY_DESTROY remove a denfry from the parent inode XHTAB,
 * and releases memory allocated for the dentry descriptor in a remote cluster. 
 ***********************************************************************************
 * @ cxy       :  server cluster identifier
 * @ dentry     : [in]  local pointer on dentry.
 **********************************************************************************/
void rpc_vfs_dentry_destroy_client( cxy_t                 cxy,
                                    struct vfs_dentry_s * dentry );

void rpc_vfs_dentry_destroy_server( xptr_t xp );

/*********************************************************************************** 
 * [14] The RPC_VFS_FILE_CREATE creates a file descriptor in a remote cluster. 
 * It returns an extended pointer on the created file structure.
 ***********************************************************************************
 * @ cxy        :  server cluster identifier
 * @ inode      : [in]  local pointer on parent inode.
 * @ file_attr  : [in]  new file attributes.
 * @ file_xp    : [out] buffer for extended pointer on created file.
 * @ error      : [out] error status (0 if success).
 **********************************************************************************/
void rpc_vfs_file_create_client( cxy_t                  cxy,
                                 struct vfs_inode_s   * inode,
                                 uint32_t               file_attr,
                                 xptr_t               * file_xp,
                                 error_t              * error );

void rpc_vfs_file_create_server( xptr_t xp );

/*********************************************************************************** 
 * [15] The RPC_VFS_FILE_DESTROY releases memory allocated for a file descriptor
 * in a remote cluster. 
 ***********************************************************************************
 * @ cxy       :  server cluster identifier
 * @ file       : [in]  local pointer on file.
 **********************************************************************************/
void rpc_vfs_file_destroy_client( cxy_t               cxy,
                                  struct vfs_file_s * file );

void rpc_vfs_file_destroy_server( xptr_t xp );

/*********************************************************************************** 
 * [16] The RPC_VFS_FS_GET_DENTRY calls the vfs_fs_new_dentry()
 * function in a remote cluster containing a parent inode directory to scan the 
 * associated mapper, find a directory entry identified by its name, and update
 * both the - existing - child inode and dentry.
 ***********************************************************************************
 * @ cxy            : server cluster identifier
 * @ parent_inode   : [in]  local pointer on parent inode.
 * @ name           : [in]  local pointer on child name (in client cluster).
 * @ child_inode_xp : [in]  extended pointer on child inode (in another cluster).
 * @ error          : [out] error status (0 if success).
 **********************************************************************************/
void rpc_vfs_fs_new_dentry_client( cxy_t                cxy,
                                   struct vfs_inode_s * parent_inode,
                                   char               * name,
                                   xptr_t               child_inode_xp,
                                   error_t            * error );

void rpc_vfs_fs_new_dentry_server( xptr_t xp );

/*********************************************************************************** 
 * [17] The RPC_VFS_FS_ADD_DENTRY calls the vfs_fs_add_dentry() function in a 
 * remote cluster containing a directory inode and mapper, to add a new dentry
 * in the mapper of this directory.
 ***********************************************************************************
 * @ cxy            : server cluster identifier
 * @ parent         : [in]  local pointer on directory inode.
 * @ dentry         : [in]  local pointer on dentry.
 * @ error          : [out] error status (0 if success).
 **********************************************************************************/
void rpc_vfs_fs_add_dentry_client( cxy_t,
                                   struct vfs_inode_s  * parent,
                                   struct vfs_dentry_s * dentry,
                                   error_t             * error );

void rpc_vfs_fs_add_dentry_server( xptr_t xp );

/*********************************************************************************** 
 * [18] The RPC_VFS_FS_REMOVE_DENTRY calls the vfs_fs_remove_dentry() function in a
 * remote cluster containing a directory inode and mapper, to remove a dentry from
 * the mapper of this directory.
 ***********************************************************************************
 * @ cxy            : server cluster identifier
 * @ parent         : [in]  local pointer on directory inode.
 * @ dentry         : [in]  local pointer on dentry.
 * @ error          : [out] error status (0 if success).
 **********************************************************************************/
void rpc_vfs_fs_remove_dentry_client( cxy_t,
                                      struct vfs_inode_s  * parent,
                                      struct vfs_dentry_s * dentry,
                                      error_t             * error );

void rpc_vfs_fs_remove_dentry_server( xptr_t xp );

/***********************************************************************************
 * [19] The RPC_VFS_INODE_LOAD_ALL_PAGES calls the vfs_inode_load_all_pages()
 * function a remote cluster containing an inode to load all pages in the
 * associated mapper.  
 ***********************************************************************************
 * @ cxy     : server cluster identifier
 * @ inode   : [in]  local pointer on inode in server cluster.
 * @ error   : [out] error status (0 if success).
 **********************************************************************************/
void rpc_vfs_inode_load_all_pages_client( cxy_t                cxy,
                                          struct vfs_inode_s * inode,
                                          error_t            * error );

void rpc_vfs_inode_load_all_pages_server( xptr_t xp );

/***********************************************************************************
 * [20] The RPC_VMM_GET_VSEG returns an extended pointer 
 * on the vseg containing a given virtual address in a given process.
 * The server cluster is supposed to be the reference cluster.
 * It returns a non zero error value if no vseg has been founded.
 ***********************************************************************************
 * @ cxy     : server cluster identifier.
 * @ process : [in]   pointer on process descriptor in server cluster. 
 * @ vaddr   : [in]   virtual address to be searched.
 * @ vseg_xp : [out]  buffer for extended pointer on vseg in client cluster.
 * @ error   : [out] local pointer on buffer for error code (in client cluster). 
 **********************************************************************************/
void rpc_vmm_get_vseg_client( cxy_t              cxy,
                              struct process_s * process,
                              intptr_t           vaddr,
                              xptr_t           * vseg_xp,
                              error_t          * error );

void rpc_vmm_get_vseg_server( xptr_t xp );

/***********************************************************************************
 * [21] The RPC_VMM_GLOBAL_UPDATE_PTE can be used by a thread that is not running
 * in reference cluster, to ask the reference cluster to update a specific entry,
 * identified by the <vpn> argument in all GPT copies of a process identified by
 * the <process> argument, using the values defined by <attr> and <ppn> arguments.
 * The server cluster is supposed to be the reference cluster.
 * It does not return any error code as the called function vmm_global_update_pte()
 * cannot fail.
 ***********************************************************************************
 * @ cxy     : server cluster identifier.
 * @ process : [in]  pointer on process descriptor in server cluster. 
 * @ vpn     : [in]  virtual address to be searched.
 * @ attr    : [in]  PTE attributes.
 * @ ppn     : [it]  PTE PPN.
 **********************************************************************************/
void rpc_vmm_global_update_pte_client( cxy_t              cxy,
                                       struct process_s * process,
                                       vpn_t              vpn,
                                       uint32_t           attr,
                                       ppn_t              ppn );

void rpc_vmm_global_update_pte_server( xptr_t xp );

/***********************************************************************************
 * [22] The RPC_KCM_ALLOC allocates memory from a given KCM in a remote cluster,
 * and returns an extended pointer on the allocated object.
  It returns XPTR_NULL if physical memory cannot be allocated. 
 ***********************************************************************************
 * @ cxy       : server cluster identifier.
 * @ kmem_type : [in]  KCM object type (as defined in kmem.h).
 * @ buf_xp    : [out] buffer for extended pointer on allocated buffer.
 **********************************************************************************/
void rpc_kcm_alloc_client( cxy_t      cxy,
                           uint32_t   kmem_type,
                           xptr_t   * buf_xp );  

void rpc_kcm_alloc_server( xptr_t xp );

/***********************************************************************************
 * [23] The RPC_KCM_FREE releases memory allocated for a KCM object of a given type,
 * in a remote cluster.
 ***********************************************************************************
 * @ cxy       : server cluster identifier.
 * @ buf       : [in] local pointer on allocated buffer. 
 * @ kmem_type : [in]  KCM object type (as defined in kmem.h).
 **********************************************************************************/
void rpc_kcm_free_client( cxy_t     cxy,
                          void    * buf,
                          uint32_t  kmem_type );

void rpc_kcm_free_server( xptr_t xp );

/***********************************************************************************
 * [24] The RPC_MAPPER_SYNC allows a client thread to synchronize on disk
 * all dirty pages of a remote mapper.
 ***********************************************************************************
 * @ cxy       : server cluster identifier.
 * @ mapper    : [in] local pointer on mapper in server cluster.
 * @ error       : [out] error status (0 if success).
 **********************************************************************************/
void rpc_mapper_sync_client( cxy_t             cxy,
                             struct mapper_s * mapper,
                             error_t         * error );

void rpc_mapper_sync_server( xptr_t xp );

/***********************************************************************************
 * [25] The RPC__MAPPER_HANDLE_MISS allows a client thread to request a remote
 * mapper to load a missing page from the IOC device.
 * On the server side, this RPC call the mapper_handle_miss() function and return
 * an extended pointer on the allocated page descriptor and an error status.
 ***********************************************************************************
 * @ cxy         : server cluster identifier.
 * @ mapper      : [in]  local pointer on mapper.
 * @ page_id     : [in]  missing page index in mapper 
 * @ buffer      : [in]  user space pointer / kernel extended pointer
 * @ page_xp     : [out] pointer on buffer for extended pointer on page descriptor.
 * @ error       : [out] error status (0 if success).
 **********************************************************************************/
void rpc_mapper_handle_miss_client( cxy_t             cxy,
                                    struct mapper_s * mapper,
                                    uint32_t          page_id,
                                    xptr_t          * page_xp,
                                    error_t         * error );
 
void rpc_mapper_handle_miss_server( xptr_t xp );

/***********************************************************************************
 * [26] The RPC_VMM_DELETE_VSEG allows any client thread  to request a remote
 * cluster to delete from a given VMM, identified by the <pid> argument
 * a given vseg, identified by the <vaddr> argument.
 ***********************************************************************************
 * @ cxy         : server cluster identifier.
 * @ pid         : [in] target process identifier.
 * @ vaddr       : [in] vseg base address.
 **********************************************************************************/
void rpc_vmm_delete_vseg_client( cxy_t       cxy,
                                 pid_t       pid,
                                 intptr_t    vaddr );
 
void rpc_vmm_delete_vseg_server( xptr_t xp );

/***********************************************************************************
 * [27] The RPC_VMM_CREATE_VSEG allows a client thread to request the remote 
 * reference cluster of a given process to allocate and register in the reference
 * process VMM a new vseg descriptor.
 * On the server side, this RPC uses the vmm_create_vseg() function, and returns
 * to the client the local pointer on the created vseg descriptor.
 ***********************************************************************************
 * @ cxy         : server cluster identifier.
 * @ process     : [in]  local pointer on process descriptor in server.
 * @ type        : [in]  vseg type.
 * @ base        : [in]  base address (unused for dynamically allocated vsegs).
 * @ size        : [in]  number of bytes.
 * @ file_offset : [in]  offset in file (for CODE, DATA, FILE types).
 * @ file_size   : [in]  can be smaller than size for DATA type.
 * @ mapper_xp   : [in]  extended pointer on mapper (for CODE, DATA, FILE types).
 * @ vseg_cxy    : [in]  target cluster for mapping (if not data type).
 * @ vseg        : [out] local pointer on vseg descriptor / NULL if failure.
 **********************************************************************************/
void rpc_vmm_create_vseg_client( cxy_t              cxy,
                                 struct process_s * process,
                                 vseg_type_t        type,
                                 intptr_t           base,
                                 uint32_t           size,
                                 uint32_t           file_offset,
                                 uint32_t           file_size,
                                 xptr_t             mapper_xp,
                                 cxy_t              vseg_cxy,
                                 struct vseg_s   ** vseg );

void rpc_vmm_create_vseg_server( xptr_t xp );

/***********************************************************************************
 * [28] The RPC_VMM_SET_COW allows a client thread to request the remote reference
 * cluster to set the COW flag and reset the WRITABLE flag of all GPT entries for
 * the DATA, MMAP and REMOTE vsegs of process identified by the <process> argument.

 * of a remote scheduler, identified by the <lid> argument.
 ***********************************************************************************
 * @ cxy         : server cluster identifier.
 * @ process     : [in]  local pointer on reference process descriptor.
 **********************************************************************************/
void rpc_vmm_set_cow_client( cxy_t              cxy,
                             struct process_s * process );

void rpc_vmm_set_cow_server( xptr_t xp );

/***********************************************************************************
 * [29] The RPC_VMM_DISPLAY allows any client thread to display the VMM state
 * of a remote reference process identified by the <cxy> and <process> arguments.
 * The type of display is defined by the <detailed> boolean argument.
 ***********************************************************************************
 * @ cxy         : server cluster identifier.
 * @ process     : [in]  local pointer on reference process descriptor.
 * @ detailed    : [in]  detailed display if true.
 **********************************************************************************/
void rpc_hal_vmm_display_client( cxy_t              cxy,
                             struct process_s * process,
                             bool_t             detailed );

void rpc_hal_vmm_display_server( xptr_t xp );


#endif
