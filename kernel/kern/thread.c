/*
 * thread.c -   thread operations implementation (user & kernel)
 *
 * Author  Ghassan Almaless (2008,2009,2010,2011,2012)
 *         Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <kernel_config.h>
#include <hal_kernel_types.h>
#include <hal_context.h>
#include <hal_irqmask.h>
#include <hal_special.h>
#include <hal_remote.h>
#include <memcpy.h>
#include <printk.h>
#include <cluster.h>
#include <process.h>
#include <scheduler.h>
#include <dev_pic.h>
#include <core.h>
#include <list.h>
#include <xlist.h>
#include <page.h>
#include <kmem.h>
#include <ppm.h>
#include <thread.h>
#include <rpc.h>

//////////////////////////////////////////////////////////////////////////////////////
// Extern global variables
//////////////////////////////////////////////////////////////////////////////////////

extern process_t            process_zero;       // allocated in kernel_init.c
extern char               * lock_type_str[];    // allocated in kernel_init.c
extern chdev_directory_t    chdev_dir;          // allocated in kernel_init.c

//////////////////////////////////////////////////////////////////////////////////////
// This function returns a printable string for the thread type.
//////////////////////////////////////////////////////////////////////////////////////
const char * thread_type_str( thread_type_t type )
{
  switch ( type ) {
  case THREAD_USER:   return "USR";
  case THREAD_RPC:    return "RPC";
  case THREAD_DEV:    return "DEV";
  case THREAD_IDLE:   return "IDL";
  default:            return "undefined";
  }
}

/////////////////////////////////////////////////////////////////////////////////////
// This static function allocates physical memory for a thread descriptor.
// It can be called by the three functions:
// - thread_user_create()
// - thread_user_fork()
// - thread_kernel_create()
/////////////////////////////////////////////////////////////////////////////////////
// @ return pointer on thread descriptor if success / return NULL if failure.
/////////////////////////////////////////////////////////////////////////////////////
static thread_t * thread_alloc( void )
{
	page_t       * page;   // pointer on page descriptor containing thread descriptor
	kmem_req_t     req;    // kmem request

	// allocates memory for thread descriptor + kernel stack
	req.type  = KMEM_PAGE;
	req.size  = CONFIG_THREAD_DESC_ORDER;
	req.flags = AF_KERNEL | AF_ZERO;
	page      = kmem_alloc( &req );

	if( page == NULL ) return NULL;

    // return pointer on new thread descriptor
    xptr_t base_xp = ppm_page2base( XPTR(local_cxy , page ) );
    return GET_PTR( base_xp );

}  // end thread_alloc()
  

/////////////////////////////////////////////////////////////////////////////////////
// This static function releases the physical memory for a thread descriptor.
// It is called by the three functions:
// - thread_user_create()
// - thread_user_fork()
// - thread_kernel_create()
/////////////////////////////////////////////////////////////////////////////////////
// @ thread  : pointer on thread descriptor.
/////////////////////////////////////////////////////////////////////////////////////
static void thread_release( thread_t * thread )
{
    kmem_req_t   req;

    xptr_t base_xp = ppm_base2page( XPTR(local_cxy , thread ) );

    req.type  = KMEM_PAGE;
    req.ptr   = GET_PTR( base_xp );
    kmem_free( &req );
}

/////////////////////////////////////////////////////////////////////////////////////
// This static function initializes a thread descriptor (kernel or user).
// It can be called by the four functions:
// - thread_user_create()
// - thread_user_fork()
// - thread_kernel_create()
// - thread_idle_init()
// It updates the local DQDT.
/////////////////////////////////////////////////////////////////////////////////////
// @ thread       : pointer on local thread descriptor
// @ process      : pointer on local process descriptor.
// @ type         : thread type.
// @ func         : pointer on thread entry function.
// @ args         : pointer on thread entry function arguments.
// @ core_lid     : target core local index.
// @ u_stack_base : stack base (user thread only)
// @ u_stack_size : stack base (user thread only)
/////////////////////////////////////////////////////////////////////////////////////
static error_t thread_init( thread_t      * thread,
                            process_t     * process,
                            thread_type_t   type,
                            void          * func,
                            void          * args,
                            lid_t           core_lid,
                            intptr_t        u_stack_base,
                            uint32_t        u_stack_size )
{
    error_t        error;
    trdid_t        trdid;      // allocated thread identifier

	cluster_t    * local_cluster = LOCAL_CLUSTER;

#if DEBUG_THREAD_INIT
uint32_t   cycle = (uint32_t)hal_get_cycles();
thread_t * this  = CURRENT_THREAD;
if( DEBUG_THREAD_INIT < cycle )
printk("\n[%s] thread[%x,%x] enter for thread %x in process %x / cycle %d\n",
__FUNCTION__, this->process->pid, this->trdid, thread, process->pid , cycle );
#endif

    // compute thread descriptor size without kernel stack
    uint32_t desc_size = (intptr_t)(&thread->signature) - (intptr_t)thread + 4; 

	// Initialize new thread descriptor
	thread->type            = type;
    thread->quantum         = 0;            // TODO
    thread->ticks_nr        = 0;            // TODO
    thread->time_last_check = 0;            // TODO
	thread->core            = &local_cluster->core_tbl[core_lid];
	thread->process         = process;

    thread->busylocks       = 0;

#if DEBUG_BUSYLOCK
    xlist_root_init( XPTR( local_cxy , &thread->busylocks_root ) );
#endif

    thread->u_stack_base    = u_stack_base;
    thread->u_stack_size    = u_stack_size;
    thread->k_stack_base    = (intptr_t)thread + desc_size;
    thread->k_stack_size    = CONFIG_THREAD_DESC_SIZE - desc_size;

    thread->entry_func      = func;         // thread entry point
    thread->entry_args      = args;         // thread function arguments
    thread->flags           = 0;            // all flags reset
    thread->errno           = 0;            // no error detected
    thread->fork_user       = 0;            // no user defined placement for fork
    thread->fork_cxy        = 0;            // user defined target cluster for fork
    thread->blocked         = THREAD_BLOCKED_GLOBAL;

    // register new thread in process descriptor, and get a TRDID
    error = process_register_thread( process, thread , &trdid );

    if( error )
    {
        printk("\n[ERROR] in %s : thread %x in process %x cannot get TRDID in cluster %x\n"
        "    for thread %s in process %x / cycle %d\n",
        __FUNCTION__, CURRENT_THREAD->trdid, CURRENT_THREAD->process->pid,
        local_cxy, thread_type_str(type), process->pid, (uint32_t)hal_get_cycles() );
        return EINVAL;
    }

    // initialize trdid 
    thread->trdid           = trdid;

    // initialize sched list
    list_entry_init( &thread->sched_list );

    // initialize waiting queue entries
    list_entry_init( &thread->wait_list );
    xlist_entry_init( XPTR( local_cxy , &thread->wait_xlist ) );

    // initialize thread info
    memset( &thread->info , 0 , sizeof(thread_info_t) );

    // initialize join_lock
    remote_busylock_init( XPTR( local_cxy , &thread->join_lock ), LOCK_THREAD_JOIN );

    // initialise signature
	thread->signature = THREAD_SIGNATURE;

    // FIXME define and call an architecture specific hal_thread_init()
    // function to initialise the save_sr field
    thread->save_sr = 0xFF13;

    // register new thread in core scheduler
    sched_register_thread( thread->core , thread );

	// update DQDT 
    dqdt_increment_threads();

#if DEBUG_THREAD_INIT
cycle = (uint32_t)hal_get_cycles();
if( DEBUG_THREAD_INIT < cycle )
printk("\n[%s] thread[%x,%x] exit for thread %x in process %x / cycle %d\n",
__FUNCTION__, this->process->pid, this->trdid, thread, process->pid, cycle );
#endif

	return 0;

} // end thread_init()

/////////////////////////////////////////////////////////
error_t thread_user_create( pid_t             pid,
                            void            * start_func,
                            void            * start_arg,
                            pthread_attr_t  * attr,
                            thread_t       ** new_thread )
{
    error_t        error;
	thread_t     * thread;       // pointer on created thread descriptor
    process_t    * process;      // pointer to local process descriptor
    lid_t          core_lid;     // selected core local index
    vseg_t       * vseg;         // stack vseg

assert( (attr != NULL) , "pthread attributes must be defined" );

#if DEBUG_THREAD_USER_CREATE
thread_t * this  = CURRENT_THREAD;
uint32_t   cycle = (uint32_t)hal_get_cycles();
if( DEBUG_THREAD_USER_CREATE < cycle )
printk("\n[%s] thread[%x,%x] enter in cluster %x for process %x / cycle %d\n",
__FUNCTION__, this->process->pid , this->trdid , local_cxy , pid , cycle );
#endif

    // get process descriptor local copy
    process = process_get_local_copy( pid );

    if( process == NULL )
    {
		printk("\n[ERROR] in %s : cannot get process descriptor %x\n",
               __FUNCTION__ , pid );
        return ENOMEM;
    }

#if( DEBUG_THREAD_USER_CREATE & 1)
if( DEBUG_THREAD_USER_CREATE < cycle )
printk("\n[%s] process descriptor = %x for process %x in cluster %x\n",
__FUNCTION__, process , pid , local_cxy );
#endif

    // select a target core in local cluster
    if( attr->attributes & PT_ATTR_CORE_DEFINED )
    {
        core_lid = attr->lid;
        if( core_lid >= LOCAL_CLUSTER->cores_nr )
        {
	        printk("\n[ERROR] in %s : illegal core index attribute = %d\n",
            __FUNCTION__ , core_lid );
            return EINVAL;
        }
    }
    else
    {
        core_lid = cluster_select_local_core();
    }

#if( DEBUG_THREAD_USER_CREATE & 1)
if( DEBUG_THREAD_USER_CREATE < cycle )
printk("\n[%s] core[%x,%d] selected\n",
__FUNCTION__, local_cxy , core_lid );
#endif

    // allocate a stack from local VMM
    vseg = vmm_create_vseg( process,
                            VSEG_TYPE_STACK,
                            0,                 // size unused
                            0,                 // length unused
                            0,                 // file_offset unused
                            0,                 // file_size unused
                            XPTR_NULL,         // mapper_xp unused
                            local_cxy );

    if( vseg == NULL )
    {
	    printk("\n[ERROR] in %s : cannot create stack vseg\n", __FUNCTION__ );
		return ENOMEM;
    }

#if( DEBUG_THREAD_USER_CREATE & 1)
if( DEBUG_THREAD_USER_CREATE < cycle )
printk("\n[%s] stack vseg created / vpn_base %x / %d pages\n",
__FUNCTION__, vseg->vpn_base, vseg->vpn_size );
#endif

    // allocate memory for thread descriptor
    thread = thread_alloc();

    if( thread == NULL )
    {
	    printk("\n[ERROR] in %s : cannot create new thread\n", __FUNCTION__ );
        vmm_delete_vseg( process->pid , vseg->min );
        return ENOMEM;
    }

#if( DEBUG_THREAD_USER_CREATE & 1)
if( DEBUG_THREAD_USER_CREATE < cycle )
printk("\n[%s] new thread descriptor %x allocated\n",
__FUNCTION__, thread );
#endif

    // initialize thread descriptor
    error = thread_init( thread,
                         process,
                         THREAD_USER,
                         start_func,
                         start_arg,
                         core_lid,
                         vseg->min,
                         vseg->max - vseg->min );
    if( error )
    {
	    printk("\n[ERROR] in %s : cannot initialize new thread\n", __FUNCTION__ );
        vmm_delete_vseg( process->pid , vseg->min );
        thread_release( thread );
        return EINVAL;
    }

#if( DEBUG_THREAD_USER_CREATE & 1)
if( DEBUG_THREAD_USER_CREATE < cycle )
printk("\n[%s] new thread descriptor initialised / trdid %x\n",
__FUNCTION__, thread->trdid );
#endif

    // set DETACHED flag if required
    if( attr->attributes & PT_ATTR_DETACH ) 
    {
        thread->flags |= THREAD_FLAG_DETACHED;
    }

    // allocate & initialize CPU context
	if( hal_cpu_context_alloc( thread ) )
    {
	    printk("\n[ERROR] in %s : cannot create CPU context\n", __FUNCTION__ );
        vmm_delete_vseg( process->pid , vseg->min );
        thread_release( thread );
        return ENOMEM;
    }
    hal_cpu_context_init( thread );

    // allocate & initialize FPU context
    if( hal_fpu_context_alloc( thread ) )
    {
	    printk("\n[ERROR] in %s : cannot create FPU context\n", __FUNCTION__ );
        vmm_delete_vseg( process->pid , vseg->min );
        thread_release( thread );
        return ENOMEM;
    }
    hal_fpu_context_init( thread );

#if( DEBUG_THREAD_USER_CREATE & 1)
if( DEBUG_THREAD_USER_CREATE < cycle )
printk("\n[%s] CPU & FPU contexts created\n",
__FUNCTION__, thread->trdid );
hal_vmm_display( process , true );
#endif

#if DEBUG_THREAD_USER_CREATE
cycle = (uint32_t)hal_get_cycles();
if( DEBUG_THREAD_USER_CREATE < cycle )
printk("\n[%s] thread[%x,%x] exit / new_thread %x / core %d / cycle %d\n",
__FUNCTION__, this->process->pid , this->trdid , thread->trdid, core_lid, cycle );
#endif

    *new_thread = thread;
	return 0;

}  // end thread_user_create()

///////////////////////////////////////////////////////
error_t thread_user_fork( xptr_t      parent_thread_xp,
                          process_t * child_process,
                          thread_t ** child_thread )
{
    error_t        error;
	thread_t     * child_ptr;        // local pointer on local child thread
    lid_t          core_lid;         // selected core local index

    thread_t     * parent_ptr;       // local pointer on remote parent thread
    cxy_t          parent_cxy;       // parent thread cluster
    process_t    * parent_process;   // local pointer on parent process
    xptr_t         parent_gpt_xp;    // extended pointer on parent thread GPT

    void         * func;             // parent thread entry_func
    void         * args;             // parent thread entry_args
    intptr_t       base;             // parent thread u_stack_base
    uint32_t       size;             // parent thread u_stack_size
    uint32_t       flags;            // parent_thread flags
    vpn_t          vpn_base;         // parent thread stack vpn_base
    vpn_t          vpn_size;         // parent thread stack vpn_size
    reg_t        * uzone;            // parent thread pointer on uzone  

    vseg_t       * vseg;             // child thread STACK vseg

#if DEBUG_THREAD_USER_FORK
uint32_t   cycle = (uint32_t)hal_get_cycles();
thread_t * this  = CURRENT_THREAD;
if( DEBUG_THREAD_USER_FORK < cycle )
printk("\n[%s] thread[%x,%x] enter / child_process %x / cycle %d\n",
__FUNCTION__, this->process->pid, this->trdid, child_process->pid, cycle );
#endif

    // select a target core in local cluster
    core_lid = cluster_select_local_core();

    // get cluster and local pointer on parent thread descriptor
    parent_cxy = GET_CXY( parent_thread_xp );
    parent_ptr = GET_PTR( parent_thread_xp );

    // get relevant fields from parent thread 
    func  = (void *)  hal_remote_lpt( XPTR( parent_cxy , &parent_ptr->entry_func    ));
    args  = (void *)  hal_remote_lpt( XPTR( parent_cxy , &parent_ptr->entry_args    ));
    base  = (intptr_t)hal_remote_lpt( XPTR( parent_cxy , &parent_ptr->u_stack_base  ));
    size  = (uint32_t)hal_remote_l32 ( XPTR( parent_cxy , &parent_ptr->u_stack_size  ));
    flags =           hal_remote_l32 ( XPTR( parent_cxy , &parent_ptr->flags         ));
    uzone = (reg_t *) hal_remote_lpt( XPTR( parent_cxy , &parent_ptr->uzone_current ));

    vpn_base = base >> CONFIG_PPM_PAGE_SHIFT;
    vpn_size = size >> CONFIG_PPM_PAGE_SHIFT;

    // get pointer on parent process in parent thread cluster
    parent_process = (process_t *)hal_remote_lpt( XPTR( parent_cxy,
                                                        &parent_ptr->process ) );
 
    // get extended pointer on parent GPT in parent thread cluster
    parent_gpt_xp = XPTR( parent_cxy , &parent_process->vmm.gpt );

    // allocate memory for child thread descriptor
    child_ptr = thread_alloc();
    if( child_ptr == NULL )
    {
        printk("\n[ERROR] in %s : cannot allocate new thread\n", __FUNCTION__ );
        return -1;
    }

    // initialize thread descriptor
    error = thread_init( child_ptr,
                         child_process,
                         THREAD_USER,
                         func,
                         args,
                         core_lid,
                         base,
                         size );
    if( error )
    {
	    printk("\n[ERROR] in %s : cannot initialize child thread\n", __FUNCTION__ );
        thread_release( child_ptr );
        return EINVAL;
    }

#if (DEBUG_THREAD_USER_FORK & 1)
if( DEBUG_THREAD_USER_FORK < cycle )
printk("\n[%s] thread[%x,%x] initialised thread %x in process %x\n",
__FUNCTION__, this->process->pid, this->trdid, child_ptr->trdid, child_process->pid );
#endif

    // return child pointer
    *child_thread = child_ptr;

    // set detached flag if required
    if( flags & THREAD_FLAG_DETACHED ) child_ptr->flags = THREAD_FLAG_DETACHED;

    // update uzone pointer in child thread descriptor
    child_ptr->uzone_current = (char *)((intptr_t)uzone +
                                        (intptr_t)child_ptr - 
                                        (intptr_t)parent_ptr );
 

    // allocate CPU context for child thread
	if( hal_cpu_context_alloc( child_ptr ) )
    {
	    printk("\n[ERROR] in %s : cannot allocate CPU context\n", __FUNCTION__ );
        thread_release( child_ptr );
        return -1;
    }

    // allocate FPU context for child thread
	if( hal_fpu_context_alloc( child_ptr ) )
    {
	    printk("\n[ERROR] in %s : cannot allocate FPU context\n", __FUNCTION__ );
        thread_release( child_ptr );
        return -1;
    }

#if (DEBUG_THREAD_USER_FORK & 1)
if( DEBUG_THREAD_USER_FORK < cycle )
printk("\n[%s] thread[%x,%x] created CPU & FPU contexts for thread %x in process %x\n",
__FUNCTION__, this->process->pid, this->trdid, child_ptr->trdid, child_process->pid );
#endif

   // create and initialize STACK vseg 
    vseg = vseg_alloc();
    vseg_init( vseg,
               VSEG_TYPE_STACK,
               base,
               size,
               vpn_base,
               vpn_size,
               0, 0, XPTR_NULL,                         // not a file vseg
               local_cxy );

    // register STACK vseg in local child VSL
    vmm_attach_vseg_to_vsl( &child_process->vmm , vseg );

#if (DEBUG_THREAD_USER_FORK & 1)
if( DEBUG_THREAD_USER_FORK < cycle )
printk("\n[%s] thread[%x,%x] created stack vseg for thread %x in process %x\n",
__FUNCTION__, this->process->pid, this->trdid, child_ptr->trdid, child_process->pid );
#endif

    // copy all valid STACK GPT entries   
    vpn_t          vpn;
    bool_t         mapped;
    ppn_t          ppn;
    for( vpn = vpn_base ; vpn < (vpn_base + vpn_size) ; vpn++ )
    {
        error = hal_gpt_pte_copy( &child_process->vmm.gpt,
                                  parent_gpt_xp,
                                  vpn,
                                  true,                 // set cow
                                  &ppn,
                                  &mapped );
        if( error )
        {
            vmm_detach_vseg_from_vsl( &child_process->vmm , vseg );
            thread_release( child_ptr );
            printk("\n[ERROR] in %s : cannot update child GPT\n", __FUNCTION__ );
            return -1;
        }

        // increment pending forks counter for the page if mapped
        if( mapped )
        {
            // get pointers on the page descriptor
            xptr_t   page_xp  = ppm_ppn2page( ppn );
            cxy_t    page_cxy = GET_CXY( page_xp );
            page_t * page_ptr = GET_PTR( page_xp );

            // get extended pointers on forks and lock fields
            xptr_t forks_xp = XPTR( page_cxy , &page_ptr->forks );
            xptr_t lock_xp  = XPTR( page_cxy , &page_ptr->lock );

            // get lock protecting page
            remote_busylock_acquire( lock_xp );  

            // increment the forks counter in page descriptor
            hal_remote_atomic_add( forks_xp , 1 );

            // release lock protecting page
            remote_busylock_release( lock_xp );  

#if (DEBUG_THREAD_USER_FORK & 1)
cycle = (uint32_t)hal_get_cycles();
if( DEBUG_THREAD_USER_FORK < cycle )
printk("\n[%s] thread[%x,%x] copied one PTE to child GPT : vpn %x / forks %d\n",
__FUNCTION__, this->process->pid, this->trdid, 
vpn, hal_remote_l32( XPTR( page_cxy , &page_ptr->forks) ) );
#endif

        }
    }

    // set COW flag for all mapped entries of STAK vseg in parent thread GPT 
    hal_gpt_set_cow( parent_gpt_xp,
                     vpn_base,
                     vpn_size );
 
#if DEBUG_THREAD_USER_FORK
cycle = (uint32_t)hal_get_cycles();
if( DEBUG_THREAD_USER_FORK < cycle )
printk("\n[%s] thread[%x,%x] exit / child_thread %x / cycle %d\n",
__FUNCTION__, this->process->pid, this->trdid, child_ptr, cycle );
#endif

	return 0;

}  // end thread_user_fork()

////////////////////////////////////////////////
error_t thread_user_exec( void     * entry_func,
                          uint32_t   argc,
                          char    ** argv )
{
    thread_t  * thread  = CURRENT_THREAD;
    process_t * process = thread->process;

#if DEBUG_THREAD_USER_EXEC
uint32_t cycle = (uint32_t)hal_get_cycles();
if( DEBUG_THREAD_USER_EXEC < cycle )
printk("\n[%s] thread[%x,%x] enter / cycle %d\n",
__FUNCTION__, process->pid, thread->trdid, cycle );
#endif

// check parent thread attributes
assert( (thread->type == THREAD_USER )          , "bad type" );
assert( (thread->signature == THREAD_SIGNATURE) , "bad signature" );
assert( (thread->busylocks == 0)                , "bad busylocks" );

	// re-initialize various thread descriptor fields
    thread->quantum         = 0;            // TODO
    thread->ticks_nr        = 0;            // TODO
    thread->time_last_check = 0;            // TODO

    thread->entry_func      = entry_func;
    thread->main_argc       = argc; 
    thread->main_argv       = argv;

    // the main thread is always detached
    thread->flags           = THREAD_FLAG_DETACHED;
    thread->blocked         = 0;
    thread->errno           = 0;
    thread->fork_user       = 0;    // not inherited
    thread->fork_cxy        = 0;    // not inherited

    // re-initialize busylocks counters
    thread->busylocks       = 0;

    // reset thread info
    memset( &thread->info , 0 , sizeof(thread_info_t) );

    // re-initialize join_lock
    remote_busylock_init( XPTR( local_cxy , &thread->join_lock ), LOCK_THREAD_JOIN );

    // allocate an user stack vseg for main thread
    vseg_t * vseg = vmm_create_vseg( process,
                                     VSEG_TYPE_STACK,
                                     0,                 // size unused
                                     0,                 // length unused
                                     0,                 // file_offset unused
                                     0,                 // file_size unused
                                     XPTR_NULL,         // mapper_xp unused
                                     local_cxy );
    if( vseg == NULL )
    {
	    printk("\n[ERROR] in %s : cannot create stack vseg for main thread\n", __FUNCTION__ );
		return -1;
    }

    // update user stack in thread descriptor
    thread->u_stack_base = vseg->min;
    thread->u_stack_size = vseg->max - vseg->min;
    
    // release FPU ownership if required
    if( thread->core->fpu_owner == thread ) thread->core->fpu_owner = NULL;

    // re-initialize  FPU context
    hal_fpu_context_init( thread );

#if DEBUG_THREAD_USER_EXEC
cycle = (uint32_t)hal_get_cycles();
if( DEBUG_THREAD_USER_EXEC < cycle )
printk("\n[%s] thread[%x,%x] set CPU context & jump to user code / cycle %d\n",
__FUNCTION__, process->pid, thread->trdid, cycle );
hal_vmm_display( process , true );
#endif

    // re-initialize CPU context... and jump to user code
	hal_cpu_context_exec( thread );

    assert( false, "we should not execute this code");
 
    return 0;

}  // end thread_user_exec()

/////////////////////////////////////////////////////////
error_t thread_kernel_create( thread_t     ** new_thread,
                              thread_type_t   type,
                              void          * func,
                              void          * args,
				              lid_t           core_lid )
{
    error_t        error;
	thread_t     * thread;       // pointer on new thread descriptor

    thread_t * this = CURRENT_THREAD; 

assert( ( (type == THREAD_IDLE) || (type == THREAD_RPC) || (type == THREAD_DEV) ) ,
"illegal thread type" );

assert( (core_lid < LOCAL_CLUSTER->cores_nr) ,
"illegal core_lid" );

#if DEBUG_THREAD_KERNEL_CREATE
uint32_t   cycle = (uint32_t)hal_get_cycles();
if( DEBUG_THREAD_KERNEL_CREATE < cycle )
printk("\n[%s] thread[%x,%x] enter / requested_type %s / cycle %d\n",
__FUNCTION__, this->process->pid, this->trdid, thread_type_str(type), cycle );
#endif

    // allocate memory for new thread descriptor
    thread = thread_alloc();

    if( thread == NULL )
    {
        printk("\n[ERROR] in %s : thread %x in process %x\n"
        "   no memory for thread descriptor\n",
        __FUNCTION__, this->trdid, this->process->pid );
        return ENOMEM;
    }

    // initialize thread descriptor
    error = thread_init( thread,
                         &process_zero,
                         type,
                         func,
                         args,
                         core_lid,
                         0 , 0 );  // no user stack for a kernel thread

    if( error ) // release allocated memory for thread descriptor
    {
        printk("\n[ERROR] in %s : thread %x in process %x\n"
        "   cannot initialize thread descriptor\n",
        __FUNCTION__, this->trdid, this->process->pid );
        thread_release( thread );
        return ENOMEM;
    }

    // allocate & initialize CPU context
	error = hal_cpu_context_alloc( thread );

    if( error )
    {
        printk("\n[ERROR] in %s : thread %x in process %x\n"
        "    cannot create CPU context\n",
        __FUNCTION__, this->trdid, this->process->pid );
        thread_release( thread );
        return EINVAL;
    }

    hal_cpu_context_init( thread );

    // set THREAD_BLOCKED_IDLE for DEV threads
    if( type == THREAD_DEV ) thread->blocked |= THREAD_BLOCKED_IDLE;

#if DEBUG_THREAD_KERNEL_CREATE
cycle = (uint32_t)hal_get_cycles();
if( DEBUG_THREAD_KERNEL_CREATE < cycle )
printk("\n[%s] thread[%x,%x] exit / new_thread %x / type %s / cycle %d\n",
__FUNCTION__, this->process->pid, this->trdid, thread, thread_type_str(type), cycle );
#endif

    *new_thread = thread;
	return 0;

} // end thread_kernel_create()

//////////////////////////////////////////////
void thread_idle_init( thread_t      * thread,
                       thread_type_t   type,
                       void          * func,
                       void          * args,
	                   lid_t           core_lid )
{

// check arguments
assert( (type == THREAD_IDLE) , "illegal thread type" );
assert( (core_lid < LOCAL_CLUSTER->cores_nr) , "illegal core index" );

    // initialize thread descriptor
    error_t  error = thread_init( thread,
                                  &process_zero,
                                  type,
                                  func,
                                  args,
                                  core_lid,
                                  0 , 0 );   // no user stack for a kernel thread

    assert( (error == 0), "cannot create thread idle" );

    // allocate & initialize CPU context if success
    error = hal_cpu_context_alloc( thread );

    assert( (error == 0), "cannot allocate CPU context" );

    hal_cpu_context_init( thread );

}  // end thread_idle_init()

///////////////////////////////////////////////////////////////////////////////////////
// TODO: check that all memory dynamically allocated during thread execution
// has been released => check vmm destroy for MMAP vsegs  [AG]
///////////////////////////////////////////////////////////////////////////////////////
void thread_destroy( thread_t * thread )
{
    reg_t        save_sr;

    process_t  * process    = thread->process;
    core_t     * core       = thread->core;

#if DEBUG_THREAD_DESTROY
uint32_t   cycle = (uint32_t)hal_get_cycles();
thread_t * this  = CURRENT_THREAD;
if( DEBUG_THREAD_DESTROY < cycle )
printk("\n[%s] thread[%x,%x] enter to destroy thread[%x,%x] / cycle %d\n",
__FUNCTION__, this->process->pid, this->trdid, process->pid, thread->trdid, cycle );
#endif

    // check busylocks counter
    thread_assert_can_yield( thread , __FUNCTION__ );

    // update intrumentation values
	process->vmm.pgfault_nr += thread->info.pgfault_nr;

    // release memory allocated for CPU context and FPU context
	hal_cpu_context_destroy( thread );
	if ( thread->type == THREAD_USER ) hal_fpu_context_destroy( thread );
	
    // release FPU ownership if required
	hal_disable_irq( &save_sr );
	if( core->fpu_owner == thread )
	{
		core->fpu_owner = NULL;
		hal_fpu_disable();
	}
	hal_restore_irq( save_sr );

    // invalidate thread descriptor
	thread->signature = 0;

    // release memory for thread descriptor
    thread_release( thread );

#if DEBUG_THREAD_DESTROY
cycle = (uint32_t)hal_get_cycles();
if( DEBUG_THREAD_DESTROY < cycle )
printk("\n[%s] thread[%x,%x] exit / destroyed thread[%x,%x] / cycle %d\n",
__FUNCTION__, this->process->pid, this->trdid, process->pid, thread->trdid, cycle );
#endif

}   // end thread_destroy()

//////////////////////////////////////////////////
inline void thread_set_req_ack( thread_t * target,
                                uint32_t * rsp_count )
{
    reg_t    save_sr;   // for critical section

    // get pointer on target thread scheduler
    scheduler_t * sched = &target->core->scheduler;

    // wait scheduler ready to handle a new request
    while( sched->req_ack_pending ) asm volatile( "nop" );
    
    // enter critical section
    hal_disable_irq( &save_sr );
      
    // set request in target thread scheduler
    sched->req_ack_pending = true;

    // set ack request in target thread "flags"
    hal_atomic_or( &target->flags , THREAD_FLAG_REQ_ACK );

    // set pointer on responses counter in target thread
    target->ack_rsp_count = rsp_count;
    
    // exit critical section
    hal_restore_irq( save_sr );

    hal_fence();

}  // thread_set_req_ack()

/////////////////////////////////////////////////////
inline void thread_reset_req_ack( thread_t * target )
{
    reg_t    save_sr;   // for critical section

    // get pointer on target thread scheduler
    scheduler_t * sched = &target->core->scheduler;

    // check signal pending in scheduler
    assert( sched->req_ack_pending , "no pending signal" );
    
    // enter critical section
    hal_disable_irq( &save_sr );
      
    // reset signal in scheduler
    sched->req_ack_pending = false;

    // reset signal in thread "flags"
    hal_atomic_and( &target->flags , ~THREAD_FLAG_REQ_ACK );

    // reset pointer on responses counter 
    target->ack_rsp_count = NULL;
    
    // exit critical section
    hal_restore_irq( save_sr );

    hal_fence();

}  // thread_reset_req_ack()

//////////////////////////////////////
void thread_block( xptr_t   thread_xp,
                   uint32_t cause )
{
    // get thread cluster and local pointer
    cxy_t      cxy = GET_CXY( thread_xp );
    thread_t * ptr = GET_PTR( thread_xp );

    // set blocking cause
    hal_remote_atomic_or( XPTR( cxy , &ptr->blocked ) , cause );
    hal_fence();

#if DEBUG_THREAD_BLOCK
uint32_t    cycle   = (uint32_t)hal_get_cycles();
process_t * process = hal_remote_lpt( XPTR( cxy , &ptr->process ) );
thread_t  * this    = CURRENT_THREAD;
if( DEBUG_THREAD_BLOCK < cycle )
printk("\n[%s] thread[%x,%x] blocked thread %x in process %x / cause %x\n",
__FUNCTION__, this->process->pid, this->trdid,
ptr->trdid, hal_remote_l32(XPTR( cxy , &process->pid )), cause );
#endif

} // end thread_block()

////////////////////////////////////////////
uint32_t thread_unblock( xptr_t   thread_xp,
                         uint32_t cause )
{
    // get thread cluster and local pointer
    cxy_t      cxy = GET_CXY( thread_xp );
    thread_t * ptr = GET_PTR( thread_xp );

    // reset blocking cause
    uint32_t previous = hal_remote_atomic_and( XPTR( cxy , &ptr->blocked ) , ~cause );
    hal_fence();

#if DEBUG_THREAD_BLOCK
uint32_t    cycle   = (uint32_t)hal_get_cycles();
process_t * process = hal_remote_lpt( XPTR( cxy , &ptr->process ) );
thread_t  * this    = CURRENT_THREAD;
if( DEBUG_THREAD_BLOCK < cycle )
printk("\n[%s] thread[%x,%x] unblocked thread %x in process %x / cause %x\n",
__FUNCTION__, this->process->pid, this->trdid,
ptr->trdid, hal_remote_l32(XPTR( cxy , &process->pid )), cause );
#endif

    // return a non zero value if the cause bit is modified 
    return( previous & cause );

}  // end thread_unblock()

//////////////////////////////////////
void thread_delete( xptr_t  target_xp,
                    pid_t   pid,
                    bool_t  is_forced )
{
    reg_t       save_sr;                // for critical section
    bool_t      target_join_done;       // joining thread arrived first
    bool_t      target_attached;        // target thread attached
    xptr_t      killer_xp;              // extended pointer on killer thread (this)
    thread_t  * killer_ptr;             // pointer on killer thread (this)
    cxy_t       target_cxy;             // target thread cluster     
    thread_t  * target_ptr;             // pointer on target thread
    xptr_t      target_flags_xp;        // extended pointer on target thread <flags>
    xptr_t      target_join_lock_xp;    // extended pointer on target thread <join_lock>
    xptr_t      target_join_xp_xp;      // extended pointer on target thread <join_xp>
    trdid_t     target_trdid;           // target thread identifier
    ltid_t      target_ltid;            // target thread local index
    xptr_t      joining_xp;             // extended pointer on joining thread
    thread_t  * joining_ptr;            // pointer on joining thread
    cxy_t       joining_cxy;            // joining thread cluster

    // get target thread cluster and local pointer
    target_cxy      = GET_CXY( target_xp );
    target_ptr      = GET_PTR( target_xp );

    // get target thread identifiers, and attached flag
    target_trdid    = hal_remote_l32( XPTR( target_cxy , &target_ptr->trdid ) );
    target_ltid     = LTID_FROM_TRDID( target_trdid );
    target_flags_xp = XPTR( target_cxy , &target_ptr->flags ); 
    target_attached = ( (hal_remote_l32( target_flags_xp ) & THREAD_FLAG_DETACHED) == 0 );

    // get killer thread pointers
    killer_ptr = CURRENT_THREAD;
    killer_xp  = XPTR( local_cxy , killer_ptr );

#if DEBUG_THREAD_DELETE
uint32_t cycle  = (uint32_t)hal_get_cycles();
if( DEBUG_THREAD_DELETE < cycle )
printk("\n[%s] killer[%x,%x] enters / target[%x,%x] / cycle %d\n",
__FUNCTION__, killer_ptr->process->pid, killer_ptr->trdid,  
target_ptr->process->pid, target_ptr->trdid, cycle );
#endif

// check target thread is not the main thread, because the main thread
// must be deleted by the parent process sys_wait() function
assert( ((CXY_FROM_PID( pid ) != target_cxy) || (target_ltid != 0)),
"tharget thread cannot be the main thread\n" );

    // check killer thread can yield 
    thread_assert_can_yield( killer_ptr , __FUNCTION__ ); 

    // if the target thread is attached, we must synchonize with the joining thread
    // before blocking and marking the target thead for delete.

    if( target_attached && (is_forced == false) ) // synchronize with joining thread 
    {
        // build extended pointers on target thread join fields
        target_join_lock_xp  = XPTR( target_cxy , &target_ptr->join_lock );
        target_join_xp_xp    = XPTR( target_cxy , &target_ptr->join_xp );

        // enter critical section
        hal_disable_irq( &save_sr );

        // take the join_lock in target thread descriptor
        remote_busylock_acquire( target_join_lock_xp );

        // get join_done from target thread descriptor
        target_join_done = ((hal_remote_l32( target_flags_xp ) & THREAD_FLAG_JOIN_DONE) != 0);
    
        if( target_join_done )                     // joining thread arrived first
        {
            // get extended pointer on joining thread
            joining_xp  = (xptr_t)hal_remote_l64( target_join_xp_xp );
            joining_ptr = GET_PTR( joining_xp );
            joining_cxy = GET_CXY( joining_xp );
            
            // reset the join_done flag in target thread
            hal_remote_atomic_and( target_flags_xp , ~THREAD_FLAG_JOIN_DONE );

            // unblock the joining thread
            thread_unblock( joining_xp , THREAD_BLOCKED_JOIN );

            // release the join_lock in target thread descriptor
            remote_busylock_release( target_join_lock_xp );

            // block the target thread 
            thread_block( target_xp , THREAD_BLOCKED_GLOBAL );

            // set the REQ_DELETE flag in target thread descriptor
            hal_remote_atomic_or( target_flags_xp , THREAD_FLAG_REQ_DELETE );

            // exit critical section
            hal_restore_irq( save_sr );

#if DEBUG_THREAD_DELETE
cycle  = (uint32_t)hal_get_cycles;
if( DEBUG_THREAD_DELETE < cycle )
printk("\n[%s] killer[%x,%x] exit / target[%x,%x] marked after join / cycle %d\n",
__FUNCTION__, killer_ptr->process->pid, killer_ptr->trdid,
target_ptr->process->pid, target_ptr->trdid, cycle );
#endif

        }
        else                                      // killer thread arrived first
        {
            // set the kill_done flag in target thread
            hal_remote_atomic_or( target_flags_xp , THREAD_FLAG_KILL_DONE );

            // block this thread on BLOCKED_JOIN
            thread_block( killer_xp , THREAD_BLOCKED_JOIN );

            // set extended pointer on killer thread in target thread
            hal_remote_s64( target_join_xp_xp , killer_xp );

            // release the join_lock in target thread descriptor
            remote_busylock_release( target_join_lock_xp );

#if DEBUG_THREAD_DELETE
cycle  = (uint32_t)hal_get_cycles;
if( DEBUG_THREAD_DELETE < cycle )
printk("\n[%s] killer[%x,%x] deschedules / target[%x,%x] not completed / cycle %d\n",
__FUNCTION__, killer_ptr->process->pid, killer_ptr->trdid,
target_ptr->process->pid, target_ptr->trdid, cycle );
#endif
            // deschedule
            sched_yield( "killer thread wait joining thread" );

            // block the target thread 
            thread_block( target_xp , THREAD_BLOCKED_GLOBAL );

            // set the REQ_DELETE flag in target thread descriptor
            hal_remote_atomic_or( target_flags_xp , THREAD_FLAG_REQ_DELETE );

            // exit critical section
            hal_restore_irq( save_sr );

#if DEBUG_THREAD_DELETE
cycle  = (uint32_t)hal_get_cycles;
if( DEBUG_THREAD_DELETE < cycle )
printk("\n[%s] killer[%x,%x] exit / target[%x,%x] marked after join / cycle %d\n",
__FUNCTION__, killer_ptr->process->pid, killer_ptr->trdid,
target_ptr->process->pid, target_ptr->trdid, cycle );
#endif

        }
    }
    else                     // no synchronization with joining thread required
    {
        // block the target thread 
        thread_block( target_xp , THREAD_BLOCKED_GLOBAL );

        // set the REQ_DELETE flag in target thread descriptor
        hal_remote_atomic_or( target_flags_xp , THREAD_FLAG_REQ_DELETE );

#if DEBUG_THREAD_DELETE
cycle  = (uint32_t)hal_get_cycles;
if( DEBUG_THREAD_DELETE < cycle )
printk("\n[%s] killer[%x,%x] exit / target [%x,%x] marked / no join / cycle %d\n",
__FUNCTION__, killer_ptr->process->pid, killer_ptr->trdid,
target_ptr->process->pid, target_ptr->trdid, cycle );
#endif

    }
}  // end thread_delete()



/////////////////////////////
void thread_idle_func( void )
{
    while( 1 )
    {
        // unmask IRQs
        hal_enable_irq( NULL );

        // force core to low-power mode (optional)
        if( CONFIG_SCHED_IDLE_MODE_SLEEP ) 
        {

#if DEBUG_THREAD_IDLE
{ 
uint32_t cycle = (uint32_t)hal_get_cycles();
if( DEBUG_THREAD_IDLE < cycle )
printk("\n[%s] idle thread on core[%x,%d] goes to sleep / cycle %d\n",
__FUNCTION__, local_cxy, CURRENT_THREAD->core->lid, cycle );
}
#endif

            hal_core_sleep();

#if DEBUG_THREAD_IDLE
{
uint32_t cycle = (uint32_t)hal_get_cycles();
if( DEBUG_THREAD_IDLE < cycle )
printk("\n[%s] idle thread on core[%x,%d] wake up / cycle %d\n",
__FUNCTION__, local_cxy, CURRENT_THREAD->core->lid, cycle );
}
#endif

        }

#if DEBUG_THREAD_IDLE
{
uint32_t cycle = (uint32_t)hal_get_cycles();
if( DEBUG_THREAD_IDLE < cycle )
sched_display( CURRENT_THREAD->core->lid );
}
#endif     
        // search a runable thread
        sched_yield( "running idle thread" );

    } // end while

}  // end thread_idle()


///////////////////////////////////////////
void thread_time_update( thread_t * thread,
                         bool_t     is_user )
{
    cycle_t current_cycle;   // current cycle counter value
    cycle_t last_cycle;      // last cycle counter value

    // get pointer on thread_info structure
    thread_info_t * info = &thread->info;

    // get last cycle counter value
    last_cycle = info->last_cycle;

    // get current cycle counter value
    current_cycle = hal_get_cycles();

    // update thread_info structure
    info->last_cycle = current_cycle;

    // update time in thread_info
    if( is_user ) info->usr_cycles += (current_cycle - last_cycle);
    else          info->sys_cycles += (current_cycle - last_cycle);

}  // end thread_time_update()

/////////////////////////////////////
xptr_t thread_get_xptr( pid_t    pid,
                        trdid_t  trdid )
{
    cxy_t         target_cxy;          // target thread cluster identifier
    ltid_t        target_thread_ltid;  // target thread local index
    thread_t    * target_thread_ptr;   // target thread local pointer
    xptr_t        target_process_xp;   // extended pointer on target process descriptor
    process_t   * target_process_ptr;  // local pointer on target process descriptor
    pid_t         target_process_pid;  // target process identifier
    xlist_entry_t root;                // root of list of process in target cluster
    xptr_t        lock_xp;             // extended pointer on lock protecting  this list

#if DEBUG_THREAD_GET_XPTR
uint32_t cycle  = (uint32_t)hal_get_cycles();
thread_t * this = CURRENT_THREAD;
if( DEBUG_THREAD_GET_XPTR < cycle )
printk("\n[%s] thread %x in process %x enters / pid %x / trdid %x / cycle %d\n",
__FUNCTION__, this->trdid, this->process->pid, pid, trdid, cycle );
#endif

    // get target cluster identifier and local thread identifier
    target_cxy         = CXY_FROM_TRDID( trdid );
    target_thread_ltid = LTID_FROM_TRDID( trdid );

    // check trdid argument
	if( (target_thread_ltid >= CONFIG_THREADS_MAX_PER_CLUSTER) || 
        cluster_is_undefined( target_cxy ) )         return XPTR_NULL;

    // get root of list of process descriptors in target cluster
    hal_remote_memcpy( XPTR( local_cxy  , &root ),
                       XPTR( target_cxy , &LOCAL_CLUSTER->pmgr.local_root ),
                       sizeof(xlist_entry_t) );

    // get extended pointer on lock protecting the list of local processes
    lock_xp = XPTR( target_cxy , &LOCAL_CLUSTER->pmgr.local_lock );

    // take the lock protecting the list of processes in target cluster
    remote_queuelock_acquire( lock_xp );

#if( DEBUG_THREAD_GET_XPTR & 1 )
if( DEBUG_THREAD_GET_XPTR < cycle )
printk("\n[%s] scan processes in cluster %x :\n", __FUNCTION__, target_cxy );
#endif

    // scan the list of local processes in target cluster
    xptr_t  iter;
    bool_t  found = false;
    XLIST_FOREACH( XPTR( target_cxy , &LOCAL_CLUSTER->pmgr.local_root ) , iter )
    {
        target_process_xp  = XLIST_ELEMENT( iter , process_t , local_list );
        target_process_ptr = GET_PTR( target_process_xp );
        target_process_pid = hal_remote_l32( XPTR( target_cxy , &target_process_ptr->pid ) );

#if( DEBUG_THREAD_GET_XPTR & 1 )
if( DEBUG_THREAD_GET_XPTR < cycle )
printk(" - process %x\n", target_process_pid );
#endif

        if( target_process_pid == pid )
        {
            found = true;
            break;
        }
    }

    // release the lock protecting the list of processes in target cluster
    remote_queuelock_release( lock_xp );

    // check PID found
    if( found == false ) 
    {

#if( DEBUG_THREAD_GET_XPTR & 1 )
if( DEBUG_THREAD_GET_XPTR < cycle )
printk("\n[%s] pid %x not found in cluster %x\n",
__FUNCTION__, pid, target_cxy );
#endif
        return XPTR_NULL;
    }

    // get target thread local pointer
    xptr_t xp = XPTR( target_cxy , &target_process_ptr->th_tbl[target_thread_ltid] );
    target_thread_ptr = (thread_t *)hal_remote_lpt( xp );

    if( target_thread_ptr == NULL )
    {

#if( DEBUG_THREAD_GET_XPTR & 1 )
if( DEBUG_THREAD_GET_XPTR < cycle )
printk("\n[%s] thread %x not registered in process %x in cluster %x\n",
__FUNCTION__, trdid, pid, target_cxy );
#endif
        return XPTR_NULL;
    }

#if DEBUG_THREAD_GET_XPTR
cycle  = (uint32_t)hal_get_cycles();
if( DEBUG_THREAD_GET_XPTR < cycle )
printk("\n[%s] thread %x in process %x exit / pid %x / trdid %x / cycle %d\n",
__FUNCTION__, this->trdid, this->process->pid, pid, trdid, cycle );
#endif

    return XPTR( target_cxy , target_thread_ptr );

}  // end thread_get_xptr()

///////////////////////////////////////////////////
void thread_assert_can_yield( thread_t    * thread,
                              const char  * func_str )
{
    // does nothing if thread does not hold any busylock

    if( thread->busylocks )
    {
        // get pointers on TXT0 chdev
        xptr_t    txt0_xp  = chdev_dir.txt_tx[0];
        cxy_t     txt0_cxy = GET_CXY( txt0_xp );
        chdev_t * txt0_ptr = GET_PTR( txt0_xp );

        // get extended pointer on TXT0 lock
        xptr_t  txt0_lock_xp = XPTR( txt0_cxy , &txt0_ptr->wait_lock );

        // get TXT0 lock 
        remote_busylock_acquire( txt0_lock_xp );

        // display error message on TXT0
        nolock_printk("\n[PANIC] in %s / thread[%x,%x] cannot yield : "
        "hold %d busylock(s) / cycle %d\n",
        func_str, thread->process->pid, thread->trdid,
        thread->busylocks - 1, (uint32_t)hal_get_cycles() );

#if DEBUG_BUSYLOCK

// scan list of busylocks
xptr_t    iter_xp;
xptr_t    root_xp  = XPTR( local_cxy , &thread->busylocks_root );
XLIST_FOREACH( root_xp , iter_xp )
{
    xptr_t       lock_xp   = XLIST_ELEMENT( iter_xp , busylock_t , xlist );
    cxy_t        lock_cxy  = GET_CXY( lock_xp );
    busylock_t * lock_ptr  = GET_PTR( lock_xp );
    uint32_t     lock_type = hal_remote_l32( XPTR( lock_cxy , &lock_ptr->type ) );
    nolock_printk(" - %s in cluster %x\n", lock_type_str[lock_type] , lock_cxy );
}

#endif

        // release TXT0 lock 
        remote_busylock_release( txt0_lock_xp );

        // suicide
        hal_core_sleep();
    }
}  // end thread_assert_can yield()

//////////////////////////////////////////////////////
void thread_display_busylocks( xptr_t       thread_xp,
                               const char * string )
{

    cxy_t      thread_cxy = GET_CXY( thread_xp );
    thread_t * thread_ptr = GET_PTR( thread_xp );

#if DEBUG_BUSYLOCK

    xptr_t     iter_xp;

    // get relevant info from target thread descriptor
    uint32_t    locks   = hal_remote_l32( XPTR( thread_cxy , &thread_ptr->busylocks ) );
    trdid_t     trdid   = hal_remote_l32( XPTR( thread_cxy , &thread_ptr->trdid ) );
    process_t * process = hal_remote_lpt( XPTR( thread_cxy , &thread_ptr->process ) );
    pid_t       pid     = hal_remote_l32( XPTR( thread_cxy , &process->pid ) );

    // get extended pointer on root of busylocks
    xptr_t root_xp = XPTR( thread_cxy , &thread_ptr->busylocks_root );

    // get pointers on TXT0 chdev
    xptr_t    txt0_xp  = chdev_dir.txt_tx[0];
    cxy_t     txt0_cxy = GET_CXY( txt0_xp );
    chdev_t * txt0_ptr = GET_PTR( txt0_xp );

    // get extended pointer on remote TXT0 lock
    xptr_t  txt0_lock_xp = XPTR( txt0_cxy , &txt0_ptr->wait_lock );

    // get TXT0 lock 
    remote_busylock_acquire( txt0_lock_xp );

    // display header 
    nolock_printk("\n***** thread[%x,%x] in <%s> : %d busylocks *****\n",
    pid, trdid, string, locks );

    // scan the xlist of busylocks when required
    if( locks )
    {
        XLIST_FOREACH( root_xp , iter_xp )
        {
            xptr_t       lock_xp   = XLIST_ELEMENT( iter_xp , busylock_t , xlist );
            cxy_t        lock_cxy  = GET_CXY( lock_xp );
            busylock_t * lock_ptr  = GET_PTR( lock_xp );
            uint32_t     lock_type = hal_remote_l32(XPTR( lock_cxy , &lock_ptr->type ));
            nolock_printk(" - %s in cluster %x\n", lock_type_str[lock_type] , lock_cxy );
        }
    }

    // release TXT0 lock
    remote_busylock_release( txt0_lock_xp );

#else

printk("\n[ERROR] in %s : set DEBUG_BUSYLOCK in kernel_config.h for %s / thread(%x,%x)\n",
__FUNCTION__, string, thread_cxy, thread_ptr );

#endif

    return;

}  // end thread_display_busylock()

