/*
 * do_syscall.c - architecture independant entry-point for system calls.
 * 
 * Author    Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_irqmask.h>
#include <do_syscall.h>
#include <errno.h>
#include <thread.h>
#include <printk.h>
#include <syscalls.h>
#include <shared_syscalls.h>
#include <syscalls_numbers.h>

///////////////////////////////////////////////////////////////////////////////////////
// This ƒonction should never be called...
///////////////////////////////////////////////////////////////////////////////////////
int sys_undefined( void )
{
    assert( false , "undefined system call" );
    return 0;
}

///////////////////////////////////////////////////////////////////////////////////////
// This array of pointers define the kernel functions implementing the syscalls.
// It must be kept consistent with the enum in "syscalls_numbers.h" file.
///////////////////////////////////////////////////////////////////////////////////////

typedef int (*sys_func_t) ();

static const sys_func_t syscall_tbl[SYSCALLS_NR] = 
{
    sys_thread_exit,        // 0
    sys_thread_yield,       // 1 
    sys_thread_create,      // 2
    sys_thread_join,        // 3
    sys_thread_detach,      // 4
    sys_thread_cancel,      // 5
    sys_sem,                // 6
    sys_condvar,            // 7
    sys_barrier,            // 8
    sys_mutex,              // 9

    sys_rename,             // 10
    sys_munmap,             // 11
    sys_open,               // 12
    sys_mmap,               // 13
    sys_read,               // 14
    sys_write,              // 15
    sys_lseek,              // 16
    sys_close,              // 17
    sys_unlink,             // 18
    sys_pipe,               // 19

    sys_chdir,              // 20
    sys_mkdir,              // 21
    sys_mkfifo,             // 22
    sys_opendir,            // 23
    sys_readdir,            // 24
    sys_closedir,           // 25
    sys_getcwd,             // 26
    sys_isatty,             // 27  
    sys_alarm,              // 28
    sys_rmdir,              // 29

    sys_utls,               // 30
    sys_chmod,              // 31
    sys_signal,             // 32 
    sys_timeofday,          // 33
    sys_kill,               // 34
    sys_getpid,             // 35
    sys_fork,               // 36
    sys_exec,               // 37
    sys_stat,               // 38
    sys_wait,               // 39

    sys_get_config,         // 40
    sys_get_core,           // 41
    sys_get_cycle,          // 42
    sys_display,            // 43
    sys_place_fork,         // 44
    sys_thread_sleep,       // 45
    sys_thread_wakeup,      // 46
    sys_trace,              // 47
    sys_fg,                 // 48
    sys_is_fg,              // 49

    sys_exit,               // 50
};

////////////////////////////////////////////
const char * syscall_str( syscalls_t index )
{
    switch (index) 
    {
    case SYS_THREAD_EXIT:                  return "THREAD_EXIT";      // 0
    case SYS_THREAD_YIELD:                 return "THREAD_YIELD";     // 1
    case SYS_THREAD_CREATE:                return "THREAD_CREATE";    // 2
    case SYS_THREAD_JOIN:                  return "THREAD_JOIN";      // 3
    case SYS_THREAD_DETACH:                return "THREAD_DETACH";    // 4
    case SYS_THREAD_CANCEL:                return "THREAD_CANCEL";    // 5
    case SYS_SEM :                         return "SEM";              // 6
    case SYS_CONDVAR:                      return "CONDVAR";          // 7
    case SYS_BARRIER:                      return "BARRIER";          // 8
    case SYS_MUTEX :                       return "MUTEX";            // 9

    case SYS_RENAME:                       return "RENAME";           // 10
    case SYS_MUNMAP:                       return "MUNMAP";           // 11
    case SYS_OPEN:                         return "OPEN";             // 12
    case SYS_MMAP:                         return "MMAP";             // 13
    case SYS_READ:                         return "READ";             // 14
    case SYS_WRITE:                        return "WRITE";            // 15
    case SYS_LSEEK:                        return "LSEEK";            // 16
    case SYS_CLOSE:                        return "CLOSE";            // 17
    case SYS_UNLINK:                       return "UNLINK";           // 18
    case SYS_PIPE:                         return "PIPE";             // 19

    case SYS_CHDIR:                        return "CHDIR";            // 20
    case SYS_MKDIR:                        return "MKDIR";            // 21
    case SYS_MKFIFO:                       return "MKFIFO";           // 22
    case SYS_OPENDIR:                      return "OPENDIR";          // 23
    case SYS_READDIR:                      return "READDIR";          // 24
    case SYS_CLOSEDIR:                     return "CLOSEDIR";         // 25
    case SYS_GETCWD:                       return "GETCWD";           // 26
    case SYS_ISATTY:                       return "ISATTY";           // 27
    case SYS_ALARM :                       return "ALARM";            // 28
    case SYS_RMDIR :                       return "RMDIR";            // 29

    case SYS_UTLS:                         return "UTLS";             // 30
    case SYS_CHMOD :                       return "CHMOD";            // 31
    case SYS_SIGNAL:                       return "SIGNAL";           // 32
    case SYS_TIMEOFDAY:                    return "TIMEOFDAY";        // 33
    case SYS_KILL:                         return "KILL";             // 34
    case SYS_GETPID:                       return "GETPID";           // 35
    case SYS_FORK:                         return "FORK";             // 36
    case SYS_EXEC:                         return "EXEC";             // 37
    case SYS_STAT:                         return "STAT";             // 38
    case SYS_WAIT:                         return "WAIT";             // 39

    case SYS_GET_CONFIG:                   return "GET_CONFIG";       // 40
    case SYS_GET_CORE:                     return "GET_CORE";         // 41
    case SYS_GET_CYCLE:                    return "GET_CYCLE";        // 42
    case SYS_DISPLAY:                      return "DISPLAY";          // 43
    case SYS_PLACE_FORK:                   return "PLACE_FORK";       // 44
    case SYS_THREAD_SLEEP:                 return "THREAD_SLEEP";     // 45
    case SYS_THREAD_WAKEUP:                return "THREAD_WAKEUP";    // 46
    case SYS_TRACE:                        return "TRACE";            // 47
    case SYS_FG:                           return "FG";               // 48
    case SYS_IS_FG:                        return "IS_FG";            // 49

    case SYS_EXIT:                         return "EXIT";             // 50

    default:                               return "undefined";
    }
}


//////////////////////////////////
reg_t do_syscall( thread_t * this,
                  reg_t      arg0,
		          reg_t      arg1,
		          reg_t      arg2,
		          reg_t      arg3,
		          reg_t      service_num )
{
	int  error = 0;
	
    assert( (this == CURRENT_THREAD),
    "wrong <this> argument\n" );

    // update user time 
	thread_time_update( this , 1 );  

    // check syscall index
	if( service_num >= SYSCALLS_NR )
	{
		printk("\n[ERROR] in %s : Undefined syscall %d, for thread %x\n",
		       __FUNCTION__ , service_num , this );

		this->errno = ENOSYS;
	    hal_disable_irq(NULL);
		return ENOSYS;;
	}

    // reset errno
	this->errno = 0;

    // call relevant kernel function
	error = syscall_tbl[service_num] ( arg0 , arg1 , arg2 , arg3 );

    // check kernel stack overflow
    assert( (CURRENT_THREAD->signature == THREAD_SIGNATURE),
    "kernel stack overflow after for thread %x in cluster %x\n", CURRENT_THREAD, local_cxy );

    // update kernel time
	thread_time_update( this , 0 );

	return error;
}
