/*
 * core.c - core descriptor access function.
 *
 * Author  Ghassan Almaless (2008,2009,2010,2011,2012)
 *         Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH.is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <kernel_config.h>
#include <hal_kernel_types.h>
#include <hal_special.h>
#include <errno.h>
#include <printk.h>
#include <thread.h>
#include <chdev.h>
#include <dev_pic.h>
#include <rpc.h>
#include <cluster.h>
#include <kmem.h>
#include <dqdt.h>
#include <core.h>

/////////////////////////////////
void core_init( core_t    * core,
                lid_t       lid,
                gid_t       gid )
{
	core->lid               = lid;
	core->gid               = gid;
	core->cycles            = 0;
	core->time_stamp        = 0;
	core->ticks_nr          = 0;
	core->usage             = 0;
	core->spurious_irqs     = 0;
	core->fpu_owner         = NULL;
	core->rand_last         = hal_time_stamp() & 0xFFF;

    // initialize scheduler
	sched_init( core );
}

//////////////////////////////////////////////
inline uint32_t core_get_rand( core_t * core )
{
	uint32_t value  = ((core->rand_last * CONFIG_RDNG_PARAM_A) +
	                    CONFIG_RDNG_PARAM_C) ^ (hal_get_cycles() & 0xFFF);
	core->rand_last = value;
	return value;
}

////////////////////////////////////
void core_get_time( core_t   * core,
                    uint32_t * tm_s, 
                    uint32_t * tm_us )
{
	*tm_s  = (core->ticks_nr*CONFIG_SCHED_TICK_MS_PERIOD)/1000;
	*tm_us = (core->ticks_nr*CONFIG_SCHED_TICK_MS_PERIOD*1000)%1000000;
}

////////////////////////////////
void core_clock( core_t * core )
{
	uint32_t ticks;

	// update ticks counter
	ticks = core->ticks_nr++;

 	// handle scheduler
	if( (ticks % CONFIG_SCHED_TICKS_PER_QUANTUM) == 0 ) sched_yield( "TICK");
}

////////////////////////////////////////
void core_compute_stats( core_t * core )
{
	thread_t * idle  = core->scheduler.idle;
	uint32_t   ticks = core->ticks_nr;

	uint32_t   idle_percent;
	uint32_t   busy_percent;
	uint32_t   usage;

	// compute cumulated usage
	ticks         = (ticks) ? ticks : 1;
	idle_percent  = (idle->ticks_nr * 100) / ticks;
	idle_percent  = (idle_percent > 100) ? 100 : idle_percent;
	busy_percent  = 100 - idle_percent;
	usage         = (busy_percent + core->usage) / 2;

	// update core descriptor
	core->usage = usage;
	hal_fence();

	core->ticks_nr = 0;
	idle->ticks_nr = 0;
}

/////////////////////////////////////
void core_reset_stats( core_t * core )
{
	thread_t * idle  = core->scheduler.idle;

	core->ticks_nr = 0;
	core->usage    = 0;
	idle->ticks_nr = 0;

	hal_fence();
}

