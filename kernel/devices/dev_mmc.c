/*
 * dev_mmc.c - MMC (Memory Cache Controler) generic device API implementation.
 * 
 * Author  Alain Greiner    (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MK
 *
 * ALMOS-MKH.is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH.is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-kernel; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_special.h>
#include <hal_drivers.h>
#include <printk.h>
#include <chdev.h>
#include <thread.h>
#include <remote_busylock.h>
#include <dev_mmc.h>

/////////////////////////////////////////////////////////////////////////////////////////
// Extern global variables
/////////////////////////////////////////////////////////////////////////////////////////

extern chdev_directory_t  chdev_dir;         // allocated in kernel_init.c

//////////////////////////////////
void dev_mmc_init( chdev_t * mmc )
{
    // get implementation from device descriptor
    uint32_t  impl = mmc->impl;

    // set mmc name
    snprintf( mmc->name , 16 , "mmc_%x" , local_cxy );

    // call driver init function
    hal_drivers_mmc_init( mmc , impl );

    // bind IRQ to CP0
    dev_pic_bind_irq( 0 , mmc );
   
    // enable IRQ
    dev_pic_enable_irq( 0 , XPTR( local_cxy , mmc ) );

}  // end dev_mmc_init()

/////////////////////////////////////////////////////////////////////////////
// This static function is called by all MMC device access functions.
// It makes some checking, takes the lock granting exclusive
// access to MMC peripheral, call the driver to execute the command
// registered in the calling thread descriptor, and releases the lock.
/////////////////////////////////////////////////////////////////////////////
static error_t dev_mmc_access( thread_t * this )
{
    // get extended pointer on MMC device descriptor
    xptr_t  dev_xp = this->mmc_cmd.dev_xp;

    assert( (dev_xp != XPTR_NULL) , "target MMC device undefined" );

    // get MMC device cluster identifier & local pointer
    cxy_t     dev_cxy = GET_CXY( dev_xp );
    chdev_t * dev_ptr = GET_PTR( dev_xp );

    // get driver command function pointer from MMC device descriptor
    dev_cmd_t * cmd = (dev_cmd_t *)hal_remote_lpt( XPTR( dev_cxy , &dev_ptr->cmd ) );

    // get the MMC device remote busylock
    remote_busylock_acquire( XPTR( dev_cxy , &dev_ptr->wait_lock ) );  

    // call driver command
    cmd( XPTR( local_cxy , this ) );
    
    // release the MMC device remote busylock
    remote_busylock_release( XPTR( dev_cxy , &dev_ptr->wait_lock ) );  

    // return operation status
    return this->mmc_cmd.error;  

}  // end dev_mmc_access()

/////////////////////////////////////////
error_t dev_mmc_inval( xptr_t     buf_xp,
                       uint32_t   buf_size )
{
    error_t error;

    // get calling thread local pointer
    thread_t  * this    = CURRENT_THREAD;

#if DEBUG_DEV_MMC
uint32_t cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEV_MMC < cycle )
printk("\n[%s] thread[%x,%x] enters / cluster %x / buffer %x\n", 
__FUNCTION__, this->process->pid, this->trdid, GET_CXY(buf_xp), GET_PTR(buf_xp) );
#endif

    // get buffer cluster and local pointer
    cxy_t  buf_cxy = GET_CXY( buf_xp );
    void * buf_ptr = GET_PTR( buf_xp );
    
    assert( (((intptr_t)buf_ptr & (CONFIG_CACHE_LINE_SIZE -1)) == 0) ,
             "buffer not aligned on cache line" );

    // store command arguments in thread descriptor
    this->mmc_cmd.dev_xp    = chdev_dir.mmc[buf_cxy];
    this->mmc_cmd.type      = MMC_CC_INVAL;
    this->mmc_cmd.buf_ptr   = buf_ptr;
    this->mmc_cmd.buf_size  = buf_size;

    // call MMC driver
    error = dev_mmc_access( this );

#if DEBUG_DEV_MMC
cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEV_MMC < cycle )
printk("\n[%s] thread[%x,%x] exit / cluster %x / buffer %x\n", 
__FUNCTION__, this->process->pid, this->trdid, GET_CXY(buf_xp), GET_PTR(buf_xp) );
#endif

    return error;
}

////////////////////////////////////////
error_t dev_mmc_sync( xptr_t     buf_xp,
                      uint32_t   buf_size )
{
    error_t error;

    thread_t  * this    = CURRENT_THREAD;

#if DEBUG_DEV_MMC
uint32_t cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEV_MMC < cycle )
printk("\n[%s] thread[%x,%x] enters / cluster %x / buffer %x\n", 
__FUNCTION__, this->process->pid, this->trdid, GET_CXY(buf_xp), GET_PTR(buf_xp) );
#endif

    // get buffer cluster and local pointer
    cxy_t  buf_cxy = GET_CXY( buf_xp );
    void * buf_ptr = GET_PTR( buf_xp );
    
    assert( (((intptr_t)buf_ptr & (CONFIG_CACHE_LINE_SIZE -1)) == 0) ,
             "buffer not aligned on cache line" );

    // store command arguments in thread descriptor
    this->mmc_cmd.dev_xp    = chdev_dir.mmc[buf_cxy];
    this->mmc_cmd.type      = MMC_CC_SYNC;
    this->mmc_cmd.buf_ptr   = buf_ptr;
    this->mmc_cmd.buf_size  = buf_size;

    // call MMC driver
    error = dev_mmc_access( this );

#if DEBUG_DEV_MMC
cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEV_MMC < cycle )
printk("\n[%s] thread[%x,%x] exit / cluster %x / buffer %x\n", 
__FUNCTION__, this->process->pid, this->trdid, GET_CXY(buf_xp), GET_PTR(buf_xp) );
#endif

    return error;
}

/////////////////////////////////////////
error_t dev_mmc_set_error( cxy_t     cxy,
                           uint32_t  index,
                           uint32_t  wdata )
{
    // get calling thread local pointer
    thread_t * this = CURRENT_THREAD;

    // store command arguments in thread descriptor
    this->mmc_cmd.dev_xp    = chdev_dir.mmc[cxy];
    this->mmc_cmd.type      = MMC_SET_ERROR;
    this->mmc_cmd.reg_index = index;
    this->mmc_cmd.reg_ptr   = &wdata;

    // execute operation
    return dev_mmc_access( this ); 
}
                        
//////////////////////////////////////////
error_t dev_mmc_get_error( cxy_t      cxy,
                           uint32_t   index,
                           uint32_t * rdata )
{
    // get calling thread local pointer
    thread_t * this = CURRENT_THREAD;

    // store command arguments in thread descriptor
    this->mmc_cmd.dev_xp    = chdev_dir.mmc[cxy];
    this->mmc_cmd.type      = MMC_GET_ERROR;
    this->mmc_cmd.reg_index = index;
    this->mmc_cmd.reg_ptr   = rdata;

    // execute operation
    return dev_mmc_access( this ); 
}
                        
////////////////////////////////////////////////////
error_t dev_mmc_get_instrumentation( cxy_t      cxy,
                                     uint32_t   index,
                                     uint32_t * rdata )
{
    // get calling thread local pointer
    thread_t * this = CURRENT_THREAD;

    // store command arguments in thread descriptor
    this->mmc_cmd.dev_xp    = chdev_dir.mmc[cxy];
    this->mmc_cmd.type      = MMC_GET_INSTRU;
    this->mmc_cmd.reg_index = index;
    this->mmc_cmd.reg_ptr   = rdata;

    // execute operation
    return dev_mmc_access( this ); 
}

