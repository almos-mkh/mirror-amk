/*
 * dev_txt.h - TXT (Text Terminal) generic device API definition.
 *
 * Author  Alain Greiner    (2016)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-kernel; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _DEV_TXT_H_
#define _DEV_TXT_H_

#include <kernel_config.h>
#include <hal_kernel_types.h>
#include <xlist.h>
#include <remote_busylock.h>

/****  Forward declarations  ****/

struct chdev_s ;

/******************************************************************************************
 *     Generic Text Terminal device definition.
 *
 * This multi-channels generic TXT device provides access to a text terminal.
 *
 * It supports two operations that must be implemented by the driver cmd() function:
 * - TXT_READ : read a single character from the text terminal identified by its channel
 *   index, using a descheduling strategy for the calling thread.
 * - TXT_WRITE : write a character string to the text terminal identified by its channel
 *   index, using a descheduling strategy for the calling thread.
 *
 * It supports one operation, that must be implemented by the driver aux() function  
 * - TXT_SYNC_WRITE write a character string to the TXT0 kernel terminal, using a busy 
 *   waiting strategy for the calling thread. 
 *****************************************************************************************/

/******************************************************************************************
 * This defines the (implementation independant) extension for the generic TXT device.
 *****************************************************************************************/

typedef struct txt_extend_s
{
    xptr_t            owner_xp;  /*! ext. pointer on current process owner (reference)   */
    xlist_entry_t     root;      /*! root of list of processes attached to same TXT      */
    remote_busylock_t lock;      /*! lock protecting this list                           */
}
txt_extend_t;

/******************************************************************************************
 * This enum defines the various implementations of the generic TXT peripheral.
 * This array must be kept consistent with the define in arch_info.h file
 *****************************************************************************************/

enum txt_impl_e
{
    IMPL_TXT_TTY =   0,
    IMPL_TXT_RS2 =   1,
    IMPL_TXT_MTY =   2,
}
txt_impl_t;

/******************************************************************************************
 * This defines the arguments passed to the driver CMD function.
 *****************************************************************************************/

typedef enum
{
    TXT_READ       = 0,
    TXT_WRITE      = 1,
    TXT_SYNC_WRITE = 2,
} dev_txt_cmd_t;

typedef struct txt_command_s
{
    xptr_t      dev_xp;    /*! extended pointer on the relevant TXT device descriptor    */
    uint32_t    type;      /*! TXT_READ / TXT_WRITE                                      */
    xptr_t      buf_xp;    /*! extended pointer on characters array                      */
    uint32_t    count;     /*! number of characters in buffer (must be 1 if to_mem)      */
    uint32_t    error;     /*! operation status (0 if success)                           */
}
txt_command_t;

/******************************************************************************************
 * This defines the arguments passed to the driver AUX function.
 * This function implement the TXT_SYNC_WRITE operation.
 *****************************************************************************************/

typedef struct txt_sync_args_s
{
    xptr_t       dev_xp;    /*! extended pointer on the TXT0_TX device descriptor        */
    const char * buffer;    /*! local pointer on characters array                        */
    uint32_t     count;     /*! number of characters in buffer                           */
    uint32_t    channel;   /*! channel, aka which tty to write to                        */
}
txt_sync_args_t;

/******************************************************************************************
 * This function returns a printable string for the comman type.
 ******************************************************************************************
 * @ type     : command type (TXT_READ / TXT_WRITE / TXT_SYNC_WRITE)
 *****************************************************************************************/
const char * dev_txt_type_str( dev_txt_cmd_t type );

/******************************************************************************************
 * This function completes the TXT chdev descriptor initialisation,
 * namely the link with the implementation specific driver.
 * The func, impl, channel, is_rxt, base fields have been previously initialised.
 * It calls the specific driver initialisation function, to initialise the hardware
 * device and the driver specific data structures when required.
 * It creates the associated server thread and allocates a WTI from local ICU.
 * It must de executed by a local thread.
 ******************************************************************************************
 * @ chdev     : local pointer on TXT device descriptor.
 *****************************************************************************************/
void dev_txt_init( struct chdev_s * chdev );

/******************************************************************************************
 * This blocking function reads a single character from the terminal identified
 * by the "channel" argument. The corresponding request is actually registered in the
 * chdev requests queue, and the calling thread is descheduled, blocked until
 * transfer completion.
 * It must be called in the client cluster.
 ******************************************************************************************
 * @ channel   : TXT channel index.
 * @ buffer    : local pointer on destination buffer for the character.
 * @ returns 0 if success / returns EINVAL if error.
 *****************************************************************************************/
error_t dev_txt_read( uint32_t        channel,
                      char          * buffer );

/******************************************************************************************
 * This blocking function writes characters on the terminal identified
 * by the "channel" argument. The corresponding request is actually registered in the
 * chdev requests queue, and the calling thread is descheduled, blocked until
 * transfer completion.
 * It must be called in the client cluster.
 ******************************************************************************************
 * @ channel   : TXT channel index.
 * @ buffer    : local pointer on source buffer containing the string.
 * @ count     : number of characters.
 * @ returns 0 if success / returns EINVAL if error.
 ****************************************************************************************/
error_t dev_txt_write( uint32_t        channel,
                       char          * buffer,
                       uint32_t        count );

/***************************************************************************************
 * This blocking function is used by the kernel to display a string on the TXT0
 * terminal, without descheduling the calling thread, without registering it
 * in the TXT0 device waiting queue, without using the TXT0 irq, and without
 * interfering with another possible TXT access to another terminal.
 * As it is used for debug, the command arguments <buffer> and <count> are registerd
 * in a specific "dbg_cmd" field of the calling thread.
 ****************************************************************************************
 * @ buffer    : local pointer on source buffer containing the string.
 * @ count     : number of characters.
 * @ returns 0 if success / returns EINVAL if error.
 ***************************************************************************************/
error_t dev_txt_sync_write( const char     * buffer,
                            uint32_t         count );

#endif	/* _DEV_TXT_H_ */
