/*
 * dev_nic.c - NIC (Network Controler) generic device API implementation.
 * 
 * Author  Alain Greiner    (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_special.h>
#include <printk.h>
#include <chdev.h>
#include <thread.h>
#include <hal_drivers.h>
#include <dev_nic.h>

/////////////////////////////////////////////////////////////////////////////////////////
// Extern global variables
/////////////////////////////////////////////////////////////////////////////////////////

extern chdev_directory_t  chdev_dir;         // allocated in kernel_init.c

//////////////////////////////////
void dev_nic_init( chdev_t * nic )
{
    // the PIC chdev must be initialized before the NIC chdev, because
    // the NIC chdev initialisation requires the routing of an external IRQ.
    xptr_t  pic_xp  = chdev_dir.pic;

    assert( (pic_xp != XPTR_NULL) , "ICU not initialised before NIC" );

    // get "impl" , "channel" , "is_rx" fields from chdev descriptor
    uint32_t  impl    = nic->impl;
    uint32_t  channel = nic->channel;
    bool_t    is_rx   = nic->is_rx;

    // set chdev name
    if( is_rx ) snprintf( nic->name , 16 , "nic%d_rx" , channel );
    else        snprintf( nic->name , 16 , "nic%d_tx" , channel );

    // call driver init function
    hal_drivers_nic_init( nic , impl );

    // select a core to execute the NIC server thread
    lid_t lid = cluster_select_local_core();

    // bind the NIC IRQ to the selected core
    // but does NOT enable it
    dev_pic_bind_irq( lid , nic );

    // create server thread
    thread_t * new_thread;
    error_t    error;

    error = thread_kernel_create( &new_thread,
                                  THREAD_DEV,
                                  &chdev_server_func,
                                  nic,
                                  lid ); 

    assert( (error == 0) , "cannot create server thread" );

    // set "server" field in chdev descriptor
    nic->server = new_thread;
    
    // set "chdev" field in thread descriptor
    new_thread->chdev = nic;

    // unblock server thread
    thread_unblock( XPTR( local_cxy , new_thread ) , THREAD_BLOCKED_GLOBAL );

}  // end dev_nic_init()

///////////////////////////////////
error_t dev_nic_read( pkd_t * pkd )
{
    error_t  error;

    // get pointers on this NIC-RX kernel thread 
    thread_t * thread_ptr = CURRENT_THREAD;
    xptr_t     thread_xp  = XPTR( local_cxy , thread_ptr );

    // get local pointer on core running this kernel thead
    core_t * core = thread_ptr->core;

// check thread can yield
assert( (thread_ptr->busylocks == 0),
"cannot yield : busylocks = %d\n", thread_ptr->busylocks );

#if DEBUG_DEV_NIC_RX
uint32_t cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEV_NIC_RX < cycle )
printk("\n[DBG] %s : thread %x enters for packet %x in cluster %x\n", 
__FUNCTION__ , thread_ptr , pkd , local_cxy );
#endif

    // get pointer on NIC-RX chdev descriptor
    uint32_t   channel = thread_ptr->chdev->channel; 
    xptr_t     dev_xp  = chdev_dir.nic_rx[channel];
    cxy_t      dev_cxy = GET_CXY( dev_xp );
    chdev_t  * dev_ptr = (chdev_t *)GET_PTR( dev_xp );

    assert( (dev_xp != XPTR_NULL) , "undefined NIC chdev descriptor" );

    assert( (dev_cxy == local_cxy) , " chdev must be local" );

    // initialize command in thread descriptor
    thread_ptr->nic_cmd.dev_xp = dev_xp;

    // call driver to test readable
    thread_ptr->nic_cmd.cmd = NIC_CMD_READABLE;
    dev_ptr->cmd( thread_xp );

    // check error
    error = thread_ptr->nic_cmd.error;
    if( error ) return error;

    // block and deschedule if queue non readable
    if( thread_ptr->nic_cmd.status == false )  
    {
        // enable NIC-RX IRQ 
        dev_pic_enable_irq( core->lid , dev_xp );

        // block client thread on THREAD_BLOCKED_IO
        thread_block( XPTR( local_cxy , thread_ptr ) , THREAD_BLOCKED_IO );

        // deschedule client thread
        sched_yield("client blocked on I/O");

        // disable NIC-RX IRQ 
        dev_pic_disable_irq( core->lid , dev_xp );
    }

    // call driver for actual read 
    thread_ptr->nic_cmd.cmd     = NIC_CMD_READ;
    thread_ptr->nic_cmd.buffer  = pkd->buffer;
    dev_ptr->cmd( thread_xp );

    // check error
    error = thread_ptr->nic_cmd.error;
    if( error ) return error;

    // returns packet length   
    pkd->length = thread_ptr->nic_cmd.length;

#if DEBUG_DEV_NIC_RX
cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEV_NIC_RX < cycle )
printk("\n[DBG] %s : thread %x exit for packet %x in cluster %x\n", 
__FUNCTION__ , thread_ptr , pkd , local_cxy );
#endif

    return 0;

}   // end dev_nic_read()


////////////////////////////////////
error_t dev_nic_write( pkd_t * pkd )
{
    error_t error;

    // get pointers on the NIC-TX kernel tread 
    thread_t * thread_ptr = CURRENT_THREAD;
    xptr_t     thread_xp  = XPTR( local_cxy , thread_ptr );

    // get local pointer on core running this kernel thead
    core_t * core = thread_ptr->core;

// check thread can yield
assert( (thread_ptr->busylocks == 0),
"cannot yield : busylocks = %d\n", thread_ptr->busylocks );

#if DEBUG_DEV_NIC_RX
uint32_t cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEV_NIC_RX < cycle )
printk("\n[DBG] %s : thread %x enters for packet %x in cluster %x\n", 
__FUNCTION__ , thread_ptr , pkd , local_cxy );
#endif

    // get pointer on NIC-TX chdev descriptor
    uint32_t   channel = thread_ptr->chdev->channel; 
    xptr_t     dev_xp  = chdev_dir.nic_tx[channel];
    cxy_t      dev_cxy = GET_CXY( dev_xp );
    chdev_t  * dev_ptr = (chdev_t *)GET_PTR( dev_xp );

    assert( (dev_xp != XPTR_NULL) , "undefined NIC chdev descriptor" );

    assert( (dev_cxy == local_cxy) , " chdev must be local" );

    // initialize command in thread descriptor
    thread_ptr->nic_cmd.dev_xp = dev_xp;

    // call driver to test writable
    thread_ptr->nic_cmd.cmd = NIC_CMD_WRITABLE;
    dev_ptr->cmd( thread_xp );

    // check error
    error = thread_ptr->nic_cmd.error;
    if( error ) return error;

    // block and deschedule if queue non writable
    if( thread_ptr->nic_cmd.status == false )  
    {
        // enable NIC-TX IRQ 
        dev_pic_enable_irq( core->lid ,dev_xp );

        // block client thread on THREAD_BLOCKED I/O condition
        thread_block( XPTR( local_cxy , thread_ptr ) , THREAD_BLOCKED_IO );

        // deschedule client thread
        sched_yield("client blocked on I/O");

        // disable NIC-TX IRQ 
        dev_pic_disable_irq( core->lid , dev_xp );
    }

    // call driver for actual write
    thread_ptr->nic_cmd.cmd    = NIC_CMD_WRITE;
    thread_ptr->nic_cmd.buffer = pkd->buffer;
    thread_ptr->nic_cmd.length = pkd->length;
    dev_ptr->cmd( thread_xp );

    // check error
    error = thread_ptr->nic_cmd.error;
    if( error ) return error;

#if DEBUG_DEV_NIC_RX
cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEV_NIC_RX < cycle )
printk("\n[DBG] %s : thread %x exit for packet %x in cluster %x\n", 
__FUNCTION__ , thread_ptr , pkd , local_cxy );
#endif

    return 0;
}  // end dev_nic_write()



