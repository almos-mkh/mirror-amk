/*
 * dev_ioc.c - IOC (Block Device Controler) generic device API implementation.
 *
 * Author  Alain Greiner    (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MK
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <kernel_config.h>
#include <hal_kernel_types.h>
#include <hal_gpt.h>
#include <hal_drivers.h>
#include <thread.h>
#include <printk.h>
#include <chdev.h>
#include <dev_ioc.h>

/////////////////////////////////////////////////////////////////////////////////////////
// Extern global variables
/////////////////////////////////////////////////////////////////////////////////////////

extern chdev_directory_t  chdev_dir;     // allocated in kernel_init.c

//////////////////////////////////
void dev_ioc_init( chdev_t * ioc )
{
    // the PIC chdev must be initialized before the IOC chdev, because
    // the IOC chdev initialisation requires the routing of an external IRQ
    xptr_t  pic_xp  = chdev_dir.pic;

    assert( (pic_xp != XPTR_NULL) , "PIC not initialised before IOC" );

    // get implementation and channel from chdev descriptor
    uint32_t  impl    = ioc->impl;
    uint32_t  channel = ioc->channel;

    // set chdev name
    snprintf( ioc->name , 16 , "ioc%d" , channel );

    // call driver init function
    hal_drivers_ioc_init( ioc, impl );

    // select a core to execute the IOC server thread
    lid_t lid = cluster_select_local_core();

    // bind the IOC IRQ to the selected core
    dev_pic_bind_irq( lid , ioc );

    // enable IRQ
    dev_pic_enable_irq( lid , XPTR( local_cxy , ioc ) );

    // create server thread
    thread_t * new_thread;
    error_t    error;

    error = thread_kernel_create( &new_thread,
                                  THREAD_DEV,
                                  &chdev_server_func,
                                  ioc,
                                  lid );

    assert( (error == 0) , "cannot create server thread" );

    // set "server" field in chdev descriptor
    ioc->server = new_thread;

    // set "chdev field in thread descriptor
    new_thread->chdev = ioc;

    // unblock server thread
    thread_unblock( XPTR( local_cxy , new_thread ) , THREAD_BLOCKED_GLOBAL );

}  // end dev_ioc_init()

//////////////////////////////////////////////////////////////////////////////////
// This static function is called by dev_ioc_read() & dev_ioc_write() functions.
// It builds and registers the command in the calling thread descriptor.
// Then, it registers the calling thead in IOC chdev waiting queue.
// Finally it blocks on the THREAD_BLOCKED_IO condition and deschedule.
////////////////////////////////////i/////////////////////////////////////////////
static error_t dev_ioc_access( uint32_t   cmd_type,
                               uint8_t  * buffer,
                               uint32_t   lba,
                               uint32_t   count )
{
    thread_t * this = CURRENT_THREAD;              // pointer on client thread

#if ( DEV_IOC_RX  || DEV_IOC_TX )
uint32_t   cycle = (uint32_t)hal_get_cycles();
#endif

    // software L2/L3 cache coherence for memory buffer
    if( chdev_dir.iob )
    {
        if (cmd_type == IOC_READ) dev_mmc_inval( XPTR( local_cxy , buffer ) , count<<9 );
        else                      dev_mmc_sync ( XPTR( local_cxy , buffer ) , count<<9 );
    }

    // get extended pointer on IOC chdev descriptor
    xptr_t  dev_xp = chdev_dir.ioc[0];

// check dev_xp
assert( (dev_xp != XPTR_NULL) , "undefined IOC chdev descriptor" );

    // register command in calling thread descriptor
    this->ioc_cmd.dev_xp    = dev_xp;
    this->ioc_cmd.type      = cmd_type;
    this->ioc_cmd.buf_xp    = XPTR( local_cxy , buffer );
    this->ioc_cmd.lba       = lba;
    this->ioc_cmd.count     = count;

    // register client thread in IOC chdev waiting queue, activate server thread,
    // block client thread on THREAD_BLOCKED_IO and deschedule.
    // it is re-activated by the ISR signaling IO operation completion.
    chdev_register_command( dev_xp );

#if(DEV_IOC_RX & 1)
if( (DEV_IOC_RX < cycle) && (cmd_type != IOC_WRITE) )
printk("\n[%s] thread[%x,%x] resumes for RX\n",
__FUNCTION__, this->process->pid , this->trdid )
#endif

#if(DEV_IOC_TX & 1)
if( (DEV_IOC_RX < cycle) && (cmd_type == IOC_WRITE) )
printk("\n[%s] thread[%x,%x] resumes for TX\n",
__FUNCTION__, this->process->pid , this->trdid )
#endif

    // return I/O operation status
    return this->ioc_cmd.error;

}  // end dev_ioc_access()

////////////////////////////////////////////
error_t dev_ioc_read( uint8_t      * buffer,
                      uint32_t       lba,
                      uint32_t       count )
{

#if DEBUG_DEV_IOC_RX
uint32_t   cycle = (uint32_t)hal_get_cycles();
thread_t * this  = CURRENT_THREAD;
if( DEBUG_DEV_IOC_RX < cycle )
printk("\n[%s] thread[%x,%x] enters / lba  %x / buffer %x / cycle %d\n",
__FUNCTION__ , this->process->pid, this->trdid, lba, buffer, cycle );
#endif

    return dev_ioc_access( IOC_READ , buffer , lba , count );
}

////////////////////////////////////////////
error_t dev_ioc_write( uint8_t     * buffer,
                       uint32_t      lba,
                       uint32_t      count )
{

#if DEBUG_DEV_IOC_TX
uint32_t   cycle = (uint32_t)hal_get_cycles();
thread_t * this  = CURRENT_THREAD;
if( DEBUG_DEV_IOC_TX < cycle )
printk("\n[%s] thread[%x,%x] enters / lba  %x / buffer %x / cycle %d\n",
__FUNCTION__ , this->process->pid, this->trdid, lba, buffer, cycle );
#endif

    return dev_ioc_access( IOC_WRITE , buffer , lba , count );
}





//////////////////////////////////////////////////////////////////////////////////
// This static function is called by dev_ioc_sync_read() & dev_ioc_sync_write().
// It builds and registers the command in the calling thread descriptor, and
// calls directly the blocking IOC driver command, that returns only when the
// IO operation is completed.
////////////////////////////////////i/////////////////////////////////////////////
error_t dev_ioc_sync_access( uint32_t   cmd_type,
                             uint8_t  * buffer,
                             uint32_t   lba,
                             uint32_t   count )
{
    // get pointer on calling thread
    thread_t * this = CURRENT_THREAD;

    // software L2/L3 cache coherence for memory buffer
    if( chdev_dir.iob )
    {
        if (cmd_type == IOC_SYNC_READ) dev_mmc_inval( XPTR(local_cxy,buffer) , count<<9 );
        else                           dev_mmc_sync ( XPTR(local_cxy,buffer) , count<<9 );
    }

    // get extended pointer on IOC[0] chdev
    xptr_t  ioc_xp = chdev_dir.ioc[0];

// check ioc_xp
assert( (ioc_xp != XPTR_NULL) , "undefined IOC chdev descriptor" );

    // register command in calling thread descriptor
    this->ioc_cmd.dev_xp    = ioc_xp;
    this->ioc_cmd.type      = cmd_type;
    this->ioc_cmd.buf_xp    = XPTR( local_cxy , buffer );
    this->ioc_cmd.lba       = lba;
    this->ioc_cmd.count     = count;

    // get driver command function
    cxy_t       ioc_cxy = GET_CXY( ioc_xp );
    chdev_t   * ioc_ptr = GET_PTR( ioc_xp );
    dev_cmd_t * cmd = (dev_cmd_t *)hal_remote_lpt( XPTR( ioc_cxy , &ioc_ptr->cmd ) );

    // get core local index for the core handling the IOC IRQ
    thread_t * server = (thread_t *)hal_remote_lpt( XPTR( ioc_cxy , &ioc_ptr->server ) );
    core_t   * core   = (core_t *)hal_remote_lpt( XPTR( ioc_cxy , &server->core ) );
    lid_t      lid    = (lid_t)hal_remote_l32( XPTR( ioc_cxy , &core->lid ) );

    // mask the IRQ
    dev_pic_disable_irq( lid , ioc_xp );

    // call driver function
    cmd( XPTR( local_cxy , this ) );

    // unmask the IRQ
    dev_pic_enable_irq( lid , ioc_xp );

    // return I/O operation status from calling thread descriptor
    return this->ioc_cmd.error;

}  // end ioc_sync_access()

/////////////////////////////////////////////
error_t dev_ioc_sync_read( uint8_t  * buffer,
                           uint32_t   lba,
                           uint32_t   count )
{

#if DEBUG_DEV_IOC_RX
thread_t * this  = CURRENT_THREAD;
uint32_t   cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEV_IOC_RX < cycle )
printk("\n[%s] thread[%x,%x] : lba  %x / buffer %x / cycle %d\n",
__FUNCTION__ , this->process->pid, this->trdid, lba, buffer, cycle );
#endif

    return dev_ioc_sync_access( IOC_SYNC_READ , buffer , lba , count );
} 

//////////////////////////////////////////////
error_t dev_ioc_sync_write( uint8_t  * buffer,
                            uint32_t   lba,
                            uint32_t   count )
{

#if DEBUG_DEV_IOC_RX
thread_t * this  = CURRENT_THREAD;
uint32_t   cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEV_IOC_RX < cycle )
printk("\n[%s] thread[%x,%x] enters / lba  %x / buffer %x / cycle %d\n",
__FUNCTION__ , this->process->pid, this->trdid, lba, buffer, cycle );
#endif

    return dev_ioc_sync_access( IOC_SYNC_WRITE , buffer , lba , count );
} 

