/*
 * dev_iob.c - IOB (bridge to external I/O) generic device API implementation.
 *
 * Authors   Alain Greiner  (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTIOBLAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <kernel_config.h>
#include <hal_kernel_types.h>
#include <hal_special.h>
#include <remote_busylock.h>
#include <chdev.h>
#include <printk.h>
#include <string.h>
#include <hal_drivers.h>
#include <dev_iob.h>

////////////////////////////////////
void dev_iob_init( chdev_t * chdev )
{
    // get implementation
    uint32_t  impl = chdev->impl;

    // set chdev name
    strcpy( chdev->name , "iob" );

    // call driver init function
    hal_drivers_iob_init( chdev , impl );
}

//////////////////////////////////////////
void dev_iob_iommu_enable( xptr_t dev_xp )
{
    // get IOB chdev descriptor cluster and local pointer
    cxy_t     dev_cxy = GET_CXY( dev_xp );
    chdev_t * dev_ptr = GET_PTR( dev_xp );

    // get pointer on set_active function
    iob_set_active_t * f = hal_remote_lpt( XPTR( dev_cxy , &dev_ptr->ext.iob.set_active ) );

    // call relevant driver function
    remote_busylock_acquire( XPTR( dev_cxy , &dev_ptr->wait_lock ) );
    f( dev_xp , 1 );
    remote_busylock_release( XPTR( dev_cxy , &dev_ptr->wait_lock ) );
}

///////////////////////////////////////////
void dev_iob_iommu_disable( xptr_t dev_xp )
{
    // get IOB chdev descriptor cluster and local pointer
    cxy_t     dev_cxy = GET_CXY( dev_xp );
    chdev_t * dev_ptr = GET_PTR( dev_xp );

    // get pointer on set_active function
    iob_set_active_t * f = hal_remote_lpt( XPTR( dev_cxy , &dev_ptr->ext.iob.set_active ) );

    // call driver function
    remote_busylock_acquire( XPTR( dev_cxy , &dev_ptr->wait_lock ) );
    f( dev_xp , 0 );
    remote_busylock_release( XPTR( dev_cxy , &dev_ptr->wait_lock ) );
}

////////////////////////////////////////
void dev_iob_set_ptpr( xptr_t    dev_xp,
                       uint32_t  wdata )
{
    // get IOB chdev descriptor cluster and local pointer
    cxy_t     dev_cxy = GET_CXY( dev_xp );
    chdev_t * dev_ptr = GET_PTR( dev_xp );

    // get pointer on set_ptpr function
    iob_set_ptpr_t * f = hal_remote_lpt( XPTR( dev_cxy , &dev_ptr->ext.iob.set_ptpr ) );

    // call driver function
    remote_busylock_acquire( XPTR( dev_cxy , &dev_ptr->wait_lock ) );
    f( dev_xp , wdata );
    remote_busylock_release( XPTR( dev_cxy , &dev_ptr->wait_lock ) );

}

////////////////////////////////////////
void dev_iob_inval_page( xptr_t  dev_xp,
                         vpn_t   vpn )
{
    // get IOB chdev descriptor cluster and local pointer
    cxy_t     dev_cxy = GET_CXY( dev_xp );
    chdev_t * dev_ptr = GET_PTR( dev_xp );

    // get pointer on inval_page function
    iob_inval_page_t * f = hal_remote_lpt( XPTR( dev_cxy , &dev_ptr->ext.iob.inval_page ) );

    // call driver function
    remote_busylock_acquire( XPTR( dev_cxy , &dev_ptr->wait_lock ) );
    f( dev_xp , vpn );
    remote_busylock_release( XPTR( dev_cxy , &dev_ptr->wait_lock ) );
}

///////////////////////////////////////////
void dev_iob_get_status( xptr_t     dev_xp,
                         uint32_t * error,
                         uint32_t * bvar,
                         uint32_t * srcid )
{
    // get IOB chdev descriptor cluster and local pointer
    cxy_t     dev_cxy = GET_CXY( dev_xp );
    chdev_t * dev_ptr = GET_PTR( dev_xp );

    // get pointer on the functions
    iob_get_error_t * f_get_error = hal_remote_lpt( XPTR( dev_cxy , &dev_ptr->ext.iob.get_error ) );
    iob_get_srcid_t * f_get_srcid = hal_remote_lpt( XPTR( dev_cxy , &dev_ptr->ext.iob.get_srcid ) );
    iob_get_bvar_t  * f_get_bvar  = hal_remote_lpt( XPTR( dev_cxy , &dev_ptr->ext.iob.get_bvar  ) );

    // call driver function
    remote_busylock_acquire( XPTR( dev_cxy , &dev_ptr->wait_lock ) );
    *error = f_get_error( dev_xp );
    *srcid = f_get_srcid( dev_xp );
    *bvar  = f_get_bvar( dev_xp );
    remote_busylock_release( XPTR( dev_cxy , &dev_ptr->wait_lock ) );
}

