/*
 * dev_pic.c - PIC (External Interrupt Controler) generic device API implementation.
 *
 * Authors   Alain Greiner  (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTPICLAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_special.h>
#include <chdev.h>
#include <printk.h>
#include <string.h>
#include <thread.h>
#include <remote_busylock.h>
#include <hal_drivers.h>
#include <dev_pic.h>
#include <cluster.h>

/////////////////////////////////////////////////////////////////////////////////////////
// Extern global variables
/////////////////////////////////////////////////////////////////////////////////////////

extern chdev_directory_t  chdev_dir;         // allocated in kernel_init.c
extern iopic_input_t      iopic_input;       // allocated in kernel_init.c

///////////////////////////////////
void dev_pic_init( chdev_t  * pic )
{
    // get implementation
    uint32_t impl = pic->impl;

    // set chdev name
    strcpy( pic->name , "pic" );

    // call the implementation-specific PIC driver
    hal_drivers_pic_init(pic, impl);
}

/////////////////////////////////////////////////
void dev_pic_extend_init( uint32_t * lapic_base )
{
    // get pointer on PIC chdev
    chdev_t * pic_ptr = (chdev_t *)GET_PTR( chdev_dir.pic );
    cxy_t     pic_cxy = GET_CXY( chdev_dir.pic );

    // get pointer on extend_init function
    extend_init_t * f = hal_remote_lpt( XPTR( pic_cxy , &pic_ptr->ext.pic.extend_init ) ); 

    // call relevant driver function
    f( lapic_base );
}

/////////////////////////////////////
void dev_pic_bind_irq( lid_t     lid,
                       chdev_t * src_chdev )
{
    // get pointer on PIC chdev
    chdev_t * pic_ptr = (chdev_t *)GET_PTR( chdev_dir.pic );
    cxy_t     pic_cxy = GET_CXY( chdev_dir.pic );

    // get pointer on bind_irq function
    bind_irq_t * f = hal_remote_lpt( XPTR( pic_cxy , &pic_ptr->ext.pic.bind_irq ) );

    // call relevant driver function
    f( lid , src_chdev );
}

/////////////////////////////////////
void dev_pic_enable_irq( lid_t   lid,
                         xptr_t  src_chdev_xp )
{

#if DEBUG_DEV_PIC
uint32_t cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEV_PIC < cycle )
printk("\n[DBG] %s : core[%x,%d] / src_chdev_cxy %x / src_chdev_ptr %x / cycle %d\n",
__FUNCTION__, local_cxy, lid, GET_CXY(src_chdev_xp), GET_PTR(src_chdev_xp), cycle );
#endif

    // get pointer on PIC chdev
    chdev_t * pic_ptr = (chdev_t *)GET_PTR( chdev_dir.pic );
    cxy_t     pic_cxy = GET_CXY( chdev_dir.pic );

    // get pointer on enable_irq function
    enable_irq_t * f = hal_remote_lpt( XPTR( pic_cxy , &pic_ptr->ext.pic.enable_irq ) );

    // call relevant driver function
    f( lid , src_chdev_xp );
}

//////////////////////////////////////
void dev_pic_disable_irq( lid_t   lid,
                          xptr_t  src_chdev_xp )
{

#if DEBUG_DEV_PIC
uint32_t cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEV_PIC < cycle )
printk("\n[DBG] %s : core[%x,%d] / src_chdev_cxy %x / src_chdev_ptr %x / cycle %d\n",
__FUNCTION__, local_cxy, lid, GET_CXY(src_chdev_xp), GET_PTR(src_chdev_xp), cycle );
#endif

    // get pointer on PIC chdev
    chdev_t * pic_ptr = (chdev_t *)GET_PTR( chdev_dir.pic );
    cxy_t     pic_cxy = GET_CXY( chdev_dir.pic );

    // get pointer on disable_irq function
    disable_irq_t * f = hal_remote_lpt( XPTR( pic_cxy , &pic_ptr->ext.pic.disable_irq ) );

    // call relevant driver function
    f( lid , src_chdev_xp );
}

////////////////////////////////////////////
void dev_pic_enable_timer( uint32_t period )
{

#if DEBUG_DEV_PIC
uint32_t cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEV_PIC < cycle )
printk("\n[DBG] %s : core[%x,%d] / period %d / cycle %d\n",
__FUNCTION__ , local_cxy , CURRENT_THREAD->core->lid , period, cycle );
#endif

    // get pointer on PIC chdev
    chdev_t * pic_ptr = (chdev_t *)GET_PTR( chdev_dir.pic );
    cxy_t     pic_cxy = GET_CXY( chdev_dir.pic );

    // get pointer on enable_timer function
    enable_timer_t * f = hal_remote_lpt( XPTR( pic_cxy , &pic_ptr->ext.pic.enable_timer ) );

    // call relevant driver function
    f( period );
}

/////////////////////////
void dev_pic_enable_ipi( void )
{

#if DEBUG_DEV_PIC
uint32_t cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEV_PIC < cycle )
printk("\n[DBG] %s : core[%x,%d] / cycle %d\n",
__FUNCTION__ , local_cxy , CURRENT_THREAD->core->lid , cycle );
#endif

    // get pointer on PIC chdev
    chdev_t * pic_ptr = (chdev_t *)GET_PTR( chdev_dir.pic );
    cxy_t     pic_cxy = GET_CXY( chdev_dir.pic );

    // get pointer on enable_timer function
    enable_ipi_t * f = hal_remote_lpt( XPTR( pic_cxy , &pic_ptr->ext.pic.enable_ipi ) );

    // call relevant driver function
    f();
}

//////////////////////////////////
void dev_pic_send_ipi( cxy_t  cxy,
                       lid_t  lid )
{

#if DEBUG_DEV_PIC
uint32_t cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEV_PIC < cycle )
printk("\n[DBG] %s : thread %x in process %x / tgt_core[%x,%d] / cycle %d\n",
__FUNCTION__, CURRENT_THREAD->trdid, CURRENT_THREAD->process->pid, cxy, lid, cycle );
#endif

    // get pointer on PIC chdev
    chdev_t * pic_ptr = (chdev_t *)GET_PTR( chdev_dir.pic );
    cxy_t     pic_cxy = GET_CXY( chdev_dir.pic );

    // get pointer on send_ipi function
    send_ipi_t * f = hal_remote_lpt( XPTR( pic_cxy , &pic_ptr->ext.pic.send_ipi ) );

    // call relevant driver function
    f( cxy , lid );
}

//////////////////////
void dev_pic_ack_ipi( void )
{

#if DEBUG_DEV_PIC
uint32_t cycle = (uint32_t)hal_get_cycles();
if( DEBUG_DEV_PIC < cycle )
printk("\n[DBG] %s : core[%x,%d] / cycle %d\n",
__FUNCTION__, local_cxy, CURRENT_THREAD->core->lid, cycle );
#endif

    // get pointer on PIC chdev
    chdev_t * pic_ptr = (chdev_t *)GET_PTR( chdev_dir.pic );
    cxy_t     pic_cxy = GET_CXY( chdev_dir.pic );

    // get pointer on ack_ipi function
    ack_ipi_t * f = hal_remote_lpt( XPTR( pic_cxy , &pic_ptr->ext.pic.ack_ipi ) );

    // call relevant driver function
    f();
}

///////////////////////////////////
void dev_pic_inputs_display( void )
{
    uint32_t k;

    // get pointers on TXT0 chdev
    xptr_t    txt0_xp  = chdev_dir.txt_tx[0];
    cxy_t     txt0_cxy = GET_CXY( txt0_xp );
    chdev_t * txt0_ptr = GET_PTR( txt0_xp );

    // get extended pointer on remote TXT0 chdev lock
    xptr_t  lock_xp = XPTR( txt0_cxy , &txt0_ptr->wait_lock );

    // get TXT0 lock in busy waiting mode
    remote_busylock_acquire( lock_xp );

    nolock_printk("\n***** iopic_inputs\n");

    for( k = 0 ; k < CONFIG_MAX_IOC_CHANNELS ; k++ )
    {
        nolock_printk(" - IOC[%d]    hwi_id = %d\n", k , iopic_input.ioc[k] );
    }

    for( k = 0 ; k < CONFIG_MAX_TXT_CHANNELS ; k++ )
    {
        nolock_printk(" - TXT_TX[%d] hwi_id = %d\n", k , iopic_input.txt_tx[k] );
        nolock_printk(" - TXT_RX[%d] hwi_id = %d\n", k , iopic_input.txt_rx[k] );
    }

    for( k = 0 ; k < CONFIG_MAX_NIC_CHANNELS ; k++ )
    {
        nolock_printk(" - NIC_TX[%d] hwi_id = %d\n", k , iopic_input.nic_tx[k] );
        nolock_printk(" - NIC_RX[%d] hwi_id = %d\n", k , iopic_input.nic_rx[k] );
    }

    // release TXT0 lock
    remote_busylock_release( lock_xp );
}


