/*
 * dev_ioc.h - IOC (Block Device Controler) generic device API definition.
 * 
 * Author  Alain Greiner    (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-kernel; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _DEV_IOC_H
#define _DEV_IOC_H

#include <kernel_config.h>
#include <hal_kernel_types.h>

/****  Forward declarations  ****/

struct chdev_s;

/*****************************************************************************************
 *     Generic Block Device Controler definition
 *
 * This device provide access to an external mass storage peripheral such as a
 * magnetic hard disk or a SD card, that can store blocks of data in a linear array
 * of sectors indexed by a simple lba (logic block address).
 * It supports four command types:
 * - READ       : move blocks from device to memory, with a descheduling policy.
 * - WRITE      : move blocks from memory to device, with a descheduling policy.
 * - SYNC_READ  : move blocks from device to memory, with a busy waiting policy.
 * - SYNC_WRITE : move blocks from memory to device, with a busy waiting policy.

 * A READ or WRITE operation requires dynamic ressource allocation. The calling thread
 * is descheduled, and the work is done by the server thread associated to IOC device.
 * The general scenario is detailed below.
 * A) the client thread start the I/O operation, by calling the dev_ioc_read()
 *    or the dev_ioc_write() kernel functions that perform the following actions: 
 *    1) it get a free WTI mailbox from the client cluster WTI allocator.
 *    2) it enables the WTI IRQ on the client cluster ICU and update interrupt vector.
 *    3) it access the PIC to link the WTI mailbox to the IOC IRQ.
 *    4) it builds the command descriptor.
 *    5) it registers in the IOC device waiting queue. 
 *    6) itblock on the THREAD_BLOCKED_IO condition and deschedule.
 * B) The server thread attached to the IOC device descriptor handles the commands
 *    registered in the waiting queue, calling the IOC driver function.
 *    Most hardware implementation have a DMA capability, but some implementations,
 *    such as the RDK (Ram Disk) implementation does not use DMA.
 * C) The ISR signaling the I/O operation completion reactivates the client thread,
 *    that releases the allocated resources:
 *    1) access the PIC to unlink the IOC IRQ.
 *    2) disable the WTI IRQ in the client cluster ICU and update interrupt vector.
 *    3) release the WTI mailbox to the client cluster WTI allocator.
 *
 * The SYNC_READ and SYNC_WRITE operations are used by the kernel in the initialisation
 * phase. These operations do not not use the IOC device waiting queue, the server thread,
 * and the IOC IRQ, but implement a busy-waiting policy for the calling thread.
 *****************************************************************************************/

/******************************************************************************************
 * This defines the (implementation independant) extension for the generic IOC device.
 *****************************************************************************************/

typedef struct ioc_extend_s
{
    uint32_t    size;      /*! number of bytes in a block                               */
    uint32_t    count;     /*! total number of blocks in physical device                */
}
ioc_extend_t;

/******************************************************************************************
 * This enum defines the various implementations of the generic IOC peripheral.
 * It must be kept consistent with the define in arch_info.h file.
 *****************************************************************************************/

typedef enum
{
    IMPL_IOC_BDV =   0,     
    IMPL_IOC_HBA =   1,  
    IMPL_IOC_SDC =   2,
    IMPL_IOC_SPI =   3,
    IMPL_IOC_RDK =   4,
}
ioc_impl_t;

/******************************************************************************************
 * This defines the (implementation independant)  command passed to the driver.
 *****************************************************************************************/

typedef enum
{
    IOC_READ       = 0,
    IOC_WRITE      = 1,
    IOC_SYNC_READ  = 2,
    IOC_SYNC_WRITE = 3,
}
cmd_type_t;

typedef struct ioc_command_s
{
    xptr_t      dev_xp;     /*! extended pointer on IOC device descriptor                */
    uint32_t    type;       /*! IOC_READ / IOC_WRITE / IOC_SYNC_READ                     */
    uint32_t    lba;        /*! first block index                                        */
    uint32_t    count;      /*! number of blocks                                         */
    xptr_t      buf_xp;     /*! extended pointer on memory buffer                        */
    uint32_t    error;      /*! operation status (0 if success)                          */
}
ioc_command_t;

/******************************************************************************************
 * This function completes the IOC chdev descriptor initialisation,
 * namely the link with the implementation specific driver.
 * The func, impl, channel, is_rx, base fields have been previously initialised.
 * It calls the specific driver initialisation function, to initialise the hardware
 * device and the specific data structures when required.
 * It creates the associated server thread and allocates a WTI from local ICU.
 * It must de executed by a local thread.
 ******************************************************************************************
 * @ chdev     : local pointer on IOC chdev descriptor.
 *****************************************************************************************/
void dev_ioc_init( struct chdev_s * chdev );

/******************************************************************************************
 * This blocking function moves one or several contiguous blocks of data
 * from the block device to a local memory buffer. The corresponding request is actually
 * registered in the device pending request queue, and the calling thread is descheduled,
 * waiting on transfer completion. It will be resumed by the IRQ signaling completion.
 * It must be called in the client cluster.
 ******************************************************************************************
 * @ buffer    : local pointer on target buffer in memory (must be block aligned).
 * @ lba       : first block index on device.
 * @ count     : number of blocks to transfer.
 * @ returns 0 if success / returns EINVAL if error.
 *****************************************************************************************/
error_t dev_ioc_read( uint8_t      * buffer,
                      uint32_t       lba,
                      uint32_t       count );

/******************************************************************************************
 * This blocking function moves one or several contiguous blocks of data
 * from a local memory buffer to the block device. The corresponding request is actually
 * registered in the device pending request queue, and the calling thread is descheduled,
 * waiting on transfer completion. It will be resumed by the IRQ signaling completion.
 * It must be called in the client cluster.
 ******************************************************************************************
 * @ buffer    : local pointer on source buffer in memory (must be block aligned).
 * @ lba       : first block index on device.
 * @ count     : number of blocks to transfer.
 * @ returns 0 if success / returns EINVAL if error.
 *****************************************************************************************/
error_t dev_ioc_write( uint8_t      * buffer,
                       uint32_t       lba,
                       uint32_t       count );

/******************************************************************************************
 * This blocking function moves one or several contiguous blocks of data
 * from the block device to a local memory buffer.
 * It does  not uses the IOC device waiting queue and server thread, and does not use 
 * the IOC IRQ, but call directly the relevant IOC driver, implementing a busy-waiting
 * policy for the calling thread.
 * It must be called in the client cluster.
 ******************************************************************************************
 * @ buffer    : local pointer on target buffer in memory (must be block aligned).
 * @ lba       : first block index on device.
 * @ count     : number of blocks to transfer.
 * @ returns 0 if success / returns EINVAL if error.
 *****************************************************************************************/
error_t dev_ioc_sync_read( uint8_t      * buffer,
                           uint32_t       lba,
                           uint32_t       count );

/******************************************************************************************
 * This blocking function moves one or several contiguous blocks of data
 * from a local memory buffer to the block device.
 * It does  not uses the IOC device waiting queue and server thread, and does not use 
 * the IOC IRQ, but call directly the relevant IOC driver, implementing a busy-waiting
 * policy for the calling thread.
 * It must be called in the client cluster.
 ******************************************************************************************
 * @ buffer    : local pointer on source buffer in memory (must be block aligned).
 * @ lba       : first block index on device.
 * @ count     : number of blocks to transfer.
 * @ returns 0 if success / returns EINVAL if error.
 *****************************************************************************************/
error_t dev_ioc_sync_write( uint8_t      * buffer,
                            uint32_t       lba,
                            uint32_t       count );

#endif	/* _DEV_IOC_H */
