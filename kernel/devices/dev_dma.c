/*
 * dev_dma.c - DMA (Interrupt Controler Unit) generic device API implementation.
 *
 * Authors   Alain Greiner  (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTDMALAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_special.h>
#include <chdev.h>
#include <thread.h>
#include <cluster.h>
#include <printk.h>
#include <memcpy.h>
#include <dev_dma.h>
#include <hal_drivers.h>

/////////////////////////////////////////////////////////////////////////////////////////
// Extern global variables
/////////////////////////////////////////////////////////////////////////////////////////

extern chdev_directory_t  chdev_dir;         // allocated in kernel_init.c

//////////////////////////////////
void dev_dma_init( chdev_t * dma )
{
    // get implementation & channel from DMA dma descriptor
    uint32_t impl    = dma->impl;
    uint32_t channel = dma->channel;

    // set dma name
    snprintf( dma->name , 16 , "dma%d_%x" , channel , local_cxy );

    // call driver init function
    hal_drivers_dma_init( dma, impl );

    // bind IRQ to the core defined by the DMA channel 
    dev_pic_bind_irq( channel , dma );

    // enable IRQ
    dev_pic_enable_irq( channel, XPTR( local_cxy , dma ) );

    // create server thread
    thread_t * new_thread;
    error_t    error;

    error = thread_kernel_create( &new_thread,
                                  THREAD_DEV,
                                  &chdev_server_func,
                                  dma,
                                  cluster_select_local_core() );
    if( error )
    {
        assert( false , "cannot create server thread" );
    }

    // initialises server field in chdev descriptor
    dma->server = new_thread;

    // initializes chdev field in thread descriptor
    new_thread->chdev = dma;
    
    // unblock server thread
    thread_unblock( XPTR( local_cxy , new_thread ) , THREAD_BLOCKED_GLOBAL );

} // dev_dma_init()

////////////////////////////////////////////////
error_t dev_dma_remote_memcpy( xptr_t    dst_xp,
                               xptr_t    src_xp,
                               uint32_t  size )
{
    thread_t * this = CURRENT_THREAD;

#if CONGIG_DEBUG_DEV_DMA
uint32_t cycle = (uint32_t)hal_get_cycles();
if( CONGIG_DEBUG_DEV_DMA < cycle )
printk("\n[DBG] %s : thread %x enters / dst %l / src %l / size = %x\n",
__FUNCTION__ , this, dst_xp, src_xp, size );
#endif

    // select DMA channel corresponding to core lid
    uint32_t channel = this->core->lid;

    // get extended pointer on selected DMA chdev descriptor
    xptr_t  dev_xp = chdev_dir.dma[channel];

    assert( (dev_xp != XPTR_NULL) , "undefined DMA chdev descriptor" );

    // register command in calling thread descriptor
    this->dma_cmd.dev_xp  = dev_xp;
    this->dma_cmd.dst_xp  = dst_xp;
    this->dma_cmd.src_xp  = src_xp;
    this->dma_cmd.size    = size;

    // register client thread in waiting queue, activate server thread
    // block client thread on THREAD_BLOCKED_IO and deschedule.
    // it is re-activated by the ISR signaling IO operation completion.
    chdev_register_command( dev_xp );

#if CONGIG_DEBUG_DEV_DMA
cycle = (uint32_t)hal_get_cycles();
if( CONGIG_DEBUG_DEV_DMA < cycle )
printk("\n[DBG] %s : thread %x exit / dst %l / src %l / size = %x\n",
__FUNCTION__ , this, dst_xp, src_xp, size );
#endif

    // return I/O operation status from calling thread descriptor
    return this->dma_cmd.error;  

}  // dev_dma_remote_memcpy()

