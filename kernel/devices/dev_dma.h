/*
 * dev_dma.h - DMA (Direct Memory Access) generic device API definition.
 *
 * Authors   Alain Greiner  (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTDMALAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _DEV_DMA_H_
#define _DEV_DMA_H_

#include <kernel_config.h>
#include <hal_kernel_types.h>

/*****************************************************************************************
 *     Generic Multi-channels DMA controller definition
 *
 * The DMA device describes a generic, multi-channels DMA controller, that can be used 
 * by the kernel to speedup memory copies. It is an "internal" device, replicated in all
 * clusters. It can only be configured by a local kernel thread, but it can move data
 * to/from remote clusters. The burst size is defined by the cache line size.
 * Each DMA channel is described by a specific chdev descriptor, handling its private
 * waiting threads queue.
 * It implement one single command : move data from a (remote) source buffer 
 * to a (remote) destination buffer.
 ****************************************************************************************/
 
/****  Forward declarations  ****/

struct chdev_s;

/*****************************************************************************************
 * This defines the (implementation independant) command passed to the driver.
 ****************************************************************************************/

typedef struct dma_command_s
{
    xptr_t      dev_xp;    /*! extended pointer on the DMA chdev descriptor             */
    xptr_t      src_xp;    /*! extended pointer on source buffer.                        */
    xptr_t      dst_xp;    /*! extended pointer on destination buffer.                   */
    uint32_t    size;      /*! number of bytes to move.                                  */
    uint32_t    error;     /*! operation status (0 if success)                           */
}
dma_command_t;

/*****************************************************************************************
 * This enum defines the various implementations of the DMA internal interrupt controller.
 * This array must be kept consistent with the define in arch_info.h file
 ****************************************************************************************/

enum dma_impl_e
{
    IMPL_DMA_SCL =   0,         /* vci_xdma component used in TSAR                      */
    IMPL_DMA_I86 =   1          /* TBD                                                  */
}
dma_impl_t;

/*****************************************************************************************
 * This function makes two initialisations:
 * - It initialises the DMA specific fields of the chdev descriptor.
 * - it initialises the implementation specific DMA hardware device and associated
 *   data structures if required.
 * It must be called by a local thread.
 *****************************************************************************************
 * @ chdev   : pointer on DMA chdev descriptor.
 ****************************************************************************************/
void dev_dma_init( struct chdev_s * chdev );

/*****************************************************************************************
 * This blocking function register a DMA request in the device queue.
 * It uses a descheduling policy to wait completion, and return an error status
 * when the transfer is completed.
 *****************************************************************************************
 * @ dst        : extended pointer on destination buffer.
 * @ src        : extended pointer on Rsource buffer.
 * @ size       : number of bytes to move.
 ****************************************************************************************/
error_t dev_dma_remote_memcpy( xptr_t     dst_xp,
                               xptr_t     src_xp,
                               uint32_t   size );    


#endif  /* _DEV_DMA_H_ */
