/*
 * dev_iob.h - IOB (bridge to external I/O) generic device API.
 *
 * Authors   Alain Greiner  (2016)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTIOBLAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _DEV_IOB_H_
#define _DEV_IOB_H_

#include <kernel_config.h>
#include <hal_kernel_types.h>

/*****************************************************************************************
 *     Generic IOB (bridge to external IO peripherals) definition.
 *
 * The IOB device is used to access external peripherals. It implements an IO-MMU service 
 * for DMA transactions launched by DMA capable external peripherals.
 *
 * This IOB peripheral is acting as a dynamically configurable bridge, used for others 
 * I/O operations. Therefore, ALMOS-MKH does not use the IOB device waiting queue,
 * and calls directly the IOB driver blocking functions, using the device lock to 
 * get exclusive access to the IOB bridge internal state.
 ****************************************************************************************/
 
/****  Forward declarations  ****/

struct chdev_s;

/*****************************************************************************************
 * This defines the specific extension for the IOB chdev descriptor.
 ****************************************************************************************/

typedef void     (iob_set_active_t) ( xptr_t iob_xp, uint32_t value );
typedef void     (iob_set_ptpr_t)   ( xptr_t iob_xp, uint32_t value );
typedef void     (iob_inval_page_t) ( xptr_t iob_xp, vpn_t vpn );
typedef uint32_t (iob_get_bvar_t)   ( xptr_t iob_xp );
typedef uint32_t (iob_get_srcid_t)  ( xptr_t iob_xp );
typedef uint32_t (iob_get_error_t)  ( xptr_t iob_xp );

typedef struct iob_extend_s
{
    iob_set_active_t  * set_active;
    iob_set_ptpr_t    * set_ptpr;
    iob_inval_page_t  * inval_page;
    iob_get_bvar_t    * get_bvar;
    iob_get_srcid_t   * get_srcid;
    iob_get_error_t   * get_error;
}
iob_extend_t;

/*****************************************************************************************
 * This enum defines the various implementations of the IOB generic device.
 * This array must be kept consistent with the define in arch_info.h file
 ****************************************************************************************/

enum iob_impl_e
{
    IMPL_IOB_TSR =   0,         /* vci_iob component used in TSAR                       */
    IMPL_IOB_I86 =   1,         /* TBD                                                  */
}
iob_impl_t;

/*****************************************************************************************
 * This function initializes the IOB device descriptor with IOMMU disabled.
 *****************************************************************************************
 * @ chdev      :  pointer on IOB chdev descriptor.
 ****************************************************************************************/
void dev_iob_init( struct chdev_s * chdev );

/*****************************************************************************************
 * This function activates the IOMMU for the IOB device identified by its
 * extended pointer.
 *****************************************************************************************
 * @ dev_xp   : extended pointer on IOB device descriptor.
 ****************************************************************************************/
void dev_iob_iommu_enable( xptr_t dev_xp );

/*****************************************************************************************
 * This function desactivates the IO-MMU for the IOB device identified by its
 * extended pointer.
 *****************************************************************************************
 * @ dev_xp   : extended pointer on IOB device descriptor.
 ****************************************************************************************/
void dev_iob_iommu_disable( xptr_t dev_xp );

/*****************************************************************************************
 * This function set a new value in the IO-MMU PTPR register.
 *****************************************************************************************
 * @ dev_xp   : extended pointer on IOB device descriptor.
 * @ wdata    : value to be written in PTPR register.
 ****************************************************************************************/
void dev_iob_set_ptpr( xptr_t    dev_xp,
                       uint32_t  wdata );
                        
/*****************************************************************************************
 * This function invalidates an IOMMU TLB entry identified by its vpn.
 *****************************************************************************************
 * @ dev_xp   : extended pointer on IOB device descriptor.
 * @ vpn      : virtual page number in IO virtual space.
 ****************************************************************************************/
void dev_iob_inval_page( xptr_t  dev_xp,
                         vpn_t   vpn );

/*****************************************************************************************
 * This function return informations relative to an error reported by the IOMMU.
 *****************************************************************************************
 * @ dev_xp   : extended pointer on IOB device descriptor.
 * @ error    : [out] pointer on buffer for erro type.
 * @ bvar     : [out] pointer on buffer for bad virtual address.
 * @ srcid    : [out] pointer on buffer for faulty peripheral index.
 ****************************************************************************************/
void dev_iob_get_status( xptr_t     dev_xp,
                         uint32_t * error,
                         uint32_t * bvar,
                         uint32_t * srcid );

#endif  /* _DEV_IOB_H_ */
