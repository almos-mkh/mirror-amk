/*
 * sys_fg.c - Kernel function implementing the "fg" system call.
 * 
 * Author    Alain Greiner (2016,2017)
 *
 * Copyright (c)  UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <kernel_config.h>
#include <hal_kernel_types.h>
#include <hal_irqmask.h>
#include <errno.h>
#include <thread.h>
#include <printk.h>
#include <process.h>
#include <shared_syscalls.h>
#include <cluster.h>
#include <rpc.h>

#include <syscalls.h>

///////////////////////
int sys_fg( pid_t pid )
{
    xptr_t      process_xp;     // extended pointer on reference process descriptor
    process_t * process_ptr;
    cxy_t       process_cxy;
    xptr_t      file_xp;        // extended pointer on STDIN file descriptor
    xptr_t      chdev_xp;       // extended pointer on TXT0_RX chdev
    chdev_t   * chdev_ptr;
    cxy_t       chdev_cxy;

    thread_t  * this    = CURRENT_THREAD;

#if DEBUG_SYS_FG
uint64_t    tm_start;
uint64_t    tm_end;
tm_start = hal_get_cycles();
if( DEBUG_SYS_FG < tm_start )
printk("\n[DBG] %s : thread %x enter / process %x / cycle %d\n",
__FUNCTION__ , CURRENT_THREAD , pid, (uint32_t)tm_start );
#endif

    // check process existence
    process_xp = cluster_get_reference_process_from_pid( pid );

    if( process_xp == XPTR_NULL )
    {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : process %x not found\n", __FUNCTION__ , pid );
#endif
        this->errno = EINVAL;
        return -1;
    }
    
    // get reference process cluster and local pointer
    process_ptr = (process_t *)GET_PTR( process_xp );
    process_cxy = GET_CXY( process_xp );

    // get extended pointer on the reference process STDIN file descriptor
    file_xp = hal_remote_l64( XPTR( process_cxy , &process_ptr->fd_array.array[0] ) );
 
    // get extended pointer on TXT_RX chdev
    chdev_xp = chdev_from_file( file_xp );

    // get chdev cluster and local pointer
    chdev_ptr = GET_PTR( chdev_xp );
    chdev_cxy = GET_CXY( chdev_xp );

    // set reference process owner in TXT_RX chdev
    hal_remote_s64( XPTR( chdev_cxy , &chdev_ptr->ext.txt.owner_xp ) , process_xp );

    // reset PROCESS_TERM_WAIT and PROCESS_TERM_STOP flags in process term_state
    hal_remote_atomic_and( XPTR( process_cxy , &process_ptr->term_state ),
                                 ~(PROCESS_TERM_WAIT | PROCESS_TERM_STOP) ); 
 
    hal_fence();

#if DEBUG_SYS_FG
tm_end = hal_get_cycles();
if( DEBUG_SYS_FG < tm_end )
printk("\n[DBG] %s : thread %x exit / process %x get TXT_%d ownership / cycle %d\n",
__FUNCTION__ , CURRENT_THREAD , pid, 
hal_remote_l32( XPTR( chdev_cxy , &chdev_ptr->channel ) ) , (uint32_t)tm_end );
#endif

	return 0;

}  // end sys_fg()

