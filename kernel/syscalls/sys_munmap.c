/*
 * sys_munmap.c - unmap a mapping from process virtual address space
 * 
 * Authors       Ghassan Almaless (2008,2009,2010,2011,2012)
 *               Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_uspace.h>
#include <hal_irqmask.h>
#include <shared_syscalls.h>
#include <errno.h>
#include <thread.h>
#include <printk.h>
#include <mapper.h>
#include <vfs.h>
#include <process.h>
#include <vmm.h>

#include <syscalls.h>

////////////////////////////////
int sys_munmap( void     * vaddr,
                uint32_t   size )
{
    error_t       error;
    vseg_t      * vseg;
    reg_t         save_sr;      // required to enable IRQs

	thread_t    * this    = CURRENT_THREAD;
	process_t   * process = this->process;

#if (DEBUG_SYS_MUNMAP || CONFIG_INSTRUMENTATION_SYSCALLS)
uint64_t     tm_start = hal_get_cycles();
#endif

#if DEBUG_SYS_MUNMAP
if( DEBUG_SYS_MUNMAP < tm_start )
printk("\n[DBG] %s : thread %x enter / process %x / cycle %d\n",
__FUNCTION__ , this, process->pid, (uint32_t)tm_start );
#endif

    // check user buffer is mapped 
    error = vmm_get_vseg( process , (intptr_t)vaddr, &vseg );

    if( error )
    {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : thread[%x,%x] / user buffer unmapped %x\n",
__FUNCTION__ , process->pid, this->trdid, (intptr_t)vaddr );
hal_vmm_display( process , false );
#endif
		this->errno = EINVAL;
		return -1;
    }

    // enable IRQs
    hal_enable_irq( &save_sr );

    // call relevant kernel function
    error = vmm_resize_vseg( process , (intptr_t)vaddr , (intptr_t)size );

    if ( error )
    {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : cannot remove mapping\n", __FUNCTION__ );
#endif
		this->errno = EINVAL;
		return -1;
    }

    // restore IRQs
    hal_restore_irq( save_sr );

#if (DEBUG_SYS_MUNMAP || CONFIG_INSTRUMENTATION_SYSCALLS)
uint64_t     tm_end = hal_get_cycles();
#endif

#if CONFIG_INSTRUMENTATION_SYSCALLS
hal_atomic_add( &syscalls_cumul_cost[SYS_MUNMAP] , tm_end - tm_start );
hal_atomic_add( &syscalls_occurences[SYS_MUNMAP] , 1 );
#endif

#if DEBUG_SYS_MUNMAP
if( DEBUG_SYS_MUNMAP < tm_start )
printk("\n[DBG] %s : thread %x exit / process %x / cycle %d\n",
__FUNCTION__ , this, process->pid, (uint32_t)tm_end );
#endif

    return 0;

}  // end sys_munmap()

