/*
 * sys_undefined.c - function executed for an undefined syscall.
 * 
 * Author    Alain Greiner (2016,2017)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <errno.h>
#include <thread.h>
#include <process.h>
#include <printk.h>

///////////////////
int sys_undefined( void )
{
    thread_t  * this    = CURRENT_THREAD;
    process_t * process = this->process;

    printk("\n[ERROR] in %s : undefined syscall calle by thread %x in process %x\n",
           __FUNCTION__ , this->trdid , process->pid );
    this->errno = EINVAL;
    return -1;
}
