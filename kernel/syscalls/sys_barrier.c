/*
 * sys_barrier.c - Access a POSIX barrier.
 * 
 * authors       Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_special.h>
#include <hal_uspace.h>
#include <errno.h>
#include <thread.h>
#include <printk.h>
#include <vmm.h>
#include <syscalls.h>
#include <remote_barrier.h>

#if DEBUG_SYS_BARRIER
//////////////////////////////////////////////////////
static char * sys_barrier_op_str( uint32_t operation )
{
  	if     ( operation == BARRIER_INIT    ) return "INIT";
  	else if( operation == BARRIER_DESTROY ) return "DESTROY";
  	else if( operation == BARRIER_WAIT    ) return "WAIT";
  	else                                    return "undefined";
}
#endif

//////////////////////////////////
int sys_barrier( intptr_t   vaddr,
                 uint32_t   operation,
                 uint32_t   count,
                 intptr_t   attr )   
{
	error_t                 error;
    vseg_t                * vseg;
    pthread_barrierattr_t   k_attr;

    thread_t  * this    = CURRENT_THREAD;
    process_t * process = this->process;

#if DEBUG_SYS_BARRIER
uint64_t   tm_start;
uint64_t   tm_end;
tm_start = hal_get_cycles();
if( DEBUG_SYS_BARRIER < tm_start )
printk("\n[%s] thread[%x,%x] enters for %s / count %d / cycle %d\n",
__FUNCTION__, process->pid, this->trdid, sys_barrier_op_str(operation), count,
(uint32_t)tm_start );
#endif

    // check vaddr in user vspace
	error = vmm_get_vseg( process , vaddr , &vseg );
	if( error )
    {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : unmapped barrier %x / thread %x / process %x\n",
__FUNCTION__ , vaddr , this->trdid , process->pid );
hal_vmm_display( process , false );
#endif
        this->errno = error;
        return -1;
    }

    // execute requested operation
	switch( operation )
	{
        //////////////////
	    case BARRIER_INIT:
        {
            if( attr != 0 )   // QDT barrier required
            {
                error = vmm_get_vseg( process , attr , &vseg );
                if( error )
                {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : unmapped barrier attributes %x / thread %x / process %x\n",
__FUNCTION__ , attr , this->trdid , process->pid );
hal_vmm_display( process , false );
#endif
                    this->errno = EINVAL;
                    return -1;
                }
  
                // copy barrier attributes into kernel space
                hal_copy_from_uspace( &k_attr , (void*)attr , sizeof(pthread_barrierattr_t) );

                if ( count != k_attr.x_size * k_attr.y_size *k_attr.nthreads )  
                {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : wrong arguments / count %d / x_size %d / y_size %d / nthreads %x\n",
__FUNCTION__, count, k_attr.x_size, k_attr.y_size, k_attr.nthreads );
#endif
                    this->errno = EINVAL;
                    return -1;
                }
  

                // call relevant system function
                error = generic_barrier_create( vaddr , count , &k_attr );
            }
            else               // simple barrier required
            {
                error = generic_barrier_create( vaddr , count , NULL );
            }

		    if( error )
            {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : cannot create barrier %x / thread %x / process %x\n",
__FUNCTION__ , vaddr , this->trdid , process->pid );
#endif
                this->errno = ENOMEM;
                return -1;
            }
			break;
        }
        //////////////////
	    case BARRIER_WAIT:
        {
            xptr_t barrier_xp = generic_barrier_from_ident( vaddr );

            if( barrier_xp == XPTR_NULL )     // user error
            {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : barrier %x not registered / thread %x / process %x\n",
__FUNCTION__ , (intptr_t)vaddr , this->trdid , process->pid );
#endif
                this->errno = EINVAL;
                return -1;
            }
            else                          // success
            {
                generic_barrier_wait( barrier_xp ); 
            }
            break;
        }
        /////////////////////
	    case BARRIER_DESTROY:
        {
            xptr_t barrier_xp = generic_barrier_from_ident( vaddr );

            if( barrier_xp == XPTR_NULL )     // user error
            {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : barrier %x not registered / thread %x / process %x\n",
__FUNCTION__ , (intptr_t)vaddr , this->trdid , process->pid );
#endif
                this->errno = EINVAL;
                return -1;
            }
            else                          // success
            {
                generic_barrier_destroy( barrier_xp ); 
            }
            break;
        }
        ////////
        default: {
            assert ( false, "illegal operation type <%x>", operation );
        }
	}  // end switch

#if DEBUG_SYS_BARRIER
tm_end = hal_get_cycles();
if( DEBUG_SYS_BARRIER < tm_end )
printk("\n[%s] thread[%x,%x] exit for %s / cost %d / cycle %d\n",
__FUNCTION__, process->pid, this->trdid, sys_barrier_op_str(operation), 
(uint32_t)(tm_end - tm_start), (uint32_t)tm_end );
#endif

	return 0;

}  // end sys_barrier()

