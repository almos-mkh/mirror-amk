/*
 * sys_read.c - read bytes from a file
 * 
 * Author     Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <kernel_config.h>
#include <hal_kernel_types.h>
#include <hal_uspace.h>
#include <hal_irqmask.h>
#include <hal_special.h>
#include <errno.h>
#include <vfs.h>
#include <vmm.h>
#include <thread.h>
#include <printk.h>
#include <process.h>


extern uint32_t enter_sys_read;
extern uint32_t enter_devfs_read;
extern uint32_t enter_txt_read;
extern uint32_t enter_chdev_cmd_read;
extern uint32_t enter_chdev_server_read;
extern uint32_t enter_tty_cmd_read;
extern uint32_t enter_tty_isr_read;
extern uint32_t exit_tty_isr_read;
extern uint32_t exit_tty_cmd_read;
extern uint32_t exit_chdev_server_read;
extern uint32_t exit_chdev_cmd_read;
extern uint32_t exit_txt_read;
extern uint32_t exit_devfs_read;
extern uint32_t exit_sys_read;


/////////////////////////////////
int sys_read( uint32_t   file_id,
              void     * vaddr,
              uint32_t   count )
{
    error_t       error;
    vseg_t      * vseg;            // required for user space checking
	xptr_t        file_xp;         // remote file extended pointer
    vfs_file_t  * file_ptr;        // remote file local pointer
    cxy_t         file_cxy;        // remote file cluster identifier
    uint32_t      file_type;       // file type
    uint32_t      file_offset;     // current file offset
    uint32_t      file_attr;       // file_attribute
    vfs_inode_t * inode_ptr;       // local pointer on associated inode
    uint32_t      nbytes;          // number of bytes actually read
    reg_t         save_sr;         // required to enable IRQs during syscall

	thread_t    * this             = CURRENT_THREAD;
	process_t   * process          = this->process;
    xptr_t        process_owner_xp = process->owner_xp;
 
#if (DEBUG_SYS_READ || CONFIG_INSTRUMENTATION_SYSCALLS)
uint64_t     tm_start = hal_get_cycles();
#endif

#if DEBUG_SYS_READ
if( DEBUG_SYS_READ < tm_start )
printk("\n[%s] thread[%x,%x] enter / vaddr %x / count %d / cycle %d\n",
__FUNCTION__, process->pid, this->trdid, vaddr, count, (uint32_t)tm_start );
#endif

#if (DEBUG_SYS_READ & 1)
enter_sys_read = (uint32_t)tm_start;
#endif

    // check file_id argument
	if( file_id >= CONFIG_PROCESS_FILE_MAX_NR )
	{

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : thread[%x,%x] illegal file descriptor index %d\n",
__FUNCTION__ , process->pid, this->trdid, file_id );
#endif
		this->errno = EBADFD;
		return -1;
	}

    // check user buffer in user space
    error = vmm_get_vseg( process , (intptr_t)vaddr , &vseg );

    if ( error )
    {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : thread[%x,%x] user buffer unmapped %x\n",
__FUNCTION__ , process->pid, this->trdid, (intptr_t)vaddr );
hal_vmm_display( process , false );
#endif
		this->errno = EINVAL;
		return -1;
    }

    // get extended pointer on remote file descriptor
    file_xp = process_fd_get_xptr( process , file_id );

    if( file_xp == XPTR_NULL )
    {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : thread[%x,%x] undefined fd_id %d\n",
__FUNCTION__, process->pid, this->trdid, file_id );
#endif
        this->errno = EBADFD;
        return -1;
    }

    // get file descriptor cluster and local pointer
    file_ptr = GET_PTR( file_xp );
    file_cxy = GET_CXY( file_xp );

    // get file type, offset, attributes and associated inode
    file_type   = hal_remote_l32( XPTR( file_cxy , &file_ptr->type ) );
    file_offset = hal_remote_l32( XPTR( file_cxy , &file_ptr->offset ) );
    inode_ptr   = hal_remote_lpt( XPTR( file_cxy , &file_ptr->inode ) );
    file_attr   = hal_remote_l32( XPTR( file_cxy , &file_ptr->attr ) );

    // enable IRQs
    hal_enable_irq( &save_sr );

    // action depend on file type
    if( file_type == INODE_TYPE_FILE )      // read from file mapper 
    {
        // check file readable
        if( (file_attr & FD_ATTR_READ_ENABLE) == 0 )
	    {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : thread[%x,%x] file %d not readable\n",
__FUNCTION__, process->pid, this->trdid, file_id );
#endif
            hal_restore_irq( save_sr );
		    this->errno = EBADFD;
		    return -1;
	    }

        // move count bytes from mapper
        nbytes = vfs_user_move( true,               // from mapper to buffer
                                file_xp,
                                vaddr, 
                                count );
        if( nbytes != count )
        {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : thread[%x,‰x] cannot read %d bytes from file %d\n",
__FUNCTION__, process->pid, this->trdid, count, file_id );
#endif
            this->errno = EIO;
            hal_restore_irq( save_sr );
            return -1;
        }
    }
    else if( file_type == INODE_TYPE_DEV )  // read from TXT device
    {
        // get cluster and pointers on TXT_RX chdev 
        xptr_t    chdev_xp  = chdev_from_file( file_xp );
        cxy_t     chdev_cxy = GET_CXY( chdev_xp );
        chdev_t * chdev_ptr = GET_PTR( chdev_xp );

        volatile xptr_t    txt_owner_xp;    
        uint32_t           iter = 0;

        while( 1 )
        {
            // extended pointer on TXT owner process
            txt_owner_xp  = hal_remote_l64( XPTR( chdev_cxy , &chdev_ptr->ext.txt.owner_xp ) );

            // check TXT_RX ownership
            if ( process_owner_xp != txt_owner_xp )
            {
                if( (iter & 0xFFF) == 0 )
                printk("\n[WARNING] in %s : thread[%x,%x] wait TXT_RX / cycle %d\n",
                __FUNCTION__, process->pid, this->trdid, (uint32_t)hal_get_cycles() );

                // deschedule without blocking
                sched_yield( "wait TXT_RX ownership" );

                iter++;
            }
            else
            {
                break;
            }
        }

        // move count bytes from device
        nbytes = devfs_user_move( true,             // from device to buffer
                                  file_xp,
                                  vaddr,
                                  count );
        if( nbytes != count )
        {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : thread[%x,‰x] cannot read data from file %d\n",
__FUNCTION__, process->pid, this->trdid, file_id );
#endif
            this->errno = EIO;
            hal_restore_irq( save_sr );
            return -1;
        }
    }
    else    // not FILE and not DEV
    {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : thread[%x,%x] / illegal inode type %\n",
__FUNCTION__, vfs_inode_type_str( file_type ) );
#endif
		this->errno = EBADFD;
        hal_restore_irq( save_sr );
		return -1;
    }

    // restore IRQs
    hal_restore_irq( save_sr );

    hal_fence();

#if (DEBUG_SYS_READ || CONFIG_INSTRUMENTATION_SYSCALLS)
uint64_t     tm_end = hal_get_cycles();
#endif

#if DEBUG_SYS_READ
if( DEBUG_SYS_READ < tm_end )
printk("\n[%s] thread[%x,%x] exit / cycle %d\n",
__FUNCTION__ , process->pid, this->trdid, (uint32_t)tm_end );
#endif

#if CONFIG_INSTRUMENTATION_SYSCALLS
hal_atomic_add( &syscalls_cumul_cost[SYS_READ] , tm_end - tm_start );
hal_atomic_add( &syscalls_occurences[SYS_READ] , 1 );
#endif

#if (DEBUG_SYS_READ & 1) 
exit_sys_read = (uint32_t)tm_end;

printk("\n***** timing to read one character *****\n"
" - enter_sys_read          = %d / delta %d\n"
" - enter_devfs_read        = %d / delta %d\n"
" - enter_txt_read          = %d / delta %d\n"
" - enter_chdev_cmd_read    = %d / delta %d\n"
" - enter_chdev_server_read = %d / delta %d\n"
" - enter_tty_cmd_read      = %d / delta %d\n"
" - enter_tty_isr_read      = %d / delta %d\n"
" - exit_tty_isr_read       = %d / delta %d\n"
" - exit_tty_cmd_read       = %d / delta %d\n"
" - exit_chdev_server_read  = %d / delta %d\n"
" - exit_chdev_cmd_read     = %d / delta %d\n"
" - exit_txt_read           = %d / delta %d\n"
" - exit_devfs_read         = %d / delta %d\n"
" - exit_sys_read           = %d / delta %d\n",
enter_sys_read          , 0 ,
enter_devfs_read        , enter_devfs_read        - enter_sys_read          ,
enter_txt_read          , enter_txt_read          - enter_devfs_read        ,
enter_chdev_cmd_read    , enter_chdev_cmd_read    - enter_txt_read          ,
enter_chdev_server_read , enter_chdev_server_read - enter_chdev_cmd_read    ,
enter_tty_cmd_read      , enter_tty_cmd_read      - enter_chdev_server_read ,
enter_tty_isr_read      , enter_tty_isr_read      - enter_tty_cmd_read      ,
exit_tty_isr_read       , exit_tty_isr_read       - enter_tty_isr_read      ,
exit_tty_cmd_read       , exit_tty_cmd_read       - exit_tty_isr_read       ,
exit_chdev_server_read  , exit_chdev_server_read  - exit_tty_cmd_read       ,
exit_chdev_cmd_read     , exit_chdev_cmd_read     - exit_chdev_server_read  ,
exit_txt_read           , exit_txt_read           - exit_chdev_cmd_read     ,
exit_devfs_read         , exit_devfs_read         - exit_txt_read           ,
exit_sys_read           , exit_sys_read           - exit_devfs_read         );
#endif
 
	return nbytes;

}  // end sys_read()
