/*
 * sys_get_core.c - get calling core cluster and local index.
 * 
 * Author    Alain Greiner (2016,2017,2018,2019)
 *  
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_uspace.h>
#include <hal_special.h>
#include <errno.h>
#include <core.h>
#include <thread.h>
#include <process.h>
#include <vmm.h>
#include <printk.h>

#include <syscalls.h>

///////////////////////////////////
int sys_place_fork( uint32_t  cxy )
{
    thread_t  * this    = CURRENT_THREAD;
    process_t * process = this->process;

    // check cxy argument
    if( cluster_is_undefined( cxy ) )
    {
        
#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : thread[%x,‰x] / illegal cxy argument %x\n",
__FUNCTION__ , process->pid , this->trdid , cxy );
#endif
        this->errno = EFAULT;
		return -1;
	}

    // set relevant arguments in calling thread descriptor
    this->fork_user = true;
    this->fork_cxy  = cxy;

	return 0; 

}  // end sys_place_fork()
