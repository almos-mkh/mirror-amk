/*
 * sys_thread_sleep.c - put the calling thread in sleep state
 * 
 * Author    Alain Greiner (2016,2017)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_special.h>
#include <scheduler.h>
#include <thread.h>
#include <printk.h>
#include <syscalls.h>

//////////////////////
int sys_thread_sleep( void )
{

    thread_t * this = CURRENT_THREAD;

#if DEBUG_SYS_THREAD_SLEEP
uint64_t     tm_start;
uint64_t     tm_end;
tm_start = hal_get_cycles();
if( DEBUG_SYS_THREAD_SLEEP < tm_start )
printk("\n[DBG] %s : thread %x n process %x blocked / cycle %d\n",
__FUNCTION__ , this->trdid, this->process->pid , (uint32_t)tm_start );
#endif

    thread_block( XPTR( local_cxy , this ) , THREAD_BLOCKED_GLOBAL );
    sched_yield("blocked on sleep");

#if DEBUG_SYS_THREAD_SLEEP
tm_end = hal_get_cycles();
if( DEBUG_SYS_THREAD_SLEEP < tm_end )
printk("\n[DBG] %s : thread %x in process %x resume / cycle %d\n",
__FUNCTION__ , this->trdid, this->process->pid , (uint32_t)tm_end );
#endif

	return 0;

}  // end sys_thread_sleep()
