/*
 * sys_get_core.c - get calling core cluster and local index.
 * 
 * Author    Alain Greiner (2016,2017)
 *  
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_uspace.h>
#include <hal_special.h>
#include <errno.h>
#include <core.h>
#include <thread.h>
#include <process.h>
#include <vmm.h>
#include <printk.h>

#include <syscalls.h>

/////////////////////////////////
int sys_get_core( uint32_t * cxy,
                  uint32_t * lid )
{
	error_t   error;
    vseg_t  * vseg;
    uint32_t  k_cxy;
    uint32_t  k_lid;

    thread_t  * this    = CURRENT_THREAD;
    process_t * process = this->process;

    // check cxy buffer in user space
    error = vmm_get_vseg( process , (intptr_t)cxy , &vseg );

	if( error )
	{

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : cxy buffer unmapped %x / thread %x / process %x\n",
__FUNCTION__ , (intptr_t)cxy , this->trdid , process->pid );
hal_vmm_display( process , false );
#endif
        this->errno = EFAULT;
		return -1;
	}

    // check lid buffer in user space
    error = vmm_get_vseg( process , (intptr_t)lid , &vseg );

	if( error )
	{

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : lid buffer unmapped %x / thread %x / process %x\n",
__FUNCTION__ , (intptr_t)lid , this->trdid , process->pid );
hal_vmm_display( process , false );
#endif
        this->errno = EFAULT;
		return -1;
	}

    // get parameters
	k_cxy = local_cxy;
	k_lid = this->core->lid;

    // copy to user space
	hal_copy_to_uspace( cxy , &k_cxy , sizeof(uint32_t) );
	hal_copy_to_uspace( lid , &k_lid , sizeof(uint32_t) );

	return 0; 

}  // end sys_get_core()
