/*
 * sys_open.c - open a file.
 *
 * Author        Alain Greiner (2016,2017)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <kernel_config.h>
#include <hal_kernel_types.h>
#include <hal_uspace.h>
#include <errno.h>
#include <thread.h>
#include <printk.h>
#include <vfs.h>
#include <process.h>
#include <remote_rwlock.h>

#include <syscalls.h>

///////////////////////////////////
int sys_open ( char     * pathname,
               uint32_t   flags,
               uint32_t   mode )
{
    error_t        error;
    xptr_t         file_xp;                 // extended pointer on vfs_file_t
    uint32_t       file_id;                 // file descriptor index
    xptr_t         root_inode_xp;           // extended pointer on path root inode

    char           kbuf[CONFIG_VFS_MAX_PATH_LENGTH];

    thread_t     * this     = CURRENT_THREAD;
    process_t    * process  = this->process;

#if (DEBUG_SYS_OPEN || CONFIG_INSTRUMENTATION_SYSCALLS)
uint64_t     tm_start = hal_get_cycles();
#endif

    // check fd_array not full
    if( process_fd_array_full() )
    {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : file descriptor array full for process %x\n",
__FUNCTION__ , process->pid );
#endif
        this->errno = ENFILE;
        return -1;
    }

    // check pathname length
    if( hal_strlen_from_uspace( pathname ) >= CONFIG_VFS_MAX_PATH_LENGTH )
    {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : pathname too long\n", __FUNCTION__ );
#endif
        this->errno = ENFILE;
        return -1;
    }

    // copy pathname in kernel space
    hal_strcpy_from_uspace( kbuf , pathname , CONFIG_VFS_MAX_PATH_LENGTH );

#if DEBUG_SYS_OPEN
if( DEBUG_SYS_OPEN < tm_start )
printk("\n[%s] thread[%x,%x] enter for <%s> / flags %x / cycle %d\n",
__FUNCTION__, process->pid, this->trdid, kbuf, flags, (uint32_t)tm_start );
#endif
 
    // get cluster and local pointer on reference process
    xptr_t      ref_xp  = process->ref_xp;
    process_t * ref_ptr = (process_t *)GET_PTR( ref_xp );
    cxy_t       ref_cxy = GET_CXY( ref_xp );

    // compute root inode for path 
    if( kbuf[0] == '/' )                        // absolute path
    {
        // use extended pointer on VFS root inode
        root_inode_xp = process->vfs_root_xp;
    }
    else                                        // relative path
    {
        // use extended pointer on CWD inode
        root_inode_xp = hal_remote_l64( XPTR( ref_cxy , &ref_ptr->cwd_xp ) );
    }

    // call the relevant VFS function
    error = vfs_open( root_inode_xp,
                      kbuf,
                      ref_xp,
                      flags,
                      mode,
                      &file_xp,
                      &file_id );

    if( error )
    {
        printk("\n[ERROR] in %s : cannot create file descriptor for %s\n",
        __FUNCTION__ , kbuf );
        this->errno = ENFILE;
        return -1;
    }

    hal_fence();

#if (DEBUG_SYS_OPEN || CONFIG_INSTRUMENTATION_SYSCALLS)
uint64_t     tm_end = hal_get_cycles();
#endif

#if DEBUG_SYS_OPEN
if( DEBUG_SYS_OPEN < tm_end )
printk("\n[%s] thread[%x,%x] exit for <%s> / cycle %d\n",
__FUNCTION__, process->pid, this->trdid, kbuf, (uint32_t)tm_end );
#endif
 
#if CONFIG_INSTRUMENTATION_SYSCALLS
hal_atomic_add( &syscalls_cumul_cost[SYS_OPEN] , tm_end - tm_start );
hal_atomic_add( &syscalls_occurences[SYS_OPEN] , 1 );
#endif

    return file_id;
}
