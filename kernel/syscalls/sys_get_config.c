/*
 * sys_get_config.c - get hardware platform parameters.
 * 
 * Author    Alain Greiner (2016,2017,2018)
 *  
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_uspace.h>
#include <hal_special.h>
#include <errno.h>
#include <core.h>
#include <thread.h>
#include <process.h>
#include <vmm.h>
#include <printk.h>

#include <syscalls.h>

//////////////////////////////////////
int sys_get_config( uint32_t * x_size,
                    uint32_t * y_size,
                    uint32_t * ncores )
{
	error_t    error;
    vseg_t   * vseg;
    uint32_t   k_x_size;
    uint32_t   k_y_size;
    uint32_t   k_ncores;

    thread_t  * this    = CURRENT_THREAD;
    process_t * process = this->process;

#if DEBUG_SYS_GET_CONFIG
uint64_t     tm_start;
uint64_t     tm_end;
tm_start = hal_get_cycles();
if( DEBUG_SYS_GET_CONFIG < tm_start )
printk("\n[DBG] %s : thread %x enter / process %x / cycle %d\n",
__FUNCTION__, this, process->pid, (uint32_t)tm_start );
#endif

    // check x_size buffer in user space
    error = vmm_get_vseg( process , (intptr_t)x_size  , &vseg );

	if( error )
	{

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : x_size buffer unmapped / thread %x / process %x\n",
__FUNCTION__ , (intptr_t)x_size , this->trdid , process->pid );
hal_vmm_display( process , false );
#endif
        this->errno = EINVAL;
		return -1;
	}

    // check y_size buffer in user space
    error = vmm_get_vseg( process , (intptr_t)y_size  , &vseg );

	if( error )
	{

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : y_size buffer unmapped / thread %x / process %x\n",
__FUNCTION__ , (intptr_t)y_size , this->trdid , process->pid );
hal_vmm_display( process , false );
#endif
        this->errno = EINVAL;
		return -1;
	}

    // check ncores buffer in user space
    error = vmm_get_vseg( process , (intptr_t)ncores  , &vseg );

	if( error )
	{

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : ncores buffer unmapped / thread %x / process %x\n",
__FUNCTION__ , (intptr_t)ncores , this->trdid , process->pid );
hal_vmm_display( process , false );
#endif
        this->errno = EINVAL;
		return -1;
	}

    // get parameters
	k_x_size  = LOCAL_CLUSTER->x_size;
	k_y_size  = LOCAL_CLUSTER->y_size;
	k_ncores  = LOCAL_CLUSTER->cores_nr;

    // copy to user space
	hal_copy_to_uspace( x_size  , &k_x_size  , sizeof(uint32_t) );
	hal_copy_to_uspace( y_size  , &k_y_size  , sizeof(uint32_t) );
	hal_copy_to_uspace( ncores  , &k_ncores  , sizeof(uint32_t) );

    hal_fence();

#if DEBUG_SYS_GET_CONFIG
tm_end = hal_get_cycles();
if( DEBUG_SYS_GET_CONFIG < tm_end )
printk("\n[DBG] %s : thread %x exit / process %x / cost %d / cycle %d\n",
__FUNCTION__, this, process->pid, (uint32_t)(tm_end-tm_start), (uint32_t)tm_end );
#endif

	return 0; 

}  // end sys_get_config()
