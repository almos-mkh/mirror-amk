/*
 * sys_alarm.c - timed sleep/wakeup
 * 
 * Author    Alain Greiner (2016,2017)
*
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <thread.h>
#include <printk.h>
#include <cluster.h>
#include <process.h>
#include <scheduler.h>
#include <core.h>

#include <syscalls.h>

////////////////////////////////
int sys_alarm( uint32_t cycles )
{
    thread_t  * this    = CURRENT_THREAD;
    process_t * process = this->process;

    printk("\n[ERROR] in %s for thread %x in process %x : not implemented yet\n",
    __FUNCTION__ , this->trdid , process->pid , cycles );
    return -1;
}



