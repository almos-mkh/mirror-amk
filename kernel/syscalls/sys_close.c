/*
 * sys_close.c  close an open file
 * 
 * Author    Alain Greiner (2016,2017)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <kernel_config.h>
#include <hal_kernel_types.h>
#include <hal_special.h>
#include <vfs.h>
#include <process.h>
#include <thread.h>
#include <printk.h>

#include <syscalls.h>

//////////////////////////////////
int sys_close ( uint32_t file_id )
{
    error_t     error;
    xptr_t      file_xp;

	thread_t  * this    = CURRENT_THREAD;
	process_t * process = this->process;

#if (DEBUG_SYS_CLOSE || CONFIG_INSTRUMENTATION_SYSCALLS)
uint64_t     tm_start = hal_get_cycles();
#endif

#if DEBUG_SYS_CLOSE
if( DEBUG_SYS_CLOSE < tm_start )
printk("\n[%s] thread[%x,%x] enter / fdid %d / cycle %d\n",
__FUNCTION__, process->pid, this->trdid, file_id, (uint32_t)tm_start );
#endif
 
    // check file_id argument
	if( file_id >= CONFIG_PROCESS_FILE_MAX_NR )
	{
        printk("\n[ERROR] in %s : illegal file descriptor index = %d\n",
               __FUNCTION__ , file_id );
		this->errno = EBADFD;
		return -1;
	}

    // get extended pointer on remote file descriptor
    file_xp = process_fd_get_xptr( process , file_id );

    if( file_xp == XPTR_NULL )
    {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : undefined file descriptor %d\n",
__FUNCTION__ , file_id );
#endif
        this->errno = EBADFD;
		return -1;
    }

    // call the relevant VFS function
	error = vfs_close( file_xp , file_id );

	if( error )
	{

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : cannot close file descriptor %d\n",
__FUNCTION__ , file_id );
#endif
		this->errno = error;
		return -1;
	}

	hal_fence();

#if (DEBUG_SYS_CLOSE || CONFIG_INSTRUMENTATION_SYSCALLS)
uint64_t     tm_end = hal_get_cycles();
#endif

#if DEBUG_SYS_CLOSE
tm_end = hal_get_cycles();
if( DEBUG_SYS_CLOSE < tm_start )
printk("\n[%s] thread[%x,%x] exit / cycle %d\n",
__FUNCTION__, process->pid, this->trdid, (uint32_t)tm_end );
#endif
 
#if CONFIG_INSTRUMENTATION_SYSCALLS
hal_atomic_add( &syscalls_cumul_cost[SYS_CLOSE] , tm_end - tm_start );
hal_atomic_add( &syscalls_occurences[SYS_CLOSE] , 1 );
#endif

	return 0;
}
