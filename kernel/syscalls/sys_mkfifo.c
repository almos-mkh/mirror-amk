/*
 * sys_mkfifo.c - creates a named FIFO file.
 *
 * Author    Alain Greiner (2016,2017)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_uspace.h>
#include <printk.h>
#include <vfs.h>
#include <process.h>
#include <thread.h>

////////////////////////////////////
int sys_mkfifo ( char    * pathname,
                 uint32_t  mode __attribute__((unused)) )
{
    error_t        error;
    char           kbuf[CONFIG_VFS_MAX_PATH_LENGTH];

    thread_t  * this    = CURRENT_THREAD;
    process_t * process = this->process;

    // check fd_array not full
    if( process_fd_array_full() )
    {
        printk("\n[ERROR] in %s : file descriptor array full for process %x\n",
               __FUNCTION__ , process->pid );
        this->errno = ENFILE;
        return -1;
    }

    // check pathname length
    if( hal_strlen_from_uspace( pathname ) >= CONFIG_VFS_MAX_PATH_LENGTH )
    {
        printk("\n[ERROR] in %s : pathname too long\n", __FUNCTION__ );
        this->errno = ENFILE;
        return -1;
    }

    // copy pathname in kernel space
    hal_strcpy_from_uspace( kbuf , pathname , CONFIG_VFS_MAX_PATH_LENGTH );

    printk("\n[ERROR] in %s : not implemented yet\n", __FUNCTION__ );
    return -1;

    if( error )
    {
        printk("\n[ERROR] in %s : cannot create named FIFO %s\n",
               __FUNCTION__ , kbuf );
        this->errno = error;
        return -1;
    }

    return 0;

} // end sys_mkfifo()
