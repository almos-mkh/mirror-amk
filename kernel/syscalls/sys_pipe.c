/*
 * sys_pipe.c - open a pipe communication channel
 * 
 * Author    Alain Greiner  (2016,1017)
 *
 * Copyright (c)  UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <vfs.h>
#include <process.h>
#include <thread.h>
#include <printk.h>

#include <syscalls.h>

//////////////////////////////////////
int sys_pipe ( uint32_t file_fd[2] )
{
    printk("\n[ERROR] in %d : not implemented yet\n", __FUNCTION__, file_fd );
    return ENOSYS;
}
