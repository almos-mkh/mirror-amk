/*
 * sys_opendir.c - Open an user accessible VFS directory.
 * 
 * Author        Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <kernel_config.h>
#include <hal_kernel_types.h>
#include <hal_uspace.h>
#include <thread.h>
#include <process.h>
#include <user_dir.h>
#include <printk.h>
#include <errno.h>
#include <vseg.h>
#include <vfs.h>
#include <syscalls.h>
#include <shared_syscalls.h>

///////////////////////////////////
int sys_opendir ( char *  pathname,
                  DIR  ** dirp )
{
    error_t        error;
    xptr_t         root_inode_xp;          // extended pointer on path root inode
    xptr_t         inode_xp;               // extended pointer on directory inode
    vfs_inode_t  * inode_ptr;              // local pointer on directory inode
    cxy_t          inode_cxy;              // directory inode cluster
    uint32_t       inode_type;             // to check directory inode type
    user_dir_t   * dir_ptr;                // local pointer on user_dir_t
    vseg_t       * vseg;                   // for user space checking
    intptr_t       ident;                  // dirent array pointer in user space                  
    char          kbuf[CONFIG_VFS_MAX_PATH_LENGTH];
	
	thread_t     * this    = CURRENT_THREAD;  // client thread
	process_t    * process = this->process;   // client process

#if (DEBUG_SYS_OPENDIR || CONFIG_INSTRUMENTATION_SYSCALLS)
uint64_t     tm_start = hal_get_cycles();
#endif

    // check DIR buffer in user space
    error = vmm_get_vseg( process , (intptr_t)dirp, &vseg );

	if( error )
	{

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s / thread[%x,%x] : DIR buffer %x unmapped\n",
__FUNCTION__ , process->pid , this->trdid, dirp );
hal_vmm_display( process , false );
#endif
		this->errno = EINVAL;
		return -1;
	}	

    // check pathname length
    if( hal_strlen_from_uspace( pathname ) >= CONFIG_VFS_MAX_PATH_LENGTH )
    {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s / thread[%x,%x] : pathname too long\n",
 __FUNCTION__ , process->pid , this->trdid );
#endif
        this->errno = ENFILE;
        return -1;
    }

    // copy pathname in kernel space
    hal_strcpy_from_uspace( kbuf , pathname , CONFIG_VFS_MAX_PATH_LENGTH );

#if DEBUG_SYS_OPENDIR
if( DEBUG_SYS_OPENDIR < tm_start )
printk("\n[%s] thread[%x,%x] enter for directory <%s> / cycle %d\n",
__FUNCTION__, process->pid, this->trdid, kbuf, (uint32_t)tm_start );
#endif

    // compute root inode for pathname 
    if( kbuf[0] == '/' )                        // absolute path
    {
        // use extended pointer on VFS root inode
        root_inode_xp = process->vfs_root_xp;
    }
    else                                        // relative path
    {
        // get cluster and local pointer on reference process
        xptr_t      ref_xp  = process->ref_xp;
        process_t * ref_ptr = (process_t *)GET_PTR( ref_xp );
        cxy_t       ref_cxy = GET_CXY( ref_xp );

        // use extended pointer on CWD inode
        root_inode_xp = hal_remote_l64( XPTR( ref_cxy , &ref_ptr->cwd_xp ) );
    }

    // get extended pointer on directory inode
    error = vfs_lookup( root_inode_xp,
                        kbuf,
                        0,
                        &inode_xp,
                        NULL );
    if( error )
	{

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s / thread[%x,%x] : cannot found directory <%s>\n",
__FUNCTION__ , process->pid , this->trdid , kbuf );
#endif
		this->errno = ENFILE;
		return -1;
	}
   
    // check inode type 
    inode_ptr  = GET_PTR( inode_xp );
    inode_cxy  = GET_CXY( inode_xp );
    inode_type = hal_remote_l32( XPTR( inode_cxy , &inode_ptr->type ) );

    if( inode_type != INODE_TYPE_DIR )
	{

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s / thread[%x,%x] : cannot found directory <%s>\n",
__FUNCTION__ , process->pid , this->trdid , kbuf );
#endif
		this->errno = ENFILE;
		return -1;
	}
   
    // create a new user_dir_t structure in target directory inode cluster 
    // map it in the reference user process VMM (in a new ANON vseg)
    // an get the local pointer on the created user_dir_t structure 
    if( inode_cxy == local_cxy )
    {
        dir_ptr = user_dir_create( inode_ptr,
                                   process->ref_xp );
    }
    else
    {
        rpc_user_dir_create_client( inode_cxy,
                                    inode_ptr,
                                    process->ref_xp,
                                    &dir_ptr );
    }

    if( dir_ptr == NULL )
	{

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s / thread[%x,%x] : cannot create user_dir for <%s>\n",
__FUNCTION__ , process->pid , this->trdid , kbuf );
#endif
		this->errno = ENFILE;
		return -1;
	}
   
    // get ident from user_dir structure
    ident = (intptr_t)hal_remote_lpt( XPTR( inode_cxy , &dir_ptr->ident ) );

    // set ident value in user buffer
    hal_copy_to_uspace( dirp , &ident , sizeof(intptr_t) );

    hal_fence();

#if (DEBUG_SYS_OPENDIR || CONFIG_INSTRUMENTATION_SYSCALLS)
uint64_t     tm_end = hal_get_cycles();
#endif

#if DEBUG_SYS_OPENDIR
if( DEBUG_SYS_OPENDIR < tm_end )
printk("\n[%s] thread[%x,%x] exit for directory <%s> / cycle %d\n",
__FUNCTION__, process->pid, this->trdid, kbuf, (uint32_t)tm_end );
#endif
 
#if CONFIG_INSTRUMENTATION_SYSCALLS
hal_atomic_add( &syscalls_cumul_cost[SYS_OPENDIR] , tm_end - tm_start );
hal_atomic_add( &syscalls_occurences[SYS_OPENDIR] , 1 );
#endif

	return 0;

}  // end sys_opendir()
