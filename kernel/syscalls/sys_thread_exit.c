/*
 * sys_thread_exit.c - terminates the execution of calling thread
 * 
 * Authors   Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_irqmask.h>
#include <thread.h>
#include <process.h>
#include <core.h>
#include <vmm.h>
#include <scheduler.h>
#include <printk.h>

#include <syscalls.h>

////////////////////////////////////////
int sys_thread_exit( void * exit_value )
{
	thread_t  * this      = CURRENT_THREAD;
    trdid_t     trdid     = this->trdid;
    process_t * process   = this->process;
    pid_t       pid       = process->pid;

    // check exit_value argument
    if( exit_value != NULL )
    {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : exit_value argument must be NULL / thread %x in process %x\n",
__FUNCTION__ , this , pid );
#endif
        this->errno = EINVAL;
        return -1;
    }


    // If calling thread is the main thread, the process must be deleted. 
    // => delete all process threads and synchronise with parent process.
    // If calling thread is not the main thread, it must be deleted.
    // => block the thread and mark it for delete.
    if( (CXY_FROM_PID( pid ) == local_cxy) && (LTID_FROM_TRDID(trdid) == 0) )
    {

#if DEBUG_SYS_THREAD_EXIT
uint64_t     tm_start = hal_get_cycles();
if( DEBUG_SYS_THREAD_EXIT < tm_start )
printk("\n[%s] thread[%x,%x] / main => delete process / cycle %d\n",
__FUNCTION__ , pid , trdid , (uint32_t)tm_start );
#endif
        // delete process
        sys_exit( 0 );
    }
    else
    {

#if DEBUG_SYS_THREAD_EXIT
uint64_t     tm_start = hal_get_cycles();
if( DEBUG_SYS_THREAD_EXIT < tm_start )
printk("\n[%s] thread[%x,%x] / not main => delete thread / cycle %d\n",
__FUNCTION__ , pid , trdid , (uint32_t)tm_start );
#endif
        // block calling thread and mark it for delete,
        thread_delete( XPTR( local_cxy , this ) , pid , false );

        // deschedule
        sched_yield( "suicide after thread_exit" );
    }

    return 0;   // never executed but required by compiler

}  // end sys_thread exit
