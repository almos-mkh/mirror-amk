/*
 * sys_timeofday.c - Get current time
 *
 * Author    Alain Greiner (2016,2017,2018,2019) 
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_uspace.h>
#include <hal_vmm.h>
#include <thread.h>
#include <printk.h>
#include <errno.h>
#include <process.h>
#include <vmm.h>
#include <core.h>
#include <shared_syscalls.h>

#include <syscalls.h>

////////////////////////////////////////
int sys_timeofday( struct timeval  * tv,
                   struct timezone * tz )
{
	error_t        error;
    vseg_t       * vseg;

	uint32_t       tm_s;
    uint32_t       tm_us;

	struct timeval k_tv;

	thread_t  *    this    = CURRENT_THREAD;
	process_t *    process = this->process;

    // check tz (non supported / must be null)
    if( tz )
    {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s for thread %x in process %x : tz argument must be NULL\n",
__FUNCTION__ , this->trdid , process->pid );
#endif
        this->errno = EINVAL;
        return -1;
    }
 
    // check tv
    error = vmm_get_vseg( process , (intptr_t)tv , &vseg );

    if( error )
    {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : user buffer tz unmapped / thread %x / process %x\n",
__FUNCTION__ , (intptr_t)tz , this->trdid , process->pid );
hal_vmm_display( process , false );
#endif
        this->errno = EINVAL;
        return -1;
    }

    // get time from calling core descriptor
    core_get_time( this->core , &tm_s , &tm_us );
	k_tv.tv_sec  = tm_s;
	k_tv.tv_usec = tm_us;

    // copy values to user space
	hal_copy_to_uspace( tv , &k_tv , sizeof(struct timeval) );

    hal_fence();

	return 0; 

}  // end sys_timeofday()
