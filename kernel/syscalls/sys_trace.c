/*
 * sys_trace.c - activate / desactivate the context switches trace for a given core
 * 
 * Author    Alain Greiner (c) (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_special.h>
#include <printk.h>
#include <thread.h>
#include <errno.h>
#include <shared_syscalls.h>
#include <syscalls.h>

///////////////////////////////
int sys_trace( bool_t   active,
               cxy_t    cxy,
               lid_t    lid )
{
    uint32_t    ncores;

    thread_t  * this    = CURRENT_THREAD;
    process_t * process = this->process;

#if DEBUG_SYS_TRACE
uint64_t    tm_start;
uint64_t    tm_end;
tm_start = hal_get_cycles();
if( DEBUG_SYS_TRACE < tm_start )
printk("\n[DBG] %s : thread %d enter / process %x / cycle = %d\n",
__FUNCTION__, this, this->process->pid, (uint32_t)tm_start );
#endif

    // check cluster identifier
    if( cluster_is_undefined( cxy ) )
    {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : illegal cxy argument %x / thread %x / process %x\n",
__FUNCTION__ , cxy , this->trdid , process->pid );
#endif
        this->errno = EINVAL;
        return -1;
    }

    // check core local index
    ncores = hal_remote_l32( XPTR( cxy , &LOCAL_CLUSTER->cores_nr ) );
    if( lid >= ncores )
    {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : illegal lid argument %x / thread %x / process %x\n",
__FUNCTION__ , lid , this->trdid , process->pid );
#endif
        this->errno = EINVAL;
        return -1;
    }

    // get local pointer on target core
    core_t * core = &LOCAL_CLUSTER->core_tbl[lid];

    // get extended pointer on target scheduler trace field
    xptr_t trace_xp = XPTR( cxy , &core->scheduler.trace );

    if ( active ) hal_remote_s32( trace_xp , 1 );
    else          hal_remote_s32( trace_xp , 0 );
    
    hal_fence();

#if DEBUG_SYS_TRACE
tm_end = hal_get_cycles();
if( DEBUG_SYS_TRACE < tm_end )
printk("\n[DBG] %s : thread %x exit / process %x / cost = %d / cycle %d\n",
__FUNCTION__, this, this->process->pid, (uint32_t)(tm_end - tm_start) , (uint32_t)tm_end );
#endif

    return 0;

}  // end sys_trace()
