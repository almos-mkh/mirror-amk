/*
 * sys_isatty.c - test if a file descriptor is a TXT device.
 * 
 * Author     Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <kernel_config.h>
#include <hal_kernel_types.h>
#include <hal_uspace.h>
#include <hal_irqmask.h>
#include <hal_special.h>
#include <errno.h>
#include <vfs.h>
#include <vmm.h>
#include <chdev.h>
#include <thread.h>
#include <printk.h>
#include <process.h>

#include <syscalls.h>

////////////////////////////////////
int sys_isatty( uint32_t   file_id )
{
	xptr_t    file_xp;     // extended pointer on remote file descriptor
    xptr_t    chdev_xp;    // extended pointer on remote chdev
    cxy_t     chdev_cxy;
    chdev_t * chdev_ptr;
    uint32_t  chdev_func;
    int       retval;

	thread_t  *  this    = CURRENT_THREAD;
	process_t *  process = this->process;
 
#if DEBUG_SYS_ISATTY
uint64_t tm_start;
uint64_t tm_end;
tm_start = hal_get_cycles();
if( DEBUG_SYS_ISATTY < tm_start )
printk("\n[DBG] %s : thread %x enter / process %x / cycle %d\n"
__FUNCTION__ , this, process->pid, (uint32_t)tm_start );
#endif

    // check file_id argument
	if( file_id >= CONFIG_PROCESS_FILE_MAX_NR )
	{

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : illegal file descriptor index = %d\n", __FUNCTION__ , file_id );
#endif
		this->errno = EBADFD;
		return -1;
	}

    // get extended pointer on remote file descriptor
    file_xp = process_fd_get_xptr( process , file_id );

    if( file_xp == XPTR_NULL )
    {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : undefined fd_id %d in process %x\n",
__FUNCTION__ , file_id , process->pid );
#endif
        this->errno = EBADFD;
        return -1;
    }

    // get file descriptor cluster and local pointer
    vfs_file_t * file_ptr = GET_PTR( file_xp );
    cxy_t        file_cxy = GET_CXY( file_xp );

    // get file type
    vfs_inode_type_t type = hal_remote_l32( XPTR( file_cxy , &file_ptr->type ) );

    // action depend on file type
    if( type != INODE_TYPE_DEV )      // not a device 
    {
        retval = 0;
	}
    else                              // it is a device
    {
        // get cluster and pointers on chdev 
        chdev_xp  = chdev_from_file( file_xp );
        chdev_cxy = GET_CXY( chdev_xp );
        chdev_ptr = GET_PTR( chdev_xp );

        // get chdev type
        chdev_func = hal_remote_l32( XPTR( chdev_cxy , &chdev_ptr->func ) );

        if( chdev_func == DEV_FUNC_TXT ) retval = 1;
        else                               retval = 0;
    }

#if DEBUG_SYS_ISATTY
tm_end = hal_get_cycles();
if( DEBUG_SYS_ISATTY < tm_end )
printk("\n[DBG] %s : thread %x exit / process %x / cost %d / cycle %d\n",
__FUNCTION__, this, process->pid, (uint32_t)(tm_end - tm_start), (uint32_t)tm_end );
#endif

	return retval;

}  // end sys_read()
