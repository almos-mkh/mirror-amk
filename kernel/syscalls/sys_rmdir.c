/*
 * sys_rmdir.c - Remove a directory from file system.
 *
 * Author    Alain Greiner (2016,2017)
 *
 * Copyright (c) 2015 UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_uspace.h>
#include <printk.h>
#include <errno.h>
#include <vfs.h>
#include <vmm.h>
#include <thread.h>
#include <process.h>

#include <syscalls.h>

////////////////////////////////
int sys_rmdir( char * pathname )
{
    // error_t     error;
    char        kbuf[CONFIG_VFS_MAX_PATH_LENGTH];
	
	thread_t  * this    = CURRENT_THREAD;
	process_t * process = this->process;

    // check pathname length
    if( hal_strlen_from_uspace( pathname ) >= CONFIG_VFS_MAX_PATH_LENGTH )
    {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : pathname too long\n", __FUNCTION__ );
#endif
        this->errno = ENFILE;
        return -1;
    }

    // copy pathname in kernel space
    hal_strcpy_from_uspace( kbuf , pathname , CONFIG_VFS_MAX_PATH_LENGTH );

    // get cluster and local pointer on reference process
    xptr_t      ref_xp  = process->ref_xp;
    process_t * ref_ptr = (process_t *)GET_PTR( ref_xp );
    cxy_t       ref_cxy = GET_CXY( ref_xp );

    // get extended pointer on cwd inode
    // xptr_t cwd_xp = hal_remote_l64( XPTR( ref_cxy , &ref_ptr->vfs_cwd_xp ) );
   
    // get the cwd lock in write mode from reference process
	remote_rwlock_wr_acquire( XPTR( ref_cxy , &ref_ptr->cwd_lock ) );

    // call the relevant VFS function
	printk("\n[ERROR] in %s : non implemented yet\n", __FUNCTION__ );

    // release the cwd lock
	remote_rwlock_wr_release( XPTR( ref_cxy , &ref_ptr->cwd_lock ) );

	return 0;

}  // end sys_rmdir()
