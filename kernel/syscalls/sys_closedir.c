/*
 * sys_closedir.c - Close an open VFS directory.
 * 
 * Author    Alain Greiner  (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <kernel_config.h>
#include <hal_kernel_types.h>
#include <vfs.h>
#include <printk.h>
#include <thread.h>
#include <process.h>
#include <user_dir.h>
#include <errno.h>
#include <syscalls.h>
#include <shared_syscalls.h>

///////////////////////////////
int sys_closedir ( DIR * dirp )
{
    xptr_t         dir_xp;       // extended pointer on user_dir_t structure
    user_dir_t   * dir_ptr;      // lcal pointer on user_dir_t structure
    cxy_t          dir_cxy;      // cluster identifier (inode cluster)

	thread_t  * this    = CURRENT_THREAD;  // client thread
	process_t * process = this->process;   // client process

#if (DEBUG_SYS_CLOSEDIR || CONFIG_INSTRUMENTATION_SYSCALLS)
uint64_t     tm_start = hal_get_cycles();
#endif

#if DEBUG_SYS_CLOSEDIR
if( DEBUG_SYS_CLOSEDIR < tm_start )
printk("\n[%s] thread[%x,%x] enter for DIR <%x> / cycle %d\n",
__FUNCTION__, process->pid, this->trdid, dirp, (uint32_t)tm_start );
#endif
 
    // get extended pointer on kernel user_dir_t structure from dirp
    dir_xp  = user_dir_from_ident( (intptr_t)dirp );

    if( dir_xp == XPTR_NULL )
	{

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s / thread[%x,%x] : DIR pointer %x not registered\n",
__FUNCTION__ , process->pid , this->trdid, dirp );
#endif
		this->errno = EINVAL;
		return -1;
	}	

    // get cluster and localpointer for user_dir_t structure
    dir_ptr = GET_PTR( dir_xp );
    dir_cxy = GET_CXY( dir_xp );
    
    // delete both user_dir_t structure and dirent array
    if( dir_cxy == local_cxy )
    {
        user_dir_destroy( dir_ptr,
                          process->ref_xp );
    }
    else
    {
        rpc_user_dir_destroy_client( dir_cxy,
                                     dir_ptr,
                                     process->ref_xp );
    }

    hal_fence();

#if (DEBUG_SYS_CLOSEDIR || CONFIG_INSTRUMENTATION_SYSCALLS)
uint64_t     tm_end = hal_get_cycles();
#endif

#if DEBUG_SYS_CLOSEDIR
if( DEBUG_SYS_CLOSEDIR < tm_end )
printk("\n[%s] thread[%x,%x] exit for DIR <%x> / cycle %d\n",
__FUNCTION__, process->pid, this->trdid, dirp, (uint32_t)tm_end );
#endif
 
#if CONFIG_INSTRUMENTATION_SYSCALLS
hal_atomic_add( &syscalls_cumul_cost[SYS_CLOSEDIR] , tm_end - tm_start );
hal_atomic_add( &syscalls_occurences[SYS_CLOSEDIR] , 1 );
#endif

    return 0;

}  // end sys_closedir()
