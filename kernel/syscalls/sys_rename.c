/*
 * sys_rename.c - Rename a file or a directory.
 *
 * Author        Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <kernel_config.h>
#include <hal_kernel_types.h>
#include <hal_uspace.h>
#include <errno.h>
#include <printk.h>
#include <thread.h>
#include <vfs.h>
#include <process.h>
#include <remote_rwlock.h>

#include <syscalls.h>

///////////////////////////
int sys_rename( char * old,
                char * new )
{
    error_t        error;
    xptr_t         old_root_inode_xp;   // extended pointer on old path root inode
    xptr_t         new_root_inode_xp;   // extended pointer on new path root inode

    char           k_old[CONFIG_VFS_MAX_PATH_LENGTH];
    char           k_new[CONFIG_VFS_MAX_PATH_LENGTH];

    thread_t     * this     = CURRENT_THREAD;
    process_t    * process  = this->process;

#if (DEBUG_SYS_RENAME || CONFIG_INSTRUMENTATION_SYSCALLS)
uint64_t     tm_start = hal_get_cycles();
#endif

    // check old name length
    if( hal_strlen_from_uspace( old ) >= CONFIG_VFS_MAX_PATH_LENGTH )
    {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : old name too long\n", __FUNCTION__ );
#endif
        this->errno = ENFILE;
        return -1;
    }

    // check new name length
    if( hal_strlen_from_uspace( new ) >= CONFIG_VFS_MAX_PATH_LENGTH )
    {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : new name too long\n", __FUNCTION__ );
#endif
        this->errno = ENFILE;
        return -1;
    }

    // copy old name an new name in kernel space
    hal_strcpy_from_uspace( k_old , old , CONFIG_VFS_MAX_PATH_LENGTH );
    hal_strcpy_from_uspace( k_new , new , CONFIG_VFS_MAX_PATH_LENGTH );

#if DEBUG_SYS_RENAME
if( DEBUG_SYS_RENAME < tm_start )
printk("\n[%s] thread[%x,%x] enter / old <%s> / new <%s> / cycle %d\n",
__FUNCTION__, process->pid, this->trdid, k_old, k_new, (uint32_t)tm_start );
#endif
 
    // get extended pointer on current working directory
    xptr_t      ref_xp  = process->ref_xp;
    process_t * ref_ptr = GET_PTR( ref_xp );
    cxy_t       ref_cxy = GET_CXY( ref_xp );
    xptr_t      cwd_xp  = hal_remote_l64( XPTR( ref_cxy , &ref_ptr->cwd_xp ) );

    // compute root inode for old path 
    if( k_old[0] == '/' )   old_root_inode_xp = process->vfs_root_xp;
    else                    old_root_inode_xp = cwd_xp;

    // compute root inode for new path 
    if( k_new[0] == '/' )   new_root_inode_xp = process->vfs_root_xp;
    else                    new_root_inode_xp = cwd_xp;

    // call the VFS function to creates the new link
    // this function takes & releases the lock protecting the Inode Tree
    error = vfs_link( old_root_inode_xp,
                      k_old,
                      new_root_inode_xp,
                      k_new );
    if( error )
    {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : cannot link new <%s>\n", __FUNCTION__, k_new );
#endif
        this->errno = ENFILE;
        return -1;
    }

    // call the VFS function to remove the old link
    // this function takes & releases the lock protecting the Inode Tree
    error = vfs_unlink( old_root_inode_xp,
                        k_old );
    if( error )
    {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : cannot unlink old <%s>\n", __FUNCTION__, k_old );
#endif
        this->errno = ENFILE;
        return -1;
    }

    hal_fence();

#if (DEBUG_SYS_RENAME || CONFIG_INSTRUMENTATION_SYSCALLS)
uint64_t     tm_end = hal_get_cycles();
#endif

#if DEBUG_SYS_RENAME
if( DEBUG_SYS_RENAME < tm_end )
printk("\n[%s] thread[%x,%x] exit / cycle %d\n",
__FUNCTION__, process->pid, this->trdid, (uint32_t)tm_end );
#endif
 
#if CONFIG_INSTRUMENTATION_SYSCALLS
hal_atomic_add( &syscalls_cumul_cost[SYS_RENAME] , tm_end - tm_start );
hal_atomic_add( &syscalls_occurences[SYS_RENAME] , 1 );
#endif

    return 0;

}  // end sys_rename()
