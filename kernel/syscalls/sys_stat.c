/*
 * sys_stat.c - kernel function implementing the "stat" syscall.
 * 
 * Author    Alain Greiner  (2016,2017,2018,2019)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_uspace.h>
#include <hal_vmm.h>
#include <hal_special.h>
#include <errno.h>
#include <thread.h>
#include <printk.h>
#include <vfs.h>
#include <vmm.h>
#include <process.h>

#include <syscalls.h>

/////////////////////////////////////
int sys_stat( char        * pathname,
              struct stat * u_stat )
{
    error_t       error;
    vseg_t      * vseg;                   // for user space checking
    struct stat   k_stat;                 // in kernel space 
    xptr_t        root_inode_xp;          // extended pointer on path root inode

    char          kbuf[CONFIG_VFS_MAX_PATH_LENGTH];
	
	thread_t  * this    = CURRENT_THREAD;
	process_t * process = this->process;

#if (DEBUG_SYS_STAT || CONFIG_INSTRUMENTATION_SYSCALLS)
uint64_t     tm_start = hal_get_cycles();
#endif

    // check stat structure in user space
    error = vmm_get_vseg( process , (intptr_t)u_stat , &vseg );

	if( error )
	{

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s / thread[%x,%x] : stat structure %x unmapped\n",
__FUNCTION__ , process->pid , this->trdid, u_stat );
hal_vmm_display( process , false );
#endif
		this->errno = EINVAL;
		return -1;
	}	

    // check pathname length
    if( hal_strlen_from_uspace( pathname ) >= CONFIG_VFS_MAX_PATH_LENGTH )
    {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s / thread[%x,%x] : pathname too long\n",
 __FUNCTION__ , process->pid , this->trdid );
#endif
        this->errno = ENFILE;
        return -1;
    }

    // copy pathname in kernel space
    hal_strcpy_from_uspace( kbuf , pathname , CONFIG_VFS_MAX_PATH_LENGTH );

#if DEBUG_SYS_STAT
if( DEBUG_SYS_STAT < tm_start )
printk("\n[%s] thread[%x,%x] enter for file <%s> / cycle %d\n",
__FUNCTION__, process->pid, this->trdid, kbuf, (uint32_t)tm_start );
#endif

    // compute root inode for path 
    if( kbuf[0] == '/' )                        // absolute path
    {
        // use extended pointer on VFS root inode
        root_inode_xp = process->vfs_root_xp;
    }
    else                                        // relative path
    {
        // get cluster and local pointer on reference process
        xptr_t      ref_xp  = process->ref_xp;
        process_t * ref_ptr = (process_t *)GET_PTR( ref_xp );
        cxy_t       ref_cxy = GET_CXY( ref_xp );

        // use extended pointer on CWD inode
        root_inode_xp = hal_remote_l64( XPTR( ref_cxy , &ref_ptr->cwd_xp ) );
    }

    // call the relevant VFS function
    error = vfs_stat( root_inode_xp,
                      kbuf,
                      &k_stat ); 
    if( error )
	{

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s / thread[%x,%x] : cannot get stats for inode <%s>\n",
__FUNCTION__ , process->pid , this->trdid , pathname );
#endif
		this->errno = ENFILE;
		return -1;
	}
   
    // copy k_stat to u_stat 
    hal_copy_to_uspace( u_stat , &k_stat , sizeof(struct stat) );

    hal_fence();

#if (DEBUG_SYS_STAT || CONFIG_INSTRUMENTATION_SYSCALLS)
uint64_t     tm_end = hal_get_cycles();
#endif

#if DEBUG_SYS_STAT
if( DEBUG_SYS_STAT < tm_end )
printk("\n[%s] thread[%x,%x] exit for file <%s> / size %d / mode %d / cycle %d\n",
__FUNCTION__, process->pid, this->trdid, kbuf,
k_stat.st_size, k_stat.st_mode, (uint32_t)tm_end );
#endif
 
#if CONFIG_INSTRUMENTATION_SYSCALLS
hal_atomic_add( &syscalls_cumul_cost[SYS_STAT] , tm_end - tm_start );
hal_atomic_add( &syscalls_occurences[SYS_STAT] , 1 );
#endif

	return 0;

}  // end sys_stat()

