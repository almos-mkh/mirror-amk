/*
 * kern/sys_lseek.c - set the read/write offset of an opened file 
 * 
 * Copyright (c) 2008,2009,2010,2011,2012 Ghassan Almaless
 * Copyright (c) 2011,2012 UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-kernel.
 *
 * ALMOS-kernel is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-kernel is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-kernel; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <kernel_config.h>
#include <hal_kernel_types.h>
#include <hal_uspace.h>
#include <errno.h>
#include <vfs.h>
#include <vmm.h>
#include <thread.h>
#include <printk.h>
#include <process.h>

////////////////////////////////
int sys_lseek (uint32_t file_id,
               uint32_t offset,
               uint32_t whence )
{
	error_t    error;
    xptr_t     file_xp;
    uint32_t   new_offset;

	thread_t  * this    = CURRENT_THREAD;
	process_t * process = this->process;

    // check file_id argument
	if( file_id >= CONFIG_PROCESS_FILE_MAX_NR )
	{
        printk("\n[ERROR] in %s : illegal file descriptor index = %d\n",
               __FUNCTION__ , file_id );
		this->errno = EBADFD;
		return -1;
	}

    // get extended pointer on remote file descriptor
    file_xp = process_fd_get_xptr( process , file_id );

    if( file_xp == XPTR_NULL )
    {
        printk("\n[ERROR] in %s : undefined file descriptor index = %d\n",
               __FUNCTION__ , file_id );
		this->errno = EBADFD;
		return -1;
    }

	/* FIXME: file may be closed in parallel 
	 * of seek/read/write/mmap ..etc 
	 * so file may be NULL or invalid */

    // call relevant VFS function
	error = vfs_lseek( file_xp , offset , whence , &new_offset );

	if( error )
	{
        printk("\n[ERROR] in %s : cannot seek file = %d\n",
               __FUNCTION__ , file_id );
		this->errno = error;
		return -1;
	}
  
	return new_offset;
}
