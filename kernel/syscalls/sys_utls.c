/*
 * kern/sys_utls.c - User Thread Local Storage
 * 
 * Author    Ghassan Almaless (2008,2009,2010,2011,2012)
 *           Alain Greiner (2016,2017)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <errno.h>
#include <thread.h>
#include <printk.h>
#include <syscalls.h>

/////////////////////////////////
int sys_utls( uint32_t operation,
              uint32_t value )
{
    thread_t * this = CURRENT_THREAD;
  
	switch(operation)
	{
	    case UTLS_SET:
		this->utls = value;
		return 0;

	    case UTLS_GET:
		return this->utls;

	    case UTLS_GET_ERRNO:
		return this->errno;

	    default:
        printk("\n[ERROR] in %s : illegal utls operation\n", __FUNCTION__ );
		this->errno = EINVAL;
		return -1;
	}

}  // end sys_utls()
