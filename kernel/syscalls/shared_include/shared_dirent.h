/*
 * shared_dirent.h - Shared structure used by the opendir() / readdir() / closedir() syscalls.
 *
 * Author  Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 * 
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _SHARED_DIRENT_H_
#define _SHARED_DIRENT_H_

/*******************************************************************************************
 * This enum defines the possible types for a dirent inode in a dirent structure.
 *
 * WARNING : these types must be kept consistent with inode types in <vfs.h> file.
 *           and with types in <shared_stat.h> file.
 ******************************************************************************************/

typedef enum 
{
    DT_REG     = 0,                     /*! regular file                                  */
    DT_DIR     = 1,                     /*! directory                                     */
    DT_FIFO    = 2,                     /*! named pipe (FIFO)                             */
    DT_PIPE    = 3,                     /*! anonymous pipe                                */
    DT_SOCK    = 4,                     /*! socket                                        */
    DT_CHR     = 5,                     /*! character device                              */
    DT_BLK     = 6,                     /*! block device                                  */
    DT_LNK     = 7,                     /*! symbolic link                                 */
    DT_UNKNOWN = 8,                     /*! undetermined type                             */
}
dirent_type_t;

/*******************************************************************************************
 * This defines the actual ALMOS-MKH implementation of the DIR user type.
 ******************************************************************************************/

typedef unsigned int   DIR;

/*******************************************************************************************
 * This structure defines the informations returned to user by the readdir() syscall.
 * 
 * WARNING: sizeof(dirent) must be 64 bytes.
 ******************************************************************************************/

struct dirent
{
    int           d_ino;                                  /*! inode identifier            */
    int           d_type;                                 /*! inode type                  */
    char          d_name[48];                             /*! dentry name                 */
    char          padding[64 - 48 - (2*sizeof(int))];
};

#endif
