/*
 * syscalls_numbers.c - Contains enum of the syscalls.
 *
 * Author    Alain Greiner (2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _SYSCALLS_NUMBERS_H_
#define _SYSCALLS_NUMBERS_H_

/******************************************************************************************
 * This enum defines the mnemonics for the syscall indexes.
 * It must be kept consistent with the array defined in do_syscalls.c
 *****************************************************************************************/
typedef enum {
    SYS_THREAD_EXIT    = 0,
    SYS_THREAD_YIELD   = 1,
    SYS_THREAD_CREATE  = 2,
    SYS_THREAD_JOIN    = 3,
    SYS_THREAD_DETACH  = 4,
    SYS_THREAD_CANCEL  = 5,
    SYS_SEM            = 6,
    SYS_CONDVAR        = 7,
    SYS_BARRIER        = 8,
    SYS_MUTEX          = 9,

    SYS_RENAME         = 10,
    SYS_MUNMAP         = 11,
    SYS_OPEN           = 12,
    SYS_MMAP           = 13,
    SYS_READ           = 14,
    SYS_WRITE          = 15,
    SYS_LSEEK          = 16,
    SYS_CLOSE          = 17,
    SYS_UNLINK         = 18,
    SYS_PIPE           = 19,

    SYS_CHDIR          = 20,
    SYS_MKDIR          = 21,
    SYS_MKFIFO         = 22,
    SYS_OPENDIR        = 23,
    SYS_READDIR        = 24,
    SYS_CLOSEDIR       = 25,
    SYS_GETCWD         = 26,
    SYS_ISATTY         = 27,
    SYS_ALARM          = 28,
    SYS_RMDIR          = 29,

    SYS_UTLS           = 30,
    SYS_CHMOD          = 31,
    SYS_SIGNAL         = 32,
    SYS_TIMEOFDAY      = 33,
    SYS_KILL           = 34,
    SYS_GETPID         = 35,
    SYS_FORK           = 36,
    SYS_EXEC           = 37,
    SYS_STAT           = 38,
    SYS_WAIT           = 39,

    SYS_GET_CONFIG     = 40,
    SYS_GET_CORE       = 41,
    SYS_GET_CYCLE      = 42,
    SYS_DISPLAY        = 43,
    SYS_PLACE_FORK     = 44,  
    SYS_THREAD_SLEEP   = 45,
    SYS_THREAD_WAKEUP  = 46,
    SYS_TRACE          = 47,
    SYS_FG             = 48,
    SYS_IS_FG          = 49,

    SYS_EXIT           = 50,

    SYSCALLS_NR        = 51,

} syscalls_t;

#endif // _SYSCALLS_NUMBERS_H_
