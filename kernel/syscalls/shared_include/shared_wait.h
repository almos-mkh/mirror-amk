/*
 * shared_wait.h - Shared macros used by the wait() syscall.
 *
 * Author  Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 * 
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _SHARED_WAIT_H_
#define _SHARED_WAIT_H_

/*********************************************************************************************
 * These macros can be used by the parent process to analyze a child process termination 
 * status, as returned by the wait() syscall.
 * The termination state is a 32 bits word:
 * - the 8 LSB bits contain the user defined exit status
 * - the 24 other bits contain the flags defined below
 ********************************************************************************************/

#define PROCESS_TERM_STOP  0x100            /*! process received a SIGSTOP signal           */
#define PROCESS_TERM_KILL  0x200            /*! process killed by a SIGKILL signal          */
#define PROCESS_TERM_EXIT  0x400            /*! process terminated by a sys_exit()          */
#define PROCESS_TERM_WAIT  0x800            /*! parent process executed a sys_wait()        */

#define WIFEXITED( status )       (status & PROCESS_TERM_EXIT) 
#define WIFSIGNALED( status )     (status & PROCESS_TERM_KILL)
#define WIFSTOPPED( status )      (status & PROCESS_TERM_STOP)
#define WEXITSTATUS( status )     (status & 0xFF)

#define WNOHANG -1     // TODO Not implemented in almos-mkh

#endif

