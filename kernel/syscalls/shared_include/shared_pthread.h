/*
 * shared_pthread.h - Shared structures and mnemonics used by the pthread related syscalls.
 *
 * Author  Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 * 
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _SHARED_PTHREAD_H_
#define _SHARED_PTHREAD_H_

/*******************************************************************************************
 *    This file defines the types and mnemonics that are shared by the kernel
 *    and by the <pthread> user level library.
 ******************************************************************************************/


/*******************************************************************************************
 * These typedef and enum define the shared information related to the POSIX thread.
 ******************************************************************************************/

typedef unsigned int    pthread_t;               

typedef struct pthread_attr_s
{
	unsigned int        attributes;      /*! user defined attributes bit vector           */
	unsigned int        cxy;             /*! target cluster identifier                    */
	unsigned int        lid;             /*! target core local index                      */
}
pthread_attr_t;

enum
{
    PT_ATTR_DETACH          = 0x0001,    /*! user defined not joinable                    */
    PT_ATTR_CLUSTER_DEFINED = 0x0002,    /*! user defined target cluster                  */
    PT_ATTR_CORE_DEFINED    = 0x0004,    /*! user defined core index in cluster           */
};

/*******************************************************************************************
 * These typedef and enum define the shared informations related to the POSIX mutex. 
 ******************************************************************************************/

typedef unsigned int    pthread_mutex_t;

typedef unsigned int    pthread_mutexattr_t;         // TODO not implemented

typedef enum
{
	MUTEX_INIT,
	MUTEX_DESTROY,
	MUTEX_LOCK,
	MUTEX_UNLOCK,
    MUTEX_TRYLOCK,
} 
mutex_operation_t;

/*******************************************************************************************
 * These typedef and enum define the shared informations related to the POSIX condvar. 
 ******************************************************************************************/

typedef unsigned int    pthread_cond_t;

typedef unsigned int    pthread_condattr_t;          // TODO not implemented

typedef enum
{
	CONDVAR_INIT,
	CONDVAR_DESTROY,
    CONDVAR_WAIT,
    CONDVAR_SIGNAL,
    CONDVAR_BROADCAST,
} 
condvar_operation_t;

/*******************************************************************************************
 * These typedef define and enum the shared informations related to the POSIX rwlock. 
 ******************************************************************************************/

typedef unsigned int    pthread_rwlock_t;            // TODO not implemented 

typedef unsigned int    pthread_rwlockattr_t;        // TODO not implemented 

/******************************************************************************************* 
 * These typedef and enum define the shared informations related to POSIX barriers.
 * The following struct is NOT used in the current implementation or the POSIX barrier
 * that is based on a - non scalable - single shared variable.
 * It can be used by another implementation, based on a distributed quadtree implemented
 * in user space, and relying on a busy waiting policy.
 ******************************************************************************************/

typedef unsigned int    pthread_barrier_t;

typedef struct pthread_barrierattr_s
{
    unsigned int        x_size;         /*! number of clusters in a row                   */
    unsigned int        y_size;         /*! number of clusters in a column                */
    unsigned int        nthreads;       /*! number of expected threads in a cluster       */
}
pthread_barrierattr_t;

typedef enum
{
	BARRIER_INIT,
	BARRIER_DESTROY,
	BARRIER_WAIT,
} 
barrier_operation_t;

/********************************************************************************************* 
 * These structures define another implementation for the POSIX barrier:
 * It is implemented as a hierarchical, physically distributed quad-tree, 
 * covering all clusters specified, with the following constraints:
 *   . The involved clusters form a mesh [x_size * y_size]
 *   . The lower left involved cluster is cluster(0,0)  
 *   . The number of threads per cluster is the same in all clusters.
 *
 * Implementation note:
 * - The quad three is implemented as a three dimensions array of node[x][y][l]
 *   . [x][y] are the cluster coordinates / max values are (QDT_XMAX-1), (QDT_YMAX-1)
 *   . [l] is the node level / 0 for terminal nodes / (QDT_LMAX-1) for the root node
 ********************************************************************************************/

/*

#define  QDT_XMAX    16                // max number of clusters in a row
#define  QDT_YMAX    16                // max number of clusters in a column
#define  QDT_LMAX    5                 // max depth of the quad tree
#define  QDT_YWIDTH  4                 // Y field in cxy, for cxy <=> (x,y) translation
#define  QDT_YMASK   0xF               // Y field in cxy, for cxy <=> (x,y) translation

typedef struct sqt_node_s
{
    volatile unsigned int sense;       // barrier state (toggle)
    volatile unsigned int count;       // number of not arrived tasks
    unsigned int          arity;       // number of locally expected tasks
    unsigned int          level;       // hierarchical level (0 is bottom)
    struct sqt_node_s   * parent;      // pointer on parent node (NULL for root)
    struct sqt_node_s   * child[4];    // pointer on children node (NULL for bottom)
}
sqt_node_t;

typedef struct pthread_barrier_s
{
    sqt_node_t          * node[QDT_XMAX][QDT_YMAX][QDT_LMAX];
}
pthread_barrier_t;

*/

#endif  // _PTHREAD_H_
