/*
 * shared_syscalls.h - Merge all shared informations used by syscalls.
 *
 * Author  Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 * 
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _SHARED_SYSCALLS_H_
#define _SHARED_SYSCALLS_H_

/******************************************************************************************* 
 * This file merges all structures, mnemonics and macros shared by the kernel and
 * the user libraries, including the list of syscalls indexes.
 * This file is only included by the kernel code.
 ******************************************************************************************/

#include <syscalls_numbers.h>

#include <shared_almos.h>
#include <shared_dirent.h>
#include <shared_fcntl.h>
#include <shared_mman.h>
#include <shared_pthread.h>
#include <shared_signal.h>
#include <shared_stat.h>
#include <shared_stdlib.h>
#include <shared_time.h>
#include <shared_unistd.h>
#include <shared_wait.h>

#endif  // _SHARED_SYSCALLS_H_
