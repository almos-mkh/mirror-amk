/*
 * shared_semaphore.h - Shared mnemonics used by the semaphore related syscalls.
 *
 * Author  Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 * 
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _SHARED_SEMAPHORE_H_
#define _SHARED_SEMAPHORE_H_

/*******************************************************************************************
 * This typedef defines the unnamed POSIX semaphore. 
 ******************************************************************************************/

typedef unsigned int      sem_t;

/******************************************************************************************* 
 * This enum defines the operation mnemonics for unnamed  POSIX semaphores.
 ******************************************************************************************/

typedef enum
{
	SEM_INIT,
	SEM_DESTROY,
	SEM_GETVALUE,
	SEM_WAIT,
	SEM_POST,
} 
sem_operation_t;

#endif  // _SEMAPHORE_H_
