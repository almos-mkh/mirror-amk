/*
 * shred_mman.h - Shared structures & mnemonics used by the <mman.h> user library.
 *
 * Author  Alain Greiner (2016,2017,2018,2019)
 *
 * Copyright (c) UPMC Sorbonne Universites
 * 
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _SHARED_MMAN_H_
#define _SHARED_MMAN_H_

/*******************************************************************************************
 * This structure is used by the mmap() syscall().
 ******************************************************************************************/

typedef enum
{
    PROT_NONE   = 0x0,         /*! no access                                              */
    PROT_EXEC   = 0x1,         /*! executable                                             */
    PROT_WRITE  = 0x2,         /*! writable                                               */
    PROT_READ   = 0x4,         /*! readable                                               */
}
mmap_prot_t;

typedef enum
{
    MAP_FILE    = 0x00000000,  /*! map an open file defined by its fdid                   */ 
    MAP_ANON    = 0x00000001,  /*! map an anonymous vseg in local cluster                 */
    MAP_REMOTE  = 0x00000002,  /*! map an anonymous vseg in remote cluster (cxy == fdid)  */
    MAP_PRIVATE = 0x00000010,  /*! modifications are private to the calling process       */
    MAP_SHARED  = 0x00000020,  /*! modifications are shared                               */
    MAP_FIXED   = 0x00000100,  /*! non supported (user defined base address)              */
}
mmap_flags_t;

typedef struct mmap_attr_s
{
	void         * addr;       /*! requested virtual address (unused : should be NULL)    */
	unsigned int   length;     /*! requested vseg size (bytes)                            */
	unsigned int   prot;       /*! access modes                                           */
	unsigned int   flags;      /*! MAP_FILE / MAP_ANON / MAP_PRIVATE / MAP_SHARED         */
	unsigned int   fdid;       /*! file descriptor index (if MAP_FILE)                    */
	unsigned int   offset;     /*! file offset (if MAP_FILE)                              */
}
mmap_attr_t;

#endif
