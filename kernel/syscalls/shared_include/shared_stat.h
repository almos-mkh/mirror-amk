/*
 * shared_stat.h - Shared structure used by the stat() syscall.
 *
 * Author  Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 * 
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _SHARED_STAT_H_
#define _SHARED_STAT_H_

/******************************************************************************************
 * This structure define the informations associated to a file descriptor,
 * returned to user space by the stat() syscall.
 *****************************************************************************************/

struct stat 
{
	unsigned int    st_dev;     /*! ID of device containing file                         */
	unsigned int    st_ino;     /*! inode number                                         */
	unsigned int    st_mode;    /*! bit vector defined below                             */
	unsigned int    st_nlink;   /*! number of hard links                                 */
	unsigned int    st_uid;     /*! user ID of owner                                     */
	unsigned int    st_gid;     /*! group ID of owner                                    */
	unsigned int    st_rdev;    /*! device ID (if special file)                          */
	unsigned int    st_size;    /*! total size, in bytes                                 */
	unsigned int    st_blksize; /*! blocksize for file system I/O                        */
	unsigned int    st_blocks;  /*! number of allocated blocks                           */
};

/******************************************************************************************
 * The st_mode field contains informations on both access rights and file types.
 * - access rights (defined by the inode <rights> field) are stored in st_mode[15:0]
 * - file types (defined by the inode <type> field) are stored in st_mode[19:16]
 * The following macros can be used to extract file type information.
 *
 * WARNING : these macros must be kept consistent with inode types in <vfs.h> file.
 *           and with types in <dirent.h> file.
 *****************************************************************************************/

#define  S_ISREG(x)   ((((x)>>16) & 0xF) == 0)    /*! it is a regular file               */
#define  S_ISDIR(x)   ((((x)>>16) & 0xF) == 1)    /*! it is a directory                  */
#define  S_ISFIFO(x)  ((((x)>>16) & 0xF) == 2)    /*! it is a named pipe                 */
#define  S_ISPIPE(x)  ((((x)>>16) & 0xF) == 3)    /*! it is an anonymous pipe            */
#define  S_ISSOCK(x)  ((((x)>>16) & 0xF) == 4)    /*! it is a socket                     */
#define  S_ISCHR(x)   ((((x)>>16) & 0xF) == 5)    /*! it is a character device           */
#define  S_ISBLK(x)   ((((x)>>16) & 0xF) == 6)    /*! it is a block device               */
#define  S_ISLNK(x)   ((((x)>>16) & 0xF) == 7)    /*! it is a symbolic link              */

#endif /* _STAT_H_ */
