/*
 * shared_unistd.h - Shared mnemonics used by the <unistd.h> user library. 
 * 
 * Author     Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 * 
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _SHARED_UNISTD_H_
#define _SHARED_UNISTD_H_

/******************************************************************************************* 
 * This enum defines the operation mnemonics for the lseek() syscall.
 ******************************************************************************************/

typedef enum
{
    SEEK_SET  = 0,             /*! new_offset <= offset                                   */
    SEEK_CUR  = 1,             /*! new_offset <= current_offset + offset                  */
    SEEK_END  = 2,             /*! new_offset <= current_size + offset                    */
}
lseek_operation_t;

#endif
