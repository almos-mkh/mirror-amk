/*
 * shared_fcntl.h - Shared structures used by file related syscalls.
 *
 * Author  Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 * 
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _SHARED_FCNTL_H_
#define _SHARED_FCNTL_H_

/******************************************************************************************* 
 * This enum defines the attributes bit-vector for the open() syscall.
 ******************************************************************************************/

typedef enum
{
    O_RDONLY   = 0x0010000,    /*! open file in read-only mode                            */
    O_WRONLY   = 0x0020000,    /*! open file in write-only mode                           */
    O_RDWR     = 0x0000000,    /*! open file in read/write mode                           */
    O_NONBLOCK = 0x0040000,    /*! do not block if data non available                     */
    O_APPEND   = 0x0080000,    /*! append on each write                                   */
    O_CREAT    = 0x0100000,    /*! create file if it does not exist                       */
    O_TRUNC    = 0x0200000,    /*! file length is forced to 0                             */
    O_EXCL     = 0x0400000,    /*! error if VFS_O_CREAT and file exist                    */
    O_SYNC	   = 0x0800000,    /*! synchronize File System on each write                  */
    O_CLOEXEC  = 0x1000000,    /*! set the close-on-exec flag in file descriptor          */
    O_DIR      = 0x2000000,    /*! new file descriptor is for a directory                 */
}
open_attr_t;

#endif
