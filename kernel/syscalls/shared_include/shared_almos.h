/*
 * shared_almos.h - Shared mnemonics used by the almos-mkh specific syscalls.
 *
 * Author  Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 * 
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _SHARED_ALMOS_H_
#define _SHARED_ALMOS_H_

/******************************************************************************************* 
 * This enum defines the operation mnemonics for the utls() syscall (Thread Local Storage).
 ******************************************************************************************/

typedef enum
{
    UTLS_SET       = 1,
    UTLS_GET       = 2,
    UTLS_GET_ERRNO = 3,
}
utls_operation_t;

/******************************************************************************************* 
 * This enum defines the type of structure for the display() syscall.
 ******************************************************************************************/

typedef enum
{
    DISPLAY_STRING            = 0,
    DISPLAY_VMM               = 1,
    DISPLAY_SCHED             = 2,
    DISPLAY_CLUSTER_PROCESSES = 3,
    DISPLAY_VFS               = 4,
    DISPLAY_CHDEV             = 5,
    DISPLAY_TXT_PROCESSES     = 6,
    DISPLAY_DQDT              = 7,
    DISPLAY_BUSYLOCKS         = 8,
    DISPLAY_MAPPER            = 9,
    DISPLAY_BARRIER           = 10,
}
display_type_t;


#endif /* _SHARED_ALMOS_H_ */

