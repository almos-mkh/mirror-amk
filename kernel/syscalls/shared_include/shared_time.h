/*
 * shared_time.h - Shared structures used by the gettimeofday() syscall.
 *
 * Author  Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 * 
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _SHARED_TIME_H_
#define _SHARED_TIME_H_

/*******************************************************************************************
 * These two structure define the informations returned by the gettimeofday() syscall().
 ******************************************************************************************/

struct timeval
{
    unsigned int tv_sec;                    /*! seconds since Jan. 1, 1970                */
    unsigned int tv_usec;                   /*! and microseconds                          */
};

struct timezone 
{
    int          tz_minuteswest;            /*! of Greenwich                              */
    int          tz_dsttime;                /*! type of dst correction to apply           */
};

#endif /* _TIME_H_*/
