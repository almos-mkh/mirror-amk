/*
 * sys_condvar.c - Access a POSIX condvar.
 * 
 * Author    Alain Greiner  (2016,2017,2018,2019)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_special.h>
#include <hal_vmm.h>
#include <errno.h>
#include <thread.h>
#include <printk.h>
#include <vmm.h>
#include <syscalls.h>
#include <remote_condvar.h>
#include <remote_mutex.h>


#if DEBUG_SYS_CONDVAR
/////////////////////////////////////////////////////
static char * sys_convar_op_str( uint32_t operation )
{
  	if     ( operation == CONDVAR_INIT      ) return "INIT";
  	else if( operation == CONDVAR_WAIT      ) return "WAIT";
  	else if( operation == CONDVAR_SIGNAL    ) return "SIGNAL";
  	else if( operation == CONDVAR_BROADCAST ) return "BROADCAST";
  	else if( operation == CONDVAR_DESTROY   ) return "DESTROY";
  	else                                      return "undefined";
}
#endif

//////////////////////////////////////
int sys_condvar( void         * condvar,
                 uint32_t       operation,
                 void         * mutex )
{
    vseg_t    * vseg;         // for condvar check
	error_t     error;
 
    thread_t  * this    = CURRENT_THREAD;
    process_t * process = this->process;

#if DEBUG_SYS_CONDVAR
uint64_t    tm_start;
uint64_t    tm_end;
tm_start = hal_get_cycles();
if( DEBUG_SYS_CONDVAR < tm_start )
printk("\n[DBG] %s : thread %x in process %x enter for %s / cycle %d\n",
__FUNCTION__, this->trdid, process->pid, sys_condvar_op_str( operation ), (uint32_t)tm_start );
#endif

    // check condvar in user vspace
	error = vmm_get_vseg( process , (intptr_t)condvar , &vseg );

	if( error )
    {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : unmapped condvar %x / thread %x / process %x\n",
__FUNCTION__ , (intptr_t)condvar , this->trdid , process->pid );
hal_vmm_display( process , false );
#endif
        this->errno = error;
        return -1;
    }

    // execute requested operation
	switch( operation )
	{
        //////////////////
        case CONDVAR_INIT:
        {
            error = remote_condvar_create( (intptr_t)condvar );
    
		    if( error )
            {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : cannot create condvar %x / thread %x / process %x\n",
__FUNCTION__ , (intptr_t)condvar , this->trdid , process->pid );
#endif
                this->errno = error;
                return -1;
            }
		    break;
        }
        //////////////////
	    case CONDVAR_WAIT:
        {
            // check mutex in user vspace
	        error = vmm_get_vseg( process , (intptr_t)mutex , &vseg );

	        if( error )
            {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : unmapped mutex %x / thread %x / process %x\n",
__FUNCTION__ , (intptr_t)mutex , this->trdid , process->pid );
#endif
                this->errno = error;
                return -1;
            }

            xptr_t condvar_xp = remote_condvar_from_ident( (intptr_t)condvar );

            if( condvar_xp == XPTR_NULL )     // user error
            {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : condvar %x not registered / thread %x / process %x\n",
__FUNCTION__ , (intptr_t)condvar , this->trdid , process->pid );
#endif
                this->errno = EINVAL;
                return -1;
            }
    
            xptr_t mutex_xp = remote_mutex_from_ident( (intptr_t)mutex );

            if( mutex_xp == XPTR_NULL )     // user error
            {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : mutex %x not registered / thread %x / process %x\n",
__FUNCTION__ , (intptr_t)mutex , this->trdid , process->pid );
#endif
                this->errno = EINVAL;
                return -1;
            }

            remote_condvar_wait( condvar_xp , mutex_xp );

            break;
        }
        ////////////////////
	    case CONDVAR_SIGNAL:
        {
            xptr_t condvar_xp = remote_condvar_from_ident( (intptr_t)condvar );

            if( condvar_xp == XPTR_NULL )     // user error
            {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : condvar %x not registered / thread %x / process %x\n",
__FUNCTION__ , (intptr_t)condvar , this->trdid , process->pid );
#endif
                this->errno = EINVAL;
                return -1;
            }

            remote_condvar_signal( condvar_xp ); 
            
            break;
        }
        ///////////////////////
	    case CONDVAR_BROADCAST:
        {
            xptr_t condvar_xp = remote_condvar_from_ident( (intptr_t)condvar );

            if( condvar_xp == XPTR_NULL )     // user error
            {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : condvar %x not registered / thread %x / process %x\n",
__FUNCTION__ , (intptr_t)condvar , this->trdid , process->pid );
#endif
                this->errno = EINVAL;
                return -1;
            }

            remote_condvar_broadcast( condvar_xp );

            break;
        }
        /////////////////////
	    case CONDVAR_DESTROY:
        {
            xptr_t condvar_xp = remote_condvar_from_ident( (intptr_t)condvar );

            if( condvar_xp == XPTR_NULL )     // user error
            {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : condvar %x not registered / thread %x / process %x\n",
__FUNCTION__ , (intptr_t)condvar , this->trdid , process->pid );
#endif
                this->errno = EINVAL;
                return -1;
            }

            remote_condvar_destroy( condvar_xp ); 

            break;
		}
        /////////
        default: {
            assert ( false, "illegal operation type <%x>\n", operation );
        }
	}   // end switch

    hal_fence();

#if DEBUG_SYS_CONDVAR
tm_start = hal_get_cycles();
if( DEBUG_SYS_MUTEX < tm_start )
printk("\n[DBG] %s : thread %x in process %x exit for %s / cycle %d\n",
__FUNCTION__, this->trdid, process->pid, sys_condvar_op_str( operation ), (uint32_t)tm_start );
#endif

	return 0;

}  // enc sys_condvar()

