/*
 * syscalls.h - Kernel side services for syscall handling.
 * 
 * Author     Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _SYSCALLS_H_
#define _SYSCALLS_H_

#include <hal_kernel_types.h>
#include <shared_syscalls.h>

/**   Forward declarations  *****/

struct thread_s;                  // defined in thread.h
struct pthread_attr_s;            // defined in thread.h
struct vfs_stat_s;                // defined in vfs.h
struct vfs_dirent_s;              // defined in vfs.h
struct mmap_attr_s;               // defined in vmm.h

/****************************************************************************************** 
 * [0] This function terminates the execution of the calling user thread, 
 * and makes the exit_value pointer available to any successful pthread_join() with the 
 * terminating thread.
 * It actually set the THREAD_SIG_EXIT signal, set the THREAD_BLOCKED_GLOBAL bit in the
 * thread descriptor and deschedule.
 * The thread will be detached from its process, and the memory allocated to the thread
 * descriptor will be released later by the scheduler.
 ******************************************************************************************
 * @ exit_vallue  : pointer to be returned to joining thread if thread is attached.
 * @ return 0 if success / return -1 if all locks not released or illegal argument.
 *****************************************************************************************/
int sys_thread_exit( void * exit_value );

/****************************************************************************************** 
 * [1] This function calls the scheduler for the core running the calling thread.
 ******************************************************************************************
 * @ x_size   : [out] number of clusters in a row.
 * @ y_size   : [out] number of clusters in a column.
 * @ ncores   : [out] number of cores per cluster.
 * @ return always 0.
 *****************************************************************************************/
int sys_thread_yield( void );

/****************************************************************************************** 
 * [2] This function creates a new user thread. The <user_attr> argument is a pointer
 * on astructure containing the thread attributes, defined in thread.h file.
 ******************************************************************************************
 * @ trdid_ptr   : [out] pointer on buffer for created thread trdid.
 * @ user_attr   : [in]  pointer on thread attributes structure.
 * @ start_func  : [in]  pointer on start function.
 * @ start_args  : [in]  pointer on start function arguments.
 * @ return 0 if success / return -1 if failure.
 *****************************************************************************************/
int sys_thread_create( trdid_t               * trdid_ptr,
                       struct pthread_attr_s * user_attr,
                       void                  * start_func,
                       void                  * start_args );

/****************************************************************************************** 
 * [3] This blocking function suspend execution of the calling thread until completion 
 * of another target thread identified by the <trdid> argument.
 * The target thread must be joinable (running in ATTACHED mode), and must be different
 * from the calling thread.
 * If the <exit_value> argument is not NULL, the value passed to pthread_exit() by the 
 * target thread is stored in the location referenced by exit_value.
 ******************************************************************************************
 * @ trdid     : [in]  target thread identifier.
 * @ thread    : [out] buffer for exit_value returned by target thread. 
 * @ return 0 if success / return -1 if failure.
 *****************************************************************************************/
int sys_thread_join( trdid_t    trdid,
                     void    ** exit_value );

/****************************************************************************************** 
 * [4] This function detach a joinable thread.
 ******************************************************************************************
 * @ trdid   : thread identifier.
 * @ return 0 if success / return -1 if failure.
 *****************************************************************************************/
int sys_thread_detach( trdid_t  trdid );

/****************************************************************************************** 
 * [5] This function requests a target thread identified by its <trdid> argument
 * to be cancelled. It calls the thread_kill() function to block the target thread
 * on the THREAD_BLOCKED_GLOBAL condition, and to set the THREAD_FLAG_REQ_DELETE.
 * The thread will be detached from its process, and the memory allocated to the thread
 * descriptor will be released by the scheduler at the next scheduling point.
 ******************************************************************************************
 * @ trdid   : thread identifier.
 * @ return 0 if success / return -1 if illegal argument.
 *****************************************************************************************/
int sys_thread_cancel( trdid_t  trdid );

/****************************************************************************************** 
 * [6] This function implement all operations on a POSIX unnamed semaphore,
 * that can be shared by threads running in different clusters.
 * The kernel structure representing a remote semaphore is in the remote_sem.h file,
 * and the code implementing the operations is in the remore_sem.c file.
 ******************************************************************************************
 * @ vaddr         : semaphore virtual address in user space == identifier.
 * @ operation     : SEM_INIT / SEM_DESTROY / SEM_GETVALUE / SEM_POST / SEM_WAIT.
 * @ init_value    : initial semaphore value.
 * @ current_value : pointer on buffer for current semaphore value.
 * @ return 0 if success / return -1 if failure.
 *****************************************************************************************/
int sys_sem( void       * vaddr,
             uint32_t     operation,
             uint32_t     init_value,
             uint32_t   * current_value );

/****************************************************************************************** 
 * [7] This function implement all operations on a POSIX condition variable.
 * The kernel structure representing a condvar is defined in the remote_condvar.h file,
 * The code implementing the operations is defined in the remote_condvar.c file.
 ******************************************************************************************
 * @ vaddr     : condvar virtual address in user space == identifier.
 * @ operation : operation type (see below).
 * @ attr      : mutex virtual address in user space == identifier.
 * @ return 0 if success / return -1 if failure.
 *****************************************************************************************/
int sys_condvar( void     * condvar,
                 uint32_t   operation,
                 void     * mutex );

/****************************************************************************************** 
 * [8] This function implement all operations on a POSIX barrier.
 * The kernel structure representing a barrier is defined in the remote_barrier.h file.
 * The code implementting the operations is defined in the remote_barrier.c file.
 ******************************************************************************************
 * @ vaddr     : barrier address in user space == identifier.
 * @ operation : BARRIER_INIT / BARRIER_DESTROY / BARRIER_WAIT.
 * @ count     : number of expected threads (only used by BARRIER_INIT).
 * @ attr      : barrier attributes address in user space (only used by BARRIER_INIT).
 * @ return 0 if success / return -1 if failure.
 *****************************************************************************************/
int sys_barrier( intptr_t   vaddr,
                 uint32_t   operation,
                 uint32_t   count,
                 intptr_t   attr );

/****************************************************************************************** 
 * [9] This function implement all operations on a POSIX mutex.
 * The kernel structure representing a barrier is defined in the remote_barrier.h file.
 * The code implementting the operations is defined in the remote_barrier.c file.
 ******************************************************************************************
 * @ vaddr     : mutex virtual address in user space == identifier.
 * @ operation : MUTEX_INIT / MUTEX_DESTROY / MUTEX_LOCK / MUTEX_UNLOCK
 * @ attr      : mutex attributes (non supported yet => must be 0).
 * @ return 0 if success / return -1 if failure.
 *****************************************************************************************/
int sys_mutex( void     * vaddr,
               uint32_t   operation,
               uint32_t   count );

/****************************************************************************************** 
 * [10] This function causes the file named <old> to be renamed as <new>.
 * If new exists, it is first removed.  Both old and new must be of the same type (both
 * must be either directories or non-directories) and must reside on the same file system.
 * It guarantees that an instance of <new> will always exist, even if the system should 
 * crash in the middle of the operation.
 ******************************************************************************************
 * @ old      : old file name.
 * @ new      : new file name.
 * @ return 0 if success / return -1 if failure.
 *****************************************************************************************/
int sys_rename( char *old,
                char *new );

/****************************************************************************************** 
 * [11] This function remove an existing mapping defined by the <addr> and <size>
 * arguments in user space.
 ******************************************************************************************
 * @ addr  : base address in user space.
 * # size  : number of bytes.
 * @ return 0 if success / return -1 if failure.
 *****************************************************************************************/
int sys_munmap( void     * addr,
                uint32_t   size );

/****************************************************************************************** 
 * [12] This function open or create an open file descriptor.
 ******************************************************************************************
 * @ pathname   : pathname (can be relative or absolute).
 * @ flags      : bit vector attributes (see in shared_fcntl.h file)
 * @ mode       : access rights.
 * @ return file descriptor index in fd_array if success / return -1 if failure.
 *****************************************************************************************/
int sys_open( char       * pathname,
              uint32_t     flags,
              uint32_t     mode );

/****************************************************************************************** 
 * [13] This function map physical memory (or a file) in the calling thread virtual space.
 * The <attr> argument is a pointer on a structure for arguments (see shared_syscalls.h).
 * The user defined virtual address (MAP_FIXED flag) is not supported.
 * TODO : the access rights checking is not implemented yet [AG]
 * TODO : the Copy on Write for MAP_PRIVATE is not implemented yet [AG]
 ******************************************************************************************
 * @ attr       : pointer on attributes structure.
 * @ return 0 if success / return -1 if failure.
 *****************************************************************************************/
int sys_mmap( mmap_attr_t * attr );

/****************************************************************************************** 
 * [14] This function read bytes from an open file identified by its file descriptor.
 * The file can be a regular file or character oriented device.
 * IRQs are enabled during this system call.
 ******************************************************************************************
 * @ file_id  : open file index in fd_array.
 * @ buf      : buffer virtual address in user space.
 * @ count    : number of bytes.
 * @ return number of bytes actually read if success / returns -1 if failure.
 *****************************************************************************************/
int sys_read( uint32_t   file_id,
              void     * buf,
              uint32_t   count );

/****************************************************************************************** 
 * [15] This function writes bytes to an open file identified by its file descriptor.
 * The file can be a regular file or character oriented device. For a regular file,
 * the target inode "size" field is updated if (offset + count) is larger than the
 * current "size" value. The size value registered in the mappers of the parent(s)
 * directory are not modified and will be asynchronously updated when the file is closed.
 * IRQs are enabled during this system call.
 ******************************************************************************************
 * @ file_id  : open file index in fd_array.
 * @ buf      : buffer virtual address in user space.
 * @ count    : number of bytes.
 * @ return number of bytes actually written if success / returns -1 if failure.
 *****************************************************************************************/
int sys_write( uint32_t   file_id,
               void     * buf,
               uint32_t   count );

/****************************************************************************************** 
 * [16] This function repositions the offset of the file descriptor identified by <file_id>,
 * according to the operation type defined by the <whence> argument.
 ******************************************************************************************
 * @ file_id  : open file index in fd_array.
 * @ offset   : used to compute new offset value.
 * @ whence   : operation type (see below).
 * @ return 0 if success / returns -1 if failure.
 *****************************************************************************************/
int sys_lseek( xptr_t    file_id,
               uint32_t  offset,
               uint32_t  whence );

/****************************************************************************************** 
 * [17] This function release the memory allocated for the file descriptor identified by 
 * the <file_id> argument, and remove the fd array_entry in all copies of the process 
 * descriptor.
 ******************************************************************************************
  file_id   : file descriptor index in fd_array.
 * @ return 0 if success / returns -1 if failure.
 *****************************************************************************************/
int sys_close( uint32_t file_id );

/****************************************************************************************** 
 * [18] This function removes a directory entry identified by the <pathname> from the
 * directory, and decrement the link count of the file referenced by the link.
 * If the link count reduces to zero, and no process has the file open, then all resources
 * associated with the file are reclaimed.  If one or more process have the file open when
 * the last link is removed, the link is removed, but the removal of the file is delayed 
 * until all references to it have been closed.
 ******************************************************************************************
 * @ pathname   : pathname (can be relative or absolute).
 * @ return 0 if success / returns -1 if failure.
 *****************************************************************************************/
int sys_unlink( char * pathname );

/****************************************************************************************** 
 * [19] This function creates in the calling thread cluster an unnamed pipe, and two
 * (read and write) file descriptors.
 * TODO not implemented yet [AG]
 ******************************************************************************************
 * @ file_id[0] : [out] read only file descriptor index.
 * @ file_id[1] : [out] write only file descriptor index.
 * @ return 0 if success / return -1 if failure.
 *****************************************************************************************/
int sys_pipe( uint32_t file_id[2] );

/****************************************************************************************** 
 * [20] This function change the current working directory in reference process descriptor.
 ******************************************************************************************
 * @ pathname   : pathname (can be relative or absolute).
 * @ return 0 if success / returns -1 if failure.
 *****************************************************************************************/
int sys_chdir( char * pathname );

/****************************************************************************************** 
 * [21] This function implements the "mkdir" system call, creating a new directory in
 * the file system, as defined by the <pathname> argument, with the access permission
 * defined by the <rights> argument. All nodes but the last in the pathname must exist.
 * It can be called by any thread running in any cluster.
 ******************************************************************************************
 * @ pathname  : pathname defining the new directory location in file system.
 * @ rights    : access rights (non used yet).
 * @ return 0 if success / return -1 if failure.
 *****************************************************************************************/
int sys_mkdir( char     * pathname,
               uint32_t   rights );

/****************************************************************************************** 
 * [22] This function creates a named FIFO file in the calling thread cluster.
 * The associated read and write file descriptors mut be be  explicitely created
 * using the sys_open() function.
 ******************************************************************************************
 * @ pathname   : pathname (can be relative or absolute).
 * @ mode       : access rights (as defined in chmod).
 * @ return 0 if success / returns -1 if failure.
 *****************************************************************************************/
int sys_mkfifo( char     * pathname,
                uint32_t   mode );

/****************************************************************************************** 
 * [23] This function creates an user level directory descriptor (including the associated
 * array of user level dirents), and intialise it from the kernel directory mapper, that
 * contains all entries in this directory). The directory is identified by the <pathname>
 * argument. If the corresponding inode is missing in the Inode Tree, the inode is created,
 * but the directory must exist in the file system. 
 * It returns a DIR pointer <dirp> on the dirent array in user space.
 ******************************************************************************************
 * @ pathname   : [in]  pathname (can be relative or absolute).
 * @ dirp       : [out] buffer for pointer on user directory (DIR).
 * @ return 0 if success / returns -1 if failure.
 *****************************************************************************************/
int sys_opendir( char * pathname,
                 DIR ** dirp );

/****************************************************************************************** 
 * [24] This function returns an user pointer on the dirent structure describing the
 * next directory entry in the directory identified by the <dirp> argument.
 ******************************************************************************************
 * @ dirp     : [in]  user pointer on dirent array identifying the open directory.
 * @ buffer   : [out] pointer on user buffer for a pointer on dirent in user space.
 * @ return O if success / returns -1 if failure.
 *****************************************************************************************/
int sys_readdir( DIR            * dirp,
                 struct dirent ** buffer );

/****************************************************************************************** 
 * [25] This function closes the directory identified by the <dirp> argument, and releases
 * all structures associated with the <dirp> pointer.
 ******************************************************************************************
 * @ dirp     : [in] user pointer on dirent array identifying the open directory.
 * @ return 0 if success / returns -1 if failure.
 *****************************************************************************************/
int sys_closedir( DIR * dirp );

/****************************************************************************************** 
 * [26] This function returns the pathname of the current working directory.
 ******************************************************************************************
 * buf     : buffer addres in user space.
 * nbytes  : user buffer size in bytes.
 * @ return 0 if success / returns -1 if failure.
 *****************************************************************************************/
int sys_getcwd( char     * buf,
                uint32_t   nbytes );

/****************************************************************************************** 
 * [27] This function tests whether a given file descriptor dentified by the <file_id>
 * argument is an open file descriptor referring to a terminal.
 ******************************************************************************************
 * @ file_id   : file descriptor index
 * @ return 1 if it is a TXT device / return 0 if it is not a TXT device.
 *****************************************************************************************/
int sys_isatty( uint32_t file_id );

/****************************************************************************************** 
 * [28] This function forces the calling thread to sleep, for a fixed number of cycles.
 ******************************************************************************************
 * cycles   : number of cycles.
 *****************************************************************************************/
int sys_alarm( uint32_t cycles );

/****************************************************************************************** 
 * [29] This function removes a directory file whose name is given by <pathname>.
 * The directory must not have any entries other than `.' and `..'.
 ******************************************************************************************
 * @ pathname   : pathname (can be relative or absolute).
 * @ return 0 if success / returns -1 if failure.
 *****************************************************************************************/
int sys_rmdir( char * pathname );

/****************************************************************************************** 
 * [30] This function implement the operations related to User Thread Local Storage.
 * It is actually implemented as an uint32_t variable in the thread descriptor.
 ******************************************************************************************
 * @ operation  : UTLS operation type as defined below.
 * @ value      : argument value for the UTLS_SET operation.
 * @ return value for the UTLS_GET and UTLS_GET_ERRNO / return -1 if failure.
 *****************************************************************************************/
int sys_utls( uint32_t operation,
              uint32_t value );

/****************************************************************************************** 
 * [31] This function change the acces rights for the file/dir identified by the
 * pathname argument.
 ******************************************************************************************
 * @ pathname   : pathname (can be relative or absolute).
 * @ rights     : acces rights.
 * @ return 0 if success / returns -1 if failure.
 *****************************************************************************************/
int sys_chmod( char       * pathname,
               uint32_t     rights );

/****************************************************************************************** 
 * [32] This function associate a specific signal handler to a given signal type.
 * The handlers for the SIGKILL and SIGSTOP signals cannot be redefined.
 ****************************************************************************************** 
 * @ sig_id    : index defining signal type (from 1 to 31).
 * @ handler   : pointer on fonction implementing the specific handler.
 * @ return 0 if success / returns -1 if failure.
 *****************************************************************************************/
int sys_signal( uint32_t   sig_id,
                void     * handler );

/****************************************************************************************** 
 * [33] This function returns in the structure <tv>, defined in the time.h file,
 * the current time (in seconds & micro-seconds).
 * It is computed from the calling core descriptor.
 * The timezone is not supported.
 ******************************************************************************************
 * @ tv      : pointer on the timeval structure.
 * @ tz      : pointer on the timezone structure : must be NULL.       
 * @ return 0 if success / returns -1 if failure.
 *****************************************************************************************/
int sys_timeofday( struct timeval  * tv,
                   struct timezone * tz );

/****************************************************************************************** 
 * [34] This function implements the "kill" system call on the kernel side.
 * It register the signal defined by the <sig_id> argument in all thread descriptors 
 * of a target process identified by the <pid> argument. This is done in all clusters
 * containing threads for the target process.
 * It can be executed by any thread running in any cluster, as this function uses
 * remote access to traverse the list of process copies stored in the owner cluster,
 * and the RPC_SIGNAL_RISE to signal the remote threads. 
 * This function does nothing for (sig_id == 0). This can be used to check process pid.
 * TODO : This first implementation supports only SIGKILL / SIGSTOP / SIGCONT values.
 ****************************************************************************************** 
 * @ pid      : target process identifier.
 * @ sig_id   : index defining the signal type. 
 * @ return 0 if success / returns -1 if failure.
 *****************************************************************************************/
int sys_kill( pid_t    pid,
              uint32_t sig_id );

/****************************************************************************************** 
 * [35] This function implements the "getpid" system call on the kernel side.
 ****************************************************************************************** 
 * @ returns the process PID for the calling thread.
 *****************************************************************************************/
int sys_getpid( void );

/****************************************************************************************** 
 * [36] This function implement the "fork" system call on the kernel side.
 * The calling process descriptor (parent process), and the associated thread descriptor
 * are replicated in a - likely - remote cluster, that becomes the child process owner. 
 * The child process get a new PID, and is linked to the parent PID. The child process 
 * inherit from its parent the memory image, and all open files (including the TXT).
 * The child process becomes the TXT terminal owner.
 * The target cluster depends on the "fork_user" flag and "fork_cxy" variable that can be
 * stored in the calling thread descriptor by the specific fork_place() system call.
 * If not, the kernel function makes a query to the DQDT to select the target cluster. 
 ****************************************************************************************** 
 * @ if success, returns child process PID to parent, and return O to child.
 * @ if failure, returns -1 to parent / no child process is created.
 *****************************************************************************************/
int sys_fork( void );

/****************************************************************************************** 
 * [37] This function implement the "exec" system call on the kernel side.
 * It creates, in the same cluster as the calling thread, a new process descriptor,
 * and a new associated main thread descriptor, executing a new memory image defined 
 * by the <filename> argument. This new process inherit from the old process the PID
 * and the PPID, as well as all open files (including the TXT). 
 * The old process descriptor, and all its threads are blocked, and marked for deletion.
 * Therefore the exec syscall does not return to the calling thread in case of success.
 * This function build an exec_info_t structure containing the new process arguments,
 * as defined by the <arv> argument, and the new process environment variables, 
 * as defined by the <envp>  argument.
 * TODO : the <argv> and <envp> arguments are not supported yet (both must be NULL).
 ****************************************************************************************** 
 * @ filename : string pointer on .elf filename (pointer in user space)
 * @ argv     : array of strings on process arguments (pointers in user space)
 * @ envp     : array of strings on environment variables (pointers in user space)
 * @ does not return if success / returns -1 if failure.
 *****************************************************************************************/
int sys_exec( char  * filename,
              char ** argv,
              char ** envp );

/****************************************************************************************** 
 * [38] This function  returns in the <stat> structure, defined in the "shared_syscalls.h" 
 * file, various informations on the file/directory identified by the <pathname> argument.
 * TODO only the <st_ino>, <st_mode>,<st_uid>,<st_gid>,<st_size> are set.
 ****************************************************************************************** 
 * @ pathname  : user pointer on file pathname.
 * @ stat      : user pointer on the stat structure.
 * @ returns O if success / returns -1 if failure.
 *****************************************************************************************/
int sys_stat( char        * pathname,
              struct stat * stat );

/****************************************************************************************** 
 * [39] This blocking function waits a change of a child process state, that can be:
 * - a termination of child following a process_make_exit().
 * - a termination of child following a process_make_kill().
 * - a blocking of child following a SIGSTOP signal.
 * In case of a multi-thread process, this function must be called by the main thread
 * runningin the reference cluster.
 * When a change has been observed, it returns the PID of the child process, and stores
 * in the <status> argument relevant information on the child state change.
 * The following macros can be used to extract information from status:
 * - WIFEXITED(status)   : is true if the child process terminated with an exit().
 * - WIFSIGNALED(status) : is true if the child process killed by a signal.
 * - WIFSTOPPED(status)  : is true if the child process is stopped by a signal.
 * - WEXITSTATUS(status) : returns the low-order 8 bits of the exit() argument.
 * If a parent process terminates without waiting for all child processes to terminate,
 * the remaining child processes are attached to the init process.
 * WARNING: negative values for the <pid> argument are not supported.
 ******************************************************************************************
 * @ searched_pid : searched child process identifier.
 * @ status       : [out] child termination status.
 * @ return child PID if success / return -1 if searched PID not found.
 *****************************************************************************************/
int sys_wait( uint32_t * status );

/****************************************************************************************** 
 * [40] This function implement the non-standard get_config() syscall.
 * It returns in <x_size>, <y_size>, <ncores> the hardware platform parameters.
 ******************************************************************************************
 * @ x_size   : [out] number of clusters in a row.
 * @ y_size   : [out] number of clusters in a column.
 * @ ncores   : [out] number of cores per cluster.
 * @ return 0 if success / return -1 if illegal arguments
 *****************************************************************************************/
int sys_get_config( uint32_t * x_size,
                    uint32_t * y_size,
                    uint32_t * ncores );

/****************************************************************************************** 
 * [41] This function implements the non-standard get_core() syscall.
 * It returns in <cxy> and <lid> the calling core cluster and local index.
 ******************************************************************************************
 * @ cxy      : [out] cluster identifier (fixed format)
 * @ lid      : [out] core local index in cluster.
 * @ return 0 if success / return -1 if illegal arguments
 *****************************************************************************************/
int sys_get_core( uint32_t * cxy,
                  uint32_t * lid );

/****************************************************************************************** 
 * [42] This function implements the non-standard get_cycle() syscall.
 * It returns in a 64 bits user buffer the calling core cycles count.
 * It uses both the hardware register and the core descriptor cycles count to take
 * into account a possible harware register overflow  in 32 bits architectures.
 ******************************************************************************************
 * cycle    : [out] address of buffer in user space.
 * @ return 0 if success / return -1 if illegal arguments
 *****************************************************************************************/
int sys_get_cycle( uint64_t * cycle );

/****************************************************************************************** 
 * [43] This debug function displays on the kernel terminal TXT0 an user defined string,
 * or the current state of a kernel structure, identified by the <type> argument.
 * The <arg0>, <arg1>, and <arg2> arguments depends on the structure type:
 * - DISPLAY_STRING          : an user defined string
 * - DISPLAY_VMM             : VSL and GPT for a process identified by <pid>.
 * - DISPLAY_SCHED           : all threads allocated to a scheduler <cxy> & <lid>.
 * - DISPLAY_CLUSTER_PROCESS : all processes registered in a cluster identified by <cxy>.  
 * - DISPLAY_TXT_PROCESS     : all processes registered in a cluster identified by <cxy>.  
 * - DISPLAY_VFS             : all files registered in the VFS cache. 
 * - DISPLAY_CHDEV           : all registered channel devices.
 * - DISPLAY_DQDT            : all DQDT nodes curren values.
 * - DISPLAY_BUSYLOCKS       : all busylocks taken by one thread.
 * - DISPLAY_MAPPER          : one page of a given mapper.
 ******************************************************************************************
 * type      : [in] type of display 
 * arg0      : [in] type dependant argument.
 * arg1      : [in] type dependant argument.
 * arg2      : [in] type dependant argument.
 * @ return 0 if success / return -1 if illegal arguments
 *****************************************************************************************/
int sys_display( reg_t  type,
                 reg_t  arg0,
                 reg_t  arg1,
                 reg_t  arg2 );

/****************************************************************************************** 
 * [44] This function implements the non-standard place_fork() syscall.
 * It can be used to specify the target cluster <cxy> for a new process created
 * by a subsequent fork() syscall.
 * WARNING: it must be called before each fork() syscall, as the placement specification
 *          is reset by the fork syscall. 
 ******************************************************************************************
 * @ cxy    : cluster identifier.
 * @ return 0 if success / return -1 if failure.
 *****************************************************************************************/
int sys_place_fork( uint32_t cxy );

/****************************************************************************************** 
 * [45] This function block the calling thread on the THREAD_BLOCKED_GLOBAL condition,
 * and deschedule.
 ******************************************************************************************
 * @ return 0 if success / returns -1 if failure.
 *****************************************************************************************/
int sys_thread_sleep( void );

/****************************************************************************************** 
 * [46] This function unblock the thread identified by its <trdid> from the 
 * THREAD_BLOCKED_GLOBAL condition.
 ******************************************************************************************
 * @ trdid  : target thread identifier.
 * @ return 0 if success / return -1 if failure.
 *****************************************************************************************/
int sys_thread_wakeup( trdid_t trdid );

/****************************************************************************************** 
 * [47] This debug function is used to activate / desactivate the context switches trace
 * for a core identified by the <cxy> and <lid> arguments.
 * It can be called by any other thread in the same process.
 ****************************************************************************************** 
 * @ active     : activate trace if true / desactivate trace if false.
 * @ cxy        : cluster identifier.
 * @ lid        : core local index.
 * @ returns O if success / returns -1 if failure.
 *****************************************************************************************/
int sys_trace( bool_t   active,
               cxy_t    cxy, 
               lid_t    lid );

/****************************************************************************************** 
 * [48] This function gives the process identified by the <pid> argument
 * the exclusive ownership of its TXT_TX terminal (put it in foreground).
 ******************************************************************************************
 * @ pid    : process identifier.
 * @ return 0 if success / return -1 if failure.
 *****************************************************************************************/
int sys_fg( pid_t   pid );

/****************************************************************************************** 
 * [49] This function returns a non-zero value in the <is_fg> buffer when the process
 * identified by the <pid> argument is the current TXT owner.
 ******************************************************************************************
 * @ pid      : process identifier.
 * @ is_fg    : pointer on buffer.
 * @ return 0 if success / return -1 if failure.
 *****************************************************************************************/
int sys_is_fg( pid_t      pid,
               uint32_t * is_fg );

/****************************************************************************************** 
 * [50] This function implements the exit system call terminating a POSIX process.
 * It can be called by any thread running in any cluster.
 * It uses both remote accesses to access the owner process descriptor, and the
 * RPC_PROCESS_SIGACTION to delete remote process and thread descriptors.
 * In the present implementation, this function implements actually the _exit():
 * - it does not flush open output streams.
 * - it does not close open streams.
 ******************************************************************************************
 * @ status   : terminaison status.
 *****************************************************************************************/
int sys_exit( uint32_t status );

#endif  // _SYSCALLS_H_
