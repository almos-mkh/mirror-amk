/*
 * kern/sys_getpid.c - Kernel function implementing the "get_pid" system call.
 * 
 * Author     Alain Greiner  (2016,2017, 2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <thread.h>
#include <process.h>
#include <syscalls.h>

//////////////////////
int sys_getpid( void )
{
    thread_t  * this    = CURRENT_THREAD;
    process_t * process = this->process;

#if (DEBUG_SYS_GETPID || CONFIG_INSTRUMENTATION_SYSCALLS)
uint64_t     tm_start = hal_get_cycles();
#endif

#if DEBUG_SYS_GETPID
tm_start = hal_get_cycles();
if( DEBUG_SYS_FG < tm_start )
printk("\n[DBG] %s : thread %x in process %x enter / cycle %d\n",
__FUNCTION__ , this->trdid , process->pid, (uint32_t)tm_start );
#endif

    // get pid value from local process descriptor    
    pid_t pid = process->pid;

#if (DEBUG_SYS_GETPID || CONFIG_INSTRUMENTATION_SYSCALLS)
uint64_t     tm_end = hal_get_cycles();
#endif

#if DEBUG_SYS_GETPID
tm_end = hal_get_cycles();
if( DEBUG_SYS_GETPID < tm_end )
printk("\n[DBG] %s : thread %x in process %x exit / cycle %d\n",
__FUNCTION__, this->trdid, process->pid, (uint32_t)tm_end );
#endif

#if CONFIG_INSTRUMENTATION_SYSCALLS
hal_atomic_add( &syscalls_cumul_cost[SYS_GETPID] , tm_end - tm_start );
hal_atomic_add( &syscalls_occurences[SYS_GETPID] , 1 );
#endif

	return pid;

}  // end sys_getpid()
