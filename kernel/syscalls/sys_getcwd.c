/*
 * sys_getcwd.c - kernel function implementing the "getcwd" syscall.
 * 
 * Author    Alain Greiner (2016,2017,2018)
 *
 * Copyright (c)  UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <kernel_config.h>
#include <hal_kernel_types.h>
#include <hal_uspace.h>
#include <hal_special.h>
#include <errno.h>
#include <vfs.h>
#include <vmm.h>
#include <process.h>
#include <thread.h>
#include <printk.h>

#include <syscalls.h>

///////////////////////////////////
int sys_getcwd ( char     * buffer,
                 uint32_t   nbytes )
{
	error_t       error;
    vseg_t      * vseg;
    char        * first;                   // first character valid in buffer

    char          kbuf[CONFIG_VFS_MAX_PATH_LENGTH]; 

	thread_t  * this    = CURRENT_THREAD;
    process_t * process = this->process;

#if (DEBUG_SYS_GETCWD || CONFIG_INSTRUMENTATION_SYSCALLS)
uint64_t     tm_start = hal_get_cycles();
#endif

    // check buffer size
	if( nbytes < CONFIG_VFS_MAX_PATH_LENGTH )
	{

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : buffer too small for thread %x,%x]\n",
__FUNCTION__ , process->pid, this->trdid );
#endif
		this->errno = EINVAL;
        return -1;
	}

    // check buffer in user space
    error = vmm_get_vseg( process, (intptr_t)buffer , &vseg );

	if( error )
	{

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : user buffer unmapped %x for thread[%x,%x]\n",
__FUNCTION__ , (intptr_t)buffer , process->pid, this->trdid );
#endif
		this->errno = EINVAL;
        return -1;
	}

#if DEBUG_SYS_GETCWD
if( DEBUG_SYS_GETCWD < tm_start )
printk("\n[%s] thread[%x,%x] enter / cycle %d\n",
__FUNCTION__, process->pid, this->trdid, (uint32_t)tm_start );
#endif

    // get extended pointer on CWD inode from the reference process
    xptr_t      ref_xp  = process->ref_xp;
    process_t * ref_ptr = GET_PTR( ref_xp );
    cxy_t       ref_cxy = GET_CXY( ref_xp );
    xptr_t      cwd_xp  = hal_remote_l64( XPTR( ref_cxy , &ref_ptr->cwd_xp ) );

    // call relevant VFS function
	error = vfs_get_path( cwd_xp,
                          kbuf, 
                          &first, 
                          CONFIG_VFS_MAX_PATH_LENGTH );

    // copy kernel buffer to user space
    hal_strcpy_to_uspace( buffer , first , CONFIG_VFS_MAX_PATH_LENGTH );

    hal_fence();

#if (DEBUG_SYS_GETCWD || CONFIG_INSTRUMENTATION_SYSCALLS)
uint64_t     tm_end = hal_get_cycles();
#endif

#if DEBUG_SYS_GETCWD
if( DEBUG_SYS_GETCWD < tm_end )
printk("\n[%s] thread[%x,%x] exit / cycle %d\n",
__FUNCTION__, process->pid, this->trdid, (uint32_t)tm_end );
#endif
 
#if CONFIG_INSTRUMENTATION_SYSCALLS
hal_atomic_add( &syscalls_cumul_cost[SYS_GETCWD] , tm_end - tm_start );
hal_atomic_add( &syscalls_occurences[SYS_GETCWD] , 1 );
#endif

	return 0;

}  // end sys_getcwd()
