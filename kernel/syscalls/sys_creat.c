/*
 * sys_creat.c - create a file
 * 
 * Author    Alain Greiner (2016,2017,2017,2019)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-kernel.
 *
 * ALMOS-kernel is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-kernel is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-kernel; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <vfs.h>
#include <syscalls.h>
#include <shared_syscalls.h>

////////////////////////////////////
int sys_creat( char     * pathname,
               uint32_t   mode )
{ 
	uint32_t    flags = O_CREAT;

    return sys_open( pathname , flags , mode );
}
