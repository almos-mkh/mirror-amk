/*
 * times.c: thread sus/usr time
 * 
 * Copyright (c) 2015 UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-kernel.
 *
 * ALMOS-kernel is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-kernel is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-kernel; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */



#include <cpu.h>
#include <thread.h>
#include <time.h>

int sys_times(struct tms *utms)
{
#ifdef CONFIG_THREAD_TIME_STAT
	struct thread_s *this;
	struct tms _tms;
	error_t err;

	if((utms == NULL) || NOT_IN_USPACE((uint_t)utms) || 
		NOT_IN_USPACE((uint_t)utms+sizeof(struct tms)))
	{
		return EINVAL;
	}

	this = current_thread;

	_tms.tms_utime = this->info.tm_usr;
	_tms.tms_stime = this->info.tm_sys;
	_tms.tms_cutime = 0;
	_tms.tms_cstime = 0;
	
	err =  cpu_copy_to_uspace(utms, &_tms, sizeof(struct tms));

	this->info.errno = err;
	if(err)	return -1;
	return 0; 
#else
	this->info.errno = ENOTSUPPORTED;
	return -1;

#endif
}
