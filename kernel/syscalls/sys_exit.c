/*
 * sys_exit.c - Kernel function implementing the "exit" system call.
 * 
 * Author    Alain Greiner (2016,2017,2018)
 *
 * Copyright (c)  UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <kernel_config.h>
#include <hal_kernel_types.h>
#include <hal_irqmask.h>
#include <errno.h>
#include <thread.h>
#include <printk.h>
#include <process.h>
#include <shared_syscalls.h>
#include <cluster.h>
#include <rpc.h>

#include <syscalls.h>

///////////////////////////////
int sys_exit( uint32_t status )
{
    xptr_t      owner_xp;          // extended pointer on owner process
    cxy_t       owner_cxy;         // owner process cluster
    process_t * owner_ptr;         // local pointer on owner process
    thread_t  * main_ptr;          // local pointer on process main thread
    xptr_t      parent_xp;         // extended pointer on parent process
    cxy_t       parent_cxy;        // parent process cluster
    process_t * parent_ptr;        // local pointer on parent process
    thread_t  * parent_main_ptr;   // local pointer on parent main thread 
    xptr_t      parent_main_xp;    // extended pointer on parent main thread
    uint32_t    term_state;        // termination status for owner process

    thread_t  * this    = CURRENT_THREAD;
    process_t * process = this->process;
    pid_t       pid     = process->pid;

#if DEBUG_SYS_EXIT
uint64_t    tm_start;
uint64_t    tm_end;
tm_start = hal_get_cycles();
if( DEBUG_SYS_EXIT < tm_start )
printk("\n[%s] thread[%x,%x] enter / status %x / cycle %d\n",
__FUNCTION__, process->pid, this->trdid , status , (uint32_t)tm_start );
#endif

    // get owner process descriptor pointers 
    owner_xp  = process->owner_xp;
    owner_cxy = GET_CXY( owner_xp );
    owner_ptr = GET_PTR( owner_xp );

#if (DEBUG_SYS_EXIT & 1)
if( DEBUG_SYS_EXIT < tm_start )
printk("\n[%s] thread[%x,%x] get owner process in cluster %x\n",
__FUNCTION__, process->pid, this->trdid, owner_cxy );
#endif

    // get local pointer on the main thread
    main_ptr  = hal_remote_lpt( XPTR( owner_cxy , &owner_ptr->th_tbl[0] ) );

    // get parent process descriptor pointers
    parent_xp  = process->parent_xp;
    parent_cxy = GET_CXY( parent_xp );
    parent_ptr = GET_PTR( parent_xp );

#if (DEBUG_SYS_EXIT & 1)
if( DEBUG_SYS_EXIT < tm_start )
printk("\n[%s] thread[%x,%x] get parent process in cluster %x\n",
__FUNCTION__, process->pid, this->trdid, parent_cxy );
#endif

    // get pointers on the parent process main thread
    parent_main_ptr = hal_remote_lpt( XPTR( parent_cxy , &parent_ptr->th_tbl[0] ) ); 
    parent_main_xp  = XPTR( parent_cxy , parent_main_ptr );

    // remove process from TXT list
    process_txt_detach( owner_xp );

#if( DEBUG_SYS_EXIT & 1)
if( DEBUG_SYS_EXIT < tm_start )
printk("\n[%s] thread[%x,%x] detached process from TXT\n",
__FUNCTION__, process->pid, this->trdid );
#endif

    // mark for delete all process threads in all clusters, 
    // but the main thread and this calling thread 
    process_sigaction( process->pid , DELETE_ALL_THREADS );

#if( DEBUG_SYS_EXIT & 1)
if( DEBUG_SYS_EXIT < tm_start )
printk("\n[%s] thread[%x,%x] deleted all threads but itself\n",
__FUNCTION__, process->pid, this->trdid );
#endif

    // mark for delete the calling thread when it is not the main
    if( (owner_cxy != local_cxy) || (main_ptr != this) )
    {

#if( DEBUG_SYS_EXIT & 1)
if( tm_start > DEBUG_SYS_EXIT )
printk("\n[%u] thread[%x,%x] marked iself for delete\n",
__FUNCTION__, process->pid, this->trdid );
#endif
        thread_delete( XPTR( local_cxy , this ) , pid , true );
    }

    // block this main thread 
    thread_block( XPTR( owner_cxy , main_ptr ) , THREAD_BLOCKED_GLOBAL );

#if( DEBUG_SYS_EXIT & 1)
if( tm_start > DEBUG_SYS_EXIT )
printk("\n[%s] thread[%x,%x] blocked main thread\n",
__FUNCTION__, process->pid, this->trdid );
#endif

    // atomically update owner process descriptor term_state to ask
    // the parent process sys_wait() function to delete the main thread 
    term_state = (status & 0xFF) | PROCESS_TERM_EXIT;
    hal_remote_atomic_or( XPTR( owner_cxy , &owner_ptr->term_state ) , term_state );

#if( DEBUG_SYS_EXIT & 1)
if( tm_start > DEBUG_SYS_EXIT )
printk("\n[%s] thread[%x,%x] set exit status %x in owner process\n",
__FUNCTION__, process->pid, this->trdid, term_state );
#endif

    // unblock the parent process main thread 
    thread_unblock( parent_main_xp , THREAD_BLOCKED_WAIT );

#if( DEBUG_SYS_EXIT & 1)
if( tm_start > DEBUG_SYS_EXIT )
printk("\n[%s] thread[%x,%x] unblocked parent main thread in process %x\n",
__FUNCTION__ , process->pid, this->trdid,
hal_remote_l32( XPTR( parent_cxy , &parent_ptr->pid) ) );
#endif

    hal_fence();

#if DEBUG_SYS_EXIT
tm_end = hal_get_cycles();
if( DEBUG_SYS_EXIT < tm_end )
printk("\n[%s] thread[%x,%x] exit / status %x / cost = %d / cycle %d\n",
__FUNCTION__, process->pid, this->trdid, status,
(uint32_t)(tm_end - tm_start), (uint32_t)tm_end );
#endif

    // this thread deschedule
    sched_yield( "process exit" );

	return 0;

}  // end sys_exit()

