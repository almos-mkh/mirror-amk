/*
 * sys_thread_wakeup.c - wakeup all indicated threads
 * 
 * Author    Alain Greiner (2016,2017)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <thread.h>
#include <printk.h>
#include <process.h>
#include <errno.h>

#include <syscalls.h>

//////////////////////////////////////
int sys_thread_wakeup( trdid_t trdid )
{
	thread_t  * this    = CURRENT_THREAD;
    process_t * process = this->process;

#if DEBUG_SYS_THREAD_WAKEUP
uint64_t     tm_start;
uint64_t     tm_end;
tm_start = hal_get_cycles();
if( DEBUG_SYS_THREAD_WAKEUP < tm_start )
printk("\n[DBG] %s : thread %x in process enter to activate thread %x / cycle %d\n",
__FUNCTION__, this->trdid, process->pid, trdid, (uint32_t)tm_start );
#endif

    // get target thread ltid and cxy
    ltid_t   target_ltid = LTID_FROM_TRDID( trdid );
    cxy_t    target_cxy  = CXY_FROM_TRDID( trdid );

    // check trdid argument
	if( (target_ltid >= CONFIG_THREADS_MAX_PER_CLUSTER) || cluster_is_undefined( target_cxy ) )  
	{

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : thread %x in process %x / illegal trdid argument %x\n",
__FUNCTION__, this->trdid, process->pid, trdid );
#endif
		this->errno = EINVAL;
		return -1;
	}

    // get extended pointer on target thread
    xptr_t thread_xp = thread_get_xptr( process->pid , trdid );

    if( thread_xp == XPTR_NULL )
    {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : thread %x in process %x cannot find thread %x/n",
__FUNCTION__ , this->trdid, process->pid, trdid );
#endif
        CURRENT_THREAD->errno = EINVAL;
        return -1;
    }

    // unblock target thread
    thread_unblock( thread_xp , THREAD_BLOCKED_GLOBAL );

#if DEBUG_SYS_THREAD_WAKEUP
tm_end = hal_get_cycles();
if( DEBUG_SYS_THREAD_WAKEUP < tm_end )
printk("\n[DBG] %s : thread %x in process %x exit / thread %x activated / cycle %d\n",
__FUNCTION__ , this->trdid, process->pid, trdid, (uint32_t)tm_end );
#endif

    return 0;

}  // end sys_thread_wakeup()
