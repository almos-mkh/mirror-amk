/*
 * sys_thread_cancel.c - terminate execution of an user thread.
 * 
 * Authors   Alain Greiner (2016,2017, 2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_irqmask.h>
#include <hal_remote.h>
#include <hal_special.h>
#include <thread.h>
#include <errno.h>
#include <printk.h>

#include <syscalls.h>

//////////////////////////////////////
int sys_thread_cancel( trdid_t trdid )
{
    reg_t        save_sr;       // required to enable IRQs
    xptr_t       target_xp;     // target thread extended pointer
    cxy_t        target_cxy;    // target thread cluster identifier
    ltid_t       target_ltid;   // target thread local index
    xptr_t       owner_xp;      // extended pointer on owner process
    cxy_t        owner_cxy;     // process owner cluster identifier

    // get killer thread pointers
	thread_t   * this    = CURRENT_THREAD;
    process_t  * process = this->process;
    pid_t        pid     = process->pid;

#if DEBUG_SYS_THREAD_CANCEL
uint64_t     tm_start;
uint64_t     tm_end;
tm_start = hal_get_cycles();
if( DEBUG_SYS_THREAD_CANCEL < tm_start )
printk("\n[DBG] %s : thread %x enter to kill thread %x in process %x / cycle %d\n",
__FUNCTION__, this , trdid , pid , (uint32_t)tm_start );
#endif

    // get extended pointer on target thread
	target_xp  = thread_get_xptr( pid , trdid );

    // check target_xp
    if( target_xp == XPTR_NULL )
    {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : target thread %x not found\n", __FUNCTION__, trdid );
#endif
        this->errno = EINVAL;
        return -1;
    }

    // get process owner cluster
    owner_xp  = process->owner_xp;
    owner_cxy = GET_CXY( owner_xp );

    // get target thread ltid and cluster
    target_cxy  = CXY_FROM_TRDID( trdid );
    target_ltid = LTID_FROM_TRDID( trdid );

    // If target thread is the main thread, the process must be deleted, 
    // This require synchronisation with parent process
    if( (target_cxy == owner_cxy) && (target_ltid == 0) )
    {
        // mark for delete all threads but the main 
        hal_enable_irq( &save_sr );
        process_sigaction( pid , DELETE_ALL_THREADS );
        hal_restore_irq( save_sr );

        // remove process from TXT list
        process_txt_detach( owner_xp );

        // block the main thread 
        thread_block( XPTR( local_cxy ,this ) , THREAD_BLOCKED_GLOBAL );

        // atomically update owner process descriptor term_state to ask
        // the parent process sys_wait() function to delete the main thread 
        hal_remote_atomic_or( XPTR( local_cxy , &process->term_state ) ,
                              PROCESS_TERM_EXIT );
    }
    else
    {
        // block target thread and mark it for delete
        thread_delete( target_xp , pid , false );
    }

#if DEBUG_SYS_THREAD_CANCEL
tm_end = hal_get_cycles();
if( DEBUG_SYS_THREAD_CANCEL < tm_end )
printk("\n[DBG] %s : thread %x exit after kill thread %x in process %x / cycle %d\n",
__FUNCTION__, this , trdid , pid , (uint32_t)tm_end );
#endif

	return 0;

}  // end sys_thread_cancel()
