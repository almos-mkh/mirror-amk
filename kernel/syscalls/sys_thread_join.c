/*
 * sys_thread_join.c - passive wait on the end of a given thread.
 * 
 * Authors    Alain Greiner (2016,2017)
 *
 * Copyright (c) 2011,2012 UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_remote.h>
#include <hal_special.h>
#include <hal_irqmask.h>
#include <thread.h>
#include <vmm.h>
#include <scheduler.h>
#include <errno.h>
#include <printk.h>
#include <remote_busylock.h>

#include <syscalls.h>

///////////////////////////////////////
int sys_thread_join ( trdid_t    trdid,
                      void    ** exit_value )
{
    reg_t         save_sr;
    xptr_t        target_xp;
    thread_t    * target_ptr;
    cxy_t         target_cxy;
    ltid_t        target_ltid;
    xptr_t        target_join_lock_xp;
    xptr_t        target_flags_xp;
    xptr_t        target_blocked_xp;
    xptr_t        target_join_xp_xp;
    xptr_t        killer_xp;
    xptr_t        joining_xp;
    thread_t    * joining_ptr;
    process_t   * process;

    // get joining thread pointers
	joining_ptr = CURRENT_THREAD;
    joining_xp  = XPTR( local_cxy , joining_ptr );
    process     = joining_ptr->process;

    // get target thread ltid and cxy
    target_ltid = LTID_FROM_TRDID( trdid );
    target_cxy  = CXY_FROM_TRDID( trdid );

#if DEBUG_SYS_THREAD_JOIN
uint64_t     tm_start;
uint64_t     tm_end;
tm_start = hal_get_cycles();
if( DEBUG_SYS_THREAD_JOIN < tm_start )
printk("\n[DBG] %s : joining thread[%x,%x] enter / target thread[%x,%x] / cycle %d\n",
__FUNCTION__ , process->pid, joining_ptr->trdid,
process->pid, trdid, (uint32_t)tm_start );
#endif

    // check trdid argument
	if( (target_ltid >= CONFIG_THREADS_MAX_PER_CLUSTER) || cluster_is_undefined(target_cxy) )
	{

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : illegal trdid argument %x\n",
__FUNCTION__, trdid );
#endif
		joining_ptr->errno = EINVAL;
		return -1;
	}

    // check exit_value argument
	if( exit_value != NULL )
	{

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : exit_value argument must be NULL\n",
__FUNCTION__ );
#endif
		joining_ptr->errno = EINVAL;
		return -1;
	}

    // check target thread != this thread
    if( joining_ptr->trdid == trdid )
    {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : this thread (%x) == target thread(%x)\n",
__FUNCTION__, joining_ptr->trdid, trdid );
#endif
        joining_ptr->errno = EDEADLK;
        return -1;
    }

    // get pointers on target thread
	target_xp  = thread_get_xptr( process->pid , trdid );
    target_ptr = GET_PTR( target_xp );

    if( target_xp == XPTR_NULL )
    {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : target thread %x not found\n",
__FUNCTION__, trdid );
#endif
        joining_ptr->errno = ESRCH;
        return -1;
    }

    // get extended pointers on various fields in target thread
    target_join_lock_xp = XPTR( target_cxy , &target_ptr->join_lock );
    target_flags_xp     = XPTR( target_cxy , &target_ptr->flags );
    target_blocked_xp   = XPTR( target_cxy , &target_ptr->blocked );
    target_join_xp_xp   = XPTR( target_cxy , &target_ptr->join_xp );

    // check target thread joinable
    if( (hal_remote_l32( target_flags_xp ) & THREAD_FLAG_DETACHED) != 0 )
    {

#if DEBUG_SYSCALLS_ERROR
printk("\n[ERROR] in %s : target thread %x not joinable\n", __FUNCTION__, trdid );
#endif
        joining_ptr->errno = EINVAL; 
        return -1;
    }

    // mask IRQs
    hal_disable_irq( &save_sr );

    // get the lock protecting the join in target thread
    remote_busylock_acquire( target_join_lock_xp );

    // test the kill_done flag from the target thread
    if( hal_remote_l32( target_flags_xp ) & THREAD_FLAG_KILL_DONE )  // killer thread is first
    {
        // get pointers on killer thread
        killer_xp  = (xptr_t)hal_remote_l64( target_join_xp_xp );

        // reset the kill_done flag in target thread
        hal_remote_atomic_and( target_flags_xp , ~THREAD_FLAG_KILL_DONE );

        // unblock the killer thread
        thread_unblock( killer_xp , THREAD_BLOCKED_JOIN );

        // release the lock protecting join      
        remote_busylock_release( target_join_lock_xp );

#if DEBUG_SYS_THREAD_JOIN
tm_end = hal_get_cycles();
if( DEBUG_SYS_THREAD_JOIN < tm_end )
printk("\n[DBG] %s : joining thread[%x,%x] exit / target thread[%x,%x] completed / cycle %d\n",
__FUNCTION__, process->pid, joining_ptr->trdid, process->pid, trdid, (uint32_t)tm_end );
#endif

    }
    else                                                          // joining thread is first
    {
        // set the join_done flag in target thread
        hal_remote_atomic_or( target_flags_xp , THREAD_FLAG_JOIN_DONE );

        // block joining thread on BLOCKED_JOIN
        thread_block( joining_xp , THREAD_BLOCKED_JOIN );

        // register the joining thread extended pointer in target thread
        hal_remote_s64( target_join_xp_xp , joining_xp );

        // release the lock protecting the join      
        remote_busylock_release( target_join_lock_xp );

#if DEBUG_SYS_THREAD_JOIN 
if( DEBUG_SYS_THREAD_JOIN < tm_start )
printk("\n[DBG] %s : joining thread[%x,%x] deschedules / target thread[%x,%x] not completed\n",
__FUNCTION__ , process->pid, joining_ptr->trdid, process->pid, trdid );
#endif
        // deschedule
        sched_yield( "joining thread waiting killer thread" );
    
#if DEBUG_SYS_THREAD_JOIN 
tm_end = hal_get_cycles();
if( DEBUG_SYS_THREAD_JOIN < tm_end )
printk("\n[DBG] %s : joining thread[%x,%x] exit / target thread[%x,%x] completed / cycle %d\n",
__FUNCTION__ , process->pid, joining_ptr->trdid, process->pid, trdid, (uint32_t)tm_end );
#endif

    }

    // restore IRQs
    hal_restore_irq( save_sr );

    return 0;

}  // end sys_thread_join()
