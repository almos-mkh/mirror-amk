/*
 * sys_thread_detach.c - detach a joinable thread
 * 
 * Authors   Alain Greiner (2016,2017)
 *
 * Copyright (c) 2011,2012 UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_remote.h>
#include <hal_special.h>
#include <thread.h>
#include <errno.h>
#include <printk.h>

#include <syscalls.h>

//////////////////////////////////////
int sys_thread_detach( trdid_t trdid )
{
    xptr_t       target_xp;
	thread_t   * target_ptr;
    cxy_t        target_cxy;
    ltid_t       target_ltid;
    uint32_t     flags;

	thread_t   * this    = CURRENT_THREAD;
    process_t  * process = this->process;

    // get target thread ltid and cxy
    target_ltid = LTID_FROM_TRDID( trdid );
    target_cxy  = CXY_FROM_TRDID( trdid );

    // check trdid argument
	if( (target_ltid >= CONFIG_THREADS_MAX_PER_CLUSTER) || cluster_is_undefined( target_cxy ) )  
	{
        printk("\n[ERROR] in %s : illegal trdid argument\n", __FUNCTION__ );
		this->errno = EINVAL;
		return -1;
	}

    // get extended pointer on target thread
	target_xp  = thread_get_xptr( process->pid , trdid );

    if( target_xp == XPTR_NULL )
    {
        printk("\n[ERROR] in %s : target thread not found\n", __FUNCTION__ );
        this->errno = ESRCH;
        return -1;
    }

    // get local pointer on target thread
    target_ptr = (thread_t *)GET_PTR( target_xp );

    // get target thread flags
    flags = hal_remote_l32( XPTR( target_cxy , &target_ptr->flags ) );

    // check target thread joinable
    if( flags & THREAD_FLAG_DETACHED )
    {
        printk("\n[ERROR] in %s : target thread not joinable\n", __FUNCTION__ );
        this->errno = EINVAL; 
        return -1;
    }

    // atomically set the thread DETACHED flag
    hal_remote_atomic_or( XPTR( target_cxy , &target_ptr->flags ) , THREAD_FLAG_DETACHED );

	return 0;

}  // end sys_thread_detach()
