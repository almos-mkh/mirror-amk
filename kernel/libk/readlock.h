/*
 * readlock.h - kernel readlock definition
 * 
 * Author   Alain Greiner    (2016)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-kernel; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _READLOCK_H_
#define _READLOCK_H_

#include <kernel_config.h>
#include <hal_kernel_types.h>

/*******************************************************************************************
 * This structure defines a local readlock, that supports several simultaneous read 
 * accesses, but only one write access.
 * - A reader does not take the exclusive ownership of the lock, but it must atomically
 * increment the registered readers count in the readlock_t structure before starting
 * access and decrement it when access is completed.
 * - A writer must take exclusive ownership before access, and must wait completion
 * of all current read access before starting its own access.
 * TODO : add a "list" field to register a taken lock in the owner thread list.
 ******************************************************************************************/

/****     Forward declarations    ****/

struct thread_s;

/*******************************************************************************************
 * This structure defines a local readlock.
 ******************************************************************************************/

typedef struct readlock_s
{
	volatile uint32_t   taken;             /*! state : free if zero / taken if non zero  */
	volatile uint32_t   count;             /*! current number of readers                 */
	struct thread_s   * owner;             /*! pointer on curent owner thread            */
}
readlock_t;

/*******************************************************************************************
 * This function initializes a local readlock.
 *******************************************************************************************
 * @ lock       : pointer on readlock
 ******************************************************************************************/
void readlock_init( readlock_t * lock );

/*******************************************************************************************
 * This function get access to a local readlock for a reader.
 *******************************************************************************************
 * @ lock       : pointer on readlock
 ******************************************************************************************/
void readlock_rd_lock( readlock_t * lock );

/*******************************************************************************************
 * This function get access to a local readlock for a writer.
 *******************************************************************************************
 * @ lock       : pointer on readlock
 ******************************************************************************************/
void readlock_wr_lock( readlock_t * lock );

/*******************************************************************************************
 * This function unlocks a local readlock for a reader.
 *******************************************************************************************
 * @ lock       : pointer on readlock
 ******************************************************************************************/
void readlock_rd_unlock( readlock_t * lock );

/*******************************************************************************************
 * This function unlocks a local readlock for a writer.
 *******************************************************************************************
 * @ lock       : pointer on readlock
 ******************************************************************************************/
void readlock_wr_unlock( readlock_t * lock );


#endif	/* _READLOCK_H_ */
