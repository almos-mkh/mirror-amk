/*
 * remote_sem.h - POSIX unnamed semaphore definition.
 * 
 * Author   Alain Greiner (2016,2017,2018)
 *
 * Copyright (c)  UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
e* the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _SEMAPHORE_H_
#define _SEMAPHORE_H_

#include <hal_kernel_types.h>
#include <xlist.h>
#include <remote_busylock.h>

/********************************************************************************************n
 * This is the kernel representation of an user level, POSIX compliant unnamed semaphore.
 *
 * It can be used by muti-threaded user applications to synchronise user threads
 * running in different clusters.
 *
* A semaphore is declared by a given user process as a "sem_t" global variable.
 * The user "sem_t" structure is implemented as an unsigned long, but the value is not
 * used by the kernel. ALMOS-MKH uses only the sem_t virtual address as an identifier.
 * For each user semaphore, ALMOS-MKH creates a kernel "remote_sem_t" structure,
 * dynamically allocated in the reference cluster by the remote_sem_create() function, 
 * and destroyed by the remote_sem_destroy() function, using RPC if the calling thread is not
 * running in the reference cluster.
 *
 * The threads accessing the semaphore with the sem_get_value(), sem_wait(), sem_post()
 * syscalls can be running in any cluster, as these syscalls access the "remote_sem_t"
 * structure with remote_read / remote_write access functions.
 ********************************************************************************************/


/*********************************************************************************************
 * This structure defines the kernel implementation of an user level semaphore.
 ********************************************************************************************/

typedef struct remote_sem_s
{
	remote_busylock_t lock;          /*! lock protecting the semaphore state                */
	uint32_t          count;         /*! current value                                      */
    intptr_t          ident;         /*! virtual address in user space == identifier        */
    xlist_entry_t     root;          /*! root of waiting thread queue                       */
    xlist_entry_t     list;          /*! member of list of semaphores in same process       */
}
remote_sem_t;


/*********************************************************************************************
 * This function returns an extended pointer on the remote semaphore identified
 * by its virtual address in a given user process. It makes an associative search, 
 * scanning the list of user semaphores rooted in the reference process descriptor.
 *********************************************************************************************
 * @ process  : pointer on local process descriptor.
 * @ ident    : semaphore virtual address, used as identifier.
 * @ returns extended pointer on semaphore if success / returns XPTR_NULL if not found. 
 ********************************************************************************************/
xptr_t remote_sem_from_ident( intptr_t  ident );

/*********************************************************************************************
 * This function implements the SEM_INIT operation.
 * It allocates memory for a remote semaphore in reference cluster, using a RPC if required, 
 * and initializes it, using remote accesses from values defined by the <vaddr> and <value>
 * arguments. It register this semaphore in the calling process list of user semaphores.
 *********************************************************************************************
 * @ vaddr     : [in] semaphore virtual address, used as identifier.
 * @ value     : [in] semaphore initial value.
 * @ returns 0 if success / returns -1 if no memory. 
 ********************************************************************************************/
error_t remote_sem_create( intptr_t  vaddr,
                           uint32_t  value );
 
/****************************yy***************************************************************
 * This function implements the SEM_DESTROY operation.
 * It desactivates the semaphore, and releases the physical memory allocated in the 
 * reference cluster, using a RPC if required.
 *********************************************************************************************
 * @ sem_xp   : [in] extended pointer on semaphore. 
 ********************************************************************************************/
void remote_sem_destroy( xptr_t sem_xp );

/****************************yy***************************************************************
 * This blocking function implements the SEM_WAIT operation.
 * - It returns only when the remote semaphore has a non-zero value, and has been 
 *   atomically decremented.
 * - If the semaphore has a zero value, it register the calling thread in the semaphore
 *   waiting queue, block the thread, and yield.
 *********************************************************************************************
 * @ sem_xp   : [in] extended pointer on semaphore. 
 ********************************************************************************************/
void remote_sem_wait( xptr_t sem_xp );

/****************************yy***************************************************************
 * This function implements the SEM_POST operation.
 * - It atomically increments the remote semaphore. 
 * - If the waiting queue is not empty, it wakes up all waiting thread.
 *********************************************************************************************
 * @ sem_xp   : [in] extended pointer on semaphore. 
 ********************************************************************************************/
void remote_sem_post( xptr_t sem_xp );

/****************************yy***************************************************************
 * This function implements the SEM_GETVALUE operation.
 * It returns in the <data> buffer the semaphore current value. 
 *********************************************************************************************
 * @ sem_xp   : [in]  extended pointer on semaphore. 
 * @ data     : [out] local pointer on buffer for returned value.
 ********************************************************************************************/
void remote_sem_get_value( xptr_t      sem_xp,
                           uint32_t  * data );


#endif	/* _SEMAPHORE_H_ */
