/*
 * barrier.c - Busy-waiting, local, kernel barrier implementaion 
 * 
 * Author   Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_remote.h>
#include <hal_atomic.h>
#include <barrier.h>

//////////////////////////////////////////////
inline void barrier_wait( barrier_t * barrier,
                          uint32_t    count ) 
{
    uint32_t  expected;

    // get barrier sense value
    uint32_t sense = barrier->sense;

	// compute expected value
    if ( sense == 0 ) expected = 1;
    else              expected = 0;

    // atomically increment current 
    uint32_t current = hal_atomic_add( &barrier->current , 1 );

    // last task reset current and toggle sense
    if( current == (count-1) ) 
    {
        barrier->current = 0;  
        barrier->sense   = expected;  
    }
    else   // other tasks poll sense 
    {
        while( barrier->sense != expected ) asm volatile ("nop");
    }
}


