/*
 * rwlock.c - kernel local read/write lock implementation.
 * 
 * Author  Alain Greiner     (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <kernel_config.h>
#include <hal_kernel_types.h>
#include <hal_atomic.h>
#include <hal_special.h>
#include <hal_irqmask.h>
#include <thread.h>
#include <printk.h>
#include <rwlock.h>

//////////////////////////////////////////////////////////////////////////////
//                Extern global variables
//////////////////////////////////////////////////////////////////////////////

extern char * lock_type_str[];          // allocated in kernel_init.c


//////////////////////////////////
void rwlock_init( rwlock_t * lock,
                  uint32_t   type )
{  
	lock->taken   = 0;
    lock->count   = 0;

    list_root_init( &lock->rd_root );
    list_root_init( &lock->wr_root );

    busylock_init( &lock->lock , type );

#if DEBUG_RWLOCK_TYPE
thread_t * this = CURRENT_THREAD;
if( DEBUG_RWLOCK_TYPE == type )
printk("\n[%s] thread[%x,%x] initialise lock %s [%x,%x]\n",
__FUNCTION__, this->process->pid, this->trdid,
lock_type_str[type], local_cxy, lock );
#endif

}

/////////////////////////////////////////
void rwlock_rd_acquire( rwlock_t * lock )
{
    thread_t * this = CURRENT_THREAD;

    // check calling thread can yield
    thread_assert_can_yield( this , __FUNCTION__ );

    // get busylock
    busylock_acquire( &lock->lock );

#if DEBUG_RWLOCK_TYPE
uint32_t lock_type = lock->lock.type;
#endif

    // block and deschedule if lock already taken
    while( lock->taken )
    {

#if DEBUG_RWLOCK_TYPE
if( (DEBUG_RWLOCK_TYPE == lock_type) || (DEBUG_RWLOCK_TYPE == 1000) )
printk("\n[%s] thread[%x,%x] READ BLOCK on rwlock %s [%x,%x] / taken %d / count %d\n",
__FUNCTION__, this->process->pid, this->trdid, 
lock_type_str[lock_type], local_cxy, lock, lock->taken, lock->count );
#endif
        // register reader thread in waiting queue
        list_add_last( &lock->rd_root , &this->wait_list );

        // block reader thread
        thread_block( XPTR( local_cxy , this ) , THREAD_BLOCKED_LOCK );
        
        // release busylock
        busylock_release( &lock->lock );

        // deschedule
        sched_yield("reader wait rwlock");
        
        // get busylock
        busylock_acquire( &lock->lock );
    }

    // increment number of readers
    lock->count++;

#if DEBUG_RWLOCK_TYPE
if( (DEBUG_RWLOCK_TYPE == lock_type) || (DEBUG_RWLOCK_TYPE == 1000) )
printk("\n[%s] thread[%x,%x] READ ACQUIRE rwlock %s [%x,%x] / taken %d / count %d\n",
__FUNCTION__, this->process->pid, this->trdid, 
lock_type_str[lock_type], local_cxy, lock, lock->taken, lock->count );
#endif

    // release busylock 
    busylock_release( &lock->lock );

}  // end rwlock_rd_acquire() 

/////////////////////////////////////////
void rwlock_wr_acquire( rwlock_t * lock )
{
    thread_t * this = CURRENT_THREAD;

    // check calling thread can yield
    thread_assert_can_yield( this , __FUNCTION__ );

    // get busylock
    busylock_acquire( &lock->lock );

#if DEBUG_RWLOCK_TYPE
uint32_t lock_type = lock->lock.type;
#endif

    // block and deschedule if lock already taken or existing read access
    while( lock->taken || lock->count )
    {

#if DEBUG_RWLOCK_TYPE
if( (DEBUG_RWLOCK_TYPE == lock_type) || (DEBUG_RWLOCK_TYPE == 1000) )
printk("\n[%s] thread[%x,%x] WRITE BLOCK on rwlock %s [%x,%x] / taken %d / count %d\n",
__FUNCTION__, this->process->pid, this->trdid, 
lock_type_str[lock_type], local_cxy, lock, lock->taken, lock->count );
#endif
        // register writer in waiting queue
        list_add_last( &lock->wr_root , &this->wait_list );

        // block writer thread
        thread_block( XPTR( local_cxy , this ) , THREAD_BLOCKED_LOCK );
        
        // release busylock
        busylock_release( &lock->lock );

        // deschedule
        sched_yield("writer wait rwlock");
        
        // get busylock
        busylock_acquire( &lock->lock );
    }

    // take the rwlock
    lock->taken = 1;

#if DEBUG_RWLOCK_TYPE
if( (DEBUG_RWLOCK_TYPE == lock_type) || (DEBUG_RWLOCK_TYPE == 1000) )
printk("\n[%s] thread[%x,%x] WRITE ACQUIRE rwlock %s [%x,%x] / taken %d / count %d\n",
__FUNCTION__, this->process->pid, this->trdid, 
lock_type_str[lock_type], local_cxy, lock, lock->taken, lock->count );
#endif

    // release busylock 
    busylock_release( &lock->lock );

}  // end rwlock_wr_acquire()

/////////////////////////////////////////
void rwlock_rd_release( rwlock_t * lock )
{
    // synchronize memory before lock release
    hal_fence();

    // get busylock
    busylock_acquire( &lock->lock );

    // decrement number of readers
    lock->count--;

#if DEBUG_RWLOCK_TYPE
thread_t * this = CURRENT_THREAD;
uint32_t lock_type = lock->lock.type;
if( (DEBUG_RWLOCK_TYPE == lock_type) || (DEBUG_RWLOCK_TYPE == 1000) )
printk("\n[%s] thread[%x,%x] READ RELEASE rwlock %s [%x,%x] / taken %d / count %d\n",
__FUNCTION__, this->process->pid, this->trdid, 
lock_type_str[lock_type], local_cxy, lock, lock->taken, lock->count );
#endif

    // release first writer in waiting queue if no current readers
    // and writers waiting queue non empty
    if( (lock->count == 0) && (list_is_empty( &lock->wr_root ) == false) )
    {
        // get first writer thread
        thread_t * thread = LIST_FIRST( &lock->wr_root , thread_t , wait_list );

#if DEBUG_RWLOCK_TYPE
if( (DEBUG_RWLOCK_TYPE == lock_type) || (DEBUG_RWLOCK_TYPE == 1000) )
printk("\n[%s] thread[%x,%x] UNBLOCK thread[%x,%x] / rwlock %s [%x,%x]\n",
__FUNCTION__, this->process->pid, this->trdid, thread->process->pid, thread->trdid,
lock_type_str[lock_type], local_cxy, lock );
#endif

        // remove this waiting thread from waiting list
        list_unlink( &thread->wait_list );

        // unblock this waiting thread
        thread_unblock( XPTR( local_cxy , thread ) , THREAD_BLOCKED_LOCK );
    }
    // release all readers in waiting queue if writers waiting queue empty
    // and readers waiting queue non empty
    else if( list_is_empty( &lock->wr_root ) && (list_is_empty( &lock->rd_root ) == false) )
    {
        while( list_is_empty( &lock->rd_root ) == false )
        {
            // get first reader thread 
            thread_t * thread = LIST_FIRST( &lock->wr_root , thread_t , wait_list );

#if DEBUG_RWLOCK_TYPE
if( (DEBUG_RWLOCK_TYPE == lock_type) || (DEBUG_RWLOCK_TYPE == 1000) )
printk("\n[%s] thread[%x,%x] UNBLOCK thread[%x,%x] / rwlock %s [%x,%x]\n",
__FUNCTION__, this->process->pid, this->trdid, thread->process->pid, thread->trdid,
lock_type_str[lock_type], local_cxy, lock );
#endif
   
            // remove this waiting thread from waiting list
            list_unlink( &thread->wait_list );

            // unblock this waiting thread
            thread_unblock( XPTR( local_cxy , thread ) , THREAD_BLOCKED_LOCK );
        }
    }

    // release busylock 
    busylock_release( &lock->lock );

}  // end rwlock_rd_release()

/////////////////////////////////////////
void rwlock_wr_release( rwlock_t * lock )
{
    // synchronize memory before lock release
    hal_fence();

    // get busylock
    busylock_acquire( &lock->lock );

    // release the rwlock
    lock->taken = 0;

#if DEBUG_RWLOCK_TYPE
thread_t * this = CURRENT_THREAD;
uint32_t lock_type = lock->lock.type;
if( (DEBUG_RWLOCK_TYPE == lock_type) || (DEBUG_RWLOCK_TYPE == 1000) )
printk("\n[%s] thread[%x,%x] WRITE RELEASE rwlock %s [%x,%x] / taken %d / count %d\n",
__FUNCTION__, this->process->pid, this->trdid, 
lock_type_str[lock_type], local_cxy, lock, lock->taken, lock->count );
#endif

    // release first waiting writer thread if writers waiting queue non empty
    if( list_is_empty( &lock->wr_root ) == false )
    {
        // get first writer thread
        thread_t * thread = LIST_FIRST( &lock->wr_root , thread_t , wait_list );

#if DEBUG_RWLOCK_TYPE
if( (DEBUG_RWLOCK_TYPE == lock_type) || (DEBUG_RWLOCK_TYPE == 1000) )
printk("\n[%s] thread[%x,%x] UNBLOCK thread[%x,%x] / rwlock %s [%x,%x]\n",
__FUNCTION__, this->process->pid, this->trdid, thread->process->pid, thread->trdid,
lock_type_str[lock_type], local_cxy, lock );
#endif
        // remove this waiting thread from waiting list
        list_unlink( &thread->wait_list );

        // unblock this waiting thread
        thread_unblock( XPTR( local_cxy , thread ) , THREAD_BLOCKED_LOCK );
    }

    // check readers waiting queue and release all if writers waiting queue empty
    else 
    {
        while( list_is_empty( &lock->rd_root ) == false )
        {
            // get first reader thread 
            thread_t * thread = LIST_FIRST( &lock->rd_root , thread_t , wait_list );

#if DEBUG_RWLOCK_TYPE
if( (DEBUG_RWLOCK_TYPE == lock_type) || (DEBUG_RWLOCK_TYPE == 1000) )
printk("\n[%s] thread[%x,%x] UNBLOCK thread[%x,%x] / rwlock %s [%x,%x]\n",
__FUNCTION__, this->process->pid, this->trdid, thread->process->pid, thread->trdid,
lock_type_str[lock_type], local_cxy, lock );
#endif
            // remove this waiting thread from waiting list
            list_unlink( &thread->wait_list );

            // unblock this waiting thread
            thread_unblock( XPTR( local_cxy , thread ) , THREAD_BLOCKED_LOCK );
        }
    }

    // release busylock 
    busylock_release( &lock->lock );

}  // end rwlock_wr_release()


