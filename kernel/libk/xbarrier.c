/*
 * xbarrier.c -  Busy-waiting, trans-clusters, kernel barrier implementation.
 *
 * Author   Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_remote.h>
#include <hal_atomic.h>
#include <xbarrier.h>


/////////////////////////////////////////////////
inline void xbarrier_wait( xptr_t    barrier_xp,
                           uint32_t  count )
{
    uint32_t  expected;

    xbarrier_t * ptr = GET_PTR( barrier_xp );
    cxy_t        cxy = GET_CXY( barrier_xp );

    // get barrier sense value
    uint32_t sense = hal_remote_l32( XPTR( cxy , &ptr->sense ) );

    // compute expected value
    if ( sense == 0 ) expected = 1;
    else              expected = 0;

    // atomically increment current
    uint32_t current = hal_remote_atomic_add( XPTR( cxy , &ptr->current ) , 1 );

    // last task reset current and toggle sense
    if( current == (count-1) )
    {
        hal_remote_s32( XPTR( cxy , &ptr->current) , 0 );
        hal_remote_s32( XPTR( cxy , &ptr->sense  ) , expected );
    }
    else   // other tasks poll the sense
    {
        while( hal_remote_l32( XPTR( cxy , &ptr->sense ) ) != expected ) asm volatile ("nop");
    }
}

