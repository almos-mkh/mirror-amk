/*
 * barrier.h - Busy-waiting, local, kernel barrier definition
 * 
 * Author  Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _BARRIER_H_
#define _BARRIER_H_

#include <kernel_config.h>
#include <hal_kernel_types.h>

/*****************************************************************************************
 * This structure defines a "rendez-vous" barrier, that can be used
 * to synchronise several kernel threads running in the same  cluster.
 * As it is used in the kernel_init phase, it implements a busy-waiting policy.
 * It does not need to be initialised, but it must be statically allocated 
 * in the KDATA segment to be properly initialised by the compiler/loader.
 ****************************************************************************************/

typedef struct barrier_s
{
    uint32_t            current;            // number of arrived threads
    volatile uint32_t   sense;              // barrier state (toggle)
    uint32_t            padding[(CONFIG_CACHE_LINE_SIZE>>2)-2];
} 
barrier_t;

/*****************************************************************************************
 * This blocking function implements a toggle barrier. It returns only when all
 * expected threads reach the barrier. It can be used several times without
 * specific initialisation. 
 *****************************************************************************************
 * @ barrier : pointer on barrier
 * @ count   : number of expected thread
 ****************************************************************************************/
inline void barrier_wait( barrier_t * barrier,  
                          uint32_t    count );


#endif	/* _BARRIER_H_ */
