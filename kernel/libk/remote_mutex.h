/*
 * remote_mutex.h -  POSIX mutex definition.
 *
 * Authors   Alain Greiner   (2016,2017,2018)
 * 
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _REMOTE_MUTEX_H_
#define _REMOTE_MUTEX_H_

#include <kernel_config.h>
#include <hal_kernel_types.h>
#include <xlist.h>

/*****************************************************************************************
 * This file defines the ALMOS-MKH implementation of an user level POSIX compliant mutex.
 *
 * It can be used by muti-threaded user applications to synchronise user threads
 * running in different clusters.
 *
 * A mutex is declared by a given user process as a "pthread_mutex_t" global variable.
 * This user type is implemented as an unsigned long, but the value is not used by the 
 * kernel. ALMOS-MKH uses only the mutex virtual address as an identifier.
 * For each user mutex, ALMOS-MKH creates a kernel "remote_mutex_t" structure, allocated
 * in the user process reference cluster by the remote_mutex_create() function, and 
 * destroyed by the remote_mutex_destroy() function, using RPC if the calling thread
 * is not running in the reference cluster. 
 *
 * The blocking "remote_mutex_lock()" function implements a descheduling policy when
 * the lock is already taken by another thread : the calling thread is registered 
 * in a waiting queue, rooted in the mutex structure, and the the calling thread 
 * is blocked on the THREAD_BLOCKED_MUTEX condition. 
 * The "remote_mutex_unlock()" function unblocks the first waiting thread in the queue
 * without releasing the mutex if queue is not empty.
 ****************************************************************************************/

/*****************************************************************************************
 * This structure defines the kernel implementation of an user level mutex.
 ****************************************************************************************/

typedef struct remote_mutex_s
{
    remote_busylock_t  lock;            /*! lock protecting the mutex state             */
    intptr_t           ident;           /*! mutex identifier (vaddr in user space)      */ 
    uint32_t           taken;           /*! mutex non allocated if 0                    */
    xlist_entry_t      list;            /*! member of list of mutex in same process     */
    xlist_entry_t      root;            /*! root of list of waiting threads             */
    xptr_t             owner;           /*! extended pointer on owner thread            */
}
remote_mutex_t;

/*****************************************************************************************
 * This function returns an extended pointer on the remote mutex, identified
 * by its virtual address in a given user process. It makes an associative search, 
 * scanning the list of mutex rooted in the reference process descriptor.
 *****************************************************************************************
 * @ ident    : mutex virtual address, used as identifier.
 * @ returns extended pointer on mutex if success / returns XPTR_NULL if not found. 
 ****************************************************************************************/
xptr_t remote_mutex_from_ident( intptr_t  ident );

/*****************************************************************************************
 * This function implements the pthread_mutex_init() syscall.
 * It allocates memory for the mutex descriptor in the reference cluster for
 * the calling process, it initializes the mutex state, and register it in the 
 * list of mutex owned by the reference process.
 *****************************************************************************************
 * @ ident       : mutex identifier (virtual address in user space).
 * @ return 0 if success / ENOMEM if no memory / EINVAL if invalid argument.
 ****************************************************************************************/
error_t remote_mutex_create( intptr_t ident );

/*****************************************************************************************
 * This function implements the pthread_mutex_destroy() syscall.
 * It releases thr memory allocated for the mutex descriptor, and remove the mutex 
 * from the list of mutex owned by the reference process.
 *****************************************************************************************
 * @ mutex_xp  : extended pointer on mutex descriptor.
 ****************************************************************************************/
void remote_mutex_destroy( xptr_t  mutex_xp );

/*****************************************************************************************
 * This blocking function implements the pthread_mutex_lock() syscall.
 * It returns only when the ownership of the mutex identified by the <mutex_xp>
 * argument has been obtained by the calling thread. It register in the mutex waiting 
 * queue when the mutex is already taken by another thread.
 *****************************************************************************************
 * @ mutex_xp  : extended pointer on mutex descriptor.
 ****************************************************************************************/
void remote_mutex_lock( xptr_t  mutex_xp );

/*****************************************************************************************
 * This function implements the pthread_mutex_unlock() syscall.
 * It cheks that the calling thread is actually the mutex owner.
 * It reset the "taken" & "owner" fields for the mutex identified by <mutex_xp>.
 * It unblocks the first thread registered in the mutex waiting queue, when the 
 * queue is not empty.
 *****************************************************************************************
 * @ mutex_xp  : extended pointer on mutex descriptor.
 * @ return 0 if success / return non zero if calling thread is not mutex owner. 
 ****************************************************************************************/
error_t remote_mutex_unlock( xptr_t  mutex_xp );

/*****************************************************************************************
 * This non blocking function function attempts to lock a mutex without blocking.
 *****************************************************************************************
 * @ mutex_xp  : extended pointer on mutex descriptor.
 * @ return 0 if success / return non zero if already taken.
 ****************************************************************************************/
error_t remote_mutex_trylock( xptr_t  mutex_xp );



#endif
