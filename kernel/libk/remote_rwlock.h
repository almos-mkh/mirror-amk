/*
 * remote_rwlock.h - kernel remote read/writelock definition.
 *
 * Authors   Alain Greiner   (2016,2017,2018)
 * 
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _REMOTE_RWLOCK_H_
#define _REMOTE_RWLOCK_H_

#include <kernel_config.h>
#include <hal_kernel_types.h>
#include <remote_busylock.h>
#include <xlist.h>

/*******************************************************************************************
 * This structure defines a kernel, global, read/write lock, supporting several simultaneous
 * read accesses, but only one write access to a globally shared object, that can be
 * accessed by threads running in any cluster.
 * Both readers and writers take the associated busylock before accessing or updating
 * the rwlock state, and releases the busylock after rwlock state update.
 * - when a reader try to access the object, it increments the readers "count" when the 
 *   lock is not "taken" by a writer. It registers in the "rd_root" waiting queue, blocks,
 *   and deschedules when the lock is taken.
 * - when a writer try to take the rwlock, it check the "taken" field. If the lock is already
 *   taken, or if the number of readers is non zero, it registers in the "wr_root" waiting
 *   queue, blocks, and deschedules. It set "taken" otherwise.
 * - when a reader completes its access, it decrement the readers "count", unblock the
 *   the first waiting writer if there is no other readers, and unblock all waiting
 *   readers if there no write request.
 * - when a  writer completes its access, it reset the "taken" field, releases the first
 *   waiting writer if queue non empty, or releases all waiting readers if no writer.
 ******************************************************************************************/


/*******************************************************************************************
 * This structure defines a remote rwlock.
 ******************************************************************************************/

typedef struct remote_rwlock_s
{
    remote_busylock_t   lock;        /*! busylock protecting the rwlock state             */
	volatile uint32_t   taken;       /*! lock taken by an exclusive writer if non zero    */
    volatile uint32_t   count;       /*! current number of simultaneous readers threads   */
    xlist_entry_t       rd_xroot;    /*! root of list of waiting readers                  */
    xlist_entry_t       wr_xroot;    /*! root of list of waiting writers                  */
}
remote_rwlock_t;


/***************************************************************************************
 * This function initializes a remote rwlock.
 * The <type> argument defines the lock usage and is only used for debug.
 * This type is actually stored in the associated busylock descriptor.
 ***************************************************************************************
 * @ lock_xp    : extended pointer on the remote rwlock
 * @ type       : lock usage for debug.
 **************************************************************************************/
void remote_rwlock_init( xptr_t   lock_xp,
                         uint32_t type );

/***************************************************************************************
 * This blocking function get access to a remote rwlock for a reader.
 ***************************************************************************************
 * @ lock_xp    : extended pointer on the remote rwlock
 **************************************************************************************/
void remote_rwlock_rd_acquire( xptr_t lock_xp );

/***************************************************************************************
 * This function releases a remote rwlock for a reader.
 ***************************************************************************************
 * @ lock_xp    : extended pointer on the remote rwlock
 **************************************************************************************/
void remote_rwlock_rd_release( xptr_t lock_xp );

/***************************************************************************************
 * This blocking function get access to a remote rwlock for a writer.
 ***************************************************************************************
 * @ lock_xp    : extended pointer on the remote rwlock
 **************************************************************************************/
void remote_rwlock_wr_acquire( xptr_t lock_xp );

/***************************************************************************************
 * This function releases a remote rwlock for a writer.
 ***************************************************************************************
 * @ lock_xp    : extended pointer on the remote rwlock
 **************************************************************************************/
void remote_rwlock_wr_release( xptr_t lock_xp );

#endif
