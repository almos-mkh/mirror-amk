/*
 * user_dir.h -  DIR related operations definition.
 *
 * Authors   Alain Greiner   (2016,2017,2018)
 * 
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _REMOTE_DIR_H_
#define _REMOTE_DIR_H_

#include <kernel_config.h>
#include <hal_kernel_types.h>
#include <xlist.h>

/*** Forward declarations   ***/
struct vfs_inode_s;

/*****************************************************************************************
 * This file defines the operations on a directory open by an user process
 *
 * - An user process open a directory by calling the "opendir()" syscall.
 *   The user_dir_create() function allocates physical memory to create in the cluster
 *   containing the target inode one or several physical pages to implement an array 
 *   of "dirent", mapped in the user process space as an ANON vseg. It allocates also
 *   an "user_dir_t" descriptor structure defined below, registered in the xlist rooted 
 *   in the reference user process descriptor. 
 * - This "user_dir_t" structure contains the total number of "entries", the "current" 
 *   pointer used by the readdir() syscall, and an "ident" field, that is actually
 *   the DIR user pointer on the "dirent" array in user space.
 * - all these structures are destroyed by the closedir() syscall, that release to the
 *   kernel all allocated memory.
 *
 * WARNING: ALMOS-MKH makes explicitely the assumption that sizeof(dirent) == 64
 ****************************************************************************************/

/*****************************************************************************************
 * This structure defines the user_dir_t decriptor, that is the kernel implementation
 * of a directory open by an user process.
 ****************************************************************************************/

typedef struct user_dir_s
{
    intptr_t           ident;           /*! pointer on dirent array in user space       */ 
    xlist_entry_t      list;            /*! member of list of open dir in same process  */
    uint32_t           entries;         /*! actual number of dirent in dirent array     */
    uint32_t           current;         /*! current dirent index in dirent array        */
}
user_dir_t;

/*****************************************************************************************
 * This function returns an extended pointer on the user_dir_t structure,
 * identified by the DIR pointer in the user process virtual space.
 * It makes an associative search, scanning the xlist of user_dir_t rooted 
 * in the reference process descriptor.
 *****************************************************************************************
 * @ ident    : [in] DIR virtual address, used as identifier.
 * @ returns extended pointer on user_dir_t if success / returns XPTR_NULL if not found. 
 ****************************************************************************************/
xptr_t user_dir_from_ident( intptr_t  ident );

/*****************************************************************************************
 * This function allocates memory and initializes a user_dir_t structure in the cluster
 * containing the directory inode identified by the <inode> argument and map the
 * user accessible dirent array in the reference user process VMM, identified by the
 * <ref_xp> argument. 
 * It must be executed by a thread running in the cluster containing the target inode.
 * Use the RPC_USER_DIR_CREATE when the client thread is remote.
 * It makes the following actions:
 * - the allocation of one user_dir_t descriptor in the directory inode cluster.
 * - the allocation of one or several physical pages in reference cluster to store 
 *   all directory entries in an array of 64 bytes dirent structures,
 * - the initialisation of this array from informations found in the directory mapper.
 * - the creation of an ANON vseg containing this dirent array in reference process VMM,
 *   and the mapping of the relevant physical pages in this vseg. 
 * - the registration of the created user_dir_t structure in the xlist rooted 
 *   in the reference process,
 * It returns a local pointer on the created user_dir_t structure.
 *****************************************************************************************
 * @ inode    : [in] local pointer on the directory inode.
 * @ ref_xp   : [in] extended pointer on the reference user process descriptor. 
 * @ return local pointer on user_dir_t if success / return XPTR_NULL if failure.
 ****************************************************************************************/
user_dir_t * user_dir_create( struct vfs_inode_s * inode,
                              xptr_t               ref_xp );

/*****************************************************************************************
 * This function removes a user_dir_t structure from the xlist of user_dir_t
 * structures rooted in the reference process descriptor, release all memory
 * allocated for the user_dir_t struct in the directory inode cluster, including 
 * the dirent array, and delete all ANON vseg copies in all process VMM copies,
 * using parallel RPCs.
 * It must be executed by a thread running in the cluster containing the target inode.
 * Use the RPC_USER_DIR_DESTROY when the client thread is remote.
 *****************************************************************************************
 * @ dir      : [in] local pointer on user_dir_t structure.
 * @ ref_xp   : [in] extended pointer on the reference user process descriptor. 
 ****************************************************************************************/
void user_dir_destroy( struct user_dir_s * dir,
                       xptr_t              ref_xp );


#endif
