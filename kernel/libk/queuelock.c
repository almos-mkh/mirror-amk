/*
 * queuelock.c - local kernel lock with waiting queue implementation.
 *
 * Authors   Alain Greiner     (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <kernel_config.h>
#include <hal_kernel_types.h>
#include <hal_atomic.h>
#include <thread.h>
#include <scheduler.h>
#include <busylock.h>
#include <queuelock.h>

//////////////////////////////////////////////////////////////////////////////
//                Extern global variables
//////////////////////////////////////////////////////////////////////////////

extern char * lock_type_str[];          // allocated in kernel_init.c


////////////////////////////////////////
void queuelock_init( queuelock_t * lock,
                     uint32_t      type )
{
    lock->taken = 0;
    list_root_init( &lock->root );
    busylock_init( &lock->lock , type );

#if DEBUG_QUEUELOCK_TYPE
thread_t * this = CURRENT_THREAD;
if( DEBUG_QUEUELOCK_TYPE == type )
printk("\n[%s] thread[%x,%x] initialise lock %s [%x,%x]\n",
__FUNCTION__, this->process->pid, this->trdid,
lock_type_str[type], local_cxy, lock );
#endif

}

////////////////////////////////////////////
void queuelock_acquire( queuelock_t * lock )
{
    thread_t * this = CURRENT_THREAD;

    // check calling thread can yield
    thread_assert_can_yield( this , __FUNCTION__ );

    // get busylock protecting access to queuelock state
    busylock_acquire( &lock->lock );

#if DEBUG_QUEUELOCK_TYPE
uint32_t   lock_type = lock->lock.type;
#endif

    // block and deschedule if lock already taken
    while( lock->taken )
    {

#if DEBUG_QUEUELOCK_TYPE
if( (DEBUG_QUEUELOCK_TYPE == lock_type) || (DEBUG_QUEUELOCK_TYPE == 1000) )
printk("\n[%s ] thread[%x,%x] BLOCK on q_lock %s [%x,%x]\n",
__FUNCTION__, this->process->pid, this->trdid,
lock_type_str[lock_type], local_cxy, lock );
#endif
        // get pointer on calling thread
        thread_t * this = CURRENT_THREAD;

        // register calling thread in waiting queue
        list_add_last( &lock->root , &this->wait_list );

        // block calling thread
        thread_block( XPTR( local_cxy , this ) , THREAD_BLOCKED_LOCK );
        
        // release busylock
        busylock_release( &lock->lock );

        // deschedule
        sched_yield("reader wait queuelock");
        
        // get busylock
        busylock_acquire( &lock->lock );
    }

#if DEBUG_QUEUELOCK_TYPE
if( (DEBUG_QUEUELOCK_TYPE == lock_type) || (DEBUG_QUEUELOCK_TYPE == 1000) )
printk("\n[%s] thread[%x,%x] ACQUIRE q_lock %s [%x,%x]\n",
__FUNCTION__, this->process->pid, this->trdid,
lock_type_str[lock_type], local_cxy, lock );
#endif

    // update queuelock state
    lock->taken = 1;

    // release busylock 
    busylock_release( &lock->lock );

}  // end queuelock_acquire()

////////////////////////////////////////////
void queuelock_release( queuelock_t * lock )
{
    // memory barrier before lock release
    hal_fence();

    // get busylock protecting access to queuelock state
    busylock_acquire( &lock->lock );

#if DEBUG_QUEUELOCK_TYPE
uint32_t   lock_type = lock->lock.type;
thread_t * this      = CURRENT_THREAD;
if( (DEBUG_QUEUELOCK_TYPE == lock_type) || (DEBUG_QUEUELOCK_TYPE == 1000) )
printk("\n[%s] thread[%x,%x] RELEASE q_lock %s [%x,%x]\n",
__FUNCTION__, this->process->pid, this->trdid,
lock_type_str[lock_type], local_cxy, lock );
#endif

    // update queuelock state
    lock->taken = 0;

    // unblock first waiting thread if waiting list not empty
    if( list_is_empty( &lock->root ) == false )
    {
        // get first waiting thread
        thread_t * thread = LIST_FIRST( &lock->root , thread_t , wait_list );

#if DEBUG_QUEUELOCK_TYPE
if( (DEBUG_QUEUELOCK_TYPE == lock_type) || (DEBUG_QUEUELOCK_TYPE == 1000) )
printk("\n[%s] thread[%x,%x] UNBLOCK thread [%x,%x] / q_lock %s [%x,%x]\n",
__FUNCTION__, this->process->pid, this->trdid, thread->process->pid, thread->trdid,
lock_type_str[lock_type], local_cxy, lock );
#endif
        // remove this waiting thread from waiting list
        list_unlink( &thread->wait_list );

        // unblock this waiting thread
        thread_unblock( XPTR( local_cxy , thread ) , THREAD_BLOCKED_LOCK );
    }

    // release busylock
    busylock_release( &lock->lock );

}  // end queuelock_release()



