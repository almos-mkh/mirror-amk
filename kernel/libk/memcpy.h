/*
 * memcpy.c - architecture independent memory copy functions definition.
 *
 * Author ALain Greiner (2016)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>


/*******************************************************************************************
 * This function copies a source buffer to a destination buffer.
 * There is no alignment constraint, but the performance is improved if the buffers
 * are both aligned on a 32 bits word boundary.
 *******************************************************************************************
 * @ dst     : pointer on destination buffer.
 * @ src     : pointer on source buffer.
 * @ size    : number of bytes.
 * @ return pointer on destination buffer.
 ******************************************************************************************/
void * memcpy( void       * dst,
               const void * src,
               uint32_t     size );

/*******************************************************************************************
 * This function sets a constant value in each byte of a target buffer.
 *******************************************************************************************
 * @ dst     : pointer on destination buffer.
 * @ val     : constant value (cast to uint8_t).
 * @ size    : number of bytes.
 * @ return pointer on destination buffer.
 ******************************************************************************************/
void * memset( void     * dst,
               uint32_t   val,
               uint32_t size);

/*******************************************************************************************
 * This function compares two buffers bytewise.
 *******************************************************************************************
 * @s1     : pointer on a buffer
 * @s2     : pointer on another buffer to compare with @s1
 * @n      : number of bytes to compare
 * @return : = 0 if s1   == s2 means structural or physical equality between @s1 and @s2.
 *           < 0 if s1[i] < s2[i] means @s1[i] is lesser than @s2[i] current one
 *           > 0 if s1[i] > s2[i] means @s1[i] is greater than @s2[i] current one.
 * Note: i is an index to the current byte of the buffer.
 ******************************************************************************************/
int memcmp( const void * s1,
            const void * s2,
            uint32_t     n);

