/*
 * queuelock.h: local kernel lock with waiting queue implementation.
 * 
 * Authors  Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-kernel; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _QUEUELOCK_H_
#define _QUEUELOCK_H_

#include <kernel_config.h>
#include <hal_kernel_types.h>
#include <busylock.h>
#include <list.h>

/*******************************************************************************************
 * This synchronisation object sequencializes concurrent read or write accesses to a
 * locally shared object in a given cluster (i.e. made by thread(s) running in same cluster.
 * This lock has only two states: taken or free.
 * When the lock has been taken by another thread T', the thread T executing the
 * queuelock_acquire() function blocks and deschedules, after registering in the waiting
 * queue rooted in lock descriptor. The first thread in the waiting queue will be unblocked 
 * by the owner thread executing the queuelock_release() function.
 * To handle concurrent accesses between a thread T executing the acquire() function
 * and a thread T' executing the release() function, the "queuelock" is protected
 * by a "busylock" used by these two access functions.
 * WARNING: this queuelock is NOT registered in the thread "local_locks" field.
 ******************************************************************************************/
 

/*******************************************************************************************
 * This structure defines a queuelock.
 ******************************************************************************************/

typedef struct queuelock_s
{
    busylock_t          lock;               /*! protect atomic access to queuelock        */
	volatile uint32_t   taken;              /*! state : free if zero / taken if non zero  */
    list_entry_t        root;               /*! root of list of locally waiting threads   */
}
queuelock_t;

/*******************************************************************************************
 * This function initializes a local queuelock in free state.
 * The <type> argument defines the lock usage and is only used for debug.
 * This type is actually stored in the associated busylock descriptor.
 *******************************************************************************************
 * @ lock   : local pointer on queuelock.
 * @ type   : lock usage for debug.
 ******************************************************************************************/
inline void queuelock_init( queuelock_t * lock,
                            uint32_t      type );

/*******************************************************************************************
 * This blocking function acquires a queuelock.
 * The calling thread and the lock must be in same cluster.
 * It uses the busylock to atomically access/update the queuelock state.
 * If the lock is already taken, the calling thread atomically registers in the list
 * of waiting threads (rooted in the lock descriptor), block and deschedule.
 *******************************************************************************************
 * @ lock   : local pointer on queuelock.
 ******************************************************************************************/
void queuelock_acquire( queuelock_t * lock );

/*******************************************************************************************
 * This function releases a queuelock.
 * The calling thread and the lock must be in same cluster.
 * It uses the busylock to atomically access/update the queuelock state.
 * If the list of waiting threads is not empty, the first waiting thread is unblocked.
 *******************************************************************************************
 * @ lock   : local pointer on queuelock.
 ******************************************************************************************/
void queuelock_release( queuelock_t * lock );


#endif	/* _QUEUELOCK_H_ */
