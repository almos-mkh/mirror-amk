/*
 * remote_fifo.h - Lock-less Single-Reader Multiple-Writers FIFO
 *
 * Authors : Mohamed Lamine Karaoui (2015)
 *           Alain Greiner (2016,2017)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MHH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _REMOTE_FIFO_H_
#define _REMOTE_FIFO_H_

#include <kernel_config.h>
#include <hal_kernel_types.h>
#include <printk.h>
#include <errno.h>
#include <hal_remote.h>

/************************************************************************************
 * This structure defines a generic, single reader, multiple writers FIFO,
 * that is used for - RPC based - inter cluster communications.
 * Each FIF0 slot can contain one 64 bits integer (or one extended pointer).
 * The number of slots is defined by the CONFIG_REMOTE_FIFO_SLOTS parameter.
 * - The write accesses are implemented using a lock-free algorithm, as it uses
 *   a ticket based mechanism to handle concurrent access between multiple writers.
 *   In case of FIFO full, the writer deschedule without blocking, to retry later.
 * - The reader must take the try_lock implemented by the "owner" field, using
 *   an atomic_add(). The TRDID is a good owner identifier, because all
 *   RPC threads in a given cluster belong to the same kernel process,
 *   and RPC threads cannot have local index LTID = 0.
*
 * WARNING : Each FIFO requires 12 + (12 * CONFIG_REMOTE_FIFO_SLOTS) bytes.
 ***********************************************************************************/

typedef struct remote_fifo_s
{
    uint32_t           owner;                            /*! owner thread trdid    */
	volatile uint32_t  wr_id;                            /*! write slot index      */
	volatile uint32_t  rd_id;                            /*! read  slot index      */
    volatile uint32_t  valid[CONFIG_REMOTE_FIFO_SLOTS];  /*! empty slot if 0       */
	uint64_t           data[CONFIG_REMOTE_FIFO_SLOTS];   /*! fifo slot content     */
}
remote_fifo_t;

/************************************************************************************
 * This function initializes the local FIFO as empty.
 * It can only initialise a local FIFO.
 ************************************************************************************
 * @ fifo    : local pointer to the local fifo.
 ***********************************************************************************/
void remote_fifo_init( remote_fifo_t * fifo );

/************************************************************************************
 * This non blocking function tries to get one item from the local fifo.
 * The reader must get exclusive access for read before calling this function.  
 * The read slot index is incremented.
 ************************************************************************************
 * @ fifo    : local pointer to the local fifo.
 * @ item    : [out] pointer on buffer for extracted item.
 * @ return  0 on success, EAGAIN if the buffer is empty.
 ***********************************************************************************/
error_t remote_fifo_get_item( remote_fifo_t * fifo, 
                              uint64_t      * item );

/************************************************************************************
 * This blocking function puts one item to a remote fifo identified
 * by an extended pointer. It gets a write ticket on the slot to be written,
 * using a remote_atomic_add() on the write slot index. Then, it waits until
 * the slot is empty, using a descheduling policy without blocking if required. 
 * It implements a watchdog, returning when the item has been successfully
 * registered, or after CONFIG_REMOTE_FIFO_MAX_ITERATIONS failures.   
 ************************************************************************************
 * @ fifo    : extended pointer to the remote fifo.
 * @ item    : item to be stored.
 * @ return  0 on success / EBUSY if a contention has been detected.
 ***********************************************************************************/
error_t remote_fifo_put_item( xptr_t     fifo,
                              uint64_t   item );

/************************************************************************************
 * Query if local fifo is empty
 ************************************************************************************
 * @ fifo    : local pointer to the local fifo.
 * @ return true if the fifo is empty, false otherwise.
 ***********************************************************************************/
bool_t remote_fifo_is_empty( remote_fifo_t * fifo );

/************************************************************************************
 * Query if remote fifo is full
 ************************************************************************************
 * @ fifo    : extended pointer to the remote fifo.
 * @ return true if the fifo is full, false otherwise.
 ***********************************************************************************/
bool_t remote_fifo_is_full( xptr_t fifo );

/************************************************************************************
 * Query number ot items in remote fifo.
 ************************************************************************************
 * @ fifo     : extended pointer to the remote fifo.
 * @ return  number of items.
 ***********************************************************************************/
uint32_t remote_fifo_items( xptr_t fifo );


#endif	/* _REMOTE_FIFO_H_ */
