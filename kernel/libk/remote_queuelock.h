/*
 * remote_queuelock.h: remote kernel lock with waiting queue definition.
 * 
 * Authors  Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-kernel; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _REMOTE_QUEUELOCK_H_
#define _REMOTE_QUEUELOCK_H_

#include <kernel_config.h>
#include <hal_kernel_types.h>
#include <remote_busylock.h>
#include <xlist.h>

/*******************************************************************************************
 * This synchronisation object sequencializes concurrent read or write accesses to a
 * globally shared object located in any cluster, made by thread(s) running in any cluster.
 * This lock has only two states: taken or free.
 * When the lock has been taken by another thread T', the thread T executing the
 * queuelock_acquire() function blocks and deschedules, after registering in the waiting
 * queue rooted in lock descriptor. The first thread in the waiting queue will be unblocked 
 * by the owner thread executing the queuelock_release() function.
 * To handle concurrent accesses between a thread T executing the acquire() function
 * and a thread T' executing the release() function, the "remote_queuelock" is protected
 * by a "remote_busylock" used by these two access functions.
 * WARNING: this remote_queuelock is NOT registered in the thread "remote_locks" field.
 ******************************************************************************************/
 

/*******************************************************************************************
 * This structure defines a remote_queuelock.
 ******************************************************************************************/

typedef struct remote_queuelock_s
{
    remote_busylock_t   lock;               /*! protect atomic access to remote_queuelock */
	volatile uint32_t   taken;              /*! state : free if zero / taken if non zero  */
    xlist_entry_t       xroot;              /*! root of xlist of waiting threads          */
}
remote_queuelock_t;

/*******************************************************************************************
 * This function initializes a local remote_queuelock in free state.
 * The <name> argument defines the lock usage and is only used for debug.
 * This type is actually stored in the associated busylock descriptor.
 *******************************************************************************************
 * @ lock_xp   : extended pointer on remote_queuelock.
 * @ type      : lock usage for debug.
******************************************************************************************/
inline void remote_queuelock_init( xptr_t   lock_xp,
                                   uint32_t type );

/*******************************************************************************************
 * This blocking function acquires a remote_queuelock.
 * The calling thread and the lock can be in different clusters.
 * It uses the remote_busylock to atomically access/update the remote_queuelock state.
 * If the lock is already taken, the calling thread atomically registers in the xlist
 * of waiting threads (rooted in the lock descriptor), block and deschedule.
 * It increments the calling thread remote_locks counter when the lock has been taken.
 * It optionally update the <owner_xp> and <xlist> debug fields.
 *******************************************************************************************
 * @ lock_xp   : extended pointer on remote_queuelock.
 ******************************************************************************************/
void remote_queuelock_acquire( xptr_t  lock_xp );

/*******************************************************************************************
 * This function releases a remote_queuelock.
 * The calling thread and the lock can be in different clusters.
 * It uses the remote_busylock to atomically access/update the remote_queuelock state.
 * If the xlist of waiting threads is not empty, the first waiting thread is unblocked.
 * It decrements the calling thread remote_locks counter when the lock has been released.
 * It optionally update the <owner_xp> and <xlist> debug fields.
 *******************************************************************************************
 * @ lock_xp   : extended pointer on remote_queuelock.
 ******************************************************************************************/
void remote_queuelock_release( xptr_t lock_xp );


#endif	/* _REMOTE_QUEUELOCK_H_ */
