/*
 * remote_rwlock.c - kernel remote read/write lock implementation.
 * 
 * Authors    Alain   Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_remote.h>
#include <hal_irqmask.h>
#include <thread.h>
#include <printk.h>
#include <cluster.h>
#include <scheduler.h>
#include <remote_rwlock.h>

//////////////////////////////////////////////////////////////////////////////
//                Extern global variables
//////////////////////////////////////////////////////////////////////////////

extern char * lock_type_str[];          // allocated in kernel_init.c


//////////////////////////////////////////
void remote_rwlock_init( xptr_t   lock_xp,
                         uint32_t type )
{ 
    remote_rwlock_t * lock_ptr = GET_PTR( lock_xp );
    cxy_t             lock_cxy = GET_CXY( lock_xp );

    hal_remote_s32 ( XPTR( lock_cxy , &lock_ptr->taken ) , 0 );
    hal_remote_s32 ( XPTR( lock_cxy , &lock_ptr->count ) , 0 );

    xlist_root_init( XPTR( lock_cxy , &lock_ptr->rd_xroot ) );
    xlist_root_init( XPTR( lock_cxy , &lock_ptr->wr_xroot ) );

    remote_busylock_init( XPTR( lock_cxy , &lock_ptr->lock ) , type ); 

#if DEBUG_RWLOCK_TYPE
thread_t * this = CURRENT_THREAD;
if( DEBUG_RWLOCK_TYPE == type )
printk("\n[%s] thread[%x,%x] initialise lock %s [%x,%x]\n",
__FUNCTION__, this->process->pid, this->trdid,
lock_type_str[type], local_cxy, lock_ptr );
#endif

}

///////////////////////////////////////////////
void remote_rwlock_rd_acquire( xptr_t lock_xp )
{ 
    thread_t * this = CURRENT_THREAD;

    // check calling thread can yield
    thread_assert_can_yield( this , __FUNCTION__ );

    // get cluster and local pointer on remote_rwlock
    remote_rwlock_t * lock_ptr = GET_PTR( lock_xp );
    cxy_t             lock_cxy = GET_CXY( lock_xp );

#if DEBUG_RWLOCK_TYPE
uint32_t lock_type = hal_remote_l32( XPTR( lock_cxy , &lock_ptr->lock.type ) );
#endif

    // build useful extended pointers
    xptr_t busylock_xp = XPTR( lock_cxy , &lock_ptr->lock );
    xptr_t taken_xp    = XPTR( lock_cxy , &lock_ptr->taken );
    xptr_t count_xp    = XPTR( lock_cxy , &lock_ptr->count );
    xptr_t rd_root_xp  = XPTR( lock_cxy , &lock_ptr->rd_xroot );

    // get busylock
    remote_busylock_acquire( busylock_xp );

    // block and deschedule if lock taken
    while( hal_remote_l32( taken_xp ) )
    {

#if DEBUG_RWLOCK_TYPE
if( (DEBUG_RWLOCK_TYPE == lock_type) || (DEBUG_RWLOCK_TYPE == 1000) )
printk("\n[%s] thread[%x,%x] READ BLOCK on rwlock %s [%x,%x] / taken %d / count %d\n",
__FUNCTION__, this->process->pid, this->trdid, 
lock_type_str[lock_type], lock_cxy, lock_ptr,
hal_remote_l32( taken_xp ), hal_remote_l32( count_xp ) );
#endif
        // get pointer on calling thread
        thread_t * this = CURRENT_THREAD;

        // register reader thread in waiting queue
        xlist_add_last( rd_root_xp , XPTR( local_cxy , &this->wait_xlist ) );

        // block reader thread
        thread_block( XPTR( local_cxy , this ) , THREAD_BLOCKED_LOCK );

        // release busylock
        remote_busylock_release( busylock_xp );

        // deschedule
        sched_yield("reader wait remote_rwlock");

        // get busylock
        remote_busylock_acquire( busylock_xp );
    }

    // increment number of readers
    hal_remote_atomic_add( count_xp , 1 );

    hal_fence();

#if DEBUG_RWLOCK_TYPE
if( (DEBUG_RWLOCK_TYPE == lock_type) || (DEBUG_RWLOCK_TYPE == 1000) )
printk("\n[%s] thread[%x,%x] READ ACQUIRE rwlock %s [%x,%x] / taken = %d / count = %d\n",
__FUNCTION__, this->process->pid, this->trdid,
lock_type_str[lock_type], lock_cxy, lock_ptr,
hal_remote_l32( taken_xp ), hal_remote_l32( count_xp ) );
#endif

    // release busylock
    remote_busylock_release( busylock_xp );

}  // end remote_rwlock_rd_acquire()

///////////////////////////////////////////////
void remote_rwlock_wr_acquire( xptr_t lock_xp )
{ 
    thread_t * this = CURRENT_THREAD;

    // check calling thread can yield
    thread_assert_can_yield( this , __FUNCTION__ );

    // get cluster and local pointer on remote_rwlock
    remote_rwlock_t * lock_ptr = GET_PTR( lock_xp );
    cxy_t             lock_cxy = GET_CXY( lock_xp );

#if DEBUG_RWLOCK_TYPE
uint32_t lock_type = hal_remote_l32( XPTR( lock_cxy , &lock_ptr->lock.type ) );
#endif

    // build useful extended pointers
    xptr_t busylock_xp = XPTR( lock_cxy , &lock_ptr->lock );
    xptr_t taken_xp    = XPTR( lock_cxy , &lock_ptr->taken );
    xptr_t count_xp    = XPTR( lock_cxy , &lock_ptr->count );
    xptr_t wr_root_xp  = XPTR( lock_cxy , &lock_ptr->wr_xroot );

    // get busylock
    remote_busylock_acquire( busylock_xp );

    // block and deschedule if lock already taken or current readers
    while( hal_remote_l32( taken_xp ) || hal_remote_l32( count_xp ) )
    {

#if DEBUG_RWLOCK_TYPE
if( (DEBUG_RWLOCK_TYPE == lock_type) || (DEBUG_RWLOCK_TYPE == 1000) )
printk("\n[%s] thread[%x,%x] WRITE BLOCK on rwlock %s [%x,%x] / taken %d / count %d\n",
__FUNCTION__, this->process->pid, this->trdid, 
lock_type_str[lock_type], lock_cxy, lock_ptr, 
hal_remote_l32( taken_xp ), hal_remote_l32( count_xp ) );
#endif

        // get local pointer on calling thread
        thread_t * this = CURRENT_THREAD;

        // register writer thread in waiting queue
        xlist_add_last( wr_root_xp , XPTR( local_cxy , &this->wait_xlist ) );

        // block writer thread
        thread_block( XPTR( local_cxy , this ) , THREAD_BLOCKED_LOCK );

        // release busylock
        remote_busylock_release( busylock_xp );

        // deschedule
        sched_yield("writer wait remote_rwlock");

        // get busylock
        remote_busylock_acquire( busylock_xp );
    }

    // take rwlock for write
    hal_remote_s32( taken_xp , 1 );

#if DEBUG_RWLOCK_TYPE
if( (DEBUG_RWLOCK_TYPE == lock_type) || (DEBUG_RWLOCK_TYPE == 1000) )
printk("\n[%s] thread[%x,%x] WRITE ACQUIRE rwlock %s [%x,%x] / taken %d / count %d\n",
__FUNCTION__, this->process->pid, this->trdid, 
lock_type_str[lock_type], lock_cxy, lock_ptr,
hal_remote_l32( taken_xp ), hal_remote_l32( count_xp ) );
#endif

    // release busylock
    remote_busylock_release( busylock_xp );

}  // end remote_rwlock_wr_acquire()


///////////////////////////////////////////////
void remote_rwlock_rd_release( xptr_t lock_xp )
{
    // memory barrier before lock release
    hal_fence();

    // get cluster and local pointer on remote_rwlock
    remote_rwlock_t * lock_ptr = GET_PTR( lock_xp );
    cxy_t             lock_cxy = GET_CXY( lock_xp );

    // build useful extended pointers
    xptr_t busylock_xp = XPTR( lock_cxy , &lock_ptr->lock );
    xptr_t count_xp    = XPTR( lock_cxy , &lock_ptr->count );
    xptr_t rd_root_xp  = XPTR( lock_cxy , &lock_ptr->rd_xroot );
    xptr_t wr_root_xp  = XPTR( lock_cxy , &lock_ptr->wr_xroot );

    // get busylock
    remote_busylock_acquire( busylock_xp );

	// decrement number of readers
    hal_remote_atomic_add( count_xp , -1 );

#if DEBUG_RWLOCK_TYPE
thread_t * this      = CURRENT_THREAD;
uint32_t   lock_type = hal_remote_l32( XPTR( lock_cxy , &lock_ptr->lock.type ) );
xptr_t     taken_xp  = XPTR( lock_cxy , &lock_ptr->taken );
if( (DEBUG_RWLOCK_TYPE == lock_type) || (DEBUG_RWLOCK_TYPE == 1000) )
printk("\n[%s] thread[%x,%x] READ RELEASE rwlock %s [%x,%x] / taken %d / count %d\n",
__FUNCTION__, this->process->pid, this->trdid,
lock_type_str[lock_type], lock_cxy, lock_ptr,
hal_remote_l32( taken_xp ), hal_remote_l32( count_xp ) );
#endif

    // release first writer in waiting queue if no current readers
    // and writers waiting queue non empty
    if( (hal_remote_l32( count_xp ) == 0) && (xlist_is_empty( wr_root_xp ) == false) )
    {
        // get first writer thread
        xptr_t      thread_xp  = XLIST_FIRST( wr_root_xp , thread_t, wait_xlist );
        cxy_t       thread_cxy = GET_CXY( thread_xp ); 
        thread_t *  thread_ptr = GET_PTR( thread_xp ); 

        // remove this waiting thread from waiting list
        xlist_unlink( XPTR( thread_cxy , &thread_ptr->wait_xlist ) );

        // unblock this waiting thread
        thread_unblock( thread_xp , THREAD_BLOCKED_LOCK );

#if DEBUG_RWLOCK_TYPE
if( (DEBUG_RWLOCK_TYPE == lock_type) || (DEBUG_RWLOCK_TYPE == 1000) )
{
    trdid_t     trdid     = hal_remote_l32( XPTR( thread_cxy , &thread_ptr->trdid ) );
    process_t * process   = hal_remote_lpt( XPTR( thread_cxy , &thread_ptr->process ) );
    uint32_t    pid       = hal_remote_l32( XPTR( thread_cxy , &process->pid ) );
    printk("\n[%s] thread[%x,%x] UNBLOCK thread[%x,%x] / rwlock %s [%x,%x]\n",
    __FUNCTION__, this->process->pid, this->trdid, pid, trdid, 
    lock_type_str[lock_type], lock_cxy, lock_ptr );
}
#endif

    }

    // release all readers in waiting queue if writers waiting queue empty
    // and readers waiting queue non empty
    else if( xlist_is_empty( wr_root_xp ) && (xlist_is_empty( rd_root_xp ) == false) )
    {
        while( xlist_is_empty( rd_root_xp ) == false )
        {
            // get first writer thread
            xptr_t      thread_xp  = XLIST_FIRST( wr_root_xp , thread_t, wait_xlist );
            cxy_t       thread_cxy = GET_CXY( thread_xp ); 
            thread_t *  thread_ptr = GET_PTR( thread_xp ); 

            // remove this waiting thread from waiting list
            xlist_unlink( XPTR( thread_cxy , &thread_ptr->wait_xlist ) );

            // unblock this waiting thread
            thread_unblock( thread_xp , THREAD_BLOCKED_LOCK );

#if DEBUG_RWLOCK_TYPE
if( (DEBUG_RWLOCK_TYPE == lock_type) || (DEBUG_RWLOCK_TYPE == 1000) )
{
    trdid_t     trdid     = hal_remote_l32( XPTR( thread_cxy , &thread_ptr->trdid ) );
    process_t * process   = hal_remote_lpt( XPTR( thread_cxy , &thread_ptr->process ) );
    uint32_t    pid       = hal_remote_l32( XPTR( thread_cxy , &process->pid ) );
    printk("\n[%s] thread[%x,%x] UNBLOCK thread[%x,%x] / rwlock %s [%x,%x]\n",
    __FUNCTION__, this->process->pid, this->trdid, pid, trdid, 
    lock_type_str[lock_type], lock_cxy, lock_ptr );
}
#endif

        }
    }

    // release busylock 
    remote_busylock_release( busylock_xp );

}  // end remote_rwlock_rd_release()

///////////////////////////////////////////////
void remote_rwlock_wr_release( xptr_t lock_xp )
{ 
    // memory barrier before lock release
    hal_fence();

    // get cluster and local pointer on remote_rwlock
    remote_rwlock_t * lock_ptr = GET_PTR( lock_xp );
    cxy_t             lock_cxy = GET_CXY( lock_xp );

    // build useful extended pointers
    xptr_t busylock_xp = XPTR( lock_cxy , &lock_ptr->lock );
    xptr_t taken_xp    = XPTR( lock_cxy , &lock_ptr->taken );
    xptr_t rd_root_xp  = XPTR( lock_cxy , &lock_ptr->rd_xroot );
    xptr_t wr_root_xp  = XPTR( lock_cxy , &lock_ptr->wr_xroot );

    // get busylock
    remote_busylock_acquire( busylock_xp );

    // release rwlock
    hal_remote_s32( taken_xp , 0 );

#if DEBUG_RWLOCK_TYPE
thread_t * this      = CURRENT_THREAD;
uint32_t   lock_type = hal_remote_l32( XPTR( lock_cxy , &lock_ptr->lock.type ) );
xptr_t     count_xp  = XPTR( lock_cxy , &lock_ptr->count );
if( (DEBUG_RWLOCK_TYPE == lock_type) || (DEBUG_RWLOCK_TYPE == 1000) )
printk("\n[%s] thread[%x,%x] WRITE RELEASE rwlock %s [%x,%x] / taken %d / count %d\n",
__FUNCTION__, this->process->pid, this->trdid,
lock_type_str[lock_type], lock_cxy, lock_ptr,
hal_remote_l32( taken_xp ), hal_remote_l32( count_xp ) );
#endif

    // unblock first waiting writer thread if writers waiting queue non empty
    if( xlist_is_empty( wr_root_xp ) == false )
    {
        // get first writer thread
        xptr_t      thread_xp  = XLIST_FIRST( wr_root_xp , thread_t, wait_xlist );
        cxy_t       thread_cxy = GET_CXY( thread_xp ); 
        thread_t *  thread_ptr = GET_PTR( thread_xp ); 

        // remove this waiting thread from waiting list
        xlist_unlink( XPTR( thread_cxy , &thread_ptr->wait_xlist ) );

        // unblock this waiting thread
        thread_unblock( thread_xp , THREAD_BLOCKED_LOCK );

#if DEBUG_RWLOCK_TYPE
if( (DEBUG_RWLOCK_TYPE == lock_type) || (DEBUG_RWLOCK_TYPE == 1000) )
{
    trdid_t     trdid     = hal_remote_l32( XPTR( thread_cxy , &thread_ptr->trdid ) );
    process_t * process   = hal_remote_lpt( XPTR( thread_cxy , &thread_ptr->process ) );
    uint32_t    pid       = hal_remote_l32( XPTR( thread_cxy , &process->pid ) );
    printk("\n[%s] thread[%x,%x] UNBLOCK thread[%x,%x] / rwlock %s [%x,%x]\n",
    __FUNCTION__, this->process->pid, this->trdid, pid, trdid, 
    lock_type_str[lock_type], lock_cxy, lock_ptr );
}
#endif

    }

    // check readers waiting queue and unblock all if writers waiting queue empty
    else 
    {
        while( xlist_is_empty( rd_root_xp ) == false )
        {
            // get first writer thread
            xptr_t      thread_xp  = XLIST_FIRST( rd_root_xp , thread_t, wait_xlist );
            cxy_t       thread_cxy = GET_CXY( thread_xp ); 
            thread_t *  thread_ptr = GET_PTR( thread_xp ); 

            // remove this waiting thread from waiting list
            xlist_unlink( XPTR( thread_cxy , &thread_ptr->wait_xlist ) );

            // unblock this waiting thread
            thread_unblock( thread_xp , THREAD_BLOCKED_LOCK );

#if DEBUG_RWLOCK_TYPE
if( (DEBUG_RWLOCK_TYPE == lock_type) || (DEBUG_RWLOCK_TYPE == 1000) )
{
    trdid_t     trdid     = hal_remote_l32( XPTR( thread_cxy , &thread_ptr->trdid ) );
    process_t * process   = hal_remote_lpt( XPTR( thread_cxy , &thread_ptr->process ) );
    uint32_t    pid       = hal_remote_l32( XPTR( thread_cxy , &process->pid ) );
    printk("\n[%s] thread[%x,%x] UNBLOCK thread[%x,%x] / rwlock %s [%x,%x]\n",
    __FUNCTION__, this->process->pid, this->trdid, pid, trdid, 
    lock_type_str[lock_type], lock_cxy, lock_ptr );
}
#endif

        }
    }

    // release busylock 
    remote_busylock_release( busylock_xp );

}  // end remote_rwlock_wr_release()



