/*
 * readlock.c - kernel readlock synchronization
 * 
 * Author  Alain Greiner     (2016}
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <kernel_config.h>
#include <hal_kernel_types.h>
#include <hal_atomic.h>
#include <hal_special.h>
#include <hal_irqmask.h>
#include <thread.h>
#include <scheduler.h>
#include <printk.h>
#include <readlock.h>

///////////////////////////////////////
void readlock_init( readlock_t * lock )
{  
	lock->taken = 0;
    lock->count = 0;
	lock->owner = NULL;
}

//////////////////////////////////////////
void readlock_rd_lock( readlock_t * lock )
{
	uint32_t           mode;
    volatile uint32_t  taken;
	thread_t         * this     = CURRENT_THREAD;
	bool_t             isAtomic = false;

    // disable IRQs
	hal_disable_irq( &mode );
  
    // loop to take the lock against concurrent write
	while( isAtomic == false )
	{
        // check lock value with a simple read
        taken = lock->taken;
		if( taken != 0 )  continue;
 
        // try to tomically take the lock
		isAtomic = hal_atomic_cas( &lock->taken , 0 , 1 );
	}

    // increment count
    lock->count++;

    // sync
    hal_fence();

    // release lock to support several parallel read access
    lock->taken = 0;

    // update calling thread locks count
	this->local_locks++;

    // enable IRQs
	hal_restore_irq( mode );
}

////////////////////////////////////////////
void readlock_rd_unlock( readlock_t * lock )
{
    uint32_t   mode;
	thread_t * this = CURRENT_THREAD;

    // disable IRQs
	hal_disable_irq( &mode );
  
    hal_atomic_dec( &lock->count );
	this->local_locks--;

    // enable IRQs
	hal_restore_irq( mode );
}

//////////////////////////////////////////
void readlock_wr_lock( readlock_t * lock )
{
	uint32_t           mode;
    volatile uint32_t  taken;
    volatile bool_t    isAtomic = false;
    volatile bool_t    noReader = false;
	thread_t         * this = CURRENT_THREAD;

    // disable IRQs
	hal_disable_irq( &mode );
  
    // loop to take the lock
	while( isAtomic == false )
	{
        // check lock state with a simple read
        taken = lock->taken;
		if( taken != 0 ) continue;
 
        // try to atomically take the lock if not already taken
		isAtomic = hal_atomic_cas( &lock->taken , 0 , 1 );
	}

    // wait completion of read accesses
    while( noReader == false )
    {
        noReader = ( lock->count == 0 );
    }
    
    lock->owner = this;
	this->local_locks++;

    // enable IRQs
	hal_restore_irq( mode );
}

////////////////////////////////////////////
void readlock_wr_unlock( readlock_t * lock )
{
    uint32_t   mode;
	thread_t * this = CURRENT_THREAD;

    // disable IRQs
	hal_disable_irq( &mode );
  
    lock->owner = NULL;
    lock->taken = 0;
    this->local_locks--;

    // enable IRQs
	hal_restore_irq( mode );
}

