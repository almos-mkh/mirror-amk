/*
 * string.h - string related helper functions definition.
 *
 * Authors      Ghassan Almaless (2007,2009,2010,2011,2012)
 *              Alain Greiner (2017)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _STRING_H_
#define _STRING_H_

#include <hal_kernel_types.h>


/********************************************************************************************
 * This function returns the length of a character string.
 ********************************************************************************************
 * @ s     : pointer on the string.
 * @ return the number of characters preceding the NUL character.
 *******************************************************************************************/
uint32_t strlen( const char * s );

/********************************************************************************************
 * This function attempts to compute the length of a string, but never scans beyond 
 * <maxlen> characters.
 ********************************************************************************************
 * @ s     : pointer on the string.
 * returns min( maxlen , number of characters preceding the NUL character).
 *******************************************************************************************/
uint32_t strnlen( const char * str, 
                  uint32_t     maxlen );

/********************************************************************************************
 * This function compares lexicographically the strind s1 and s2.
 * Characters are considered unsigned.
 * I does not compare characters after the first NUL character.
 ********************************************************************************************
 * @ s1   : pointer on string.
 * @ s2   : pointer on string.
 * @ return 0 if s1 == s2 / return 1 if s1 > s2 / return -1 if s1 < s2
 *******************************************************************************************/
int strcmp ( const char * s1, 
             const char * s2 );

/********************************************************************************************
 * This function compares lexicographically the strind s1 and s2.
 * I does not compare more than <n> characters and stops after the first NUL character.
 ********************************************************************************************
 * @ s1   : pointer on string.
 * @ s2   : pointer on string.
 * @ n    : max number of compared characters.
 * @ return 0 if s1 == s2 / return 1 if s1 > s2 / return -1 if s1 < s2
 *******************************************************************************************/
int strncmp ( const char * s1,
              const char * s2,
              uint32_t     n );

/********************************************************************************************
 * This function compares lexicographically the strind s1 and s2, ignoring case.
 ********************************************************************************************
 * @ s1   : pointer on string.
 * @ s2   : pointer on string.
 * @ return 0 if s1 == s2 / return 1 if s1 > s2 / return -1 if s1 < s2
 *******************************************************************************************/
int strcasecmp ( const char * s1,
                 const char * s2 );

/********************************************************************************************
 * this function copies the <src> buffer to the <dst> buffer, including the terminating NUL.
 ********************************************************************************************
 * @ dst   : pointer on destination buffer.
 * @ src   : pointer on source buffer.
 *******************************************************************************************/
char * strcpy ( char * dst, 
                char * src );

/********************************************************************************************
 * This function copies <n> characters from the <sr> buffer to the <dst> buffer.
 ********************************************************************************************
 * @ dst   : pointer on destination buffer.
 * @ src   : pointer on source buffer.
 * @ n     : number of characters to be copied.
 *******************************************************************************************/
char * strncpy ( char    * dst,
                 char    * src,
                 uint32_t  n );

/********************************************************************************************
 * This function locates the first occurence of the <find> string in the <s> string.
 ********************************************************************************************
 * @ s     : string to be analysed.
 * @ find  : searched string
 * @ return pointer on the found string / return NULL if not found.
 *******************************************************************************************/
char * strstr( char       * s,
               const char * find);

/********************************************************************************************
 * This function locates the first occurence of the <c> character in the <s> string.
 ********************************************************************************************
 * @ s     : string to be analysed.
 * @ c     : searched character value (casted to a char)
 * @ return pointer on the found character / return NULL if not found.
 *******************************************************************************************/
char * strchr ( const char * s,
                int          c );

/********************************************************************************************
 * This function locates the last occurence of the <c> character in the <s> string.
 ********************************************************************************************
 * @ s     : string to be analysed.
 * @ c     : searched character value (casted to a char)
 * @ return pointer on the found character / return NULL if not found.
 *******************************************************************************************/
char * strrchr ( const char * t,
                 int          c );

/********************************************************************************************
 * This function returns a lower case ASCII code if input is in [A...Z] range.
 ********************************************************************************************
 * @ c     : character to be forced to lower case.
 * @ return lower case ASCII code.
 *******************************************************************************************/
inline uint8_t to_lower( uint8_t  c );

/********************************************************************************************
 * This function returns an upper case ASCII code if input is in [a...z] range.
 ********************************************************************************************
 * @ c     : character to be forced to upper case.
 * @ return upper case ASCII code.
 *******************************************************************************************/
inline uint8_t to_upper( uint8_t  c );

#endif	/* _STRING_H_ */
