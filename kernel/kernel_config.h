/*
 * kernel_config.h - global kernel configuration arguments
 * 
 * Authors  Mohamed Lamine Karaoui (2015)
 *          Alain Greiner (2016,2017,2018,2019)
 *
 * Copyright (c)  UPMC Sorbonne Universites
 * 
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _KERNEL_CONFIG_H_
#define _KERNEL_CONFIG_H_

#define CONFIG_ALMOS_VERSION           "Version 1.1 / October 2018"

////////////////////////////////////////////////////////////////////////////////////////////
//                              KERNEL DEBUG
//  Each debug variable control one kernel function, or one small group of functions. 
//  - trace is generated only when cycle > debug_value.
//  - detailed trace is enabled when (debug_value & Ox1) is non zero.
////////////////////////////////////////////////////////////////////////////////////////////

#define DEBUG_BARRIER_CREATE              0
#define DEBUG_BARRIER_DESTROY             0
#define DEBUG_BARRIER_WAIT                0

#define DEBUG_BUSYLOCK                    0
#define DEBUG_BUSYLOCK_PID                0x10001    // thread pid (when detailed debug)
#define DEBUG_BUSYLOCK_TRDID              0x10000    // thread trdid (when detailed debug)
                 
#define DEBUG_CHDEV_CMD_RX                0
#define DEBUG_CHDEV_CMD_TX                0
#define DEBUG_CHDEV_SERVER_RX             0
#define DEBUG_CHDEV_SERVER_TX             0

#define DEBUG_CLUSTER_INIT                0
#define DEBUG_CLUSTER_PID_ALLOC           0
#define DEBUG_CLUSTER_PID_RELEASE         0
#define DEBUG_CLUSTER_PROCESS_COPIES      0

#define DEBUG_DEV_TXT_RX                  0
#define DEBUG_DEV_TXT_TX                  0
#define DEBUG_DEV_IOC_RX                  0
#define DEBUG_DEV_IOC_TX                  0
#define DEBUG_DEV_NIC_RX                  0
#define DEBUG_DEV_NIC_RX                  0
#define DEBUG_DEV_FBF_RX                  0
#define DEBUG_DEV_FBF_TX                  0
#define DEBUG_DEV_DMA                     0
#define DEBUG_DEV_MMC                     0
#define DEBUG_DEV_PIC                     0

#define DEBUG_DEVFS_GLOBAL_INIT           0
#define DEBUG_DEVFS_LOCAL_INIT            0
#define DEBUG_DEVFS_MOVE                  0

#define DEBUG_ELF_LOAD                    0

#define DEBUG_DQDT_INIT                   0
#define DEBUG_DQDT_UPDATE_THREADS         0
#define DEBUG_DQDT_SELECT_FOR_PROCESS     0
#define DEBUG_DQDT_UPDATE_PAGES           0
#define DEBUG_DQDT_SELECT_FOR_MEMORY      0

#define DEBUG_FATFS_ADD_DENTRY            0
#define DEBUG_FATFS_CLUSTER_ALLOC         0
#define DEBUG_FATFS_CTX_INIT              0
#define DEBUG_FATFS_FREE_CLUSTERS         0
#define DEBUG_FATFS_GET_CLUSTER           0
#define DEBUG_FATFS_GET_USER_DIR          0
#define DEBUG_FATFS_MOVE_PAGE             0
#define DEBUG_FATFS_NEW_DENTRY            0
#define DEBUG_FATFS_RELEASE_INODE         0
#define DEBUG_FATFS_REMOVE_DENTRY         0
#define DEBUG_FATFS_SYNC_FAT              0
#define DEBUG_FATFS_SYNC_FSINFO           0
#define DEBUG_FATFS_SYNC_INODE            0
#define DEBUG_FATFS_UPDATE_DENTRY         0

#define DEBUG_HAL_EXCEPTIONS              0
#define DEBUG_HAL_GPT_SET_PTE             0
#define DEBUG_HAL_GPT_COPY                0
#define DEBUG_HAL_GPT_CREATE              0
#define DEBUG_HAL_GPT_DESTROY             0
#define DEBUG_HAL_IOC_RX                  0
#define DEBUG_HAL_IOC_TX                  0
#define DEBUG_HAL_IRQS                    0
#define DEBUG_HAL_KENTRY                  0
#define DEBUG_HAL_TXT_RX                  0
#define DEBUG_HAL_TXT_TX                  0
#define DEBUG_HAL_USPACE                  0
#define DEBUG_HAL_VMM                     0

#define DEBUG_KCM                         0
#define DEBUG_KMEM                        0

#define DEBUG_KERNEL_INIT                 0

#define DEBUG_MAPPER_GET_PAGE             0
#define DEBUG_MAPPER_HANDLE_MISS          0
#define DEBUG_MAPPER_MOVE_USER            0
#define DEBUG_MAPPER_MOVE_KERNEL          0
#define DEBUG_MAPPER_SYNC                 0

#define DEBUG_MUTEX                       0

#define DEBUG_PPM_ALLOC_PAGES             0
#define DEBUG_PPM_FREE_PAGES              0
   
#define DEBUG_PROCESS_COPY_INIT           0
#define DEBUG_PROCESS_DESTROY             0
#define DEBUG_PROCESS_FD_REGISTER         0
#define DEBUG_PROCESS_GET_LOCAL_COPY      0
#define DEBUG_PROCESS_INIT_CREATE         0
#define DEBUG_PROCESS_MAKE_EXEC           0
#define DEBUG_PROCESS_MAKE_FORK           0
#define DEBUG_PROCESS_REFERENCE_INIT      0
#define DEBUG_PROCESS_SIGACTION           0
#define DEBUG_PROCESS_TXT                 0
#define DEBUG_PROCESS_ZERO_CREATE         0

#define DEBUG_QUEUELOCK_TYPE              0    // lock type (0 : undefined / 1000 : all types)

#define DEBUG_RPC_CLIENT_GENERIC          0
#define DEBUG_RPC_SERVER_GENERIC          0

#define DEBUG_RPC_KCM_ALLOC               0
#define DEBUG_RPC_KCM_FREE                0
#define DEBUG_RPC_MAPPER_HANDLE_MISS      0
#define DEBUG_RPC_MAPPER_MOVE_USER        0
#define DEBUG_RPC_PMEM_GET_PAGES          0
#define DEBUG_RPC_PMEM_RELEASE_PAGES      0
#define DEBUG_RPC_PROCESS_MAKE_FORK       0
#define DEBUG_RPC_PROCESS_SIGACTION       0
#define DEBUG_RPC_THREAD_USER_CREATE      0
#define DEBUG_RPC_THREAD_KERNEL_CREATE    0
#define DEBUG_RPC_VFS_INODE_CREATE        0
#define DEBUG_RPC_VFS_INODE_DESTROY       0
#define DEBUG_RPC_VFS_DENTRY_CREATE       0
#define DEBUG_RPC_VFS_DENTRY_DESTROY      0
#define DEBUG_RPC_VFS_FILE_CREATE         0
#define DEBUG_RPC_VFS_FILE_DESTROY        0
#define DEBUG_RPC_VFS_DEVICE_GET_DENTRY   0
#define DEBUG_RPC_VMM_CREATE_VSEG         0
#define DEBUG_RPC_VMM_GET_PTE             0
#define DEBUG_RPC_VMM_GET_VSEG            0
#define DEBUG_RPC_VMM_DELETE_VSEG         0

#define DEBUG_RWLOCK_TYPE                 0    // lock type (0 : undefined / 1000 : all types) 

#define DEBUG_SCHED_HANDLE_SIGNALS        2
#define DEBUG_SCHED_YIELD                 0     
#define DEBUG_SCHED_RPC_ACTIVATE          0

#define DEBUG_SEM                         0

#define DEBUG_SYSCALLS_ERROR              2

#define DEBUG_SYS_BARRIER                 0
#define DEBUG_SYS_CLOSE                   0
#define DEBUG_SYS_CLOSEDIR                0
#define DEBUG_SYS_CONDVAR                 0
#define DEBUG_SYS_DISPLAY                 0
#define DEBUG_SYS_EXEC                    0
#define DEBUG_SYS_EXIT                    0
#define DEBUG_SYS_FG                      0
#define DEBUG_SYS_FORK                    0
#define DEBUG_SYS_GET_CONFIG              0
#define DEBUG_SYS_GETCWD                  0
#define DEBUG_SYS_GETPID                  0
#define DEBUG_SYS_ISATTY                  0
#define DEBUG_SYS_IS_FG                   0
#define DEBUG_SYS_KILL                    0
#define DEBUG_SYS_OPEN                    0
#define DEBUG_SYS_OPENDIR                 0
#define DEBUG_SYS_MKDIR                   0
#define DEBUG_SYS_MMAP                    0
#define DEBUG_SYS_MUNMAP                  0
#define DEBUG_SYS_MUTEX                   0
#define DEBUG_SYS_READ                    0
#define DEBUG_SYS_READDIR                 0
#define DEBUG_SYS_SEM                     0
#define DEBUG_SYS_STAT                    0
#define DEBUG_SYS_THREAD_CANCEL           0
#define DEBUG_SYS_THREAD_CREATE           0
#define DEBUG_SYS_THREAD_EXIT             0
#define DEBUG_SYS_THREAD_JOIN             0
#define DEBUG_SYS_THREAD_SLEEP            0
#define DEBUG_SYS_THREAD_WAKEUP           0
#define DEBUG_SYS_THREAD_YIELD            0
#define DEBUG_SYS_TRACE                   0
#define DEBUG_SYS_UNLINK                  0
#define DEBUG_SYS_WAIT                    0
#define DEBUG_SYS_WRITE                   0

#define DEBUG_THREAD_BLOCK                0
#define DEBUG_THREAD_DELETE               0
#define DEBUG_THREAD_DESTROY              0
#define DEBUG_THREAD_GET_XPTR             0
#define DEBUG_THREAD_IDLE                 0
#define DEBUG_THREAD_INIT                 0
#define DEBUG_THREAD_KERNEL_CREATE        0
#define DEBUG_THREAD_USER_CREATE          0
#define DEBUG_THREAD_USER_FORK            0
#define DEBUG_THREAD_USER_EXEC            0

#define DEBUG_USER_DIR                    0

#define DEBUG_VFS_ADD_CHILD               0
#define DEBUG_VFS_ADD_SPECIAL             0
#define DEBUG_VFS_CHDIR                   0
#define DEBUG_VFS_CLOSE                   0
#define DEBUG_VFS_DENTRY_CREATE           0
#define DEBUG_VFS_FILE_CREATE             0
#define DEBUG_VFS_GET_PATH                0
#define DEBUG_VFS_INODE_CREATE            0
#define DEBUG_VFS_INODE_LOAD_ALL          0
#define DEBUG_VFS_LINK                    0
#define DEBUG_VFS_LOOKUP                  0
#define DEBUG_VFS_LSEEK                   0
#define DEBUG_VFS_MKDIR                   0
#define DEBUG_VFS_NEW_CHILD_INIT          0
#define DEBUG_VFS_OPEN                    0
#define DEBUG_VFS_OPENDIR                 0
#define DEBUG_VFS_STAT                    0
#define DEBUG_VFS_UNLINK                  0

#define DEBUG_VMM_CREATE_VSEG             0
#define DEBUG_VMM_DELETE_VSEG             0
#define DEBUG_VMM_DESTROY                 0
#define DEBUG_VMM_FORK_COPY               0
#define DEBUG_VMM_GET_ONE_PPN             0
#define DEBUG_VMM_GET_PTE                 0
#define DEBUG_VMM_HANDLE_PAGE_FAULT       0
#define DEBUG_VMM_HANDLE_COW              0
#define DEBUG_VMM_INIT                    0
#define DEBUG_VMM_MMAP_ALLOC              0
#define DEBUG_VMM_PAGE_ALLOCATE           0
#define DEBUG_VMM_RESIZE_VSEG             0
#define DEBUG_VMM_SET_COW                 0
#define DEBUG_VMM_UPDATE_PTE              0

#define DEBUG_XHTAB                       0


////////////////////////////////////////////////////////////////////////////////////////////
//                KERNEL LOCKS TYPES MNEMONICS (for debug)
// It must be kept consistent with the lock_name[] array defined in kernel_init.c file.
////////////////////////////////////////////////////////////////////////////////////////////

#define LOCK_CLUSTER_KCM       1   // local  (B)  protect dynamic KCM creation in cluster
#define LOCK_PPM_FREE          2   // local  (B)  protect PPM allocator free_pages lists
#define LOCK_SCHED_STATE       3   // local  (B)  protect scheduler state for given core
#define LOCK_VMM_STACK         4   // local  (B)  protect VMM stack vseg allocator   
#define LOCK_VMM_MMAP          5   // local  (B)  protect VMM mmap vseg allocator
#define LOCK_VFS_CTX           6   // local  (B)  protect vfs context state
#define LOCK_KCM_STATE         7   // local  (B)  protect KCM allocator state
#define LOCK_KHM_STATE         8   // local  (B)  protect KHM allocator state
#define LOCK_HTAB_STATE        9   // local  (B)  protect a local htab state

#define LOCK_THREAD_JOIN      10   // remote (B)  protect join/exit between two threads
#define LOCK_XHTAB_STATE      11   // remote (B)  protect  a distributed xhtab state
#define LOCK_CHDEV_QUEUE      12   // remote (B)  protect chdev threads waiting queue
#define LOCK_CHDEV_TXT0       13   // remote (B)  protect access to kernel terminal TXT0
#define LOCK_CHDEV_TXTLIST    14   // remote (B)  protect xlist of processes using same TXT
#define LOCK_PAGE_STATE       15   // remote (B)  protect physical page descriptor state
#define LOCK_MUTEX_STATE      16   // remote (B)  protect user mutex state
#define LOCK_CONDVAR_STATE    17   // remote (B)  protect user condvar state
#define LOCK_SEM_STATE        18   // remote (B)  protect user semaphore state
#define LOCK_PROCESS_CWD      19   // remote (B)  protect current working directory in process
#define LOCK_BARRIER_STATE    20   // remote (B)  protect user barrier state

#define LOCK_CLUSTER_PREFTBL  21   // local  (Q)  protect array of ref. processes in cluster

#define LOCK_PPM_DIRTY        22   // remote (Q)  protect PPM dirty_pages list
#define LOCK_CLUSTER_LOCALS   23   // remote (Q)  protect xlist of local processes in cluster
#define LOCK_CLUSTER_COPIES   24   // remote (Q)  protect xlist of process copies in cluster
#define LOCK_PROCESS_CHILDREN 25   // remote (Q)  protect xlist of chidren process in process
#define LOCK_PROCESS_USERSYNC 26   // remote (Q)  protect lists of user synchros in process
#define LOCK_PROCESS_FDARRAY  27   // remote (Q)  protect array of open files in owner process 
#define LOCK_FATFS_FREE       28   // remote (Q)  protect the FATFS context (free clusters)
#define LOCK_PROCESS_DIR      29   // remote (Q)  protect xlist of open directories in process

#define LOCK_PROCESS_THTBL    30   // local  (RW) protect local array of threads in a process 

#define LOCK_MAPPER_STATE     31   // remote (RW) protect mapper state
#define LOCK_VFS_SIZE         32   // remote (RW) protect inode state and associated mapper
#define LOCK_VFS_FILE         33   // remote (RW) protect file descriptor state
#define LOCK_VMM_VSL          34   // remote (RW) protect VSL (local list of vsegs)
#define LOCK_VMM_GPT          35   // remote (RW) protect GPT (local page table)
#define LOCK_VFS_MAIN         36   // remote (RW) protect vfs traversal (in root inode)


////////////////////////////////////////////////////////////////////////////////////////////
//                          HARDWARE CONFIGURATION     
////////////////////////////////////////////////////////////////////////////////////////////

#define CONFIG_KERNEL_IDENTITY_MAP          true       // True for 32 bits cores             
#define CONFIG_MAX_CLUSTERS_X               16         // max number of clusters in row
#define CONFIG_MAX_CLUSTERS_Y               16         // max number of clusters in column
#define CONFIG_MAX_CLUSTERS                 (CONFIG_MAX_CLUSTERS_X * CONFIG_MAX_CLUSTERS_Y) 
#define CONFIG_MAX_LOCAL_CORES              4          // max number of cores per cluster
#define CONFIG_MAX_INT_DEV                  4          // max number of internal peripherals
#define CONFIG_MAX_EXT_DEV                  8          // max number of external peripherals
#define CONFIG_MAX_INTERNAL_IRQS            32         // max number of internal IRQs
#define CONFIG_MAX_EXTERNAL_IRQS            32         // max number of external IRQs
#define CONFIG_IRQS_PER_CORE                4          // number of input IRQs per core
#define CONFIG_CLUSTER_SPAN                 32         // ln(phys. address space per cluster)
#define CONFIG_CACHE_LINE_SIZE              64         // number of bytes in cache line

#define CONFIG_CACHE_LINE_ALIGNED           __attribute__((aligned(CONFIG_CACHE_LINE_SIZE)))

#define CONFIG_MAX_IOC_CHANNELS             1          // max number of IOC device channels
#define CONFIG_MAX_FBF_CHANNELS             1          // max number of FBF device channels
#define CONFIG_MAX_TXT_CHANNELS             8          // max number of TXT device channels
#define CONFIG_MAX_DMA_CHANNELS             4          // max number of DMA device channels
#define CONFIG_MAX_NIC_CHANNELS             4          // max number of NIC device channels

#define CONFIG_TXT_ECHO_MODE                1          // echo mode for TXT peripheral
#define CONFIG_TXT_KBUF_SIZE                512        // kernel buffer for TXT write

#define CONFIG_FBF_WIDTH                    128
#define CONFIG_FBF_HEIGHT                   128

#define CONFIG_CPU_CTX_SIZE                 256        // up to 64 uint32_t registers
#define CONFIG_FPU_CTX_SIZE                 256        // up to 32 uint64_t registers
#define CONFIG_NIC_CHBUF_DEPTH              1          // number of containers in NIC chbuf

////////////////////////////////////////////////////////////////////////////////////////////
//                            PROCESS MANAGEMENT       
////////////////////////////////////////////////////////////////////////////////////////////

#define CONFIG_MAX_PROCESS_PER_CLUSTER      16         // max number of owned process
#define CONFIG_PROCESS_FILE_MAX_NR          16         // max number of open files
#define CONFIG_PROCESS_MAX_CHILDREN         1024       // max number of children process
#define CONFIG_PROCESS_HEAP_MIN_SIZE        0x00010000 // user heap min size (bytes)
#define CONFIG_PROCESS_HEAP_MAX_SIZE        0x30000000 // user heap max size (bytes)
#define CONFIG_PROCESS_INIT_PATH            "/bin/user/init.elf"

////////////////////////////////////////////////////////////////////////////////////////////
//                            VIRTUAL FILE SYSTEM        
////////////////////////////////////////////////////////////////////////////////////////////

#define CONFIG_VFS_MAX_INODES               128        // max number of inodes per cluster
#define CONFIG_VFS_MAX_NAME_LENGTH          56         // dentry name max length (bytes)
#define CONFIG_VFS_MAX_PATH_LENGTH          256        // pathname max length (bytes)
#define CONFIG_VFS_FREE_CLUSTERS_MIN        32         // min number of free clusters
#define CONFIG_VFS_MAX_DENTRIES             63         // max number of dentries in one dir

#define CONFIG_VFS_ROOT_IS_FATFS            1          // root FS is FATFS if non zero
#define CONFIG_VFS_ROOT_IS_EX2FS            0          // root FS is EX2FS if non zero

#define CONFIG_MAPPER_GRDXT_W1              7      // number of bits for RADIX_TREE_IX1
#define CONFIG_MAPPER_GRDXT_W2              7      // number of bits for RADIX_TREE_IX2
#define CONFIG_MAPPER_GRDXT_W3              6      // number of bits for RADIX_TREE_IX3

////////////////////////////////////////////////////////////////////////////////////////////
//                                  DQDT        
////////////////////////////////////////////////////////////////////////////////////////////

#define CONFIG_DQDT_LEVELS_NR               5
#define CONFIG_DQDT_TICKS_PER_QUANTUM       1        // number of ticks between updates

////////////////////////////////////////////////////////////////////////////////////////////
//                              RANDOM NUMBERS 
////////////////////////////////////////////////////////////////////////////////////////////

#define CONFIG_RDNG_PARAM_A                 65519
#define CONFIG_RDNG_PARAM_C                 64037

////////////////////////////////////////////////////////////////////////////////////////////
//                                SCHEDULING 
////////////////////////////////////////////////////////////////////////////////////////////

#define CONFIG_SCHED_TICK_MS_PERIOD         10000   // number of milliseconds per period
#define CONFIG_SCHED_TICKS_PER_QUANTUM      1       // number of ticks between scheduling
#define CONFIG_SCHED_MAX_THREADS_NR         32      // max number of threads per core
#define CONFIG_SCHED_IDLE_MODE_SLEEP        0       // idle thread use sleep mode if non 0

////////////////////////////////////////////////////////////////////////////////////////////
//                                 THREADS 
////////////////////////////////////////////////////////////////////////////////////////////

#define CONFIG_THREADS_MAX_PER_CLUSTER      32      // max threads per cluster per process
#define CONFIG_THREAD_DESC_SIZE             0x4000  // thread desc size (with kernel stack)
#define CONFIG_THREAD_DESC_ORDER            2       // ln( number of 4K pages )

////////////////////////////////////////////////////////////////////////////////////////////
//                             REMOTE PROCEDURE CALL
////////////////////////////////////////////////////////////////////////////////////////////

#define CONFIG_REMOTE_FIFO_SLOTS		    16
#define CONFIG_REMOTE_FIFO_MAX_ITERATIONS   1024
#define CONFIG_RPC_THREADS_MAX              4      // max number of RPC threads per core

////////////////////////////////////////////////////////////////////////////////////////////
//             32 bits  USER SPACE SEGMENTATION / all values are numbers of pages
////////////////////////////////////////////////////////////////////////////////////////////

#define CONFIG_VMM_VSPACE_SIZE        0x100000     // virtual space          : 4   Gbytes

#define CONFIG_VMM_UTILS_BASE         0x000200     // UTILS zone base        : 2   Mbytes
#define CONFIG_VMM_ELF_BASE           0x000400     // ELF zone base          : 4   Mbytes
#define CONFIG_VMM_HEAP_BASE          0x002000     // HEAP zone base         : 32  Mbytes
#define CONFIG_VMM_STACK_BASE         0x0C0000     // STACK zone base        : 3   Gbytes

#define CONFIG_VMM_ARGS_SIZE          0x000004     // args vseg size         : 16  Kbytes
#define CONFIG_VMM_ENVS_SIZE          0x000008     // envs vseg size         : 32  Kbytes
#define CONFIG_VMM_STACK_SIZE         0x000100     // single stack vseg size : 1   Mbytes

////////////////////////////////////////////////////////////////////////////////////////////
//                      PHYSICAL MEMORY MANAGEMENT         
////////////////////////////////////////////////////////////////////////////////////////////

#define CONFIG_PPM_PAGE_SIZE          4096         // physical page size (bytes)
#define CONFIG_PPM_PAGE_SHIFT         12           // physical page shift (bits)
#define CONFIG_PPM_PAGE_MASK          0x00000FFF   // physical page mask     
#define CONFIG_PPM_MAX_ORDER          16           // ln(total number of pages per cluster)
#define CONFIG_PPM_HEAP_ORDER         10           // ln(number of heap pages per cluster)
#define CONFIG_PPM_MAX_RSVD           32           // max reserved zones on the machine

#define CONFIG_KCM_SLOT_SIZE          64           // smallest allocated block (bytes)

#define CONFIG_PPM_PAGE_ALIGNED       __attribute__((aligned(CONFIG_PPM_PAGE_SIZE)))


////////////////////////////////////////////////////////////////////////////////////////////
//                 INSTRUMENTATION
////////////////////////////////////////////////////////////////////////////////////////////

#define CONFIG_INTRUMENTATION_SYSCALLS  0





#endif	/* _KERNEL_CONFIG_H_ */
