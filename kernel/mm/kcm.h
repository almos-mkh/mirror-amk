/*
 * kcm.h - Per-cluster Kernel Cache Manager definition.
 *
 * Authors  Ghassan Almaless (2008,2009,2010,2011,2012)
 *          Alain Greiner    (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _KCM_H_
#define _KCM_H_

#include <list.h>
#include <hal_kernel_types.h>
#include <busylock.h>
#include <page.h>
#include <bits.h>
#include <kmem.h>

/****************************************************************************************
 * This structure defines a generic Kernel Cache Manager, that is a block allocator,
 * for fixed size objects. It exists a specific KCM allocator for each object type.
 * The actual allocated block size is the smallest multiple of the KCM slot, that
 * contain one single object. The KCM slot is 64 bytes, as it must be large enough
 * to store the kcm_page descriptor, defined below.
 * The various KCM allocators themselves are not statically allocated in the cluster
 * manager, but are dynamically allocated when required, using the embedded KCM
 * allocator defined in the cluster manager, to allocate the other ones...
 ***************************************************************************************/

typedef struct kcm_s
{
	busylock_t           lock;             /*! protect KCM ammocator                   */
	uint32_t             block_size;       /*! rounded block size (bytes)              */
	uint32_t             blocks_nr;        /*! max number of blocks per page           */

	list_entry_t         active_root;      /*! root of active pages list               */
	list_entry_t         busy_root;        /*! root of busy pages list                 */
	list_entry_t         free_root;        /*! root of free pages list                 */

	uint32_t             free_pages_nr;    /*! number of free pages                    */
	uint32_t             busy_pages_nr;    /*! number of busy pages                    */
	uint32_t             active_pages_nr;  /*! number of active pages                  */

	uint32_t             type;             /*! KCM type                                */
}
kcm_t;


/****************************************************************************************
 * This structure defines a KCM-page descriptor.
 * A KCM-page contains at most (CONFIG_PPM_PAGE_SIZE / CONFIG_KCM_SLOT_SIZE) blocks.
 * This kcm page descriptor is stored in the first slot of the page.
 ***************************************************************************************/

typedef struct kcm_page_s
{
	uint32_t        bitmap[2];             /*! at most 64 blocks in a single page      */
	list_entry_t    list;                  /*! [active / busy / free] list member      */
	kcm_t         * kcm;                   /*! pointer on kcm allocator                */
	page_t        * page;                  /*! pointer on the physical page descriptor */
	uint32_t        count;                 /*! number of allocated blocks              */
	uint32_t        busy;                  /*! page busy if non zero                   */
	uint32_t        active;                /*! page active if non zero                 */
}
kcm_page_t;

/****************************************************************************************
 * This function initializes a generic Kernel Cache Manager.
 ****************************************************************************************
 * @ kcm      : pointer on KCM manager to initialize.
 * @ type     : KCM allocator type.
 ***************************************************************************************/
void kcm_init( kcm_t    * kcm,
               uint32_t   type );

/****************************************************************************************
 * This function releases all memory allocated to a generic Kernel Cache Manager.
 ****************************************************************************************
 * @ kcm      : pointer on KCM manager to destroy.
 ***************************************************************************************/
void kcm_destroy( kcm_t  * kcm );

/****************************************************************************************
 * This function allocates one single object from a Kernel Cache Manager
 * The object size must be smaller than one page size.
 ****************************************************************************************
 * @ kcm      :  pointer on the selected KCM allocator
 * @ return pointer on allocated block if success / return NULL if failure
 ***************************************************************************************/
void * kcm_alloc( kcm_t * kcm );

/****************************************************************************************
 * This function releases a previouly allocated block containing one object.
 ****************************************************************************************
 * @ ptr   : local pointer on the allocated buffer.
 ***************************************************************************************/
void kcm_free( void * ptr );

/****************************************************************************************
 * This function prints KCM allocator state (for debug only).
 ****************************************************************************************
 * @ kcm   : local pointer on the selected KCM allocator.
 ***************************************************************************************/
void kcm_print( kcm_t * kcm );

#endif	/* _KCM_H_ */
