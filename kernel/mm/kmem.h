/*
 * kmem.h - kernel unified memory allocator interface
 *
 * Authors  Ghassan Almaless (2008,2009,2010,2011,2012)
 *          Alain Greiner (2016,2017,2018)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _KMEM_H_
#define _KMEM_H_

#include <hal_kernel_types.h>
#include <kcm.h>

/*************************************************************************************
 * This enum defines the Kernel Memory Types for dynamically allocated objects.
 * WARNING : this enum must be kepts consistent with use in kmem.c file.
 ************************************************************************************/

enum
{
    KMEM_PAGE             = 0,   /*! reserved for PPM allocator                     */
    KMEM_GENERIC          = 1,   /*! reserved for KHM allocator                     */
    KMEM_KCM              = 2,   /*! kcm_t                                          */
    KMEM_VSEG             = 3,   /*! vseg_t                                         */
    KMEM_DEVICE           = 4,   /*! device_t                                       */
    KMEM_MAPPER           = 5,   /*! mapper_t                                       */
    KMEM_PROCESS          = 6,   /*! process_t                                      */
    KMEM_CPU_CTX          = 7,   /*! hal_cpu_context_t                              */
    KMEM_FPU_CTX          = 8,   /*! hal_fpu_context_t                              */
    KMEM_GEN_BARRIER      = 9,   /*! generi_cbarrier_t                              */

    KMEM_SMP_BARRIER      = 10,  /*! simple_barrier_t                               */
    KMEM_DEVFS_CTX        = 11,  /*! fatfs_inode_t                                  */
    KMEM_FATFS_CTX        = 12,  /*! fatfs_ctx_t                                    */
    KMEM_VFS_CTX          = 13,  /*! vfs_context_t                                  */
    KMEM_VFS_INODE        = 14,  /*! vfs_inode_t                                    */
    KMEM_VFS_DENTRY       = 15,  /*! vfs_dentry_t                                   */
    KMEM_VFS_FILE         = 16,  /*! vfs_file_t                                     */
    KMEM_SEM              = 17,  /*! remote_sem_t                                   */
    KMEM_CONDVAR          = 18,  /*! remote_condvar_t                               */
    KMEM_MUTEX            = 19,  /*! remote_mutex_t                                 */

    KMEM_DIR              = 20,  /*! remote_dir_t                                   */
    KMEM_512_BYTES        = 21,  /*! 512 bytes aligned                              */

    KMEM_TYPES_NR         = 22,
};

/*************************************************************************************
 * This defines the generic Allocation Flags that can be associated to
 * a Kernel Memory Request.
 ************************************************************************************/

#define AF_NONE       0x0000   // no attributes
#define AF_KERNEL     0x0001   // for kernel use
#define AF_ZERO       0x0002   // must be reset to 0

/*************************************************************************************
 * This structure defines a Kernel Memory Request.
 ************************************************************************************/

typedef struct kmem_req_s
{
    uint32_t      type;   /*! request type                                          */
    uint32_t      size;   /*! ln2(nb_pages) if PPM / bytes if KHM / unused by KCM   */
    uint32_t      flags;  /*! request attributes                                    */
    void        * ptr;    /*! local pointer on allocated buffer (only used by free) */
}
kmem_req_t;

/*************************************************************************************
 * This generic function allocates physical memory in the local cluster
 * as specified by the request descriptor.
 * It uses three specialised physical memory allocators, depending on request type:
 * - PPM (Physical Pages Manager) allocates N contiguous physical pages,
 *       N must be a power of 2.
 * - KHM (Kernel Heap Manager) allocates a physical memory buffer,
 *       that can have any size.
 * - KCM (Kernel Cache Manager) allocates various fixed size objects,
 *       handling a dedicated cache for each object type.
 *************************************************************************************
 * @ req   : local pointer to allocation request.
 * @ return a local pointer on page descriptor if KMEM_PAGE.
 *   return a local pointer to allocated buffer if KCM or KHM.
 *   return NULL if no physical memory available.
 ************************************************************************************/
void * kmem_alloc( kmem_req_t * req );

/*************************************************************************************
 * This function releases previously allocated physical memory, as specified
 * by the "type" and "ptr" fiels of the kmem-req_t request.
 *************************************************************************************
 * @ req : local pointer to request descriptor.
 ************************************************************************************/
void  kmem_free ( kmem_req_t * req );

/*************************************************************************************
 * This function returns a printable string for a kmem object type.
 *************************************************************************************
 * @ type   : kmem object type.
 ************************************************************************************/
char * kmem_type_str( uint32_t type );

/*************************************************************************************
 * This function returns the size (bytes) for a kmem object type.
 *************************************************************************************
 * @ type   : kmem object type.
 ************************************************************************************/
uint32_t kmem_type_size( uint32_t type );

/*************************************************************************************
 * This function displays the content of the KCM pointers Table
 ************************************************************************************/
void kmem_print_kcm_table( void );


#endif	/* _KMEM_H_ */
