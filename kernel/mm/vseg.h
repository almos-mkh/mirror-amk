/*
 * vseg.h - virtual segment (vseg) related operations
 *
 * Authors   Ghassan Almaless (2008,2009,2010,2011, 2012)
 *           Mohamed Lamine Karaoui (2015)
 *           Alain Greiner (2016,2017,2018,2019)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef _VSEG_H_
#define _VSEG_H_

#include <hal_kernel_types.h>
#include <vfs.h>

/****  Forward declarations  ****/

struct vmm_s;

/*******************************************************************************************
 * This enum defines the vseg types.
 * Note : the KDATA and KDEV types are not used by the TSAR HAL, because the accesses
 *        to kernel data or kernel devices are done through the DATA extension address
 *        register, but these types are probably required by the I86 HAL [AG].
 ******************************************************************************************/

typedef enum
{
    VSEG_TYPE_CODE   = 0,          /*! executable user code     / private / localized     */
    VSEG_TYPE_DATA   = 1,          /*! initialized user data    / public  / distributed   */
    VSEG_TYPE_STACK  = 2,          /*! execution user stack     / private / localized     */
    VSEG_TYPE_ANON   = 3,          /*! anonymous mmap           / public  / localized     */
    VSEG_TYPE_FILE   = 4,          /*! file mmap                / public  / localized     */
    VSEG_TYPE_REMOTE = 5,          /*! remote mmap              / public  / localized     */

    VSEG_TYPE_KCODE  = 6,          /*! executable kernel code   / private / localized     */
    VSEG_TYPE_KDATA  = 7,          /*! initialized kernel data  / private / localized     */
    VSEG_TYPE_KDEV   = 8,          /*! kernel peripheral device / public  / localized     */
}
vseg_type_t;


/*******************************************************************************************
 * These masks define the vseg generic (hardware independent) flags.
 ******************************************************************************************/

#define VSEG_USER     0x0001       /*! user accessible                                    */
#define VSEG_WRITE    0x0002       /*! writeable                                          */
#define VSEG_EXEC     0x0004       /*! executable                                         */
#define VSEG_CACHE    0x0008       /*! cachable                                           */
#define VSEG_PRIVATE  0x0010       /*! should not be accessed from another cluster        */
#define VSEG_DISTRIB  0x0020       /*! physically distributed on all clusters             */

/*******************************************************************************************
 * This structure defines a virtual segment descriptor.
 * - The VSL contains only local vsegs, but is implemented as an xlist, because it can be
 *   accessed by thread running in a remote cluster.
 * - The zombi list is used by the local MMAP allocator. It is implemented as a local list.
 ******************************************************************************************/

typedef struct vseg_s
{
    xlist_entry_t     xlist;        /*! all vsegs in same VSL                             */
    list_entry_t      zlist;        /*! all vsegs in same zombi list                      */
    struct vmm_s    * vmm;          /*! pointer on associated VM manager                  */
    uint32_t          type;         /*! vseg type                                         */
    intptr_t          min;          /*! segment min virtual address                       */
    intptr_t          max;          /*! segment max virtual address (excluded)            */
    vpn_t             vpn_base;     /*! first page of vseg                                */
    vpn_t             vpn_size;     /*! number of pages occupied                          */
    uint32_t          flags;        /*! vseg attributes                                   */
    xptr_t            mapper_xp;    /*! xptr on remote mapper (for types CODE/DATA/FILE)  */
    intptr_t          file_offset;  /*! vseg offset in file (for types CODE/DATA/FILE)    */
    intptr_t          file_size;    /*! max segment size in mapper (for type CODE/DATA)   */
    cxy_t             cxy;          /*! physical mapping (for non distributed vseg)       */ 
}
vseg_t;

/*******************************************************************************************
 * This function returns a printable string for the vseg type. 
 *******************************************************************************************
 * @ vseg_type  : type of vseg
 * @ return pointer on string
 ******************************************************************************************/
char * vseg_type_str( uint32_t vseg_type );

/*******************************************************************************************
 * This function allocates physical memory for a new vseg descriptor from the local cluster
 * physical memory allocator.
 *******************************************************************************************
 * @ return pointer on allocated vseg descriptor if success / return NULL if failure.
 ******************************************************************************************/
vseg_t * vseg_alloc( void );

/*******************************************************************************************
 * This function releases the physical memory allocated for a vseg descriptor 
 * to the local cluster physical memory allocator.
 *******************************************************************************************
 * @ vseg   : local pointer on released vseg descriptor.
 ******************************************************************************************/
void vseg_free( vseg_t * vseg );

/*******************************************************************************************
 * This function initializes a local vseg descriptor, from the arguments values.
 * It does NOT register the vseg in the local VMM.
 *******************************************************************************************
 * @ vseg      : pointer on the vseg descriptor.
 * @ base      : vseg base address.
 * @ size      : vseg size (bytes).
 * @ vpn_base  : first page index.
 * @ vpn_size  : number of pages.
 * @ type      : vseg type.
 * @ cxy       : target cluster for physical mapping.
 ******************************************************************************************/
void vseg_init( vseg_t      * vseg,
	            vseg_type_t   type,
                intptr_t      base,
	            uint32_t      size,
                vpn_t         vpn_base,
                vpn_t         vpn_size,
                uint32_t      file_offset,
                uint32_t      file_size,
                xptr_t        mapper_xp,
                cxy_t         cxy );

/*******************************************************************************************
 * This function initializes a local vseg descriptor from values contained in a reference
 * remote vseg descriptor. It does NOT register the vseg in the local VMM.
 *******************************************************************************************
 * @ vseg      : pointer on the vseg descriptor.
 * @ ref_xp    : extended pointer on the reference vseg descriptor.
 ******************************************************************************************/
void vseg_init_from_ref( vseg_t * vseg,
                         xptr_t   ref_xp );


#endif /* _VSEG_H_ */
