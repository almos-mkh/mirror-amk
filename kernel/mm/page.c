/*
 * page.c - physical page related operations implementation
 *
 * Authors  Ghassan Almaless (2008,2009,2010,2011,2012)
 *          Alain Greiner    (2016,2017)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <hal_kernel_types.h>
#include <hal_special.h>
#include <hal_atomic.h>
#include <list.h>
#include <queuelock.h>
#include <memcpy.h>
#include <printk.h>
#include <vfs.h>
#include <process.h>
#include <page.h>


////////////////////////////////////////
inline void page_init( page_t   * page )
{
	page->flags    = 0;
	page->order    = 0;
	page->mapper   = NULL;
	page->index    = 0;
	page->refcount = 0;
	page->forks    = 0;

	remote_busylock_init( XPTR( local_cxy , &page->lock ), LOCK_PAGE_STATE );

	list_entry_init( &page->list );
}

////////////////////////////////////////////
inline void page_set_flag( page_t   * page,
                           uint32_t   value )
{
	hal_atomic_or( (uint32_t *)&page->flags , (uint32_t)value );
}

//////////////////////////////////////////////
inline void page_clear_flag( page_t   * page,
                             uint32_t   value )
{
	hal_atomic_and( (uint32_t *)&page->flags , ~((uint32_t)value) );
}

//////////////////////////////////////////////
inline bool_t page_is_flag( page_t   * page,
                            uint32_t   value )
{
    return ( (page->flags & value) ? 1 : 0 );
}

////////////////////////////////////////////
inline void page_refcount_up( page_t *page )
{
    hal_atomic_add( &page->refcount , +1 );
}

//////////////////////////////////////////////
inline void page_refcount_down( page_t *page )
{
    hal_atomic_add( &page->refcount , -1 );
}

///////////////////////////////
void page_zero( page_t * page )
{
	uint32_t   size = (1 << page->order) * CONFIG_PPM_PAGE_SIZE;

	xptr_t base_xp = ppm_page2base( XPTR( local_cxy , page ) );

	memset( GET_PTR( base_xp ) , 0 , size );
}

////////////////////////////////
void page_print( page_t * page )
{
	printk("*** Page %d : base = %x / flags = %x / order = %d / count = %d\n",
	        page->index,
	        GET_PTR( ppm_page2base( XPTR( local_cxy , page ) ) ),
	        page->flags,
	        page->order,
	        page->refcount );
}

