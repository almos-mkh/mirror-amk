/****************************************************************************
 * This file contains a minimal kernel used for testing the boot-loader.    *
 ****************************************************************************/

#include <boot-info.h>
#include <boot_types.h>
#include <boot_utils.h>

__attribute__((section(".kdata"))) boot_info_t boot_info;

__attribute__((section(".kinit"))) void        kern_test_init()
{
    if (boot_get_procid() == 0)
        boot_printf("\nBSCPU reached the kernel !!!!!\n");

    while (1);
}
