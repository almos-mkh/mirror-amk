/*********************************************************************************
 * This file defines various configuration parameters for ALMOS-MKH boot-loader. 
 ********************************************************************************/

#ifndef _BOOT_CONFIG_H
#define _BOOT_CONFIG_H

// Debug options
#define DEBUG_BOOT_MULTI    0        
#define DEBUG_BOOT_INFO     0
#define DEBUG_BOOT_ELF      0
#define DEBUG_BOOT_IOC      0
#define DEBUG_BOOT_WAKUP    0
#define DEBUG_BOOT_FAT32    0

// Boot cluster definition 
#define BOOT_CLUSTER_CXY    0

// cache line
#define CACHE_LINE_SIZE     64

// paths for kernel.elf and arch_info.bin files
#define ARCHINFO_PATHNAME   "arch_info.bin"
#define KERNEL_PATHNAME     "bin/kernel/kernel.elf"

// Preloader segment
#define PRELOADER_BASE      0x00000000      // 'preloader' physical base address
#define PRELOADER_MAX_SIZE  0x00004000      // 'preloader' max size

// kcode segment
#define KCODE_BASE          0x00004000      // 'kcode' segment physical base address
#define KCODE_MAX_SIZE      0x000FC000      // 'kcode' + 'kdata' segments max size

// boot.elf file temporary buffer
#define BOOT_BASE           0x00200000      // 'boot.elf' file physical base address   
#define BOOT_MAX_SIZE       0x00010000      // 'boot.elf' file max size (64 Kbytes)

// arch_info file temporary buffer
#define ARCHINFO_BASE       0x00300000      // 'arch_info.bin' file physical base address
#define ARCHINFO_MAX_SIZE   0x00010000      // 'arch_info.bin' file max size (64 Kbytes)

// kernel.elf file temporary buffer
#define KERN_BASE           0x00400000      // 'kernel.elf' file base address 
#define KERN_MAX_SIZE       0x00200000      // 'kernel.elf' file max size

// Temporary stacks segments
#define BOOT_STACK_BASE     0x00600000      // Boot stack base address
#define BOOT_STACK_SIZE     0x00004000      // Boot stack size (16Kb)



#endif  // _BOOT_CONFIG_H
