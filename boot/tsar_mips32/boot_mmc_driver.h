///////////////////////////////////////////////////////////////////////////////////
// File     : mmc_driver.h
// Date     : 01/11/2013
// Author   : alain greiner
// Copyright (c) UPMC-LIP6
///////////////////////////////////////////////////////////////////////////////////
// This simplified driver provides only one INVAL command, and does not
// use any lock to access the MMC registers, as the boot-loader does not
// generate concurrent accesses to the MMC configuration interface. 
///////////////////////////////////////////////////////////////////////////////////

#ifndef _GIET_MMC_DRIVERS_H_
#define _GIET_MMC_DRIVERS_H_

#include <hal_kernel_types.h>

///////////////////////////////////////////////////////////////////////////////////
// TSAR Memory Cache configuration registers offsets and commands
///////////////////////////////////////////////////////////////////////////////////

enum SoclibMemCacheFunc
{
    MEMC_CONFIG = 0,
    MEMC_INSTRM = 1,
    MEMC_RERROR = 2,

    MEMC_FUNC_SPAN = 0x200
};

enum SoclibMemCacheConfigRegs
{
    MEMC_ADDR_LO,
    MEMC_ADDR_HI,
    MEMC_BUF_LENGTH,
    MEMC_CMD_TYPE
};

enum SoclibMemCacheConfigCmd
{
    MEMC_CMD_NOP,
    MEMC_CMD_INVAL,
    MEMC_CMD_SYNC
};

#define MMC_REG(func,idx) ((func<<7)|idx) 

///////////////////////////////////////////////////////////////////////////////////
// MEMC access functions (for TSAR architecture)
///////////////////////////////////////////////////////////////////////////////////

extern int boot_mmc_inval( uint64_t buf_paddr, 
                           uint32_t buf_length );


#endif

// Local Variables:
// tab-width: 4
// c-basic-offset: 4
// c-file-offsets:((innamespace . 0)(inline-open . 0))
// indent-tabs-mode: nil
// End:
// vim: filetype=c:expandtab:shiftwidth=4:tabstop=4:softtabstop=4

