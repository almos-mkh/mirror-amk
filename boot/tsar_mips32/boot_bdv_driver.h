///////////////////////////////////////////////////////////////////////////////////
// File     : boot_bdv_driver.h
// Date     : 01/11/2013
// Authors  : Alain Greiner / Vu Son
// Copyright (c) UPMC-LIP6
///////////////////////////////////////////////////////////////////////////////////
// This file defines a simplified driver for the SocLib vci_block_device,  
// a single channel, block-oriented, external mass storage peripheral. 
// The driver is used by the ALMOS-MKH boot-loader when the USE_IOC_BDV flag 
// is set in the hard_config.h file.
// All accesses to the device registers are performed via 2 low-level 
// functions boot_bdv_get_register() and boot_bdv_set_register().  
// Since the driver is used by the boot-loader, it implements synchronous 
// accesses to block device by polling the BDV_STATUS register to detect 
// transfer completion, as interrupts are not activated.                    
///////////////////////////////////////////////////////////////////////////////////

#ifndef BOOT_BDV_DRIVER_H
#define BOOT_BDV_DRIVER_H

#include <hal_kernel_types.h>

/****************************************************************************
 *                            Driver register map.                          *
 ****************************************************************************/

enum BDV_registers
{
    BDV_BUFFER      = 0,    /* 32 LSB of the source or destination buffer   */
    BDV_LBA         = 1,    /* LBA of the first block to be transfered.     */
    BDV_COUNT       = 2,    /* Number of blocks to be transfered.           */
    BDV_OPERATION   = 3,    /* Operation to perform.                        */
    BDV_STATUS      = 4,    /* Transfer status.                             */
    BDV_IRQ_ENABLE  = 5,    /* Boolean enabling the IRQ line.               */
    BDV_SIZE        = 6,    /* Count of addessable blocks in the device.    */
    BDV_BLOCK_SIZE  = 7,    /* Block size (in bytes).                       */
    BDV_BUFFER_EXT  = 8,    /* 32 MSB of the source or destination buffer   */

    BDV_SPAN        = 9,    /* BDV segment size (32 bits words)             */
};

/****************************************************************************
 *                             Driver operations.                           *
 ****************************************************************************/

enum BDV_operations
{
    BDV_NOOP    = 0,
    BDV_READ    = 1,
    BDV_WRITE   = 2,
};

/****************************************************************************
 *                           Driver status values.                          *
 ****************************************************************************/

enum BDV_status
{
    BDV_IDLE        = 0,
    BDV_BUSY        = 1,
    BDV_READ_SUCC   = 2,
    BDV_WRITE_SUCC  = 3,
    BDV_READ_ERR    = 4,
    BDV_WRITE_ERR   = 5,
};

/****************************************************************************
 *                           Driver API functions.                          *
 ****************************************************************************/

/****************************************************************************
 * This function checks if a block size is indeed 512 bytes, and            *
 * desactivates the interrupts.                                             *
 * @ returns 0 on success, -1 on error.                                     *
 ****************************************************************************/
int boot_bdv_init( void );

/****************************************************************************
 * This function transfers 'count' blocks from the block device to memory.
 * @ lba        : LBA of the first block on the block device.               *
 * @ buf_paddr  : memory buffer physical address.                           *
 * @ count      : number of blocks to be transfered.                        *
 * @ returns 0 on success, -1 on error.                                     *
 ****************************************************************************/
int boot_bdv_access( uint32_t  lba,
                     xptr_t    buf_paddr,
                     uint32_t  count );

#endif // BOOT_BDV_DRIVER_H
