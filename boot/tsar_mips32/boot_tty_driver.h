/*
 * boot_tty_driver.h - TSAR bootloader TTY driver definition.
 * 
 * Authors :   Alain Greiner / Vu Son  (2016)
 *
 * Copyright (c) UPMC Sorbonne Universites
 *
 * This file is part of ALMOS-MKH.
 *
 * ALMOS-MKH is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2.0 of the License.
 *
 * ALMOS-MKH is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ALMOS-MKH; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

/****************************************************************************
 * This file defines a nano-driver for SocLib vci_multi_tty component, 
 * used by the ALMOS-MKH boot-loader.  
 * The SEG_TTY_BASE address must be defined in the 'hard_config.h' file.
 * All accesses to the device registers are performed via 2 low-level 
 * functions 'boot_tty_get_register()' and 'boot_tty_set_register()'.
 ****************************************************************************/

#ifndef BOOT_TTY_DRIVER_H
#define BOOT_TTY_DRIVER_H

#include <hal_kernel_types.h>

/****************************************************************************
 *                           Driver register map.                           *
 ****************************************************************************/

enum TTY_registers
{
    TTY_WRITE_REG   = 0,    /* character to be displayed on screen          */
    TTY_STATUS_REG  = 1,    /* read and write buffer status                 */
    TTY_READ_REG    = 2,    /* character in the keyboard                    */
    TTY_CONFIG_REG  = 3,    /* unused                                       */
    
    TTY_SPAN    = 4,    /* segment size for one channel ( words )           */
};

/****************************************************************************
 *                           Driver status value.                           *
 ****************************************************************************/

enum TTY_status
{
    TTY_STATUS_RX_FULL = 1, /* Set if TTY_READ register contains a data.    */
    TTY_STATUS_TX_FULL = 2, /* Set if TTY_WRITE register contains a data.   */
};

/****************************************************************************
 * This function writes a character string from the 'buf' buffer to the 
 * boot TTY terminal. It tests the TTY_STATUS register before writing each
 * character of the string to the TTY_WRITE register. If TTY_WRITE_BUSY 
 * bit is set, it keeps testing the TTY_STATUS register. If after 10000 
 * retries the bit is still set, the function reports an error and returns.
 ****************************************************************************
 * @ buf    : buffer containing the string to be printed.                
 * @ nbytes : number of characters to be printed.                       
 * @ returns 0 on success, -1 on error.     
 ****************************************************************************/
int boot_tty_write( const char * buf,
                    uint32_t     nbytes );

#endif // BOOT_TTY_DRIVER_H
